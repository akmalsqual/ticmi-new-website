<?php

namespace App\Providers;

use Blade;
use Config;
use Illuminate\Support\ServiceProvider;
use Log;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
	    Blade::extend(function($value){
		    return preg_replace('/\{\?(.+)\?\}/', '<?php ${1} ?>', $value);
	    });

//	    \URL::forceSchema('https');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    	//
    }
}
