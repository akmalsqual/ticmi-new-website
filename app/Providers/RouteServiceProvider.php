<?php

namespace App\Providers;

use App\Http\Requests\Request;
use App\Model\EmitenData;
use App\Model\Keranjang;
use App\Model\PesertaWorkshop;
use App\Model\SuratRiset;
use App\Model\Workshop;
use Auth;
use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        //

        parent::boot($router);
	
	    $router->model('user', 'App\User', function () {
		    throw new NotFoundHttpException;
	    });
	
	    $router->model('role', 'App\Model\Role', function () {
		    throw new NotFoundHttpException;
	    });
	
	    $router->model('permission', 'App\Model\Permission', function () {
		    throw new NotFoundHttpException;
	    });
	
	    $router->model('emiten', 'App\Model\Emiten', function () {
		    throw new NotFoundHttpException;
	    });
	
	    $router->model('emitendata', 'App\Model\EmitenData', function () {
		    throw new NotFoundHttpException;
	    });
	    
	    $router->model('keranjang', 'App\Model\Keranjang', function () {
		    throw new NotFoundHttpException;
	    });
	    
	    $router->model('invoice', 'App\Model\Keranjang', function () {
		    throw new NotFoundHttpException;
	    });
	    
	    $router->model('pengumuman', 'App\Model\Peraturan', function () {
		    throw new NotFoundHttpException;
	    });
	    
	    $router->model('peraturan', 'App\Model\Peraturan', function () {
		    throw new NotFoundHttpException;
	    });
	    
	    $router->model('suratriset', 'App\Model\SuratRiset', function () {
		    throw new NotFoundHttpException;
	    });

	    $router->model('pesertaworkshop', 'App\Model\PesertaWorkshop', function () {
		    throw new NotFoundHttpException;
	    });
	    
	    $router->bind('invoice', function ($invoice){
	    	$invoice = Keranjang::where('invoice_no',$invoice)->first();
	    	if ($invoice && $invoice->count() > 0) {
	    		return $invoice;
		    } else abort(404);
	    });

	    $router->bind('emitendataencrypt', function ($emitendataencrypt){
	       $idemitendata = decrypt($emitendataencrypt);
	       $emitendata = EmitenData::find($idemitendata);
	       if ($emitendata && $emitendata->count() > 0) {
	           return $emitendata;
           } else abort(404);
        });

	    $router->bind('pengumumanencrypt', function ($pengumumanencrypt){
	       $idpengumuman = decrypt($pengumumanencrypt);
	       $pengumuman = EmitenData::find($idpengumuman);
	       if ($pengumuman && $pengumuman->count() > 0) {
	           return $pengumuman;
           } else abort(404);
        });

	    $router->bind('peraturanencrypt', function ($peraturanencrypt){
	       $idperaturan = decrypt($peraturanencrypt);
	       $peraturan = EmitenData::find($idperaturan);
	       if ($peraturan && $peraturan->count() > 0) {
	           return $peraturan;
           } else abort(404);
        });

	    $router->bind('invoicesuratriset', function ($invoicesuratriset){
            $suratRiset = SuratRiset::where('invoiceNumber',strval($invoicesuratriset))->first();
	       if ($suratRiset && $suratRiset->count() > 0) {
	           return $suratRiset;
           } else abort(404);
        });
	    
	    $router->bind('workshopslug', function ($workshopslug){
	    	$workshop = Workshop::whereSlugs($workshopslug)->first();
	    	if ($workshop && $workshop->count() > 0) {
	    		return $workshop;
		    } else abort(404);
	    });

	    $router->bind('pesertaworkshopinvoice', function ($pesertaworkshopinvoice){
	    	$pesertaWorkshop = PesertaWorkshop::where('invoice_no',$pesertaworkshopinvoice)->first();
	    	if ($pesertaWorkshop && $pesertaWorkshop->count() > 0) {
	    		return $pesertaWorkshop;
		    } else abort(404);
	    });
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $this->mapWebRoutes($router);

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    protected function mapWebRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace, 'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
