<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
    	if ($e instanceof \ReflectionException) {
//    		$e = new NotFoundHttpException($e->getMessage(), $e);
		    abort(404,'Not Found');
	    }
	    if ($e instanceof ModelNotFoundException || $e instanceof \InvalidArgumentException) {
		    $e = new NotFoundHttpException($e->getMessage(), $e);
		    abort(404,'Not found');
	    }
    	if ($e instanceof TokenMismatchException) {
    	    return redirect()->back()->with('message','Halaman pendaftaran telah expire, mohon untuk coba lagi');
	    }
	    
        return parent::render($request, $e);
    }
}
