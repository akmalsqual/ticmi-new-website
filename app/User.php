<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
	use EntrustUserTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','no_hp','is_verify','status','newsletter','no_sid','sid_card','profesi'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function keranjang()
    {
        return $this->hasMany('App\Model\Keranjang');
    }

    public function keranjang_detail()
    {
        return $this->hasMany('App\Model\KeranjangDetail');
    }

    public function konfirmasi()
    {
        return $this->hasMany('App\Model\Konfirmasi');
    }
	
	public function suratriset()
	{
		return $this->hasMany('App\Model\SuratRiset');
    }
	
	public function user_wingamers()
	{
		return $this->hasOne('App\Model\UsersWinGamers');
    }
}
