<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Model\PelatihanBsn;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendPendaftaranPelatihanBsn extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
	
	protected $pelatihanBsn;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(PelatihanBsn $pelatihanBsn)
    {
        $this->pelatihanBsn = $pelatihanBsn;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
	    
	    $pelatihanBsn = $this->pelatihanBsn;
	    $mailer->send('email-template.email-pelatihanbsn',['pelatihan'=>$pelatihanBsn], function ($m) use ($pelatihanBsn){
		    $m->from('noreply@ticmi.co.id', 'TICMI');
		    $m->to($pelatihanBsn->email, $pelatihanBsn->nama)->subject('Konfirmasi Pendaftaran Pelatihan ISO SNI 31000 Manajemen Risiko');
	    });
    }
}
