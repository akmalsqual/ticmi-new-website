<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Model\SuratRiset;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailPengajuanSuratRiset extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $suratRiset;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(SuratRiset $suratRiset)
    {
        $this->suratRiset = $suratRiset;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $suratRiset = $this->suratRiset;
        $user = \Auth::user();
    }
}
