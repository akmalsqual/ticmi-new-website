<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\User;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Str;

class SendEmailResetPassword extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $member;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->member = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $member = $this->member;
	    $token = Str::quickRandom(6);
	    $newPass = bcrypt($token);
	    if ($member->update(['password'=>$newPass])) {
		    $mailer->queue('email-template.email-reset-password',['member'=>$member,'token'=>$token], function ($m) use ($member){
			    $m->from('noreply@ticmi.co.id', 'TICMI');
			    $m->to($member->email, $member->name)->subject('TICMI - Reset Password');
		    });
	    }
    }
}
