<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Model\PelatihanBsn;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailSertifikatBsn extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
	
	protected $pelatihanBsn;
	
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(PelatihanBsn $pelatihanBsn)
    {
        $this->pelatihanBsn = $pelatihanBsn;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
	    $pelatihanBsn = $this->pelatihanBsn;
	    
	    $data = ['pelatihan'=>$pelatihanBsn];
	    $pdf = \Barryvdh\DomPDF\Facade::loadView('risk.pdf',$data);
	    $pdf->setPaper('a4', 'landscape');
	
	    $mailer->send('email-template.email-sertifikat',['pelatihan'=>$pelatihanBsn],function ($m) use ($pdf,$pelatihanBsn){
		    $m->from('noreply@ticmi.co.id', 'TICMI');
		    $m->to($pelatihanBsn->email, $pelatihanBsn->nama)->subject('Sertifikat Kehadiran dan Materi Pelatihan SNI ISO 31000 Manajemen Risiko');
		    $m->attachData($pdf->output(), 'Sertifikat Pelatihan SNI ISO 31000.pdf');
	    });
    }
}
