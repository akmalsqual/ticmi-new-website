<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\User;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailVerificationRegister extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    
    protected $member;
	
	/**
	 * Create a new job instance.
	 *
	 * @param \App\User $user
	 */
    public function __construct(User $user)
    {
        $this->member = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
    	$member = $this->member;
	    $mailer->queue('email-template.email-register',['member'=>$member], function ($m) use ($member){
		    $m->from('noreply@ticmi.co.id', 'TICMI');
		    $m->to($member->email, $member->name)->subject('TICMI - Verifikasi Email');
	    });
    }
}
