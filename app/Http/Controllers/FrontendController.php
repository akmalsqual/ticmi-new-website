<?php

namespace App\Http\Controllers;

use App\Http\Helpers\CartHelper;
use App\Http\Helpers\GeneralHelper;
use App\Jobs\SendEmailBlastFidelitas;
use App\Jobs\SendEmailKonfirmasiKehadiranBsn;
use App\Jobs\SendEmailPendaftaranBimbel;
use App\Jobs\SendEmailPendaftaranCMPDP;
use App\Jobs\SendEmailSertifikatBsn;
use App\Jobs\SendEmailVerificationRegister;
use App\Jobs\SendPendaftaranPelatihanBsn;
use App\Model\Acara;
use App\Model\Admisi;
use App\Model\Batch;
use App\Model\Cabang;
use App\Model\CMPDP;
use App\Model\Customer;
use App\Model\DaftarUjian;
use App\Model\Keranjang;
use App\Model\PelatihanBsn;
use App\Model\Pengajar;
use App\Model\PerusahaanAnggotaBursa;
use App\Model\PerusahaanNonAnggotaBursa;
use App\Model\PesertaWorkshop;
use App\Model\Program;
use App\Model\ProgramModel;
use App\Model\Workshop;
use App\User;
use Barryvdh\DomPDF\PDF;
use Carbon\Carbon;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Mail;
use Xthiago\PDFVersionConverter\Guesser\RegexGuesser;

class FrontendController extends Controller
{
	public function welcome()
	{
		$cabang  = Cabang::where('aktif', 'y')->orderBy('urutan')->get();
		$program = Program::where('aktif', 'y')->get();
		
		$time      = strtotime("-7 days");
		$batch     = Batch::where("mulai", '!=', "y")->where("approve", "y")->where("pasang", "y")->where("tanggal", ">", $time)->get();
		$pengajar  = Pengajar::where('foto', '!=', '')->where('aktif', 'y')->limit(4)->orderByRaw("RAND()")->get();
		$workshops = Workshop::where('is_publish', 1)->where('tgl_mulai', '>', date('Y-m-d'))->get();
		
		return view('welcome', compact('cabang', 'program', 'batch', 'pengajar', 'workshops'));
	}
	
	public function profilPerusahaan()
	{
		$pageTitle = "Profil The Indonesia Capital Market Institute";
		return view('frontend.profil-perusahaan', compact('pageTitle'));
	}
	
	public function visiMisi()
	{
		$pageTitle = "Visi dan Misi";
		return view('frontend.visi-misi', compact('pageTitle'));
	}
	
	public function sejarah()
	{
		$pageTitle = "Sejarah TICMI";
		return view('frontend.sejarah', compact('pageTitle'));
	}
	
	public function struktur()
	{
		$pageTitle = "Struktur Organisasi";
		return view('frontend.struktur', compact('pageTitle'));
	}
	
	public function wppeProduct()
	{
		$pageTitle = "Wakil Perantara Pedagang Efek";
		$program   = Program::where('nm_program', 'WPPE')->first();
		$time       = strtotime("-7 days");
		$otherbatch = Batch::whereIn('kd_program', [1,18,19])->where("mulai", '!=', "y")->where("approve", "y")->where("pasang", "y")->where("tanggal", ">", $time)->get();
		return view('frontend.wppe-product', compact('program', 'pageTitle', 'otherbatch'));
	}
	
	public function wmiProduct()
	{
		$pageTitle = "Wakil Manajer Investasi";
		$program   = Program::where('nm_program', 'WMI')->first();
		$time       = strtotime("-7 days");
		$otherbatch = Batch::where('kd_program', 2)->where("mulai", '!=', "y")->where("approve", "y")->where("pasang", "y")->where("tanggal", ">", $time)->get();
		return view('frontend.wmi-product', compact('program', 'pageTitle', 'otherbatch'));
	}
	
	public function aspmProduct()
	{
		$pageTitle = "Ahli Syariah Pasar Modal";
		$program   = Program::where('nm_program', 'ASPM')->first();
		$time       = strtotime("-7 days");
		$otherbatch = Batch::where('kd_program', 22)->where("mulai", '!=', "y")->where("approve", "y")->where("pasang", "y")->where("tanggal", ">", $time)->get();
		return view('frontend.aspm-product', compact('program', 'pageTitle', 'otherbatch'));
	}
	
	public function wpeeProduct()
	{
		$pageTitle = "Wakil Penjamin Emisi Efek";
		$program   = Program::where('nm_program', 'WPEE')->first();
		$time       = strtotime("-7 days");
		$otherbatch = Batch::where('kd_program', 24)->where("mulai", '!=', "y")->where("approve", "y")->where("pasang", "y")->where("tanggal", ">", $time)->get();
		return view('frontend.wpee-product', compact('program', 'pageTitle', 'otherbatch'));
	}
	
	public function waiverProduct()
	{
		$pageTitle = "Progmram Waiver";
		return view('frontend.waiver-product', compact('pageTitle'));
	}
	
	public function dataHistoris()
	{
		$pageTitle = "Data Historis";
		return view('frontend.data-historis', compact('pageTitle'));
	}
	
	public function perpustakaan()
	{
		$pageTitle = "Perpustakaan";
		return view('frontend.perpustakaan', compact('pageTitle'));
	}
	
	public function permintaanData()
	{
		$pageTitle = "Permintaan Data Khusus";
		return view('frontend.permintaan-data', compact('pageTitle'));
	}
	
	public function standarKelulusan()
	{
		$pageTitle = "Standar Kelulusan Ujian Sertifikasi TICMI";
		return view('frontend.standar-kelulusan', compact('pageTitle'));
	}
	
	public function faq()
	{
		$pageTitle = "Frequently Asked Question";
		return view('frontend.faq', compact('pageTitle'));
	}
	
	public function karir()
	{
		$pageTitle = "TICMI Career Development Center Kesempatan berkarir di Industri Pasar Modal Indonesia";
		
		$karir = PerusahaanAnggotaBursa::whereHas('karir', function ($query) {
			$query->whereRaw("valid_until >= '".date('Y-m-d')."'");
			$query->where('is_publish', 1);
		});
		
		$karir = $karir->get();
		
		$karir2 = PerusahaanNonAnggotaBursa::whereHas('karir', function ($query) {
			$query->whereRaw("valid_until >= '".date('Y-m-d')."'");
			$query->where('is_publish', 1);
		});
		$karir2 = $karir2->get();

		$karir = $karir->merge($karir2);
		return view('frontend.karir', compact('pageTitle', 'karir'));
	}
	
	public function hubungiKami()
	{
		$pageTitle = "Hubungi Kami";
		return view('frontend.hubungi-kami', compact('pageTitle'));
	}
	
	public function cmk()
	{
		$pageTitle = "Cerdas Mengelola Keuangan";
		$ogimage   = asset('/assets/images/ticmi/cmk/banner-cmk2.jpg');
		$workshops = Workshop::where('is_publish', 1)->where('slugs', 'LIKE', "cmk%")->where('tgl_mulai', '>', date('Y-m-d'))->orderBy('tgl_mulai')->get();
		return view('frontend.cmk', compact('pageTitle', 'workshops', 'ogimage'));
	}
	
	public function rivan()
	{
		$pageTitle = "Seminar Bersama Rivan Kurniawan - Investment Grade Investing Strategy";
		$workshops = Workshop::where('is_publish', 1)->where('slugs', 'LIKE', "rivan%")->where('tgl_mulai', '>', date('Y-m-d'))->orderBy('tgl_mulai')->get();
		return view('frontend.seminar-rivan', compact('pageTitle', 'workshops'));
	}
	
	public function ppl()
	{
		$pageTitle = "Program Pendidikan Lanjutan (PPL)";
//		$workshops = Workshop::where('is_publish', 1)->where('slugs', 'LIKE', "ppl%")->where('tgl_mulai', '>', date('Y-m-d'))->orderBy('tgl_mulai')->get();
		$workshops = Workshop::where('is_publish', 1)->where('t_program_id', 34)->where('tgl_mulai', '>', date('Y-m-d'))->orderBy('tgl_mulai')->get();
		return view('frontend.seminar-ppl', compact('pageTitle', 'workshops'));
	}
	
	public function bimbelUjian()
	{
		$pageTitle  = "Bimbingan & Ujian Ulang Gratis - TICMI";
		$allProgram = Program::where('aktif', 'y')->orderBy('id')->get()->pluck('nm_program', 'id');
//		$allCabang  = ['Jakarta'=>'Jakarta','Banda Aceh'=>'Banda Aceh','Bandung'=>'Bandung','Bengkulu'=>'Bengkulu','Jayapura'=>'Jayapura','Lampung'=>'Lampung','Manado'=>'Manado','Padang'=>'Padang','Pontianak'=>'Pontianak','Semarang'=>'Semarang','Surabaya'=>'Surabaya'];
		$allCabang = [
			'Jakarta'   => 'Jakarta', 'Surabaya' => 'Surabaya', 'Semarang' => 'Semarang', 'Pontianak' => 'Pontianak', 'Yogyakarta' => 'Yogyakarta'
			, 'Bandung' => 'Bandung', 'Jayapura' => 'Jayapura', 'Palembang' => 'Palembang', 'Kendari' => 'Kendari', 'Padang' => 'Padang', 'Bengkulu' => 'Bengkulu',
			'Banda Aceh'      => 'Banda Aceh', 'Pangkalpinang' => 'Pangkalpinang', 'Lampung' => 'Lampung','Ambon'=>'Ambon','Manokwari'=>'Manokwari',
			'Jambi' => 'Jambi', 'Medan' => 'Medan', 'Pekanbaru' => 'Pekanbaru','Batam' => 'Batam', 'Banjarmasin' => 'Banjarmasin', 'Denpasar' => 'Denpasar',
			'Palangkaraya' => 'Palangkaraya'
		];
		
		return view('frontend.bimbel.index', compact('pageTitle', 'allProgram', 'allCabang'));
	}
	
	public function bimbelUjianCek(Request $request)
	{
		$this->validate($request, [
			'email'       => 'required|email',
			'cabang'      => 'required',
			'kd_program'  => 'required',
			'tanggal_jam' => 'required'
		]);
		
		$email      = $request->get('email');
		$cabang     = $request->get('cabang');
		$kd_program = $request->get('kd_program');
		$tanggal    = $request->get('tanggal');
		$admisi     = Admisi::where('email', $email)->where('aktif', 'y')->where('kd_program', $kd_program)->first();
		
		if ($admisi && count($admisi) > 0) {
			if (trim($admisi->kd_program) == $kd_program || !empty($nip)) {
				if (empty($admisi->nip)) {
//					if ($kd_program == 1 && $admisi->batch < 126) {
//						return response()->json(['email' => [" Maaf Anda sudah melewati batas untuk ujian 18 Bulan"]], 422);
//					} elseif ($kd_program == 2 && $admisi->batch < 108) {
//						return response()->json(['email' => [" Maaf Anda sudah melewati batas untuk ujian 18 Bulan"]], 422);
//					} else {
//					}
					if ($kd_program == 2) {
						if ($admisi->batch >= 108 && $admisi->batch != 120) {
							$result = [
								'status' => true,
							];
						} else {
							return response()->json(['email' => [" Maaf Anda sudah melewati batas untuk ujian 18 Bulan"]], 422);
						}
					} elseif ($kd_program == 1) {
						if ($admisi->batch >= 111 && $admisi->batch != 128 && $admisi->batch != 129) {
							$result = [
								'status' => true,
							];
						} else {
							return response()->json(['email' => [" Maaf Anda sudah melewati batas untuk ujian 18 Bulan"]], 422);
						}
					} else {
						$result = [
							'status' => true,
						];
					}
					echo json_encode($result);
				} else {
					return response()->json(['email' => [" Maaf Anda sudah lulus"]], 422);
				}
			} else {
				return response()->json(['email' => ["Maaf Anda tidak dapat mengikuti ujian ulang ini."], 'admisi' => $admisi], 422);
			}
		} else {
			return response()->json(['email' => ["Maaf Kami tidak menemukan peserta dengan email <strong>$email</strong>"]], 422);
		}
	}
	
	public function bimbelUjianStore(Request $request)
	{
		$this->validate($request, [
			'email'       => 'required|email',
			'cabang'      => 'required',
			'kd_program'  => 'required',
			'tanggal_jam' => 'required'
		]);
		
		$email       = $request->get('email');
		$cabang      = $request->get('cabang');
		$tanggal_jam = $request->get('tanggal_jam');
		$kd_program  = $request->get('kd_program');
		
		$tanggal_jam = explode('|', $tanggal_jam);
		
		$tanggal = trim($tanggal_jam[0]);
		$sesi    = trim($tanggal_jam[1]);

//		if ($kd_program == 1 || $kd_program == 19) {
//			$tanggal = '2017-07-23';
//		} else {
//			$tanggal = '2017-07-30';
//		}
//		if ($kd_program == 1 || $kd_program == 2) {
//			$sesi = '08:00-17:30';
//		} else {
//			$sesi = '08:00-15:30';
//		}
		
		$admisi = Admisi::where('email', $email)->where('kd_program', $kd_program)->where('aktif', 'y')->first();
		if ($admisi && count($admisi) > 0) {
			if (empty($admisi->nip)) {
				$cek = DaftarUjian::where('email', $email)->where('tanggal', date('F d Y', strtotime($tanggal)))->first();
				if ($cek && count($cek) > 0) {
					flash()->error('Maaf Anda sudah terdaftar di peserta Ujian ulang pada tanggal tersebut');
				} else {
					if ($kd_program == 2) {
						if ($admisi->batch < 108 || $admisi->batch == 120) {
							flash()->error('Maaf Anda sudah melewati batas untuk ujian 18 Bulan');
							return redirect()->route('bimbel-ujian');
						}
					} elseif ($kd_program == 1) {
						if ($admisi->batch < 111 || $admisi->batch == 128 || $admisi->batch == 129) {
							flash()->error('Maaf Anda sudah melewati batas untuk ujian 18 Bulan');
							return redirect()->route('bimbel-ujian');
						}
					}
					
					$daftarUjian = new DaftarUjian();
					
					$daftarUjian->tanggal   = date('F d Y', strtotime($tanggal));
					$daftarUjian->gelombang = $sesi;
					$daftarUjian->email     = $email;
					$daftarUjian->nama      = $admisi->nm_peserta;
					$daftarUjian->program   = $kd_program;
					$daftarUjian->aktif     = 'y';
					$daftarUjian->cabang    = trim($cabang);
					if (trim($cabang) == 'Jakarta' && date('F d Y', strtotime($tanggal)) != 'July 30 2017') {
						$daftarUjian->remark = 'Gratis';
					} else {
						$daftarUjian->remark = 'Bimbel';
					}

//					return $daftarUjian;
					
					if ($daftarUjian->save()) {
						$admisi->update(['status' => '']);
						$job = (new SendEmailPendaftaranBimbel($daftarUjian))->delay(5);
						$this->dispatch($job);
						flash()->success('Pendaftaran ujian ulang telah sukses.');
						return redirect()->route('bimbel-ujian.message');
					} else {
						flash()->error('Maaf pendaftaran ujian ulang gagal, silahkan mencoba mendaftar kembali');
					}
				}
			} else {
				flash()->error('Maaf Anda tidak dapat mengikuti ujian, dikarenakan sudah lulus')->important();
			}
		} else {
			flash()->error('Maaf Kami tidak dapat menemukan data peserta dengan email <strong>' . $email . '</strong>')->important();
		}
		return redirect()->route('bimbel-ujian');
	}
	
	public function bimbelMessage()
	{
		$pageTitle = "Bimbingan & Ujian Ulang Gratis - TICMI";
		return view('frontend.bimbel.message', compact('pageTitle'));
	}
	
	public function cekJadwalBimbel(Request $request)
	{
		$kd_program = $request->get('kd_program');
		$cabang     = $request->get('cabang');
		$option     = '';
		
		if ($cabang == 'Jakarta') {
			$limit = 36;
			$count8_1  = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '09:00-11:00')->where('cabang', 'Jakarta')->get()->count();
			$count8_2  = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Jakarta')->get()->count();
			$count8_3  = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '16:00-18:00')->where('cabang', 'Jakarta')->get()->count();
			$count9_1  = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '09:00-11:00')->where('cabang', 'Jakarta')->get()->count();
			$count9_2  = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Jakarta')->get()->count();
			$count9_3  = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '16:00-18:00')->where('cabang', 'Jakarta')->get()->count();
			$count10_1  = DaftarUjian::where('tanggal', 'August 10 2017')->where('gelombang', '09:00-11:00')->where('cabang', 'Jakarta')->get()->count();
			$count10_2  = DaftarUjian::where('tanggal', 'August 10 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Jakarta')->get()->count();
			$count10_3  = DaftarUjian::where('tanggal', 'August 10 2017')->where('gelombang', '16:00-18:00')->where('cabang', 'Jakarta')->get()->count();
			$count11_1  = DaftarUjian::where('tanggal', 'August 11 2017')->where('gelombang', '09:00-11:00')->where('cabang', 'Jakarta')->get()->count();
			$count11_2  = DaftarUjian::where('tanggal', 'August 11 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Jakarta')->get()->count();
			$count11_3  = DaftarUjian::where('tanggal', 'August 11 2017')->where('gelombang', '16:00-18:00')->where('cabang', 'Jakarta')->get()->count();
			$count14_1  = DaftarUjian::where('tanggal', 'August 14 2017')->where('gelombang', '09:00-11:00')->where('cabang', 'Jakarta')->get()->count();
			$count14_2  = DaftarUjian::where('tanggal', 'August 14 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Jakarta')->get()->count();
			$count14_3  = DaftarUjian::where('tanggal', 'August 14 2017')->where('gelombang', '16:00-18:00')->where('cabang', 'Jakarta')->get()->count();
			$count15_1  = DaftarUjian::where('tanggal', 'August 15 2017')->where('gelombang', '09:00-11:00')->where('cabang', 'Jakarta')->get()->count();
			$count15_2  = DaftarUjian::where('tanggal', 'August 15 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Jakarta')->get()->count();
			$count15_3  = DaftarUjian::where('tanggal', 'August 15 2017')->where('gelombang', '16:00-18:00')->where('cabang', 'Jakarta')->get()->count();
			$count16_1  = DaftarUjian::where('tanggal', 'August 16 2017')->where('gelombang', '09:00-11:00')->where('cabang', 'Jakarta')->get()->count();
			$count16_2  = DaftarUjian::where('tanggal', 'August 16 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Jakarta')->get()->count();
			$count16_3  = DaftarUjian::where('tanggal', 'August 16 2017')->where('gelombang', '16:00-18:00')->where('cabang', 'Jakarta')->get()->count();
			$count17_1  = DaftarUjian::where('tanggal', 'August 17 2017')->where('gelombang', '09:00-11:00')->where('cabang', 'Jakarta')->get()->count();
			$count17_2  = DaftarUjian::where('tanggal', 'August 17 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Jakarta')->get()->count();
			$count17_3  = DaftarUjian::where('tanggal', 'August 17 2017')->where('gelombang', '16:00-18:00')->where('cabang', 'Jakarta')->get()->count();
			$count18_1  = DaftarUjian::where('tanggal', 'August 18 2017')->where('gelombang', '09:00-11:00')->where('cabang', 'Jakarta')->get()->count();
			$count18_2  = DaftarUjian::where('tanggal', 'August 18 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Jakarta')->get()->count();
			$count18_3  = DaftarUjian::where('tanggal', 'August 18 2017')->where('gelombang', '16:00-18:00')->where('cabang', 'Jakarta')->get()->count();
			

			$option .= '<optgroup label="Selasa, 8 Agustus 2017">';
			if ($count8_1 <= $limit) $option .= "<option value='August 08 2017 | 09:00-11:00' " . (($count8_1 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 09:00-11:00 WIB (sisa " . ($limit - $count8_1) . " kursi)</option>";
			if ($count8_2 <= $limit) $option .= "<option value='August 08 2017 | 13:00-15:00' " . (($count8_2 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count8_2) . " kursi)</option>";
			if ($count8_3 <= $limit) $option .= "<option value='August 08 2017 | 16:00-18:00' " . (($count8_3 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 16:00-18:00 WIB (sisa " . ($limit - $count8_3) . " kursi)</option>";
			$option .= '</optgroup>';
			$option .= '<optgroup label="Rabu, 9 Agustus 2017">';
			if ($count9_1 <= $limit) $option .= "<option value='August 09 2017 | 09:00-11:00' " . (($count9_1 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 09:00-11:00 WIB (sisa " . ($limit - $count9_1) . " kursi)</option>";
			if ($count9_2 <= $limit) $option .= "<option value='August 09 2017 | 13:00-15:00' " . (($count9_2 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count9_2) . " kursi)</option>";
			if ($count9_3 <= $limit) $option .= "<option value='August 09 2017 | 16:00-18:00' " . (($count9_3 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 16:00-18:00 WIB (sisa " . ($limit - $count9_3) . " kursi)</option>";
			$option .= '</optgroup>';
			$option .= '<optgroup label="Kamis, 10 Agustus 2017">';
			if ($count10_1 <= $limit) $option .= "<option value='August 10 2017 | 09:00-11:00' " . (($count10_1 >= $limit) ? 'disabled' : '') . ">10 Agustus 2017 - 09:00-11:00 WIB (sisa " . ($limit - $count10_1) . " kursi)</option>";
			if ($count10_2 <= $limit) $option .= "<option value='August 10 2017 | 13:00-15:00' " . (($count10_2 >= $limit) ? 'disabled' : '') . ">10 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count10_2) . " kursi)</option>";
			if ($count10_3 <= $limit) $option .= "<option value='August 10 2017 | 16:00-18:00' " . (($count10_3 >= $limit) ? 'disabled' : '') . ">10 Agustus 2017 - 16:00-18:00 WIB (sisa " . ($limit - $count10_3) . " kursi)</option>";
			$option .= '</optgroup>';
			$option .= '<optgroup label="Jumat, 11 Agustus 2017">';
			if ($count11_1 <= $limit) $option .= "<option value='August 11 2017 | 09:00-11:00' " . (($count11_1 >= $limit) ? 'disabled' : '') . ">11 Agustus 2017 - 09:00-11:00 WIB (sisa " . ($limit - $count11_1) . " kursi)</option>";
			if ($count11_2 <= $limit) $option .= "<option value='August 11 2017 | 13:00-15:00' " . (($count11_2 >= $limit) ? 'disabled' : '') . ">11 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count11_2) . " kursi)</option>";
			if ($count11_3 <= $limit) $option .= "<option value='August 11 2017 | 16:00-18:00' " . (($count11_3 >= $limit) ? 'disabled' : '') . ">11 Agustus 2017 - 16:00-18:00 WIB (sisa " . ($limit - $count11_3) . " kursi)</option>";
			$option .= '</optgroup>';
			$option .= '<optgroup label="Senin, 14 Agustus 2017">';
			if ($count14_1 <= $limit) $option .= "<option value='August 14 2017 | 09:00-11:00' " . (($count14_1 >= $limit) ? 'disabled' : '') . ">14 Agustus 2017 - 09:00-11:00 WIB (sisa " . ($limit - $count14_1) . " kursi)</option>";
			if ($count14_2 <= $limit) $option .= "<option value='August 14 2017 | 13:00-15:00' " . (($count14_2 >= $limit) ? 'disabled' : '') . ">14 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count14_2) . " kursi)</option>";
			if ($count14_3 <= $limit) $option .= "<option value='August 14 2017 | 16:00-18:00' " . (($count14_3 >= $limit) ? 'disabled' : '') . ">14 Agustus 2017 - 16:00-18:00 WIB (sisa " . ($limit - $count14_3) . " kursi)</option>";
			$option .= '</optgroup>';
			$option .= '<optgroup label="Selasa, 15 Agustus 2017">';
			if ($count15_1 <= $limit) $option .= "<option value='August 15 2017 | 09:00-11:00' " . (($count15_1 >= $limit) ? 'disabled' : '') . ">15 Agustus 2017 - 09:00-11:00 WIB (sisa " . ($limit - $count15_1) . " kursi)</option>";
			if ($count15_2 <= $limit) $option .= "<option value='August 15 2017 | 13:00-15:00' " . (($count15_2 >= $limit) ? 'disabled' : '') . ">15 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count15_2) . " kursi)</option>";
			if ($count15_3 <= $limit) $option .= "<option value='August 15 2017 | 16:00-18:00' " . (($count15_3 >= $limit) ? 'disabled' : '') . ">15 Agustus 2017 - 16:00-18:00 WIB (sisa " . ($limit - $count15_3) . " kursi)</option>";
			$option .= '</optgroup>';
			$option .= '<optgroup label="Rabu, 16 Agustus 2017">';
			if ($count16_1 <= $limit) $option .= "<option value='August 16 2017 | 09:00-11:00' " . (($count16_1 >= $limit) ? 'disabled' : '') . ">16 Agustus 2017 - 09:00-11:00 WIB (sisa " . ($limit - $count16_1) . " kursi)</option>";
			if ($count16_2 <= $limit) $option .= "<option value='August 16 2017 | 13:00-15:00' " . (($count16_2 >= $limit) ? 'disabled' : '') . ">16 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count16_2) . " kursi)</option>";
			if ($count16_3 <= $limit) $option .= "<option value='August 16 2017 | 16:00-18:00' " . (($count16_3 >= $limit) ? 'disabled' : '') . ">16 Agustus 2017 - 16:00-18:00 WIB (sisa " . ($limit - $count16_3) . " kursi)</option>";
			$option .= '</optgroup>';
//			$option .= '<optgroup label="Kamis, 17 Agustus 2017">';
//			if ($count17_1 <= $limit) $option .= "<option value='August 17 2017 | 09:00-11:00' " . (($count17_1 >= $limit) ? 'disabled' : '') . ">17 Agustus 2017 - 09:00-11:00 WIB (sisa " . ($limit - $count17_1) . " kursi)</option>";
//			if ($count17_2 <= $limit) $option .= "<option value='August 17 2017 | 13:00-15:00' " . (($count17_2 >= $limit) ? 'disabled' : '') . ">17 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count17_2) . " kursi)</option>";
//			if ($count17_3 <= $limit) $option .= "<option value='August 17 2017 | 16:00-18:00' " . (($count17_3 >= $limit) ? 'disabled' : '') . ">17 Agustus 2017 - 16:00-18:00 WIB (sisa " . ($limit - $count17_3) . " kursi)</option>";
//			$option .= '</optgroup>';
			$option .= '<optgroup label="Jumat, 18 Agustus 2017">';
			if ($count18_1 <= $limit) $option .= "<option value='August 18 2017 | 09:00-11:00' " . (($count18_1 >= $limit) ? 'disabled' : '') . ">18 Agustus 2017 - 09:00-11:00 WIB (sisa " . ($limit - $count18_1) . " kursi)</option>";
			if ($count18_2 <= $limit) $option .= "<option value='August 18 2017 | 13:00-15:00' " . (($count18_2 >= $limit) ? 'disabled' : '') . ">18 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count18_2) . " kursi)</option>";
			if ($count18_3 <= $limit) $option .= "<option value='August 18 2017 | 16:00-18:00' " . (($count18_3 >= $limit) ? 'disabled' : '') . ">18 Agustus 2017 - 16:00-18:00 WIB (sisa " . ($limit - $count18_3) . " kursi)</option>";
			$option .= '</optgroup>';
			
		} elseif ($cabang == 'Surabaya') {
			$limit = 195;
			if ($kd_program == 19) {
				$count1_1 = DaftarUjian::where('tanggal', 'August 01 2017')->where('gelombang', '09:00-11:00')->where('cabang', 'Surabaya')->get()->count();
				$count1_2 = DaftarUjian::where('tanggal', 'August 01 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Surabaya')->get()->count();
				$count2_1 = DaftarUjian::where('tanggal', 'August 02 2017')->where('gelombang', '09:00-11:00')->where('cabang', 'Surabaya')->get()->count();
				
//				$option .= '<optgroup label="Selasa, 1 Agustus 2017">';
//				if ($count1_1 <= $limit) $option .= "<option value='August 01 2017 | 09:00-11:00' " . (($count1_1 >= $limit) ? 'disabled' : '') . ">1 Agustus 2017 - 09:00-11:00 WIB (sisa " . ($limit - $count1_1) . " kursi)</option>";
//				if ($count1_2 <= $limit) $option .= "<option value='August 01 2017 | 13:00-15:00' " . (($count1_1 >= $limit) ? 'disabled' : '') . ">1 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count1_2) . " kursi)</option>";
//				$option .= '</optgroup>';
				
				$option .= '<optgroup label="Rabu, 2 Agustus 2017">';
				if ($count2_1 <= $limit) $option .= "<option value='August 02 2017 | 09:00-11:00' " . (($count2_1 >= $limit) ? 'disabled' : '') . ">2 Agustus 2017 - 09:00-11:00 WIB (sisa " . ($limit - $count2_1) . " kursi)</option>";
				$option .= '</optgroup>';
				
			} elseif ($kd_program == 18 || $kd_program == 1) {
				$count2_2 = DaftarUjian::where('tanggal', 'August 02 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Surabaya')->get()->count();
				$option   .= '<optgroup label="Rabu, 2 Agustus 2017">';
				if ($count2_2 <= $limit) $option .= "<option value='August 02 2017 | 09:00-11:00' " . (($count2_2 >= $limit) ? 'disabled' : '') . ">2 Agustus 2017 - 09:00-11:00 WIB (sisa " . ($limit - $count2_2) . " kursi)</option>";
				$option .= '</optgroup>';
//				$option = "<option value='August 02 2017 | 13:00-15:00'>2 Agustus 2017 - 13:00-15:00 WIB</option>";
			} else {
				$option = "<option value=''>Tidak Ada jadwal</option>";
			}
		} elseif ($cabang == 'Semarang') {
			$limit = 20;
//			if ($kd_program == 1 || $kd_program == 19) {
//				$count26_1 = DaftarUjian::where('tanggal', 'July 26 2017')->where('gelombang', '08:30-10:30')->where('cabang', 'Semarang')->get()->count();
//				$count26_2 = DaftarUjian::where('tanggal', 'July 26 2017')->where('gelombang', '11:00-13:00')->where('cabang', 'Semarang')->get()->count();
//				$count26_3 = DaftarUjian::where('tanggal', 'July 26 2017')->where('gelombang', '13:30-15:30')->where('cabang', 'Semarang')->get()->count();
//				$count26_4 = DaftarUjian::where('tanggal', 'July 26 2017')->where('gelombang', '16:00-16:00')->where('cabang', 'Semarang')->get()->count();
//
//				$count27_1 = DaftarUjian::where('tanggal', 'July 27 2017')->where('gelombang', '08:30-10:30')->where('cabang', 'Semarang')->get()->count();
//				$count27_2 = DaftarUjian::where('tanggal', 'July 27 2017')->where('gelombang', '11:00-13:00')->where('cabang', 'Semarang')->get()->count();
//				$count27_3 = DaftarUjian::where('tanggal', 'July 27 2017')->where('gelombang', '13:30-15:30')->where('cabang', 'Semarang')->get()->count();
//				$count27_4 = DaftarUjian::where('tanggal', 'July 27 2017')->where('gelombang', '16:00-16:00')->where('cabang', 'Semarang')->get()->count();
//
//				$count28_1 = DaftarUjian::where('tanggal', 'July 28 2017')->where('gelombang', '08:30-10:30')->where('cabang', 'Semarang')->get()->count();
////			$count28_2 = DaftarUjian::where('tanggal','July 28 2017')->where('gelombang','11:00-13:00')->where('cabang','Semarang')->get()->count();
//				$count28_3 = DaftarUjian::where('tanggal', 'July 28 2017')->where('gelombang', '13:30-15:30')->where('cabang', 'Semarang')->get()->count();
//				$count28_4 = DaftarUjian::where('tanggal', 'July 28 2017')->where('gelombang', '16:00-16:00')->where('cabang', 'Semarang')->get()->count();
//
//				$option .= '<optgroup label="Rabu, 26 Juli 2017">';
//				if ($count26_1 <= $limit) $option .= "<option value='July 26 2017 | 08:30-10:30' " . (($count26_1 >= $limit) ? 'disabled' : '') . ">26 Juli 2017 - 08:30-10:30 WIB (sisa " . ($limit - $count26_1) . " kursi)</option>";
//				if ($count26_2 <= $limit) $option .= "<option value='July 26 2017 | 11:00-13:00' " . (($count26_2 >= $limit) ? 'disabled' : '') . ">26 Juli 2017 - 11:00-13:00 WIB (sisa " . ($limit - $count26_2) . " kursi)</option>";
//				if ($count26_3 <= $limit) $option .= "<option value='July 26 2017 | 13:30-15:30' " . (($count26_3 >= $limit) ? 'disabled' : '') . ">26 Juli 2017 - 13:30-15:30 WIB (sisa " . ($limit - $count26_3) . " kursi)</option>";
//				if ($count26_4 <= $limit) $option .= "<option value='July 26 2017 | 16:00-18:00' " . (($count26_4 >= $limit) ? 'disabled' : '') . ">26 Juli 2017 - 16:00-18:00 WIB (sisa " . ($limit - $count26_4) . " kursi)</option>";
//				$option .= '</optgroup>';
//
//				$option .= '<optgroup label="Kamis, 27 Juli 2017">';
//				if ($count27_1 <= $limit) $option .= "<option value='July 27 2017 | 08:30-10:30' " . (($count27_1 >= $limit) ? 'disabled' : '') . ">27 Juli 2017 - 08:30-10:30 WIB (sisa " . ($limit - $count27_1) . " kursi)</option>";
//				if ($count27_2 <= $limit) $option .= "<option value='July 27 2017 | 11:00-13:00' " . (($count27_2 >= $limit) ? 'disabled' : '') . ">27 Juli 2017 - 11:00-13:00 WIB (sisa " . ($limit - $count27_2) . " kursi)</option>";
//				if ($count27_3 <= $limit) $option .= "<option value='July 27 2017 | 13:30-15:30' " . (($count27_3 >= $limit) ? 'disabled' : '') . ">27 Juli 2017 - 13:30-15:30 WIB (sisa " . ($limit - $count27_3) . " kursi)</option>";
//				if ($count27_4 <= $limit) $option .= "<option value='July 27 2017 | 16:00-18:00' " . (($count27_4 >= $limit) ? 'disabled' : '') . ">27 Juli 2017 - 16:00-18:00 WIB (sisa " . ($limit - $count27_4) . " kursi)</option>";
//				$option .= '</optgroup>';
//
//				$option .= '<optgroup label="Jum\'at, 28 Juli 2017">';
//				if ($count28_1 <= $limit) $option .= "<option value='July 28 2017 | 08:30-10:30' " . (($count28_1 >= $limit) ? 'disabled' : '') . ">28 Juli 2017 - 08:30-10:30 WIB (sisa " . ($limit - $count28_1) . " kursi)</option>";
////			if ($count28_2 <= $limit) $option .= "<option value='July 28 2017 | 11:00-13:00'>28 Juli 2017 - 11:00-13:00 WIB (sisa ".($limit - $count28_2)." kursi)</option>";
//				if ($count28_3 <= $limit) $option .= "<option value='July 28 2017 | 13:30-15:30' " . (($count28_3 >= $limit) ? 'disabled' : '') . ">28 Juli 2017 - 13:30-15:30 WIB (sisa " . ($limit - $count28_3) . " kursi)</option>";
//				if ($count28_4 <= $limit) $option .= "<option value='July 28 2017 | 16:00-18:00' " . (($count28_4 >= $limit) ? 'disabled' : '') . ">28 Juli 2017 - 16:00-18:00 WIB (sisa " . ($limit - $count28_4) . " kursi)</option>";
//				$option .= '</optgroup>';
//			} else {
				$option = "<option value=''>Tidak Ada jadwal</option>";
//			}
			
		} elseif ($cabang == 'Pontianak') {
//			if ($kd_program == 1 || $kd_program == 19) {
//				$option .= "<option value='July 27 2017 | 08:00-15:00'>27 Juli 2017 - 08:00-15:00 WIB</option>";
//			} else {
				$option = "<option value=''>Tidak Ada jadwal</option>";
//			}
		} elseif ($cabang == "Yogyakarta") {
			$limit = 20;
			if ($kd_program == 1 || $kd_program == 2 || $kd_program == 19) {
				$count31 = DaftarUjian::where('tanggal', 'August 10 2017')->where('gelombang', '08:00-10:00')->where('cabang', 'Yogyakarta')->get()->count();
				$count1  = DaftarUjian::where('tanggal', 'August 10 2017')->where('gelombang', '10:30-12:30')->where('cabang', 'Yogyakarta')->get()->count();
				$count2  = DaftarUjian::where('tanggal', 'August 11 2017')->where('gelombang', '08:00-10:00')->where('cabang', 'Yogyakarta')->get()->count();

				if ($count31 <= $limit) $option .= "<option value='August 10 2017 | 08:00-10:00' " . (($count31 >= $limit) ? 'disabled' : '') . ">10 Agustus 2017 - 08:00-10:00 WIB (sisa " . ($limit - $count31) . " kursi)</option>";
				if ($count1 <= $limit) $option .= "<option value='August 10 2017 | 10:30-12:30' " . (($count1 >= $limit) ? 'disabled' : '') . ">10 Agustus 2017 - 10:30-12:30 WIB (sisa " . ($limit - $count1) . " kursi)</option>";
				if ($count2 <= $limit) $option .= "<option value='August 11 2017 | 08:00-10:00' " . (($count2 >= $limit) ? 'disabled' : '') . ">11 Agustus 2017 - 08:00-10:00 WIB (sisa " . ($limit - $count2) . " kursi)</option>";
			} else {
				$option = "<option value=''>Tidak Ada jadwal</option>";
			}
		} elseif ($cabang == "Bandung") {
			$limit = 60;
			if ($kd_program == 19) {
				$count29_1 = DaftarUjian::where('tanggal', 'July 29 2017')->where('gelombang', '08:00-10:00')->where('cabang', 'Bandung')->get()->count();
				$count29_2 = DaftarUjian::where('tanggal', 'July 29 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Bandung')->get()->count();
				$count5_1  = DaftarUjian::where('tanggal', 'August 05 2017')->where('gelombang', '08:00-10:00')->where('cabang', 'Bandung')->get()->count();
				$count5_2  = DaftarUjian::where('tanggal', 'August 05 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Bandung')->get()->count();
				
//				$option .= '<optgroup label="Sabtu, 29 Juli 2017">';
//				if ($count29_1 <= $limit) $option .= "<option value='July 29 2017 | 08:00-10:00' " . (($count29_1 >= $limit) ? 'disabled' : '') . ">29 Juli 2017 - 08:00-10:00 WIB (sisa " . ($limit - $count29_1) . " kursi)</option>";
//				if ($count29_2 <= $limit) $option .= "<option value='July 29 2017 | 13:00-15:00' " . (($count29_2 >= $limit) ? 'disabled' : '') . ">29 Juli 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count29_2) . " kursi)</option>";
//				$option .= '</optgroup>';
				
				$option .= '<optgroup label="Sabtu, 5 Agustus 2017">';
				if ($count5_1 <= $limit) $option .= "<option value='August 05 2017 | 08:00-10:00' " . (($count5_1 >= $limit) ? 'disabled' : '') . ">5 Agustus 2017 - 08:00-10:00 WIB (sisa " . ($limit - $count5_1) . " kursi)</option>";
				if ($count5_2 <= $limit) $option .= "<option value='August 05 2017 | 13:00-15:00' " . (($count5_2 >= $limit) ? 'disabled' : '') . ">5 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count5_2) . " kursi)</option>";
				$option .= '</optgroup>';
			} else {
				$option = "<option value=''>Tidak Ada jadwal</option>";
			}
		} elseif ($cabang == "Jayapura") {
			$limit = 20;
			$count8_1 = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Jayapura')->get()->count();
			$count8_2 = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Jayapura')->get()->count();
			$count9_1 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Jayapura')->get()->count();
			$count9_2 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Jayapura')->get()->count();
			
			$option .= '<optgroup label="Selasa, 8 Agustus 2017">';
			if ($count8_1 <= $limit) $option .= "<option value='August 08 2017 | 10:00-12:00' " . (($count8_1 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count8_1) . " kursi)</option>";
			if ($count8_2 <= $limit) $option .= "<option value='August 08 2017 | 13:00-15:00' " . (($count8_2 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count8_2) . " kursi)</option>";
			$option .= '</optgroup>';
			$option .= '<optgroup label="Rabu, 9 Agustus 2017">';
			if ($count9_1 <= $limit) $option .= "<option value='August 09 2017 | 10:00-12:00' " . (($count9_1 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count9_1) . " kursi)</option>";
			if ($count9_2 <= $limit) $option .= "<option value='August 09 2017 | 13:00-15:00' " . (($count9_2 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count9_2) . " kursi)</option>";
			$option .= '</optgroup>';
		} elseif ($cabang == "Palembang") {
			if ($kd_program == 19 || $kd_program == 1) {
				$option .= "<option value='August 07 2017 | 14:00-16:00'>7 Agustus 2017 - 14:00-16:00 WIB</option>";
			} else {
				$option = "<option value=''>Tidak Ada jadwal</option>";
			}
		} elseif ($cabang == "Kendari") {
			$limit = 15;
			if ($kd_program == 19 || $kd_program == 1) {
				$count8_1 = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Kendari')->get()->count();
				$count8_2 = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Kendari')->get()->count();
				$count9_1 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Kendari')->get()->count();
				$count9_2 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Kendari')->get()->count();
				
				$option .= '<optgroup label="Selasa, 8 Agustus 2017">';
				if ($count8_1 <= $limit) $option .= "<option value='August 08 2017 | 10:00-12:00' " . (($count8_1 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count8_1) . " kursi)</option>";
				if ($count8_2 <= $limit) $option .= "<option value='August 08 2017 | 13:00-15:00' " . (($count8_2 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count8_2) . " kursi)</option>";
				$option .= '</optgroup>';
				$option .= '<optgroup label="Rabu, 9 Agustus 2017">';
				if ($count9_1 <= $limit) $option .= "<option value='August 09 2017 | 10:00-12:00' " . (($count9_1 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count9_1) . " kursi)</option>";
				if ($count9_2 <= $limit) $option .= "<option value='August 09 2017 | 13:00-15:00' " . (($count9_2 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count9_2) . " kursi)</option>";
				$option .= '</optgroup>';
			} else {
				$option = "<option value=''>Tidak Ada jadwal</option>";
			}
		} elseif ($cabang == "Padang") {
			$limit = 15;
			if ($kd_program == 19 || $kd_program == 1) {
				$count3_1 = DaftarUjian::where('tanggal', 'August 03 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Padang')->get()->count();
				$count3_2 = DaftarUjian::where('tanggal', 'August 03 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Padang')->get()->count();
				$count4_1 = DaftarUjian::where('tanggal', 'August 04 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Padang')->get()->count();
				$count4_2 = DaftarUjian::where('tanggal', 'August 04 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Padang')->get()->count();
				$count7_1 = DaftarUjian::where('tanggal', 'August 07 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Padang')->get()->count();
				$count7_2 = DaftarUjian::where('tanggal', 'August 07 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Padang')->get()->count();
				$count8_1 = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Padang')->get()->count();
				$count8_2 = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Padang')->get()->count();
				$count9_1 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Padang')->get()->count();
				$count9_2 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Padang')->get()->count();
				
//				$option .= '<optgroup label="Kamis, 3 Agustus 2017">';
//				if ($count3_1 <= $limit) $option .= "<option value='August 03 2017 | 10:00-12:00' " . (($count3_1 >= $limit) ? 'disabled' : '') . ">03 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count3_1) . " kursi)</option>";
//				if ($count3_2 <= $limit) $option .= "<option value='August 03 2017 | 13:00-15:00' " . (($count3_2 >= $limit) ? 'disabled' : '') . ">03 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count3_2) . " kursi)</option>";
//				$option .= '</optgroup>';
				$option .= '<optgroup label="Jum\'at, 4 Agustus 2017">';
				if ($count4_1 <= $limit) $option .= "<option value='August 04 2017 | 10:00-12:00' " . (($count4_1 >= $limit) ? 'disabled' : '') . ">04 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count4_1) . " kursi)</option>";
				if ($count4_2 <= $limit) $option .= "<option value='August 04 2017 | 13:00-15:00' " . (($count4_2 >= $limit) ? 'disabled' : '') . ">04 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count4_2) . " kursi)</option>";
				$option .= '</optgroup>';
				$option .= '<optgroup label="Minggu 6 Agustus 2017">';
					$option .= "<option value='August 06 2017 | 10:00-12:00'>6 Agustus 2017 - 10:00-12:00 WIB</option>";
				$option .= '</option>';
				$option .= '<optgroup label="Senin, 7 Agustus 2017">';
				if ($count7_1 <= $limit) $option .= "<option value='August 07 2017 | 10:00-12:00' " . (($count7_1 >= $limit) ? 'disabled' : '') . ">07 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count7_1) . " kursi)</option>";
				if ($count7_2 <= $limit) $option .= "<option value='August 07 2017 | 13:00-15:00' " . (($count7_2 >= $limit) ? 'disabled' : '') . ">07 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count7_2) . " kursi)</option>";
				$option .= '</optgroup>';
				$option .= '<optgroup label="Selasa, 8 Agustus 2017">';
				if ($count8_1 <= $limit) $option .= "<option value='August 08 2017 | 10:00-12:00' " . (($count8_1 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count8_1) . " kursi)</option>";
				if ($count8_2 <= $limit) $option .= "<option value='August 08 2017 | 13:00-15:00' " . (($count8_2 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count8_2) . " kursi)</option>";
				$option .= '</optgroup>';
				$option .= '<optgroup label="Rabu, 9 Agustus 2017">';
				if ($count9_1 <= $limit) $option .= "<option value='August 09 2017 | 10:00-12:00' " . (($count9_1 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count9_1) . " kursi)</option>";
				if ($count9_2 <= $limit) $option .= "<option value='August 09 2017 | 13:00-15:00' " . (($count9_2 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count9_2) . " kursi)</option>";
				$option .= '</optgroup>';
			} else {
				$option = "<option value=''>Tidak Ada jadwal</option>";
			}
		} elseif ($cabang == "Bengkulu") {
			if ($kd_program == 19 || $kd_program == 1) {
//				$option .= "<option value='July 27 2017 | 09:00-11:00'>27 Juli 2017 - 09:00-11:00 WIB</option>";
//				$option .= "<option value='July 28 2017 | 09:00-11:00'>28 Juli 2017 - 09:00-11:00 WIB</option>";
//				$option .= "<option value='July 31 2017 | 09:00-11:00'>31 Juli 2017 - 09:00-11:00 WIB</option>";
//				$option .= "<option value='August 01 2017 | 09:00-11:00'>1 Agustus 2017 - 09:00-11:00 WIB</option>";
//				$option .= "<option value='August 02 2017 | 09:00-11:00'>2 Agustus 2017 - 09:00-11:00 WIB</option>";
//				$option .= "<option value='August 03 2017 | 09:00-11:00'>3 Agustus 2017 - 09:00-11:00 WIB</option>";
				$option .= "<option value='August 04 2017 | 09:00-11:00'>4 Agustus 2017 - 09:00-11:00 WIB</option>";
				$option .= "<option value='August 07 2017 | 09:00-11:00'>7 Agustus 2017 - 09:00-11:00 WIB</option>";
				$option .= "<option value='August 08 2017 | 09:00-11:00'>8 Agustus 2017 - 09:00-11:00 WIB</option>";
				$option .= "<option value='August 09 2017 | 09:00-11:00'>9 Agustus 2017 - 09:00-11:00 WIB</option>";
			} else {
				$option = "<option value=''>Tidak Ada jadwal</option>";
			}
		} elseif ($cabang == 'Aceh') {
			$limit = 20;
			if ($kd_program == 19 || $kd_program == 1) {
				$count7_1 = DaftarUjian::where('tanggal', 'August 07 2017')->where('gelombang', '09:00-11:00')->where('cabang', 'Banda Aceh')->get()->count();
				$count7_2 = DaftarUjian::where('tanggal', 'August 07 2017')->where('gelombang', '11:00-13:00')->where('cabang', 'Banda Aceh')->get()->count();
				$count7_3 = DaftarUjian::where('tanggal', 'August 07 2017')->where('gelombang', '14:00-16:00')->where('cabang', 'Banda Aceh')->get()->count();
				
				$option .= '<optgroup label="Senin, 7 Agustus 2017">';
				if ($count7_1 <= $limit) $option .= "<option value='August 07 2017 | 09:00-11:00' " . (($count7_1 >= $limit) ? 'disabled' : '') . ">7 Agustus 2017 - 09:00-11:00 WIB (sisa " . ($limit - $count7_1) . " kursi)</option>";
				if ($count7_2 <= $limit) $option .= "<option value='August 07 2017 | 11:00-13:00' " . (($count7_2 >= $limit) ? 'disabled' : '') . ">7 Agustus 2017 - 11:00-13:00 WIB (sisa " . ($limit - $count7_2) . " kursi)</option>";
				if ($count7_3 <= $limit) $option .= "<option value='August 07 2017 | 14:00-16:00' " . (($count7_3 >= $limit) ? 'disabled' : '') . ">7 Agustus 2017 - 14:00-16:00 WIB (sisa " . ($limit - $count7_3) . " kursi)</option>";
				$option .= '</optgroup>';
				
				$count8_1 = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Aceh')->get()->count();
				$count8_2 = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Aceh')->get()->count();
				$count9_1 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Aceh')->get()->count();
				$count9_2 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Aceh')->get()->count();
				
				$option .= '<optgroup label="Selasa, 8 Agustus 2017">';
				if ($count8_1 <= $limit) $option .= "<option value='August 08 2017 | 10:00-12:00' " . (($count8_1 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count8_1) . " kursi)</option>";
				if ($count8_2 <= $limit) $option .= "<option value='August 08 2017 | 13:00-15:00' " . (($count8_2 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count8_2) . " kursi)</option>";
				$option .= '</optgroup>';
				$option .= '<optgroup label="Rabu, 9 Agustus 2017">';
				if ($count9_1 <= $limit) $option .= "<option value='August 09 2017 | 10:00-12:00' " . (($count9_1 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count9_1) . " kursi)</option>";
				if ($count9_2 <= $limit) $option .= "<option value='August 09 2017 | 13:00-15:00' " . (($count9_2 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count9_2) . " kursi)</option>";
				$option .= '</optgroup>';
			} else {
				$option = "<option value=''>Tidak Ada jadwal</option>";
			}
		} elseif ($cabang == "Pangkalpinang") {
			$limit = 15;
			if ($kd_program == 19 || $kd_program == 1) {
				$count8_1 = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Pangkalpinang')->get()->count();
				$count8_2 = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Pangkalpinang')->get()->count();
				$count9_1 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Pangkalpinang')->get()->count();
				$count9_2 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Pangkalpinang')->get()->count();
				
				$option .= '<optgroup label="Selasa, 8 Agustus 2017">';
				if ($count8_1 <= $limit) $option .= "<option value='August 08 2017 | 10:00-12:00' " . (($count8_1 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count8_1) . " kursi)</option>";
				if ($count8_2 <= $limit) $option .= "<option value='August 08 2017 | 13:00-15:00' " . (($count8_2 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count8_2) . " kursi)</option>";
				$option .= '</optgroup>';
				$option .= '<optgroup label="Rabu, 9 Agustus 2017">';
				if ($count9_1 <= $limit) $option .= "<option value='August 09 2017 | 10:00-12:00' " . (($count9_1 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count9_1) . " kursi)</option>";
				if ($count9_2 <= $limit) $option .= "<option value='August 09 2017 | 13:00-15:00' " . (($count9_2 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count9_2) . " kursi)</option>";
				$option .= '</optgroup>';
			} else {
				$option = "<option value=''>Tidak Ada jadwal</option>";
			}
		} elseif ($cabang == 'Lampung') {
			$limit = 50;
			if ($kd_program == 19) {
				$count7_1 = DaftarUjian::where('tanggal', 'August 07 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Lampung')->get()->count();
				$count7_2 = DaftarUjian::where('tanggal', 'August 07 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Lampung')->get()->count();
				
				if ($count7_1 <= $limit) $option .= "<option value='August 07 2017 | 10:00-12:00' " . (($count7_1 >= $limit) ? 'disabled' : '') . ">7 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count7_1) . " kursi)</option>";
				if ($count7_2 <= $limit) $option .= "<option value='August 07 2017 | 13:00-15:00' " . (($count7_2 >= $limit) ? 'disabled' : '') . ">7 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count7_2) . " kursi)</option>";
			} else {
				$option = "<option value=''>Tidak Ada jadwal</option>";
			}
		} elseif ($cabang == 'Ambon') {
			$limit = 15;
			if ($kd_program == 19 || $kd_program == 1) {
				$count8_1 = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Batam')->get()->count();
				$count8_2 = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Batam')->get()->count();
				$count9_1 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Batam')->get()->count();
				$count9_2 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Batam')->get()->count();
				
				$option .= '<optgroup label="Rabu, 9 Agustus 2017">';
				if ($count9_1 <= $limit) $option .= "<option value='August 09 2017 | 10:00-12:00' " . (($count9_1 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 10:00-12:00 WITA (sisa " . ($limit - $count9_1) . " kursi)</option>";
				if ($count9_2 <= $limit) $option .= "<option value='August 09 2017 | 13:00-15:00' " . (($count9_2 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 13:00-15:00 WITA (sisa " . ($limit - $count9_2) . " kursi)</option>";
				$option .= '</optgroup>';
			} else {
				$option = "<option value=''>Tidak Ada jadwal</option>";
			}
		} elseif ($cabang == 'Manokwari') {
			if ($kd_program == 19 || $kd_program == 1) {
				$option .= "<option value='August 08 2017 | 10:00-12:00'>8 Agustus 2017 - 10:00-12:00 WIT</option>";
			} else {
				$option = "<option value=''>Tidak Ada jadwal</option>";
			}
		} elseif ($cabang == 'Jambi') {
			$limit = 20;
			if ($kd_program == 19 || $kd_program == 1) {
				$count5_1 = DaftarUjian::where('tanggal', 'August 05 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Jambi')->get()->count();
				$count5_2 = DaftarUjian::where('tanggal', 'August 05 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Jambi')->get()->count();
				
				if ($count5_1 <= $limit) $option .= "<option value='August 05 2017 | 10:00-12:00' " . (($count5_1 >= $limit) ? 'disabled' : '') . ">5 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count5_1) . " kursi)</option>";
				if ($count5_2 <= $limit) $option .= "<option value='August 05 2017 | 13:00-15:00' " . (($count5_2 >= $limit) ? 'disabled' : '') . ">5 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count5_2) . " kursi)</option>";
			} else {
				$option = "<option value=''>Tidak Ada jadwal</option>";
			}
		} elseif ($cabang == 'Medan') {
			$limit = 20;
			if ($kd_program == 19 || $kd_program == 1) {
				$count2_1 = DaftarUjian::where('tanggal', 'August 02 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Medan')->get()->count();
				$count2_2 = DaftarUjian::where('tanggal', 'August 02 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Medan')->get()->count();
				$count3_1 = DaftarUjian::where('tanggal', 'August 03 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Medan')->get()->count();
				$count3_2 = DaftarUjian::where('tanggal', 'August 03 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Medan')->get()->count();
				$count4_1 = DaftarUjian::where('tanggal', 'August 04 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Medan')->get()->count();
				$count4_2 = DaftarUjian::where('tanggal', 'August 04 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Medan')->get()->count();
				$count7_1 = DaftarUjian::where('tanggal', 'August 07 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Medan')->get()->count();
				$count7_2 = DaftarUjian::where('tanggal', 'August 07 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Medan')->get()->count();
				$count8_1 = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Medan')->get()->count();
				$count8_2 = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Medan')->get()->count();
				$count9_1 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Medan')->get()->count();
				$count9_2 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Medan')->get()->count();
				
//				$option .= '<optgroup label="Rabu, 2 Agustus 2017">';
//				if ($count2_1 <= $limit) $option .= "<option value='August 02 2017 | 10:00-12:00' " . (($count2_1 >= $limit) ? 'disabled' : '') . ">02 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count2_1) . " kursi)</option>";
//				if ($count2_2 <= $limit) $option .= "<option value='August 02 2017 | 13:00-15:00' " . (($count2_2 >= $limit) ? 'disabled' : '') . ">02 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count2_2) . " kursi)</option>";
//				$option .= '</optgroup>';
//				$option .= '<optgroup label="Kamis, 3 Agustus 2017">';
//				if ($count3_1 <= $limit) $option .= "<option value='August 03 2017 | 10:00-12:00' " . (($count3_1 >= $limit) ? 'disabled' : '') . ">03 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count3_1) . " kursi)</option>";
//				if ($count3_2 <= $limit) $option .= "<option value='August 03 2017 | 13:00-15:00' " . (($count3_2 >= $limit) ? 'disabled' : '') . ">03 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count3_2) . " kursi)</option>";
//				$option .= '</optgroup>';
				$option .= '<optgroup label="Jum\'at, 4 Agustus 2017">';
				if ($count4_1 <= $limit) $option .= "<option value='August 04 2017 | 10:00-12:00' " . (($count4_1 >= $limit) ? 'disabled' : '') . ">04 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count4_1) . " kursi)</option>";
				if ($count4_2 <= $limit) $option .= "<option value='August 04 2017 | 13:00-15:00' " . (($count4_2 >= $limit) ? 'disabled' : '') . ">04 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count4_2) . " kursi)</option>";
				$option .= '</optgroup>';
				$option .= '<optgroup label="Senin, 7 Agustus 2017">';
				if ($count7_1 <= $limit) $option .= "<option value='August 07 2017 | 10:00-12:00' " . (($count7_1 >= $limit) ? 'disabled' : '') . ">07 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count7_1) . " kursi)</option>";
				if ($count7_2 <= $limit) $option .= "<option value='August 07 2017 | 13:00-15:00' " . (($count7_2 >= $limit) ? 'disabled' : '') . ">07 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count7_2) . " kursi)</option>";
				$option .= '</optgroup>';
				$option .= '<optgroup label="Selasa, 8 Agustus 2017">';
				if ($count8_1 <= $limit) $option .= "<option value='August 08 2017 | 10:00-12:00' " . (($count8_1 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count8_1) . " kursi)</option>";
				if ($count8_2 <= $limit) $option .= "<option value='August 08 2017 | 13:00-15:00' " . (($count8_2 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count8_2) . " kursi)</option>";
				$option .= '</optgroup>';
				$option .= '<optgroup label="Rabu, 9 Agustus 2017">';
				if ($count9_1 <= $limit) $option .= "<option value='August 09 2017 | 10:00-12:00' " . (($count9_1 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count9_1) . " kursi)</option>";
				if ($count9_2 <= $limit) $option .= "<option value='August 09 2017 | 13:00-15:00' " . (($count9_2 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count9_2) . " kursi)</option>";
				$option .= '</optgroup>';
			} else {
				$option = "<option value=''>Tidak Ada jadwal</option>";
			}
		} elseif ($cabang == 'Pekanbaru') {
			$limit = 20;
			if ($kd_program == 19 || $kd_program == 1) {
				$count2_1 = DaftarUjian::where('tanggal', 'August 02 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Pekanbaru')->get()->count();
				$count2_2 = DaftarUjian::where('tanggal', 'August 02 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Pekanbaru')->get()->count();
				$count3_1 = DaftarUjian::where('tanggal', 'August 03 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Pekanbaru')->get()->count();
				$count3_2 = DaftarUjian::where('tanggal', 'August 03 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Pekanbaru')->get()->count();
				$count4_1 = DaftarUjian::where('tanggal', 'August 04 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Pekanbaru')->get()->count();
				$count4_2 = DaftarUjian::where('tanggal', 'August 04 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Pekanbaru')->get()->count();
				$count7_1 = DaftarUjian::where('tanggal', 'August 07 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Pekanbaru')->get()->count();
				$count7_2 = DaftarUjian::where('tanggal', 'August 07 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Pekanbaru')->get()->count();
				$count8_1 = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Pekanbaru')->get()->count();
				$count8_2 = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Pekanbaru')->get()->count();
				$count9_1 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Pekanbaru')->get()->count();
				$count9_2 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Pekanbaru')->get()->count();
				
//				$option .= '<optgroup label="Rabu, 2 Agustus 2017">';
//				if ($count2_1 <= $limit) $option .= "<option value='August 02 2017 | 10:00-12:00' " . (($count2_1 >= $limit) ? 'disabled' : '') . ">02 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count2_1) . " kursi)</option>";
//				if ($count2_2 <= $limit) $option .= "<option value='August 02 2017 | 13:00-15:00' " . (($count2_2 >= $limit) ? 'disabled' : '') . ">02 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count2_2) . " kursi)</option>";
//				$option .= '</optgroup>';
//				$option .= '<optgroup label="Kamis, 3 Agustus 2017">';
//				if ($count3_1 <= $limit) $option .= "<option value='August 03 2017 | 10:00-12:00' " . (($count3_1 >= $limit) ? 'disabled' : '') . ">03 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count3_1) . " kursi)</option>";
//				if ($count3_2 <= $limit) $option .= "<option value='August 03 2017 | 13:00-15:00' " . (($count3_2 >= $limit) ? 'disabled' : '') . ">03 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count3_2) . " kursi)</option>";
//				$option .= '</optgroup>';
				$option .= '<optgroup label="Jum\'at, 4 Agustus 2017">';
				if ($count4_1 <= $limit) $option .= "<option value='August 04 2017 | 10:00-12:00' " . (($count4_1 >= $limit) ? 'disabled' : '') . ">04 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count4_1) . " kursi)</option>";
				if ($count4_2 <= $limit) $option .= "<option value='August 04 2017 | 13:00-15:00' " . (($count4_2 >= $limit) ? 'disabled' : '') . ">04 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count4_2) . " kursi)</option>";
				$option .= '</optgroup>';
				$option .= '<optgroup label="Senin, 7 Agustus 2017">';
				if ($count7_1 <= $limit) $option .= "<option value='August 07 2017 | 10:00-12:00' " . (($count7_1 >= $limit) ? 'disabled' : '') . ">07 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count7_1) . " kursi)</option>";
				if ($count7_2 <= $limit) $option .= "<option value='August 07 2017 | 13:00-15:00' " . (($count7_2 >= $limit) ? 'disabled' : '') . ">07 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count7_2) . " kursi)</option>";
				$option .= '</optgroup>';
				$option .= '<optgroup label="Selasa, 8 Agustus 2017">';
				if ($count8_1 <= $limit) $option .= "<option value='August 08 2017 | 10:00-12:00' " . (($count8_1 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count8_1) . " kursi)</option>";
				if ($count8_2 <= $limit) $option .= "<option value='August 08 2017 | 13:00-15:00' " . (($count8_2 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count8_2) . " kursi)</option>";
				$option .= '</optgroup>';
				$option .= '<optgroup label="Rabu, 9 Agustus 2017">';
				if ($count9_1 <= $limit) $option .= "<option value='August 09 2017 | 10:00-12:00' " . (($count9_1 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count9_1) . " kursi)</option>";
				if ($count9_2 <= $limit) $option .= "<option value='August 09 2017 | 13:00-15:00' " . (($count9_2 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count9_2) . " kursi)</option>";
				$option .= '</optgroup>';
			} else {
				$option = "<option value=''>Tidak Ada jadwal</option>";
			}
		} elseif ($cabang == 'Batam') {
			$limit = 20;
			if ($kd_program == 19 || $kd_program == 1) {
				$count7_1 = DaftarUjian::where('tanggal', 'August 07 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Batam')->get()->count();
				$count7_2 = DaftarUjian::where('tanggal', 'August 07 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Batam')->get()->count();
				$count8_1 = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Batam')->get()->count();
				$count8_2 = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Batam')->get()->count();
				$count9_1 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Batam')->get()->count();
				$count9_2 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Batam')->get()->count();
				
				$option .= '<optgroup label="Senin, 7 Agustus 2017">';
				if ($count7_1 <= $limit) $option .= "<option value='August 07 2017 | 10:00-12:00' " . (($count7_1 >= $limit) ? 'disabled' : '') . ">07 Agustus 2017 - 10:00-12:00 WITA (sisa " . ($limit - $count7_1) . " kursi)</option>";
				if ($count7_2 <= $limit) $option .= "<option value='August 07 2017 | 13:00-15:00' " . (($count7_2 >= $limit) ? 'disabled' : '') . ">07 Agustus 2017 - 13:00-15:00 WITA (sisa " . ($limit - $count7_2) . " kursi)</option>";
				$option .= '</optgroup>';
				$option .= '<optgroup label="Selasa, 8 Agustus 2017">';
				if ($count8_1 <= $limit) $option .= "<option value='August 08 2017 | 10:00-12:00' " . (($count8_1 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 10:00-12:00 WITA (sisa " . ($limit - $count8_1) . " kursi)</option>";
				if ($count8_2 <= $limit) $option .= "<option value='August 08 2017 | 13:00-15:00' " . (($count8_2 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 13:00-15:00 WITA (sisa " . ($limit - $count8_2) . " kursi)</option>";
				$option .= '</optgroup>';
				$option .= '<optgroup label="Rabu, 9 Agustus 2017">';
				if ($count9_1 <= $limit) $option .= "<option value='August 09 2017 | 10:00-12:00' " . (($count9_1 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 10:00-12:00 WITA (sisa " . ($limit - $count9_1) . " kursi)</option>";
				if ($count9_2 <= $limit) $option .= "<option value='August 09 2017 | 13:00-15:00' " . (($count9_2 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 13:00-15:00 WITA (sisa " . ($limit - $count9_2) . " kursi)</option>";
				$option .= '</optgroup>';
			} else {
				$option = "<option value=''>Tidak Ada jadwal</option>";
			}
		} elseif ($cabang == 'Banjarmasin') {
			$limit = 10;
			if ($kd_program == 19 || $kd_program == 1) {
				$count7_1 = DaftarUjian::where('tanggal', 'August 07 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Banjarmasin')->get()->count();
				$count7_2 = DaftarUjian::where('tanggal', 'August 07 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Banjarmasin')->get()->count();
				$count8_1 = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Banjarmasin')->get()->count();
				$count8_2 = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Banjarmasin')->get()->count();
				$count9_1 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Banjarmasin')->get()->count();
				$count9_2 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Banjarmasin')->get()->count();
				
				$option .= '<optgroup label="Senin, 7 Agustus 2017">';
				if ($count7_1 <= $limit) $option .= "<option value='August 07 2017 | 10:00-12:00' " . (($count7_1 >= $limit) ? 'disabled' : '') . ">07 Agustus 2017 - 10:00-12:00 WITA (sisa " . ($limit - $count7_1) . " kursi)</option>";
				if ($count7_2 <= $limit) $option .= "<option value='August 07 2017 | 13:00-15:00' " . (($count7_2 >= $limit) ? 'disabled' : '') . ">07 Agustus 2017 - 13:00-15:00 WITA (sisa " . ($limit - $count7_2) . " kursi)</option>";
				$option .= '</optgroup>';
				$option .= '<optgroup label="Selasa, 8 Agustus 2017">';
				if ($count8_1 <= $limit) $option .= "<option value='August 08 2017 | 10:00-12:00' " . (($count8_1 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 10:00-12:00 WITA (sisa " . ($limit - $count8_1) . " kursi)</option>";
				if ($count8_2 <= $limit) $option .= "<option value='August 08 2017 | 13:00-15:00' " . (($count8_2 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 13:00-15:00 WITA (sisa " . ($limit - $count8_2) . " kursi)</option>";
				$option .= '</optgroup>';
				$option .= '<optgroup label="Rabu, 9 Agustus 2017">';
				if ($count9_1 <= $limit) $option .= "<option value='August 09 2017 | 10:00-12:00' " . (($count9_1 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 10:00-12:00 WITA (sisa " . ($limit - $count9_1) . " kursi)</option>";
				if ($count9_2 <= $limit) $option .= "<option value='August 09 2017 | 13:00-15:00' " . (($count9_2 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 13:00-15:00 WITA (sisa " . ($limit - $count9_2) . " kursi)</option>";
				$option .= '</optgroup>';
			} else {
				$option = "<option value=''>Tidak Ada jadwal</option>";
			}
		} elseif ($cabang == 'Denpasar') {
			$limit = 10;
			if ($kd_program == 19 || $kd_program == 1) {
				$count7_1 = DaftarUjian::where('tanggal', 'August 07 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Denpasar')->get()->count();
				$count7_2 = DaftarUjian::where('tanggal', 'August 07 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Denpasar')->get()->count();
				$count8_1 = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Denpasar')->get()->count();
				$count8_2 = DaftarUjian::where('tanggal', 'August 08 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Denpasar')->get()->count();
				$count9_1 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Denpasar')->get()->count();
				$count9_2 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Denpasar')->get()->count();
				
				$option .= '<optgroup label="Senin, 7 Agustus 2017">';
				if ($count7_1 <= $limit) $option .= "<option value='August 07 2017 | 10:00-12:00' " . (($count7_1 >= $limit) ? 'disabled' : '') . ">07 Agustus 2017 - 10:00-12:00 WITA (sisa " . ($limit - $count7_1) . " kursi)</option>";
				if ($count7_2 <= $limit) $option .= "<option value='August 07 2017 | 13:00-15:00' " . (($count7_2 >= $limit) ? 'disabled' : '') . ">07 Agustus 2017 - 13:00-15:00 WITA (sisa " . ($limit - $count7_2) . " kursi)</option>";
				$option .= '</optgroup>';
				$option .= '<optgroup label="Selasa, 8 Agustus 2017">';
				if ($count8_1 <= $limit) $option .= "<option value='August 08 2017 | 10:00-12:00' " . (($count8_1 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 10:00-12:00 WITA (sisa " . ($limit - $count8_1) . " kursi)</option>";
				if ($count8_2 <= $limit) $option .= "<option value='August 08 2017 | 13:00-15:00' " . (($count8_2 >= $limit) ? 'disabled' : '') . ">08 Agustus 2017 - 13:00-15:00 WITA (sisa " . ($limit - $count8_2) . " kursi)</option>";
				$option .= '</optgroup>';
				$option .= '<optgroup label="Rabu, 9 Agustus 2017">';
				if ($count9_1 <= $limit) $option .= "<option value='August 09 2017 | 10:00-12:00' " . (($count9_1 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 10:00-12:00 WITA (sisa " . ($limit - $count9_1) . " kursi)</option>";
				if ($count9_2 <= $limit) $option .= "<option value='August 09 2017 | 13:00-15:00' " . (($count9_2 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 13:00-15:00 WITA (sisa " . ($limit - $count9_2) . " kursi)</option>";
				$option .= '</optgroup>';
			} else {
				$option = "<option value=''>Tidak Ada jadwal</option>";
			}
		} elseif ($cabang == 'Palangkaraya') {
			$limit = 10;
			if ($kd_program == 19 || $kd_program == 1) {
				$count9_1 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '10:00-12:00')->where('cabang', 'Palangkaraya')->get()->count();
				$count9_2 = DaftarUjian::where('tanggal', 'August 09 2017')->where('gelombang', '13:00-15:00')->where('cabang', 'Palangkaraya')->get()->count();
				
				$option .= '<optgroup label="Rabu, 9 Agustus 2017">';
				if ($count9_1 <= $limit) $option .= "<option value='August 09 2017 | 10:00-12:00' " . (($count9_1 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 10:00-12:00 WIB (sisa " . ($limit - $count9_1) . " kursi)</option>";
				if ($count9_2 <= $limit) $option .= "<option value='August 09 2017 | 13:00-15:00' " . (($count9_2 >= $limit) ? 'disabled' : '') . ">09 Agustus 2017 - 13:00-15:00 WIB (sisa " . ($limit - $count9_2) . " kursi)</option>";
				$option .= '</optgroup>';
			} else {
				$option = "<option value=''>Tidak Ada jadwal</option>";
			}
		}
		echo $option;
	}
	
	public function reportBimbel()
	{
		$ujian          = DaftarUjian::where('remark', 'Bimbel')->get();
		$ujian2         = DaftarUjian::where('remark', 'Gratis')->get();
		$allProgram     = [1 => 'WPPE', 19 => 'WPPE Pemasaran', 18 => 'WPPE Pemasaran Terbatas', 2 => 'WMI'];
		$allCabang      = [
			'Jakarta'   => 'Jakarta', 'Surabaya' => 'Surabaya', 'Semarang' => 'Semarang', 'Pontianak' => 'Pontianak', 'Yogyakarta' => 'Yogyakarta'
			, 'Bandung' => 'Bandung', 'Jayapura' => 'Jayapura', 'Palembang' => 'Palembang', 'Kendari' => 'Kendari', 'Padang' => 'Padang', 'Bengkulu' => 'Bengkulu',
			'Banda Aceh'      => 'Banda Aceh', 'Pangkalpinang' => 'Pangkalpinang', 'Lampung' => 'Lampung','Ambon'=>'Ambon','Manokwari'=>'Manokwari',
			'Jambi' => 'Jambi', 'Medan' => 'Medan', 'Pekanbaru' => 'Pekanbaru', 'Batam' => 'Batam', 'Banjarmasin' => 'Banjarmasin', 'Denpasar' => 'Denpasar',
			'Palangkaraya' => 'Palangkaraya'
		];
		$tgl_ujian      = [
			'July 25 2017',
			'July 26 2017',
			'July 27 2017',
			'July 28 2017',
			'July 31 2017',
			'August 01 2017',
			'August 02 2017',
			'August 03 2017',
			'August 04 2017',
			'August 07 2017',
			'August 08 2017',
			'August 09 2017',
			'August 10 2017',
			'August 11 2017',
			'August 14 2017',
			'August 15 2017',
			'August 16 2017',
			'August 17 2017',
			'August 18 2017',
		];
		$wppe           = Admisi::select('id')->where('kd_program', 1)->where('nip', '!=', '0')->get()->count();
		$wppe_goceng    = Admisi::select('id')->where('kd_program', 1)->where('nip', '!=', '0')->whereRaw("FROM_UNIXTIME(tanggal) BETWEEN '2017-07-08' AND '2017-08-18'")->get()->count();
		$wppe_p         = Admisi::select('id')->where('kd_program', 19)->where('nip', '!=', '0')->get()->count();
		$wppe_p_goceng  = Admisi::select('id')->where('kd_program', 19)->where('nip', '!=', '0')->whereRaw("FROM_UNIXTIME(tanggal) BETWEEN '2017-07-08' AND '2017-08-18'")->get()->count();
		$wppe_pt        = Admisi::select('id')->where('kd_program', 18)->where('nip', '!=', '0')->get()->count();
		$wppe_pt_goceng = Admisi::select('id')->where('kd_program', 18)->where('nip', '!=', '0')->whereRaw("FROM_UNIXTIME(tanggal) BETWEEN '2017-07-08' AND '2017-08-18'")->get()->count();
		$wmi            = Admisi::select('id')->where('kd_program', 2)->where('nip', '!=', '0')->get()->count();
		$wmi_goceng     = Admisi::select('id')->where('kd_program', 2)->where('nip', '!=', '0')->whereRaw("FROM_UNIXTIME(tanggal) BETWEEN '2017-07-08' AND '2017-08-18'")->get()->count();
		$aspm           = Admisi::select('id')->where('kd_program', 22)->where('nip', '!=', '0')->get()->count();
		$aspm_goceng           = Admisi::select('id')->where('kd_program', 22)->where('nip', '!=', '0')->whereRaw("FROM_UNIXTIME(tanggal) BETWEEN '2017-07-08' AND '2017-08-18'")->get()->count();
		return view('frontend.bimbel.report', compact('ujian', 'allCabang', 'allProgram', 'ujian2', 'tgl_ujian','wppe','wppe_goceng','wppe_p','wppe_p_goceng','wppe_pt','wppe_pt_goceng','wmi','wmi_goceng','aspm','aspm_goceng'));
	}
	
	public function resendBimbel()
	{
		$ujian = DaftarUjian::where('cabang', 'Jakarta')->first();
		return view('email-template.email-reminder-bimbel', compact('ujian'));
//		$resend = DaftarUjian::where('gelombang', 'Bimbel')->get();
//		foreach ($resend as $item) {
//			$job = (new SendEmailPendaftaranBimbel($item))->delay(5);
//			$this->dispatch($job);
//		}
	}
	
	public function pendaftaranDetail(Request $request, $batchid, $batchname)
	{
		$pageTitle  = $batchname;
		$batch      = Batch::findOrFail($batchid);
		$time       = strtotime("-7 days");
		$otherbatch = Batch::where('id', '!=', $batchid)->where("mulai", '!=', "y")->where("approve", "y")->where("pasang", "y")->where("tanggal", ">", $time)->get();
		return view('frontend.pendaftaran-detail', compact('batch', 'otherbatch', 'pageTitle'));
	}
	
	public function showPengajar(Request $request, $id, $name)
	{
		$pengajar      = Pengajar::find($id);
		$otherPengajar = Pengajar::where('foto', '!=', '')->where('aktif', 'y')->where('id', '!=', $id)->limit(4)->orderByRaw("RAND()")->get();
		$keahlian      = GeneralHelper::getKategori($pengajar->history);
		$time          = strtotime("-7 days");
		$batch         = Batch::where("mulai", '!=', "y")->where("approve", "y")->where("pasang", "y")->where("tanggal", ">", $time)->get();
		return view('frontend.pengajar-view', compact('pengajar', 'otherPengajar', 'keahlian', 'batch'));
	}
	
	public function login()
	{
		$pageTitle = "Login";
		return view('frontend.login', compact('pageTitle'));
	}
	
	public function riskManagement()
	{
		return view('risk.index');
	}
	
	public function pendaftaranRiskManagement()
	{
		$arrKelasSesi1 = [
			''                                      => 'PILIH KELAS SESI I ( 09:30 - 11:30 )',
			'Pendidikan (Ruang Seminar I)'          => 'Pendidikan (Ruang Seminar I)',
			'Keuangan (Ruang Seminar II)'           => 'Keuangan (Ruang Seminar II)',
			'Telekomunikasi IT (Ruang Seminar III)' => 'Telekomunikasi IT (Ruang Seminar III)',
			'Pasar Modal (Ruang Auditorium)'        => 'Pasar Modal (Ruang Auditorium)'
		];
		
		$arrKelasSesi2 = [
			''                                   => 'PILIH KELAS SESI II ( 13:30 - 15:30 )',
			'Infrastruktur (Ruang Seminar I)'    => 'Infrastruktur (Ruang Seminar I)',
			'Tambang Mineral (Ruang Seminar II)' => 'Tambang Mineral (Ruang Seminar II)',
			'Pasar Modal (Ruang Seminar III)'    => 'Pasar Modal (Ruang Seminar III)'
		];
		
		$pendidikan    = PelatihanBsn::where('kelas', 'Pendidikan (Sesi I, Ruang Seminar 1)')->get();
		$keuangan      = PelatihanBsn::where('kelas', 'Keuangan (Sesi I, Ruang Seminar 2)')->get();
		$tambang       = PelatihanBsn::where('kelas', 'Tambang Mineral (Sesi I, Ruang Seminar 3)')->get();
		$pasarmodal1   = PelatihanBsn::where('kelas', 'Pasar Modal (Sesi I, Ruang Auditorium)')->get();
		$telekom       = PelatihanBsn::where('kelas', 'Telekomunikasi IT (Sesi II, Ruang Seminar 1)')->get();
		$infrastruktur = PelatihanBsn::where('kelas', 'Infrastruktur (Sesi II, Ruang Seminar 2)')->get();
		$pasarmodal2   = PelatihanBsn::where('kelas', 'Pasar Modal (Sesi II, Ruang Seminar 3)')->get();
		
		return view('risk.daftar', compact('arrKelasSesi1', 'arrKelasSesi2', 'pendidikan', 'keuangan', 'tambang', 'pasarmodal1', 'telekom', 'infrastruktur', 'pasarmodal2'));
	}
	
	public function storePendaftaranRiskManagement(Requests\PelatihanBsnRequest $request)
	{
		$pelatihan = new PelatihanBsn($request->all());
		
		if ($pelatihan->save()) {
			$this->dispatch(new SendPendaftaranPelatihanBsn($pelatihan));
			flash()->success('Pendaftaran pelatihan telah sukses.');
			return redirect()->route('pelatihan.daftar.success', ['pelatihanbsn' => $pelatihan]);
		} else {
			flash()->error('Pendaftaran pelatihan gagal');
			return redirect()->route('pelatihan.daftar.failed', ['pelatihanbsn' => $pelatihan]);
		}
	}
	
	public function absenPesertaPelatihanBsn($email = null)
	{
//		if (date("d-m-Y") == '21-10-2016')  {
		if ($email) {
			$peserta = PelatihanBsn::where('email', $email)->first();
			if ($peserta && $peserta->count() > 0) {
				return view('risk.absen', compact('peserta', 'email'));
			} else {
				flash()->error('Data tidak ditemukan');
				return view('risk.kehadiran');
//					return redirect()->route('pelatihan.index');
			}
		} else {
			flash()->error('Email tidak ditemukan');
			return view('risk.kehadiran');
//				return redirect()->route('pelatihan.index');
		}
//		} else {
//			flash()->error('Tanggal Salah');
//			return view('risk.kehadiran');
////			return redirect()->route('pelatihan.index');
//		}
	}
	
	public function storeAbsenPelatihanBsn($email = null, $fromlaporan = null)
	{
		$peserta = PelatihanBsn::where('email', $email)->first();
		if ($peserta && $peserta->count() > 0) {
			if (empty($peserta->absen)) {
				if ($peserta->update(['absen' => date('Y-m-d H:i:s')])) {
					flash()->success('Absensi berhasil disimpan');
				} else {
					flash()->error('Absensi gagal, silahkan ulangi kembali');
				}
			} else {
				flash()->error('Peserta sudah melakukan Absensi sebelumnya');
			}
		} else {
			flash()->error('Peserta dengan email <strong>' . $email . '</strong> tidak ditemukan');
		}
		if (!empty($fromlaporan)) {
			return redirect()->route('laporanbsn.detail', ['kelas' => $fromlaporan]);
		} else {
			return view('risk.kehadiran');
		}
	}
	
	public function laporanBsn()
	{
		$pendidikan    = PelatihanBsn::where('kelas', 'Pendidikan (Sesi I, Ruang Seminar 1)')->get();
		$keuangan      = PelatihanBsn::where('kelas', 'Keuangan (Sesi I, Ruang Seminar 2)')->get();
		$tambang       = PelatihanBsn::where('kelas', 'Tambang Mineral (Sesi I, Ruang Seminar 3)')->get();
		$pasarmodal1   = PelatihanBsn::where('kelas', 'Pasar Modal (Sesi I, Ruang Auditorium)')->get();
		$telekom       = PelatihanBsn::where('kelas', 'Telekomunikasi IT (Sesi II, Ruang Seminar 1)')->get();
		$infrastruktur = PelatihanBsn::where('kelas', 'Infrastruktur (Sesi II, Ruang Seminar 2)')->get();
		$pasarmodal2   = PelatihanBsn::where('kelas', 'Pasar Modal (Sesi II, Ruang Seminar 3)')->get();
		
		$pesertaAbsen = PelatihanBsn::whereNotNull('absen')->get();
		
		return view('risk.laporan', compact('pendidikan', 'keuangan', 'tambang', 'pasarmodal1', 'telekom', 'infrastruktur', 'pasarmodal2', 'pesertaAbsen'));
	}
	
	public function laporanBsn2()
	{
		$pendidikan    = PelatihanBsn::where('kelas', 'Pendidikan (Sesi I, Ruang Seminar 1)')->get();
		$keuangan      = PelatihanBsn::where('kelas', 'Keuangan (Sesi I, Ruang Seminar 2)')->get();
		$tambang       = PelatihanBsn::where('kelas', 'Tambang Mineral (Sesi I, Ruang Seminar 3)')->get();
		$pasarmodal1   = PelatihanBsn::where('kelas', 'Pasar Modal (Sesi I, Ruang Auditorium)')->get();
		$telekom       = PelatihanBsn::where('kelas', 'Telekomunikasi IT (Sesi II, Ruang Seminar 1)')->get();
		$infrastruktur = PelatihanBsn::where('kelas', 'Infrastruktur (Sesi II, Ruang Seminar 2)')->get();
		$pasarmodal2   = PelatihanBsn::where('kelas', 'Pasar Modal (Sesi II, Ruang Seminar 3)')->get();
		
		$pesertaAbsen = PelatihanBsn::whereNotNull('absen')->get();
		
		return view('risk.laporan2', compact('pendidikan', 'keuangan', 'tambang', 'pasarmodal1', 'telekom', 'infrastruktur', 'pasarmodal2', 'pesertaAbsen'));
	}
	
	public function laporanBsnDetail($kelas = null)
	{
		$peserta = PelatihanBsn::when(($kelas == 'pendidikan'), function ($query) {
			return $query->where('kelas', 'Pendidikan (Sesi I, Ruang Seminar 1)');
		})->when(($kelas == 'keuangan'), function ($query) {
			return $query->where('kelas', 'Keuangan (Sesi I, Ruang Seminar 2)');
		})->when(($kelas == 'tambang'), function ($query) {
			return $query->where('kelas', 'Tambang Mineral (Sesi I, Ruang Seminar 3)');
		})->when(($kelas == 'pasarmodal1'), function ($query) {
			return $query->where('kelas', 'Pasar Modal (Sesi I, Ruang Auditorium)');
		})->when(($kelas == 'telekom'), function ($query) {
			return $query->where('kelas', 'Telekomunikasi IT (Sesi II, Ruang Seminar 1)');
		})->when(($kelas == 'infrastruktur'), function ($query) {
			return $query->where('kelas', 'Infrastruktur (Sesi II, Ruang Seminar 2)');
		})->when(($kelas == 'pasarmodal2'), function ($query) {
			return $query->where('kelas', 'Pasar Modal (Sesi II, Ruang Seminar 3)');
		})
			->get();
		return view('risk.laporandetail', compact('peserta', 'kelas'));
	}
	
	public function konfirmasiKehadiranBsn($email = null, $status = null)
	{
		$message = '';
		if ($email && $status) {
			$peserta = PelatihanBsn::where('email', $email)->first();
			if ($peserta && $peserta->count() > 0) {
				if (!empty($peserta->hadir)) {
					flash()->warning('Maaf, Anda telah melakukan konfirmasi kehadiran sebelumnya');
				} else {
					if ($peserta->update(['hadir' => $status])) {
						if ($status == 'yes') {
							$message = 'Sampai berjuma di acara Pelatihan SNI ISO 31000 Menajemen Risiko';
						} elseif ($status == 'no') {
							$message = 'Anda memilih tidak dapat hadir di acara Pelatihan SNI ISO 31000 Manajemen Risiko';
						}
						flash()->success('Terima kasih atas konfirmasi kehadiran Anda. ' . $message);
					} else {
						flash()->error('Maaf konfirmasi kehadiran gagal kami terima, mohon untuk mengulangi kembali konfirmasi kehadiran Anda dengan klik salah satu tombol yang ada di Email Anda');
					}
				}
				return view('risk.kehadiran');
			} else {
				return redirect('/');
			}
		} else {
			return redirect('/');
		}
	}
	
	public function resendemail($email = null, $fromlaporan = null)
	{
		$pelatihan = PelatihanBsn::where('email', $email)->first();
		if ($pelatihan && $pelatihan->count() > 0) {
			Mail::send('email-template.email-pelatihanbsn', ['pelatihan' => $pelatihan], function ($m) use ($pelatihan) {
				$m->from('noreply@ticmi.co.id', 'TICMI');
				$m->to($pelatihan->email, $pelatihan->nama)->subject('Konfirmasi Pendaftaran Pelatihan ISO SNI 31000 Manajemen Risiko');
			});
			
			flash()->success('Email Notifikasi berhasil dikirim');
		} else {
			flash()->error('Data dengan email : ' . $email . ' tidak ditemukan');
		}
		return redirect()->route('laporanbsn.detail', ['kelas' => $fromlaporan]);
	}
	
	
	
	public function konfirmasi_kedatangan()
	{
		$pageTitle = "Konfirmasi Kehadiran Acara TICMI";
		return view('risk.acara', compact('pageTitle'));
	}
	
	public function KonfirmasiAcara(Request $request)
	{
		$alumni = '';
		$this->validate($request, [
			'email' => 'required|email'
		]);
		$email = $request->get('email');
		if ($email) {
			$cekAlumni = Admisi::where('email', $email)->where('nip','!=', 0)->get();
			$cekMember = User::where('email', $email)->where('is_verify', 1)->get();
			// var_dump($cekAlumni);
			if($cekAlumni && $cekAlumni->count() > 0){
				$request->offsetSet('alumni', 'Y');
				$peserta = Acara::where('email', $email)->get();
				if ($peserta && $peserta->count() > 0) {
					flash()->warning('Maaf, Anda telah melakukan konfirmasi kehadiran sebelumnya');
					return redirect()->route('konfirmasi.daftar');
					
				} else {
					$cekPeserta = Acara::where('alumni', 'Y')->get();
					// var_dump($cekPeserta->count());
					if($cekPeserta && $cekPeserta->count() < 70){
						Acara::create($request->all());
						flash()->success('Konfirmasi kehadiran Anda Berhasil. ' );
						return redirect()->route('konfirmasi.daftar');
					}else{
						flash()->error('Maaf Quota sudah tidak tersedia. ' );
						return redirect()->route('konfirmasi.daftar');
					}
				}
				
			}elseif($cekMember && $cekMember->count() > 0){
				$request->offsetSet('alumni', 'N');
				$peserta = Acara::where('email', $email)->get();
				
				if ($peserta && $peserta->count() > 0) {
					flash()->warning('Maaf, Anda telah melakukan konfirmasi kehadiran sebelumnya');
					return redirect()->route('konfirmasi.daftar');
					
				} else {
					$cekPeserta = Acara::where('alumni', 'N')->get();
					// var_dump($cekPeserta->count());
					if($cekPeserta && $cekPeserta->count() < 40){
						Acara::create($request->all());
						flash()->success('Konfirmasi kehadiran Anda Berhasil. ' );
						return redirect()->route('konfirmasi.daftar');
					}else{
						flash()->warning('Maaf Quota sudah tidak tersedia. ' );
						return redirect()->route('konfirmasi.daftar');
					}
				}
				
			}else{
				flash()->warning('Mohon maaf anda tidak terdaftar di buku tamu. ' );
				return redirect()->route('konfirmasi.daftar');
			}
		}
	}
	
	public function blastEmailKehadiranBsn()
	{
		$peserta = PelatihanBsn::where(\DB::raw('hadir'))->get();
//		return $peserta;
		foreach ($peserta as $item) {
//			echo $item->nama.' - '.$item->email.' - '.$item->hadir.'<br/>';
			$jobs = (new SendEmailKonfirmasiKehadiranBsn($item))->delay(10);
			$this->dispatch($jobs);
		}
	}
	
	public function blastFidelitas()
	{
		
		Mail::send('email-template.email-fidelitas', [], function ($m) {
			$m->from('noreply@ticmi.co.id', 'TICMI');
			$m->cc('irwansyahdanway@gmail.com', 'Irwansyah');
			$m->to('m.hakim.adiprasetya@gmail.com ', 'M Hakim Adiprasetya')->subject('Corporate Finance Qualification Preparatory Class');
		});
//		$akmal = Customer::find(42065);
//		return $akmal;

//		$jobs = (new SendEmailBlastFidelitas($akmal))->delay(60);
//		$jobs = new SendEmailBlastFidelitas($akmal);
//		$this->dispatch($jobs);

//		$customers = Customer::where(\DB::raw('YEAR(registeredDate)'),2016)->limit(250)->get();
////		$no = 1;
//		foreach ($customers as $customer) {
//			$jobs = (new SendEmailBlastFidelitas($customer))->delay(10);
//			$this->dispatch($jobs);
//		}
	}
	
	public function cekemail()
	{
//		echo phpinfo();
//		$redis = app()->make('redis');
//		$redis->set('key1','cobaredis');
//		return $redis->get('key1');
//		$user = User::findOrFail($id);
//		$user = ['nama'=>'Vita Fella'];
//
//		$pelatihan = PelatihanBsn::find(17);
//
//		$jobs = new SendEmailKonfirmasiKehadiranBsn($pelatihan);
//		$this->dispatch($jobs);

//		Mail::send('email-template.email-kehadiran-bsn',['pelatihan'=>$pelatihan], function ($m){
//			$m->from('noreply@ticmi.co.id', 'TICMI');
//			$m->to('akmal.squal@gmail.com', 'Aakmal')->subject('Konfirmasi Kehadiran Pelatihan SNI ISO 31000 Manajemen Risiko');
//		});


//		Mail::send('email-template.email-fidelitas',[], function ($m){
//			$m->from('noreply@ticmi.co.id', 'TICMI');
//			$m->to('akmal.squal@gmail.com', 'Akmal');
//			$m->to('irwansyahdanway@gmail.com', 'Irwansyah')->subject('Corporate Finance qualification preparatory class');
//		});

//		$jobs = (new SendPendaftaranPelatihanBsn($pelatihan))->delay(3);
//		$this->dispatch($jobs);
//		$this->dispatch(new SendPendaftaranPelatihanBsn($pelatihan));

//		$jobs = new SendEmailVerificationRegister();
//		$this->dispatch($jobs);
//
//		$keranjang = Keranjang::find(5);


//		$cmpdp = CMPDP::find(1);
//		$jobs = (new SendEmailPendaftaranCMPDP($cmpdp))->delay(3)->onQueue('cmpdp');
//		$this->dispatch($jobs);

//		$member = User::find(41);
//		$pass   = bcrypt('123456');
//		echo $pass;
//		$jobs = (new SendEmailVerificationRegister($user))->delay(3);
//		$this->dispatch($jobs);
		

		
//		$admisi = Admisi::select('email','nm_peserta')->where('is_career',0)->where('nip','!=',0)->groupBy('email')->get();
//		$ujian = DaftarUjian::find(4814);
		
		Mail::send('email-template.email-approval-data-alumni',[], function ($m){
			$m->from('noreply@ticmi.co.id', 'TICMI');
			$m->to('cdc@ticmi.co.id','TICMI Career Development Center');
			$m->subject('Konfirmasi Career Development Center Alumnus TICMI');
            $admisi = Admisi::select('email','nm_peserta')->where('karir','!=','y')->where('nip','!=',0)->groupBy('email')->skip(3000)->take(1000)->get();
            foreach ($admisi as $item) {
                if (filter_var($item->email,FILTER_VALIDATE_EMAIL)) {
                    $m->bcc($item->email);
                }
            }
//			$m->bcc('mety.yusantiati@ticmi.co.id','Mety Yusantiati');
//			$m->bcc('octaviantika.kumala@ticmi.co.id','Octaviantika Benazir Kumala');
		});
		
		
//		$pesertaWorkshop = PesertaWorkshop::find(234);
//		$referral = null;
//		if ($pesertaWorkshop->has_referral) {
//			$referral = PesertaWorkshop::where('referral_from',$pesertaWorkshop->id)->get();
//		}
//		return view('email-template.email-approval-data-alumni');

//		return $admisi;
	}
	
	public function tesabsen($email = null)
	{
		$peserta = null;
		if ($email == 'tes@ticmi.co.id' || $email == 'tes2@ticmi.co.id') {
			$tes     = [
				"nama"      => 'Akmal Bashan',
				'email'     => $email,
				'no_hp'     => '089766856655',
				'institusi' => 'PT. Semut Berbaris Rapi',
				'jabatan'   => 'Vice Executif Officer',
				'kelas'     => 'Pasar Modal (Sesi I Seminar 1)',
				'hadir'     => 'yes',
				'absen'     => null
			];
			$peserta = new PelatihanBsn($tes);
		}
		return view('risk.tesabsen', compact('peserta', 'email'));
	}
	
	public function tesabsenstore($email = null)
	{
		flash()->success('Absensi berhasil disimpan');
		return view('risk.kehadiran');
	}
	
	public function sendSertifikat($email = null, $fromlaporan = null)
	{
		$pelatihan = PelatihanBsn::where('email', $email)->where('survey', 1)->whereNotNull(\DB::raw('absen'))->first();
//		return $pelatihan;
		if ($pelatihan && $pelatihan->count() > 0) {
			$jobs = new SendEmailSertifikatBsn($pelatihan);
			$this->dispatch($jobs);
			flash()->success('Sertifikat berhasil dikirim ke ' . $pelatihan->nama . ' (' . $pelatihan->email . ')');
		} else {
			flash()->error('Data tidak ditemukan');
		}
		return redirect()->route('laporanbsn.detail', ['kelas' => $fromlaporan]);
	}
	
	public function sendBlastSertifikat()
	{
		$peserta = PelatihanBsn::where('survey', 1)->whereNotNull(\DB::raw('absen'))->get();
		foreach ($peserta as $item) {
			$jobs = new SendEmailSertifikatBsn($item);
			$this->dispatch($jobs);
		}
	}
	
	public function tespdf()
	{
//		$path = public_path('msky-2012-pr-00-0.pdf[0]');
//		$path = public_path('pdf-bsn/79---bdmn-2015-ar-q0-12.pdf');
		$path = '/data/pdf/emiten-data/2015/ar/366---msky-2015-ar-q0-12.pdf';
//		$pdfthumb = new \Imagick($path);
//
//		$fp = public_path('pdf-bsn/coba.jpg');
//		$pdfthumb->readImage($path);
//		$pdfthumb->setImageResolution(100,150);
//		$pdfthumb->setImageFormat('png');
//        $pdfthumb->resizeImage(350,250,1,0);
//        $pdfthumb->enhanceImage();
//        $pdfthumb->setBackgroundColor(new \ImagickPixel('#ffffff'));
//		$pdfthumb->scaleImage(350,250,true);

//        header("Content-Type: image/jpeg");
//        $thumbnail = $pdfthumb->getImageBlob();
//        echo $thumbnail;
//		$pdfthumb->writeImage($fp);


//		return view('tespage.tespdf');

//		$gueser = new RegexGuesser();
//		if ($gueser->guess($path) > 1.4) {
//			$command = new GhostscriptConverterCommand();
//			$filesystem = new \Symfony\Component\Filesystem\Filesystem();
//
//			$converter = new GhostscriptConverter($command,$filesystem);
//			$converter->convert($path,'1.4');
//		}
		
		$pdf = new \FPDI();
		
		$pageCount = $pdf->setSourceFile($path);
//		$tplIdx = $pdf->importPage(10, '/MediaBox');

//		$pageCount = 10;
		for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
			// import a page
			$templateId = $pdf->importPage($pageNo);
			// get the size of the imported page
			$size = $pdf->getTemplateSize($templateId);
			
			// create a page (landscape or portrait depending on the imported page size)
			if ($size['w'] > $size['h']) {
				$pdf->AddPage('L', [$size['w'], $size['h']]);
			} else {
				$pdf->AddPage('P', [$size['w'], $size['h']]);
			}
			
			// use the imported page
//			$pdf->AddPage();
			$pdf->useTemplate($templateId, 0, 0, null);
//
//			$pdf->SetFont('Helvetica');
//			$pdf->SetXY(5, 5);
//			$pdf->Write(8, 'A complete document imported with FPDI');
		}


//		$pdf->addPage();
//		$pdf->useTemplate($tplIdx, 0, 0, null);
		
		$pdf->Output('namafile');

//		$pelatihan = PelatihanBsn::find(2);
//		$data = ['pelatihan'=>$pelatihan];
//		$pdf = \Barryvdh\DomPDF\Facade::loadView('risk.pdf',$data);
//		$pdf->setPaper('a4', 'landscape');

//		$jobs = new SendEmailSertifikatBsn($pelatihan);

//		$this->dispatch($jobs);


//		Mail::send('email-template.coba-email',[],function ($m) use ($pdf){
//			$m->from('noreply@ticmi.co.id', 'TICMI');
//			$m->to('akmal.squal@gmail.com', 'Akmal Bashan')->subject('Coba Attachment');
//			$m->attachData($pdf->output(), 'Sertifikat Pelatihan SNI ISO 31000.pdf');
//		});

//		return view('email-template.coba-email');
	}
}
