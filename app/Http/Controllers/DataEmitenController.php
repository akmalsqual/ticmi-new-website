<?php

namespace App\Http\Controllers;

use App\Http\Helpers\GeneralHelper;
use App\Model\DownloadLog;
use App\Model\Emiten;
use App\Model\EmitenData;
use App\Model\Peraturan;
use App\Model\Sector;
use FPDI;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use JavaScript;
use Laracasts\Utilities\JavaScript\JavaScriptFacade;
use Xthiago\PDFVersionConverter\Converter\GhostscriptConverter;
use Xthiago\PDFVersionConverter\Converter\GhostscriptConverterCommand;
use Xthiago\PDFVersionConverter\Guesser\RegexGuesser;

class DataEmitenController extends Controller
{
    public function index(Request $request, $doctype, $type)
    {
        $title = $request->get('title');
//		$quartal         = $request->get('quartal');
        $year = $request->get('year');
        $code = $request->get('code');
//		$prospectus_type = $request->get('prospectus_type');
//		$idSector        = $request->get('idSector');

//		$emiten = [''=>''] + Emiten::lists('name','code')->all();
        $emiten = Emiten::select(DB::raw("CONCAT(code,' - ',name) as name,code"))->orderBy('code')->lists('name', 'code')->all();

        if ($doctype && $type) {
            $data = EmitenData::where('docType', $doctype)->where('type', $type)->orderBy('year', 'DESC');

            if ($title) $data->where('title', 'LIKE', "%$title%");
//			if ($quartal) $data->where('quarter',$quartal);
            if ($year) $data->where('year', $year);
//			if ($prospectus_type) $data->where('prospectus_type',$prospectus_type);
//			if ($idSector) $data->where('idSector',$idSector);

            if ($doctype != 'CA' && $type != 'pi') {
                $data->leftJoin('cmeds_emiten', function ($join) use ($code) {
                    $join->on('cmeds_emitendata.idEmiten', '=', 'cmeds_emiten.idEmiten');
//					$join->where('cmeds_emiten.isDeleted','=','0');
                });
                if ($code) $data->where('code', 'LIKE', "%$code%");
            }
//			if ($type == 'pi') {
//				$data->leftJoin('cmeds_sector', function ($join) {
//					$join->on('cmeds_emitendata.idSector','=','cmeds_sector.idSector');
//				});
//			}

//			return $data->toSql();
            $emitenData = $data->paginate(12);
//			return $emitenData;
//			return \DB::getQueryLog();
            $parentsektor = ['' => ''] + Sector::where('idParentSector', 0)->lists('name', 'idSector')->all();
            $jenisData = GeneralHelper::getJenisData($type, $doctype);
            return view('frontend.emitendata.index', compact('emitenData', 'emiten', 'doctype', 'type', 'title', 'quartal', 'year', 'code', 'prospectus_type', 'parentsektor', 'jenisData'));
        } else {
            abort(404);
        }
    }

    public function viewpdf($doctype, $type, EmitenData $emitendata)
    {
        $path = GeneralHelper::getPdfUrl($emitendata->docType, $emitendata->type, $emitendata->year);

        $limitYear = date('Y') - 5;
        $numpages = 0;
        if (auth()->check()) {
            if ($emitendata->year < $limitYear && auth()->user()->roles()->first() == 'member') {
                $numpages = 10;
            }
        } else {
            if ($emitendata->year < $limitYear) {
                $numpages = 10;
            }
        }

        JavaScript::put([
            'base_url' => url('/'),
//            'pdf_url' => route('dataemiten.getpdf', ['doctype' => $doctype, 'type' => $type, 'emitendata' => $emitendata]),
			'pdf_url' => url($path.$emitendata->filename),
//			'pdf_url' => url('ptro-1990-ar-q0-04.pdf'),
            'num_pages' => $numpages,
            'idEmitenData' => $emitendata->idEmitenData
        ]);

        return view('frontend.emitendata.viewpdf', compact('emitendata'));
    }

    public function getpdf($doctype, $type, EmitenData $emitendata)
    {
        $path = GeneralHelper::getPdfUrl($emitendata->docType, $emitendata->type, $emitendata->year);
        $path .= $emitendata->filename;
        class_exists('TCPDF', true);
        $pdf = new FPDI();

        $pageCount = $pdf->setSourceFile($path);

        /**
         * Cek data tahun dan member role
         */
        $limitYear = date('Y') - 5;
        if (auth()->check()) {
            if ($emitendata->year < $limitYear && auth()->user()->roles()->first() == 'member') {
                $pageCount = 10;
            }
        } else {
            if ($emitendata->year < $limitYear) {
                $pageCount = 10;
            }
        }

        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
            // import a page
            $templateId = $pdf->importPage($pageNo);
            // get the size of the imported page
            $size = $pdf->getTemplateSize($templateId);

            // create a page (landscape or portrait depending on the imported page size)
            if ($size['w'] > $size['h']) {
                $pdf->AddPage('L', array($size['w'], $size['h']));
            } else {
                $pdf->AddPage('P', array($size['w'], $size['h']));
            }

            // use the imported page
//			$pdf->AddPage();
            $pdf->useTemplate($templateId, 0, 0, null);
//
//			$pdf->SetFont('Helvetica');
//			$pdf->SetXY(5, 5);
//			$pdf->Write(8, 'A complete document imported with FPDI');
        }

        $pdf->Output($emitendata->filename);
    }

    public function getthumbnail($doctype, $type, EmitenData $emitenData)
    {
        $base_pdf_path = '/data/pdf/';
        $path = GeneralHelper::getPdfUrl($doctype, $type, $emitenData->year);
        $path = $path . $emitenData->filename;
        if (file_exists($path)) {
            echo 'OK';
            $thumb = new \Imagick($path . '[0]');
//			$thumb->setImageResolution(100,150);
//			$thumb->setImageFormat('png');
//			$thumb->resizeImage(350,250,1,0);
//			//		$thumb->enhanceImage();
//			$thumb->setBackgroundColor(new \ImagickPixel('#ffffff'));
//
//			header("Content-Type: image/jpeg");
//			$thumbnail = $thumb->getImageBlob();
//			echo $thumbnail;
        } else {
            echo 'false';
//			header("Content-Type: image/jpeg");
//			$image = file_get_contents(public_path('assets/images/default/logo-ticmi.jpg'));
//			echo $image;
        }
    }

    public function getFile(Request $request, $doctype, $type, $emitendataencrypt)
    {
        $path = GeneralHelper::getPdfUrl($emitendataencrypt->docType, $emitendataencrypt->type, $emitendataencrypt->year);

        $file = explode('.', $emitendataencrypt->filename);
        $extfile = array_pop($file);

        if ($extfile !== 'pdf') {
            $extfile = 'pdf';
        }
        $filename = implode('.', $file) . '.' . $extfile;
        $path .= $filename;
        $idEncrypt = encrypt($emitendataencrypt->idEmitenData);
        $from_email = $request->get('from_email');
        /**
         * Cek Download Log for current date
         */
        $cekLog = DownloadLog::where('user_id', \Auth::user()->id)->where('idEmitenData', $emitendataencrypt->idEmitenData)->where(DB::raw('YEAR(download_date)'), date('Y'))->WHERE(DB::raw('MONTH(download_date)'), date('m'))->first();
        if ($cekLog && $cekLog->count() > 0) {
            $cekLog->increment('counter');
            if ($from_email == 1) {
                $cekLog->increment('from_email');
            } else {
                $cekLog->increment('from_web');
            }
        } else {
            $download_log = new DownloadLog();
            $download_log->user_id = \Auth::user()->id;
            $download_log->idEmitenData = $emitendataencrypt->idEmitenData;
            $download_log->emiten_code = $emitendataencrypt->emiten->code;
            $download_log->file_title = $emitendataencrypt->title;
            $download_log->file_type = $emitendataencrypt->type;
            $download_log->file_doctype = $emitendataencrypt->docType;
            $download_log->year = $emitendataencrypt->year;
            $download_log->quarter = $emitendataencrypt->quarter;
            $download_log->download_date = date('Y-m-d H:i:s');
            $download_log->counter = 1;
            $download_log->save();
        }

        return view('frontend.emitendata.downloadcounter', compact('idEncrypt', 'doctype', 'type', 'path'));
    }

    public function getFileDownload($doctype, $type, $emitendataencrypt)
    {
        $path = GeneralHelper::getPdfUrl($emitendataencrypt->docType, $emitendataencrypt->type, $emitendataencrypt->year);
        $file = explode('.', $emitendataencrypt->filename);
        $extfile = array_pop($file);

        if ($extfile !== 'pdf') {
            $extfile = 'pdf';
        }
        $filename = implode('.', $file) . '.' . $extfile;
        $path .= $filename;

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header('Content-Type: application/pdf');
        header("Content-Length: " . (string)(filesize($path)));
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header("Content-Transfer-Encoding: binary\n");

        readfile($path); // outputs the content of the file

        exit();
    }

    public function pengumuman(Request $request, $doctype, $type)
    {
        $title = $request->get('title');
        $year = $request->get('year');

        $data = Peraturan::where('doctype', $doctype)->orderBy('date', 'DESC');

        if ($title) {
            $data->where('title', 'LIKE', "%$title%");
        }
        if ($year) {
            $data->where('date', 'LIKE', "$year%");
        }
        $pengumuman = $data->paginate(12);
		
        $jenisData = GeneralHelper::getJenisData($type,$doctype);
        return view('frontend.emitendata.pengumuman', compact('pengumuman', 'doctype', 'type', 'year', 'title', 'jenisData'));
    }

    public function viewPengumumanPdf($doctype, $type, Peraturan $pengumuman)
    {
        $path = GeneralHelper::getPdfUrl($doctype, $type, date('Y', strtotime($pengumuman->date)));
        $file = explode('.', $pengumuman->filename);
        $extfile = array_pop($file);

        if ($extfile !== 'pdf') {
            $extfile = 'pdf';
        }
        $filename = implode('.', $file) . '.' . $extfile;
        JavaScript::put([
            'base_url' => url('/'),
            'pdf_url' => 'http://dev.ticmi.co.id' . $path . $filename,
            'num_pages' => 1
        ]);

        return view('frontend.emitendata.viewpdf', compact('pengumuman'));
    }

    public function getPengumumanFile(Request $request, $doctype, $type, Peraturan $pengumumanencrypt)
    {
        $path = GeneralHelper::getPdfUrl($pengumumanencrypt->docType, $type, date('Y', strtotime($pengumumanencrypt->date)));
        $path .= $pengumumanencrypt->filename;
        $from_email = $request->get('from_email');

        /**
         * Cek Download Log for current date
         */
        $cekLog = DownloadLog::where('user_id', \Auth::user()->id)->where('idEmitenData', $pengumumanencrypt->idPeraturan)->where(DB::raw('YEAR(download_date)'), date('Y'))->WHERE(DB::raw('MONTH(download_date)'), date('m'))->first();
        if ($cekLog && $cekLog->count() > 0) {
            $cekLog->increment('counter');
            if ($from_email == 1) {
                $cekLog->increment('from_email');
            } else {
                $cekLog->increment('from_web');
            }
        } else {
            $download_log = new DownloadLog();
            $download_log->user_id = \Auth::user()->id;
            $download_log->idEmitenData = $pengumumanencrypt->idPeraturan;
            $download_log->institusi = $pengumumanencrypt->institusi;
            $download_log->file_title = $pengumumanencrypt->title;
            $download_log->file_type = $pengumumanencrypt->docType;
            $download_log->file_doctype = $pengumumanencrypt->docType;
            $download_log->date_file = $pengumumanencrypt->date;
            $download_log->download_date = date('Y-m-d H:i:s');
            $download_log->counter = 1;
            $download_log->save();
        }

        $idEncrypt = encrypt($pengumumanencrypt->idPeraturan);

        return view('frontend.emitendata.downloadcounter', compact('idEncrypt', 'doctype', 'type', 'path'));
    }

    public function getPengumumanDownload($doctype, $type, Peraturan $pengumumanencrypt)
    {
        $path = GeneralHelper::getPdfUrl($pengumumanencrypt->docType, $type, date('Y', strtotime($pengumumanencrypt->date)));
        $path .= $pengumumanencrypt->filename;

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header('Content-Type: application/pdf');
        header("Content-Length: " . (string)(filesize($path)));
        header('Content-Disposition: attachment; filename="' . basename($pengumumanencrypt->filename) . '"');
        header("Content-Transfer-Encoding: binary\n");

        readfile($path); // outputs the content of the file

        exit();
    }

    public function peraturan(Request $request, $doctype, $type)
    {
        $title = $request->get('title');
        $year = $request->get('year');

        $data = Peraturan::where('doctype', $doctype)->orderBy('date', 'DESC');

        if ($title) {
            $data->where('title', 'LIKE', "%$title%");
        }
        if ($year) {
            $data->where('date', 'LIKE', "$year%");
        }
        $pengumuman = $data->paginate(12);
	
	    $jenisData = GeneralHelper::getJenisData($type,$doctype);
        return view('frontend.emitendata.pengumuman', compact('pengumuman', 'doctype', 'type', 'year', 'title', 'jenisData'));
    }

    public function corporateAction(Request $request, $doctype, $type)
    {
        if ($doctype && $type) {
            $year = $request->get('year');
            $title = $request->get('title');
            $data = EmitenData::where('docType', $doctype)->where('type', $type)->orderBy('idEmitenData', 'DESC');
            $corporateAction = $data->paginate(12);
            $jenisData = GeneralHelper::getJenisData($type, $doctype);
            return view('frontend.emitendata.corporateaction.index', compact('doctype', 'type', 'corporateAction', 'year', 'title', 'jenisData'));
        } else {
            abort(404);
        }
    }
}
