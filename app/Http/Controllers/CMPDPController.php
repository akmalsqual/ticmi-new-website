<?php

namespace App\Http\Controllers;

use App\Http\Helpers\GeneralHelper;
use App\Http\Requests\CMPDPRegistrationRequest;
use App\Jobs\SendEmailPendaftaranCMPDP;
use App\Model\CMPDP;
use App\Model\CMPDPLolos;
use App\Model\Kabupaten;
use App\Model\Kecamatan;
use App\Model\Provinsi;
use Illuminate\Http\Request;

use App\Http\Requests;

class CMPDPController extends Controller
{
	
	/**
	 * Halaman website CMPDP
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function cmpdp()
	{
		$pageTitle = "CMPDP | Capital Market Professional Development Program";
		return view('frontend.cmpdp.cmpdp', compact('pageTitle'));
	}
	
	/**
	 * Halaman pendaftaran CMDPD
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function pendaftaran()
	{
		if (date('Y-m-d H:i:s') > '2017-05-25 23:58:00') {
			return redirect()->route('cmpdp');
		} else {
			$pageTitle = "Pendaftaran Capital Market Professional Development Program";
			$provinsi  = Provinsi::all();
			$provinsi  = array_pluck($provinsi, 'name', 'id');
			$kabupaten = Kabupaten::all();
			$kabupaten = array_pluck($kabupaten, 'name', 'id');
			$kecamatan = Kecamatan::all();
			$kecamatan = array_pluck($kecamatan, 'name', 'id');
			
			$kota_seleksi = [
				'Riau' => 'Riau',
				'Padang' => 'Padang',
				'Lampung' => 'Lampung',
				'Batam' => 'Batam',
				'Pontianak' => 'Pontianak',
				'Banjarmasin' => 'Banjarmasin',
				'Balikpapan' => 'Balikpapan',
				'Bandung' => 'Bandung',
				'Jakarta' => 'Jakarta',
				'Yogyakarta' => 'Yogyakarta',
				'Surabaya' => 'Surabaya',
				'Denpasar' => 'Denpasar',
				'Manado' => 'Manado',
				'Makassar' => 'Makassar',
				'Semarang' => 'Semarang',
				'Banda Aceh' => 'Banda Aceh',
				'Medan' => 'Medan',
				'Jayapura' => 'Jayapura',
				'Palembang' => 'Palembang',
				'Jambi' => 'Jambi',
				'Kendari' => 'Kendari',
				'Pangkalpinang' => 'Pangkalpinang',
				'Palangka Raya' => 'Palangka Raya',
				'Bengkulu' => 'Bengkulu',
				'Manokwari' => 'Manokwari',
				'Ambon' => 'Ambon'
			];
			return view('frontend.cmpdp.pendaftaran', compact('pageTitle', 'provinsi', 'kabupaten', 'kecamatan','kota_seleksi'));
		}
	}

	/**
	 * Halaman pendaftaran CMDPD
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function pendaftaran2()
	{
        $pageTitle = "Pendaftaran Capital Market Professional Development Program";
        $provinsi  = Provinsi::all();
        $provinsi  = array_pluck($provinsi, 'name', 'id');
        $kabupaten = Kabupaten::all();
        $kabupaten = array_pluck($kabupaten, 'name', 'id');
        $kecamatan = Kecamatan::all();
        $kecamatan = array_pluck($kecamatan, 'name', 'id');

        $kota_seleksi = [
            'Riau' => 'Riau',
            'Padang' => 'Padang',
            'Lampung' => 'Lampung',
            'Batam' => 'Batam',
            'Pontianak' => 'Pontianak',
            'Banjarmasin' => 'Banjarmasin',
            'Balikpapan' => 'Balikpapan',
            'Bandung' => 'Bandung',
            'Jakarta' => 'Jakarta',
            'Yogyakarta' => 'Yogyakarta',
            'Surabaya' => 'Surabaya',
            'Denpasar' => 'Denpasar',
            'Manado' => 'Manado',
            'Makassar' => 'Makassar',
            'Semarang' => 'Semarang',
            'Banda Aceh' => 'Banda Aceh',
            'Medan' => 'Medan',
            'Jayapura' => 'Jayapura',
            'Palembang' => 'Palembang',
            'Jambi' => 'Jambi',
            'Kendari' => 'Kendari',
            'Pangkalpinang' => 'Pangkalpinang',
            'Palangka Raya' => 'Palangka Raya',
            'Bengkulu' => 'Bengkulu',
            'Manokwari' => 'Manokwari',
            'Ambon' => 'Ambon'
        ];
        return view('frontend.cmpdp.pendaftaran', compact('pageTitle', 'provinsi', 'kabupaten', 'kecamatan','kota_seleksi'));
	}
	
	/**
	 * Proses simpan data form pendaftaran CMPDP
	 * @param \App\Http\Requests\CMPDPRegistrationRequest $request
	 * @return \App\Http\Controllers\CMPDPController|\Illuminate\Http\RedirectResponse
	 */
	public function pendaftaranStore(CMPDPRegistrationRequest $request)
	{
//		if (date('Y-m-d H:i:s') > '2017-05-25 23:58:00') {
//			return redirect()->route('cmpdp');
//		}
		$cmpdp = new CMPDP($request->all());
		
		$no_daftar        = GeneralHelper::getNoPendaftaranCMPDP();
		$cmpdp->no_daftar = $no_daftar;
		/** Upload pas foto */
		if ($request->hasFile('pas_foto')) {
			if ($request->file('pas_foto')->isValid()) {
				$pas_foto          = $request->file('pas_foto');
				$pas_foto_filename = 'pas-foto-' . $no_daftar . '.' . $pas_foto->getClientOriginalExtension();
				$uploadPath        = public_path('upload/cmpdp2017/pas_foto');
				$pas_foto->move($uploadPath, $pas_foto_filename);
				$cmpdp->pas_foto = $pas_foto_filename;
			}
		}
		
		/** Upload CV */
		if ($request->hasFile('cv')) {
			if ($request->file('cv')->isValid()) {
				$cv          = $request->file('cv');
				$cv_filename = 'cv-' . $no_daftar . '.' . $cv->getClientOriginalExtension();
				$uploadPath  = public_path('upload/cmpdp2017/cv');
				$cv->move($uploadPath, $cv_filename);
				$cmpdp->cv = $cv_filename;
			}
		}
		
		/** Upload Transkrip Nilai */
		if ($request->hasFile('transkrip_nilai')) {
			if ($request->file('transkrip_nilai')->isValid()) {
				$transkrip_nilai          = $request->file('transkrip_nilai');
				$transkrip_nilai_filename = 'transkrip_nilai-' . $no_daftar . '.' . $transkrip_nilai->getClientOriginalExtension();
				$uploadPath  = public_path('upload/cmpdp2017/transkrip_nilai');
				$transkrip_nilai->move($uploadPath, $transkrip_nilai_filename);
				$cmpdp->transkrip_nilai = $transkrip_nilai_filename;
			}
		}
		
		if ($cmpdp->save()) {
			flash()->success('Pendaftaran berhasil');
			$job = (new SendEmailPendaftaranCMPDP($cmpdp))->delay(3)->onQueue('cmpdp');
			$this->dispatchNow($job);
			return redirect()->route('cmpdp.pendaftaran.sukses', ['status' => 'sukses']);
		} else {
			flash()->error('Pendaftaran gagal tersimpan, silahkan ulangi kembali pendaftaran Anda');
			return redirect()->route('cmpdp.pendaftaran')->withInput($request->all());
		}
	}
	
	/**
	 * @param string $status
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function pendaftaranSukses($status = '')
	{
		$pageTitle = "CMPDP | Capital Market Professional Development Program";
		return view('frontend.cmpdp.status', compact('status','pageTitle'));
	}
	
	public function pengumuman(Request $request)
	{
		return redirect()->route('cmpdp');
//		$pageTitle = "CMPDP | Ujian Nasional Capital Market Professional Development Program (CMPDP) 2017";
//		$no_daftar = $request->get('no_daftar');
//		$nama = $request->get('nama_lengkap');
//
//		$data = CMPDPLolos::orderBy('no_daftar');
//		$data->when($no_daftar, function ($query) use ($no_daftar) {
//			return $query->where('no_daftar','LIKE',"%$no_daftar%");
//		})->when($nama, function ($query) use ($nama) {
//			return $query->where('nama_lengkap','LIKE',"%$nama%");
//		});
//
//		$cmpdp = $data->paginate(25);
//		return view('frontend.cmpdp.pengumuman', compact('cmpdp','no_daftar','nama','pageTitle'));
	}
	
	public function lokasiUjian()
	{
		$pageTitle = "CMPDP | Capital Market Professional Development Program";
		return view('frontend.cmpdp.lokasi-ujian',compact('pageTitle'));
	}
}
