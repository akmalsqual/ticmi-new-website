<?php

namespace App\Http\Controllers;

use App\Http\Helpers\CartHelper;
use App\Http\Requests\WorkshopRequest;
use App\Jobs\SendEmailKonfirmasiWorkshop;
use App\Jobs\SendEmailPendaftaranWorkshop;
use App\Model\Batch;
use App\Model\Cart;
use App\Model\Konfirmasi;
use App\Model\PesertaWorkshop;
use App\Model\Workshop;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class WorkshopController extends Controller
{
	/**
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Model\Workshop $workshop
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index(Request $request, Workshop $workshop)
	{
		if ($workshop && $workshop->count() > 0) {
			$pageTitle = $workshop->workshop_name;
			$pelatihan  = $workshop;
			$time       = strtotime("-7 days");
			$otherbatch = Batch::where("mulai", '!=', "y")->where("approve", "y")->where("pasang", "y")->where("tanggal", ">", $time)->get();
			return view('workshop.index', compact('otherbatch', 'pelatihan','pageTitle'));
		} else {
			abort(404);
		}
	}
	
	/**
	 * Halaman Informasi Pendaftaran
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Model\Workshop $workshop
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
	 */
	public function pendaftaran(Request $request, Workshop $workshop)
	{
		if ($workshop && $workshop->count() > 0) {
			$pageTitle = $workshop->workshop_name;
			$pelatihan = $workshop;
//			if ($workshop->is_publish == 1 && $workshop->peserta()->count() < $workshop->minimal_peserta)
			if ($workshop->peserta()->count() < $workshop->minimal_peserta)
				return view('workshop.daftar', compact('pelatihan','pageTitle'));
			else {
				return redirect('/');
			}
		} else {
			abort(404);
		}
	}
	
	/**
	 * Halaman Checkout untuk Pendaftaran Workshop
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Model\Workshop $workshop
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
	 */
	public function checkout(Request $request, Workshop $workshop)
	{
//		session()->forget('referral_promo');
		if ($workshop && $workshop->count() > 0) {
//			if ($workshop->is_publish == 1 && $workshop->peserta()->count() < $workshop->minimal_peserta)
			if ($workshop->peserta()->count() < $workshop->minimal_peserta)
				return view('workshop.checkout', compact('workshop'));
			else {
				return redirect('/');
			}
		} else {
			abort(404);
		}
	}
	
	/**
	 * Storing data pendaftaran workshop
	 *
	 * @param \App\Http\Requests\WorkshopRequest $request
	 * @param \App\Model\Workshop $workshop
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function storePendaftaran(WorkshopRequest $request, Workshop $workshop)
	{
		setlocale(LC_TIME, 'id_ID.UTF-8');
		$pesertaWorkshop                  = new PesertaWorkshop($request->all());
		$pesertaWorkshop->invoice_no      = CartHelper::getWorkshopInvoice();
		$pesertaWorkshop->virtual_account = CartHelper::getVirtualAccount('041');
		$pesertaWorkshop->urutan          = CartHelper::getUrutanPeserta($workshop);
		$pesertaWorkshop->workshop_id     = $workshop->id;
		$pesertaWorkshop->biaya           = $workshop->biaya;
		
		/**
		 * Check if there's Discount or Promo
		 */
		if (!empty($workshop->jml_peserta_diskon) && empty($workshop->jml_group_referral)) {
			if ($pesertaWorkshop->urutan <= $workshop->jml_peserta_diskon) {
				$pesertaWorkshop->diskon      = $workshop->nominal_diskon;
				$pesertaWorkshop->total_bayar = $workshop->biaya - $workshop->nominal_diskon;
			} else {
				$pesertaWorkshop->diskon      = 0;
				$pesertaWorkshop->total_bayar = $workshop->biaya;
			}
		} elseif (empty($workshop->jml_peserta_diskon) && !empty($workshop->jml_group_referral)) {
			if (session()->has('referral_promo') && count(session()->get('referral_promo')) > 0) {
				$pesertaWorkshop->diskon       = $workshop->nominal_diskon;
				$pesertaWorkshop->total_bayar  = $workshop->biaya - $workshop->nominal_diskon;
				$pesertaWorkshop->has_referral = 1;
			} else {
				$pesertaWorkshop->total_bayar  = $workshop->biaya;
				$pesertaWorkshop->diskon        = 0;
			}
		} else {
			$pesertaWorkshop->diskon      = 0;
			$pesertaWorkshop->total_bayar = $workshop->biaya;
		}
		
		
		if ($pesertaWorkshop->save()) {
			
			/**
			 * Saving Referral if there any
			 */
			if (empty($workshop->jml_peserta_diskon) && !empty($workshop->jml_group_referral)) {
				if (session()->has('referral_promo') && count(session()->get('referral_promo')) > 0) {
					foreach (session()->get('referral_promo') as $key => $value) {
						$pesertaWorkshopReferral                  = new PesertaWorkshop();
						$pesertaWorkshopReferral->nama            = $value['nama'];
						$pesertaWorkshopReferral->email           = $value['email'];
						$pesertaWorkshopReferral->no_hp           = $value['no_hp'];
						$pesertaWorkshopReferral->invoice_no      = $pesertaWorkshop->invoice_no;
						$pesertaWorkshopReferral->virtual_account = $pesertaWorkshop->virtual_account;
						$pesertaWorkshopReferral->urutan          = CartHelper::getUrutanPeserta($workshop);
						$pesertaWorkshopReferral->workshop_id     = $workshop->id;
						$pesertaWorkshopReferral->biaya           = $workshop->biaya;
						$pesertaWorkshopReferral->diskon          = $workshop->nominal_diskon;
						$pesertaWorkshopReferral->total_bayar     = $workshop->biaya - $workshop->nominal_diskon;
						$pesertaWorkshopReferral->referral_from   = $pesertaWorkshop->id;
						
						$pesertaWorkshopReferral->save();
					}
				}
			}
			
			/** Send Email Notification */
			$job = (new SendEmailPendaftaranWorkshop($pesertaWorkshop))->delay(10)->onQueue('cmpdp');
			$this->dispatch($job);
			/** Removing Session */
			session()->forget('referral_promo');
			flash()->success('Pendaftaran berhasil');
			return redirect()->route('pelatihan.info', ['workshopslug' => $workshop->slugs, 'pesertaworkshopinvoice' => $pesertaWorkshop->invoice_no]);
		} else {
			flash()->error('Pendaftaran gagal');
			return redirect()->route('pelatihan.checkout', ['workshopslug' => $workshop->slugs]);
		}
	}
	
	/**
	 * Message Users get after Pendaftaran
	 *
	 * @param \App\Model\Workshop $workshop
	 * @param \App\Model\PesertaWorkshop $pesertaWorkshop
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function afterPendaftaran(Workshop $workshop, PesertaWorkshop $pesertaWorkshop)
	{
		$referral = null;
		if ($pesertaWorkshop->has_referral) {
			$referral = PesertaWorkshop::where('referral_from',$pesertaWorkshop->id)->get();
		}
		return view('workshop.message', compact('pesertaWorkshop', 'workshop', 'referral'));
	}
	
	/**
	 * Showing Konfirmasi Page
	 *
	 * @param \App\Model\Workshop $workshop
	 * @param \App\Model\PesertaWorkshop $pesertaWorkshop
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function konfirmasi(Workshop $workshop, PesertaWorkshop $pesertaWorkshop)
	{
		$referral = null;
		if ($pesertaWorkshop->has_referral) {
			$referral = PesertaWorkshop::where('referral_from',$pesertaWorkshop->id)->get();
		}
		return view('workshop.konfirmasi', compact('pesertaWorkshop', 'workshop', 'referral'));
	}
	
	/**
	 * Storing Confirmation data
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Model\Workshop $workshop
	 * @param \App\Model\PesertaWorkshop $pesertaWorkshop
	 * @return $this|\Illuminate\Http\RedirectResponse
	 */
	public function storeKonfirmasi(Request $request, Workshop $workshop, PesertaWorkshop $pesertaWorkshop)
	{
		$this->validate($request, [
			'invoice_no'     => 'required',
			'nominal'        => 'required',
			'no_rekening'    => 'required',
			'nama_rekening'  => 'required',
			'tgl_transfer'   => 'required|date_format:Y-m-d',
			'jam_transfer'   => 'required',
			'bukti_transfer' => 'mimes:jpeg,jpg,png'
		]);
		
		$invoiceNo = $request->get('invoice_no');
		$cek       = Konfirmasi::where('invoice_no', $invoiceNo)->first();
		if ($cek && $cek->count() > 0) {
			flash()->success('Konfirmasi pembayaran sudah diterima');
			return redirect()->route('pelatihan.info', ['workshopslug' => $workshop->slugs, 'pesertaworkshopinvoice' => $pesertaWorkshop->invoice_no]);
		} else {
			
			$konfirmasi                  = new Konfirmasi($request->all());
			$konfirmasi->user_id         = $pesertaWorkshop->id;
			$konfirmasi->jns_konfirmasi  = 3;
			$konfirmasi->keranjang_id    = $workshop->id;
			$konfirmasi->virtual_account = $pesertaWorkshop->virtual_account;
			
			/**
			 * Cek jika ada upload bukti transfer
			 */
			if ($request->hasFile('bukti_transfer')) {
				if ($request->file('bukti_transfer')->isValid()) {
					$image    = $request->file('bukti_transfer');
					$filename = $pesertaWorkshop->invoice_no . '.' . $image->getClientOriginalExtension();
					
					$uploadPath = public_path('upload/bukti_konfirmasi/');
					$request->file('bukti_transfer')->move($uploadPath, $filename);
					/** set bukti transfer */
					$konfirmasi->bukti_transfer = $filename;
				}
			}
			
			if ($konfirmasi->save()) {
				flash()->success('Konfirmasi pembayaran sudah diterima');
				$pesertaWorkshop->update(['is_confirm' => 1, 'confirm_at' => date('Y-m-d H:i:s')]);
				
				/**
				 * Jika Workshop ada promo referral maka referralnya juga di set sudah konfirmasi
				 */
				PesertaWorkshop::where('referral_from',$pesertaWorkshop->id)->update(['is_confirm' => 1, 'confirm_at' => date('Y-m-d H:i:s')]);
				
				$job = (new SendEmailKonfirmasiWorkshop($pesertaWorkshop))->delay(10);
				$this->dispatch($job);
				return redirect()->route('pelatihan.info', ['workshopslug' => $workshop->slugs, 'pesertaworkshopinvoice' => $pesertaWorkshop->invoice_no]);
			} else {
				flash()->error('Konfirmasi gagal disimpan, silahkan isi form kembali');
				return redirect()->route('pelatihan.konfirmasi', ['workshopslug' => $workshop->slugs, 'pesertaworkshopinvoice' => $pesertaWorkshop->invoice])->withInput();
			}
		}
	}
	
	/**
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Model\Workshop $workshop
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function addNewReferral(Request $request, Workshop $workshop)
	{
		/**
		 * Request Validation
		 */
		$this->validate($request, [
			'add_referral_name' => 'required|max:255',
			'add_referral_email' => 'email|max:255',
			'add_referral_no_hp' => 'required|max:64'
		]);
		$add_referral_name = $request->get('add_referral_name');
		$add_referral_email = $request->get('add_referral_email');
		$add_referral_no_hp = $request->get('add_referral_no_hp');
		
		$user_referral = collect(['email'=>$add_referral_email,'nama'=>$add_referral_name,'no_hp'=>$add_referral_no_hp]);
		
		/**
		 * Cek jika user memasukkan email sendiri
		 */
		if ($add_referral_email == auth()->user()->email) {
			flash()->error('Anda tidak bisa memasukkan email yang sama dengan akun Anda');
			return redirect()->route('pelatihan.checkout', ['workshopslug' => $workshop->slugs]);
		}
		
		/**
		 * Cek user apakah sudah terdaftar pada seminar/workshop yang akan diikuti
		 */
		$check = PesertaWorkshop::where('workshop_id', $workshop->id)->where('email', trim($add_referral_email))->first();
		if ($check && $check->count() > 0) {
			/** Send Notification that user already registered on the workshop */
			flash()->error('Maaf Anda tidak dapat mendaftar User dengan Email : ' . $add_referral_email . ' dikarenakan sudah terdaftar di seminar/workshop ' . $workshop->workshop_name . '.');
		} else {
			/**
			 * Checking Session is already exist or not
			 */
			if ($request->session()->has('referral_promo')) {
				$dataReferral = $request->session()->get('referral_promo');
				
				/**
				 * Check if user is already on session
				 */
				if (in_array($add_referral_email, $dataReferral)) {
					flash()->error('Peserta sudah pernah dimasukkan ke tambahan peserta');
				} else {
					$dataReferral[] = $user_referral;
					$request->session()->put('referral_promo', $dataReferral);
				}
			} else {
				$dataReferral[] = $user_referral;
				$request->session()->put('referral_promo', $dataReferral);
				flash()->success('Peserta berhasil ditambahkan');
			}
		}
		
		return redirect()->route('pelatihan.checkout', ['workshopslug' => $workshop->slugs]);
	}
	
	/**
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Model\Workshop $workshop
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function addNewReferralOld(Request $request, Workshop $workshop)
	{
		/**
		 * Request Validation
		 */
		$this->validate($request, [
			'add_referral' => 'required|max:255',
		]);
		$add_referral = $request->get('add_referral');
		
		/**
		 * Check if user exist on Users database
		 */
		$user_referral = User::where('email', trim($add_referral))->where('is_verify', 1)->first();
		if ($user_referral && $user_referral->count() > 0) {
			/**
			 * Check if user already registered on the workshop
			 */
			$check = PesertaWorkshop::where('workshop_id', $workshop->id)->where('email', trim($add_referral))->first();
			if ($check && $check->count() > 0) {
				/** Send Notification that user already registered on the workshop */
				flash()->error('Maaf Anda tidak dapat mendaftar User dengan Email : ' . $add_referral . ' dikarenakan sudah terdaftar di workshop ' . $workshop->workshop_name . '.');
			} else {
				/**
				 * Checking Session is already exist or not
				 */
				if ($request->session()->has('referral_promo')) {
					$dataReferral = $request->session()->get('referral_promo');
					
					/**
					 * Check if user is already on session
					 */
					if (array_key_exists($user_referral->id, $dataReferral)) {
						flash()->error('Peserta sudah pernah dimasukkan ke tambahan peserta');
					} else {
						$dataReferral[$user_referral->id] = $user_referral;
						$request->session()->put('referral_promo', $dataReferral);
					}
				} else {
					$dataReferral[$user_referral->id] = $user_referral;
					$request->session()->put('referral_promo', $dataReferral);
					flash()->success('Peserta berhasil ditambahkan');
				}
			}
		} else {
			/** Send Error Message if Users dont exist or not verified yet */
			flash()->error('Maaf kami tidak menemukan User dengan Email <strong>' . $add_referral . '</strong> pada data Member Kami, pastikan Peserta yang akan Anda daftarkan sudah melakukan <strong>Registrasi</strong> dan melakukan <strong>Verifikasi Email</strong>. ');
		}
		return redirect()->route('pelatihan.checkout', ['workshopslug' => $workshop->slugs]);
	}
	
	public function removeReferral(Request $request, Workshop $workshop, $idreferral)
	{
		if (session()->has('referral_promo')) {
			$request->session()->forget('referral_promo.' . $idreferral);
			flash()->success('Data peserta berhasil dihapus');
		} else {
			flash()->error('Anda belum mempunyai data peserta tambahan');
		}
		return redirect()->route('pelatihan.checkout', ['workshopslug' => $workshop->slugs]);
	}
	
	public function resendEmailRegister(Workshop $workshop, PesertaWorkshop $pesertaWorkshop)
	{
		$job = (new SendEmailPendaftaranWorkshop($pesertaWorkshop))->delay(10);
		$this->dispatch($job);
	}
	
	public function workshopReport(Request $request, Workshop $workshop)
	{
		$workshop->load('peserta');
//		return $workshop;
		return view('workshop.laporan', compact('workshop'));
	}
}