<?php

namespace App\Http\Controllers;

use App\Model\Kabupaten;
use App\Model\Kecamatan;
use Illuminate\Http\Request;

use App\Http\Requests;

class AjaxController extends Controller
{
	public function getKabupatenByProvinsi(Request $request, $pluck = false)
	{
		$id_provinsi = $request->get('id_provinsi');
		$kabupaten   = Kabupaten::where('province_id', $id_provinsi)->get();
		if ($kabupaten && $kabupaten->count() > 0) {
			if ($pluck) {
				$kabupaten = array_pluck($kabupaten, 'name', 'name');
				return $kabupaten;
			} else {
				$select = "<option value=''>Pilih Kabupaten/Kota</option>";
				foreach ($kabupaten as $item) {
					$select .= "<option value='" . $item->id . "'>" . $item->name . "</option>";
				}
				return $select;
			}
		} else {
			return false;
		}
	}
	
	public function getKecamatanByKabupaten(Request $request, $pluck = false)
	{
		$id_kabupaten = $request->get('id_kabupaten');
		$kecamatan    = Kecamatan::where('regency_id', $id_kabupaten)->get();
		if ($kecamatan && $kecamatan->count() > 0) {
			if ($pluck) {
				$kecamatan = array_pluck($kecamatan, 'name', 'name');
				return $kecamatan;
			} else {
				$select = "<option value=''>Pilih Kecamatan</option>";
				foreach ($kecamatan as $item) {
					$select .= "<option value='" . $item->id . "'>" . $item->name . "</option>";
				}
				return $select;
			}
		} else {
			return false;
		}
	}
}
