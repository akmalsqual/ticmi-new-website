<?php

namespace App\Http\Controllers;

use App\Http\Helpers\CartHelper;
use App\Http\Helpers\Pbkdf2Helper;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\RegistrationRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\SuratRisetRequest;
use App\Jobs\SendEmailResetPassword;
use App\Jobs\SendEmailVerificationRegister;
use App\Model\Admisi;
use App\Model\Customer;
use App\Model\Keranjang;
use App\Model\PesertaWorkshop;
use App\Model\Role;
use App\Model\SuratRiset;
use App\Model\UsersWinGamers;
use App\Model\Workshop;
use App\User;
use Crypt;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;

/**
 * Class MemberController
 * @package App\Http\Controllers
 */
class MemberController extends Controller
{
	/**
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function loginProses(Request $request)
	{
		/** Get Requested Data */
		$email    = $request->get('email');
		$password = $request->get('password');
		/** Cek jika email ada di tabel users */
		$cek = User::with('roles')->where('email', $email)->first();
		if ($cek && $cek->count() > 0) {
			/**
			 * Jika email sudah ada ditabel users maka lakukan login attempt
			 */
			if (Auth::attempt(['email' => $email, 'password' => $password, 'is_verify' => 1, 'status' => 1])) {
				return redirect()->intended('/dashboard');
			} else {
				flash()->error('Login gagal, periksa kembali email dan password yang Anda dimasukkan.');
				return redirect('login');
			}
		} else {
			/**
			 * Jika email tidak ada di tabel user maka cek di tabel t_admisi dan di table cmeds_customer
			 */
			$cek_tabel_akademik = Admisi::where('email', $email)->first();
			if ($cek_tabel_akademik && $cek_tabel_akademik->count() > 0) {
				/**
				 * Jika email ada di tabel t_admisi maka lanjutkan validasi passwordnya
				 */
				$validate_password = Pbkdf2Helper::validate_password($password, $cek_tabel_akademik->password);
				if ($validate_password) {
					/**
					 * Jika passwordnya benar maka akan dibuatkan user di table users
					 */
					$moveUser             = new User();
					$moveUser->name       = $cek_tabel_akademik->nm_peserta;
					$moveUser->email      = $cek_tabel_akademik->email;
					$moveUser->password   = bcrypt($password);
					$moveUser->no_hp      = $cek_tabel_akademik->hp;
					$moveUser->tgl_lahir  = $cek_tabel_akademik->tgl_lahir;
					$moveUser->profesi    = $cek_tabel_akademik->pekerjaan;
					$moveUser->is_verify  = 1;
					$moveUser->status     = 1;
					$moveUser->newsletter = 1;
					$moveUser->save();
					
					/**
					 * Assign Role
					 */
					$roleUser = Role::where('name', 'register')->first();
					$moveUser->attachRole($roleUser);
					
					/**
					 * Proses login attempt user setelah disimpan datanya di tabel users
					 */
					if (\Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password'), 'is_verify' => 1, 'status' => 1])) {
						return redirect()->intended('/dashboard');
					}
				} else {
					/**
					 * Jika password tidak valid maka akan dikirim email untuk reset password
					 */
					$moveUser             = new User();
					$moveUser->name       = $cek_tabel_akademik->nm_peserta;
					$moveUser->email      = $cek_tabel_akademik->email;
					$moveUser->password   = bcrypt($password);
					$moveUser->no_hp      = $cek_tabel_akademik->hp;
					$moveUser->tgl_lahir  = $cek_tabel_akademik->tgl_lahir;
					$moveUser->profesi    = $cek_tabel_akademik->pekerjaan;
					$moveUser->is_verify  = 1;
					$moveUser->status     = 1;
					$moveUser->newsletter = 1;
					$moveUser->save();
					
					/**
					 * Assign Role
					 */
					$roleUser = Role::where('name', 'register')->first();
					$moveUser->attachRole($roleUser);
					
					$job = (new SendEmailResetPassword($moveUser))->delay(2);
					$this->dispatch($job);
					
					flash()->success('Untuk menjaga keamanan sistem dan akun Anda, Password Anda Kami telah reset, silahkan cek email Anda untuk cek password baru Anda.');
					return redirect()->route('forgotpassword');
				}
			} else {
				/**
				 * Jika tidak ada di tabel t_admisi maka selanjutnya akan di cek di tabel cmeds_customer
				 */
				$cek_tabel_cmeds = Customer::where('email', $email)->first();
				if ($cek_tabel_cmeds && $cek_tabel_cmeds->count() > 0) {
					/**
					 * Jika ada ditabel cmeds_customer ada maka user akan dipindahkan dan akan direset password
					 */
					$moveUser             = new User();
					$moveUser->name       = $cek_tabel_cmeds->name;
					$moveUser->email      = $cek_tabel_cmeds->email;
					$moveUser->password   = bcrypt($password);
					$moveUser->no_hp      = $cek_tabel_cmeds->phone;
					$moveUser->tgl_lahir  = $cek_tabel_cmeds->dateOfBirth;
					$moveUser->profesi    = $cek_tabel_cmeds->occupation;
					$moveUser->is_verify  = 1;
					$moveUser->status     = 1;
					$moveUser->newsletter = 1;
					$moveUser->save();
					
					/**
					 * Assign Role
					 */
					$roleUser = Role::where('name', 'register')->first();
					$moveUser->attachRole($roleUser);
					
					$job = (new SendEmailResetPassword($moveUser))->delay(2);
					$this->dispatch($job);
					
					flash()->success('Untuk menjaga keamanan sistem dan akun Anda, Password Anda Kami telah reset, silahkan cek email Anda untuk cek password baru Anda.');
					return redirect()->route('forgotpassword');
				} else {
					flash()->error('Maaf kami tidak menemukan email $email di data Kami, silahkan mendaftar untuk dapat login');
					return redirect()->route('login');
				}
				
			}
		}
	}
	
	public function registration()
	{
		$profesi = [
			'Pelajar/Mahasiswa'      => 'Pelajar/Mahasiswa',
			'Investor'               => 'Investor',
			'Peneliti/Dosen'         => 'Peneliti/Dosen',
			'Karyawan Swasta'        => 'Karyawan Swasta',
			'Karyawan Anggota Bursa' => 'Karyawan Anggota Bursa',
			'Lainnya'                => 'Lainnya'
		];
		return view('frontend.member.registration', compact('profesi'));
	}
	
	public function storeRegistration(RegistrationRequest $request)
	{
		$user           = new User($request->all());
		$password       = \Hash::make($request->get('password'));
		$user->password = $password;
		
		/** Tanggal Lahir */
		$tgl_lahir       = $request->get('tahun_lahir') . '-' . $request->get('bulan_lahir') . '-' . $request->get('tanggal_lahir');
		$user->tgl_lahir = $tgl_lahir;
		
		/** Profesi */
		if ($request->get('profesi') == 'Lainnya') {
			$user->profesi = $request->get('profesi_lainnya');
		}
		
		/**
		 * Cek No SID untuk assign Role
		 */
		
		$no_sid             = $request->get('no_sid');
		$verify_tgl_sid     = date('dm', strtotime($tgl_lahir));
		$tgl_lahir_from_sid = substr($no_sid, 3, 4);
		if ($tgl_lahir_from_sid == $verify_tgl_sid) {
			$role               = 'member';
			$user->is_valid_sid = 1;
		} else {
			$role = 'register';
		}
		
		if ($request->hasFile('sid_card')) {
			if ($request->file('sid_card')->isValid()) {
				/** @var  $image  Get The Image */
				$image    = $request->file('sid_card');
				$filename = $request->get('no_sid') . '-' . time() . '.' . $image->getClientOriginalExtension();
				
				$uploadPath = public_path('upload/kartu_akses/' . $filename);
//				Image::make($image->getRealPath())->resize(300,null, function ($constraint){
//					$constraint->aspectRatio();
//				})->save($uploadPath);
				$user->sid_card = $filename;
				$uploadPath     = public_path('upload/kartu_akses/');
				$request->file('sid_card')->move($uploadPath, $filename);
				if ($user->save()) {
//					flash()->success('Pendaftaran Berhasil, silahkan cek email Anda untuk verifikasi email');
					
					/**
					 * Assign Role
					 */
					$roleUser = Role::where('name', $role)->first();
					$user->attachRole($roleUser);
					/**
					 * Kirim Email Verifikasi
					 */
					$jobs = (new SendEmailVerificationRegister($user))->delay(2);
					$this->dispatch($jobs);
					
				} else {
					flash()->error('Pendaftaran gagal, silahkan ulangi kembali pendaftaran Anda');
					return redirect()->route('register');
				}
			}
		} else {
			if ($user->save()) {
//				flash()->success('Pendaftaran Berhasil, silahkan cek email Anda untuk verifikasi email');
				
				/**
				 * Assign Role
				 */
				$roleUser = Role::where('name', $role)->first();
				$user->attachRole($roleUser);
				/**
				 * Kirim Email Verifikasi
				 */
				$jobs = (new SendEmailVerificationRegister($user))->delay(2);
				$this->dispatch($jobs);
			} else {
				flash()->error('Pendaftaran gagal, silahkan ulangi kembali pendaftaran Anda');
				return redirect()->route('register');
			}
		}
		
		return redirect()->route('registration.success', ['email' => $user->email]);
	}
	
	public function successRegistration($email)
	{
		$user = User::where('email', $email)->first();
		return view('frontend.member.registration-success', compact('user'));
	}
	
	public function verifyRegistration($useremail, $userid)
	{
		$email = \Crypt::decrypt($useremail);
		$user  = User::where('email', $email)->where('id', $userid->id)->first();
//		return $email."-".$user;
		if ($user && count($user) > 0) {
			if ($user->is_verify == 0) {
				$update = [
					'is_verify'  => 1,
					'status'     => 1,
					'updated_at' => date('Y-m-d')
				];
				if ($user->update($update)) {
					flash()->success('Verifikasi email berhasil');
				} else {
					flash()->error('Verifikasi email gagal');
				}
			} else {
				flash()->error('Anda sudah melakukan verifikasi sebelumnya');
			}
		} else {
			flash()->error('Maaf akun dengan email ' . $email . ' tidak ada di data kami. Silahkan lakukan pendaftaran.');
		}
		return view('frontend.member.verify', compact('user'));
	}
	
	public function resendVerificationEmail($email)
	{
		$user = User::where('email', $email)->first();
		if ($user && count($user) > 0) {
			$jobs = (new SendEmailVerificationRegister($user))->delay(2);
			$this->dispatch($jobs);
			flash()->success('Email verifikasi telah dikirim, silahkan cek kembali email Anda');
		} else {
			flash()->error('Maaf User dengan email ' . $email . ' tidak ditemukan.');
		}
		return redirect()->route('registration.success', ['email' => $email]);
	}
	
	public function forgotPassword()
	{
		$pageTitle = 'Reset Password Anda';
		return view('frontend.member.forgot-password', compact('pageTitle'));
	}
	
	public function checkPassword(Request $request)
	{
		$password = $request->get('password');
		$response = ['status' => false, 'message' => ''];
		if ($password) {
			if (\Hash::check($password, auth()->user()->password)) {
				$data = [
					'key'      => 'fg385m2j!rfds%3*feg6',
					'name'      => auth()->user()->name,
					'email'     => auth()->user()->email,
					'hp'        => auth()->user()->no_hp,
					'loginid'   => auth()->user()->email,
					'password'  => $password,
					'password2' => $password,
					'referrer'  => 'ticmi',
				];
				
				$client = new Client();
				$res    = $client->request('POST', 'https://www.winnetnews.com/registrasi_wingamers/winsimulation', [
					'form_params' => $data
				]);
				if ($res->getStatusCode() == 200) {
					/**
					 * Jika proses POST berhasil
					 */
					$res_val = $res->getBody()->getContents();
					$res_val = json_decode($res_val);
					
					if ($res_val->status) {
						/**
						 * Store data response API wingamers ke table users_wingamers
						 */
						$dataUserWinGamers = [
							'user_id'      => auth()->user()->id,
							'loginid'      => $res_val->LOGINID,
							'cif'          => $res_val->CIF,
							'url_conf'     => $res_val->URL_CONF,
							'accountcode'  => $res_val->ACCOUNTCODE,
							'date_expired' => $res_val->DATE_EXPIRED
						];
						
						$userWinGamers = new UsersWinGamers($dataUserWinGamers);
						if ($userWinGamers->save()) {
							$response['status'] = true;
							$response           = array_merge($response, $dataUserWinGamers);
						}
					} else {
						/**
						 * Kondisi jika status return dari API wingamers false
						 */
						if ($res_val->message == 'Login ID Sudah ada, gunakan Login ID Lain') {
							/**
							 * Jika user sudah ada di database win gamers
							 * Step 1 : Tarik user info dan masukkan ke table users_wingamers
							 */
							$resInfo = $client->request('POST', 'https://www.winnetnews.com/registrasi_wingamers/winsimulation', [
								'form_params' => [
									'action'  => 'get_user_info',
									'loginid' => auth()->user()->email,
								]
							]);
							if ($resInfo->getStatusCode() == 200) {
								$resInfo_val = $resInfo->getBody()->getContents();
								$resInfo_val = json_decode($resInfo_val);
								
								if ($resInfo_val->status) {
									/**
									 * Jika status response true ,
									 * Step 2 : Store data ke table users_wingamers
									 */
									$dataUserWinGamers = [
										'user_id'      => auth()->user()->id,
										'loginid'      => $resInfo_val->LOGINID,
										'cif'          => $resInfo_val->CIF,
										'url_conf'     => $resInfo_val->URL_CONF,
										'accountcode'  => $resInfo_val->ACCOUNTCODE,
										'date_expired' => $resInfo_val->DATE_EXPIRED
									];
									
									$userWinGamers = new UsersWinGamers($dataUserWinGamers);
									if ($userWinGamers->save()) {
										/**
										 * Ubah password yang di data wingamers agar sesuai dengan password di ticmi
										 */
										
										$resChgPass = $client->request('POST', 'https://www.winnetnews.com/registrasi_wingamers/winsimulation', [
											'form_params' => [
												'key'           => 'fg385m2j!rfds%3*feg6',
												'action'        => 'change_password',
												'loginid'       => $resInfo_val->LOGINID,
												'old_password'  => $resInfo_val->PASSWORD,
												'new_password'  => $password,
												'new_password2' => $password
											]
										]);
										if ($resChgPass->getStatusCode() == 200) {
											/**
											 * Jika Password berhasil di update
											 */
											$resChgPass_val = $resChgPass->getBody()->getContents();
											$resChgPass_val = json_decode($resChgPass_val);
										}
										$response['status'] = true;
										$response           = array_merge($response, $dataUserWinGamers);
									}
								} else {
									/**
									 * Jika status response false
									 */
									$response['message'] = $resInfo_val->message;
								}
							} else {
								/**
								 * Response jika url API wingamers tidak bisa diakses
								 */
								$response['message'] = 'Online Trading Simulation sedang tidak bisa diakses, silahkan coba beberapa menit lagi';
							}
						} else {
							$response = $res_val;
						}
					}
				} else {
					/**
					 * Response jika url API wingamers tidak bisa diakses
					 */
					$response['message'] = 'Online Trading Simulation sedang tidak bisa diakses, silahkan coba beberapa menit lagi';
				}
			} else {
				/**
				 * Response jika password yang dimasukkan user salah
				 */
				$response['message'] = 'Password salah';
			}
		} else {
			/**
			 * Response jika user tidak memasukkan password
			 */
			$response['message'] = 'Anda tidak memasukkan password';
		}
		echo json_encode($response);
	}
	
	public function changePassword()
	{
		return view('frontend.member.ubah-password');
	}
	
	public function updatePassword(ChangePasswordRequest $request)
	{
		$old_password = $request->get('old_password');
		$new_password = $request->get('new_password');
		if (\Hash::check($old_password, auth()->user()->password)) {
			$update = ['password' => bcrypt($new_password)];
			if (Auth::user()->update($update)) {
				/**
				 * cek jika punya user OLTS di table users_wingamers
				 */
				$client = new Client();
				if (auth()->user()->user_wingamers) {
					/**
					 * Jika user sudah ada di database win gamers
					 * Step 1 : Tarik user info dan masukkan ke table users_wingamers
					 */
					$resInfo = $client->request('POST', 'https://www.winnetnews.com/registrasi_wingamers/winsimulation', [
						'form_params' => [
							'action'  => 'get_user_info',
							'loginid' => auth()->user()->email,
						]
					]);
					if ($resInfo->getStatusCode() == 200) {
						$resInfo_val = $resInfo->getBody()->getContents();
						$resInfo_val = json_decode($resInfo_val);
						if ($resInfo_val->status) {
							/**
							 * Jika status response true ,
							 * Step 2 : Update data ke table users_wingamers dan ke wingamers API
							 */
							
							/**
							 * Ubah password yang di data wingamers agar sesuai dengan password di ticmi
							 */
							
							$resChgPass = $client->request('POST', 'https://www.winnetnews.com/registrasi_wingamers/winsimulation', [
								'form_params' => [
									'key'           => 'fg385m2j!rfds%3*feg6',
									'action'        => 'change_password',
									'loginid'       => $resInfo_val->LOGINID,
									'old_password'  => $resInfo_val->PASSWORD,
									'new_password'  => $new_password,
									'new_password2' => $new_password
								]
							]);
							if ($resChgPass->getStatusCode() == 200) {
								/**
								 * Jika Password berhasil di update
								 */
								$resChgPass_val = $resChgPass->getBody()->getContents();
								$resChgPass_val = json_decode($resChgPass_val);
							}
						}
					}
				}
				flash()->success('Password berhasil diubah, silahkan logout dan login kembali.');
			} else {
				flash()->error('Password gagal diubah, silahkan coba kembali');
			}
		} else {
			flash()->error('Password lama yang Anda masukkan tidak sesuai dengan password saat ini');
		}
		return redirect()->route('member.profile.password');
	}
	
	public function testapi()
	{
//		var_dump('http:\/\/www.winnetnews.com\/winsimulation\/confirmation?cif=AKM00001&accountcode=AKM170324000001&loginid=AKMAL.BASHAN@ICAMEL.CO.ID');
		$client = new Client();
		
		$res = $client->request('POST', 'https://www.winnetnews.com/registrasi_wingamers/winsimulation', [
			'form_params' => [
				'action'  => 'get_user_info',
				'loginid' => auth()->user()->email,
			]
		]);
		
		$a    = $res->getBody()->getContents();
		$data = json_decode($a);
		
		return $a;
	}
	
	public function testpost(Request $request)
	{
//		return \GuzzleHttp\json_encode(['as','ag']);
		\Log::useDailyFiles(storage_path('logs/info/info.log'));
		\Log::info('Coba info', ['user' => auth()->user()]);
	}
	
	public function prosesForgotPassword(Request $request)
	{
		$this->validate($request, [
			'email' => 'required|email'
		]);
		$email = $request->get('email');
		
		$member = User::where('email', $email)->first();
		if ($member && count($member) > 0) {
			$job = (new SendEmailResetPassword($member))->delay(2);
			$this->dispatch($job);
			
			flash()->success('Password Anda berhasil direset, silahkan cek email Anda untuk cek password baru Anda.');
		} else {
			flash()->error('Maaf Kami tidak menemukan akun dengan email <strong>' . $email . '</strong>');
		}
		return redirect()->route('resetpassword', ['useremail' => Crypt::encrypt($member->email), 'user' => $member->id]);
	}
	
	public function resetPassword($useremail, $userid)
	{
		return view('frontend.member.reset-password', compact('useremail','userid'));
	}
	
	public function resetPasswordProses(ResetPasswordRequest $request, $useremail, $userid)
	{
		$token    = $request->get('token');
		$password = $request->get('password');
		
		$email = Crypt::decrypt($useremail);
		$user = User::where('email',$email)->where('id',$userid->id)->first();
		if ($user && $user->count() > 0) {
			if (\Hash::check(trim($token), $user->password)) {
				$new_password = bcrypt($password);
				if ($user->update(['password'=>$new_password])) {
					flash()->success('Password berhasil di reset, silahkan login');
					return redirect('login');
				} else {
					flash()->error('Maaf perubahan password gagal silahkan isi form reset password kembali');
				}
			} else {
				flash()->error('Maaf token yang Anda masukkan tidak sesuai, silahkan cek kembali token pada email Anda');
			}
		} else {
			flash()->error('Maaf Kami tidak menemukan akun dengan email <strong>' . $email . '</strong>');
		}
		return redirect()->route('resetpassword',['useremail'=>$useremail,'userid'=>$userid->id]);
	}
	
	public function deleteMember($email)
	{
		$user = User::where('email', $email)->first();
		if ($user) {
			$user->detachRoles($user->roles);
			$user->delete();
			echo "OK";
		} else {
			echo "User tidak ditemukan";
		}
	}
	
	public function dashboard()
	{
		$user = Auth::user();
		if (empty($user->no_hp) || empty($user->tgl_lahir) || empty($user->profesi)) {
			flash()->info('Data profil Anda masih belum lengkap, mohon untuk melengkapinya.');
			return redirect()->route('member.profile');
		} else {
			$keranjang = Keranjang::where('is_cancel', 0)->where('is_payment_approve', 0)->where('user_id', Auth::user()->id)->get();
			return view('frontend.member.dashboard', compact('keranjang'));
		}
	}
	
	public function profile()
	{
		$dashboardTitle = "Profile";
		$profesi        = [
			'Pelajar/Mahasiswa'      => 'Pelajar/Mahasiswa',
			'Investor'               => 'Investor',
			'Peneliti/Dosen'         => 'Peneliti/Dosen',
			'Karyawan Swasta'        => 'Karyawan Swasta',
			'Karyawan Anggota Bursa' => 'Karyawan Anggota Bursa',
			'Lainnya'                => 'Lainnya'
		];
		return view('frontend.member.profile', compact('dashboardTitle', 'profesi'));
	}
	
	public function storeProfile(ProfileRequest $request)
	{
		$user            = User::find(Auth::user()->id);
		$name            = $request->get('name');
		$no_hp           = $request->get('no_hp');
		$tgl_lahir       = $request->get('tgl_lahir');
		$no_sid          = $request->get('no_sid');
		$profesi         = $request->get('profesi');
		$profesi_lainnya = $request->get('profesi_lainnya');
		
		/**
		 * Cek No SID untuk assign Role jika user masih role register
		 */
		if ($user->is_valid_sid == 0) {
			
			$verify_tgl_sid     = date('dm', strtotime($tgl_lahir));
			$tgl_lahir_from_sid = substr($no_sid, 3, 4);
			if ($tgl_lahir_from_sid == $verify_tgl_sid) {
				/** Detaching register Role */
				$roleRegister = Role::where('name', 'register')->first();
				$user->detachRole($roleRegister);
				/** Attaching member Role */
				$roleMember = Role::where('name', 'member')->first();
				$user->attachRole($roleMember);
				$user->is_valid_sid = 1;
				$user->no_sid       = $no_sid;
			} else {
				flash()->warning('SID yang Anda masukkan tidak valid, silahkan masukkan SID yang valid sesuai dengan yang ada di kartu Akses KSEI Anda');
			}
			
			if ($request->hasFile('sid_card')) {
				if ($request->file('sid_card')->isValid()) {
					/** @var  $image  Get The Image */
					$image    = $request->file('sid_card');
					$filename = $request->get('no_sid') . '-' . time() . '.' . $image->getClientOriginalExtension();

//					$uploadPath = public_path('upload/kartu_akses/' . $filename);
//					Image::make($image->getRealPath())->resize(300,null, function ($constraint){
//						$constraint->aspectRatio();
//					})->save($uploadPath);
					
					$user->sid_card = $filename;
					$uploadPath     = public_path('upload/kartu_akses/');
					$request->file('sid_card')->move($uploadPath, $filename);
				}
			}
		}
		
		$user->name      = $name;
		$user->no_hp     = $no_hp;
		$user->tgl_lahir = $tgl_lahir;
		if ($profesi == 'Lainnya') {
			$user->profesi = $profesi_lainnya;
		} else {
			$user->profesi = $profesi;
		}
		
		if ($user->save()) {
			flash()->success('Data Profile berhasil diubah');
		} else {
			flash()->error('Data Profile gagal diubah');
		}
		return redirect()->route('member.profile');
	}
	
	public function riwataPembelianData()
	{
		$keranjang = Keranjang::where('user_id', Auth::user()->id)->orderBy('created_at', 'DESC')->get();
		return view('frontend.member.riwayat-pembelian-data', compact('keranjang'));
	}
	
	public function daftarKonfirmasi()
	{
		$keranjang = Keranjang::where('user_id', Auth::user()->id)->orderBy('created_at', 'DESC')->where('is_confirm', 0)->where('is_cancel', 0)->get();
		$seminars = PesertaWorkshop::where('email', Auth::user()->email)->where('is_confirm','0')->where('is_payment_approved','0')->get();
		return view('frontend.member.daftar-konfirmasi', compact('keranjang','seminars'));
	}
	
	public function suratRiset()
	{
//		$user = auth()->user()->keranjang()->where('is_payment_approve',1)->get();
//		return $user;
		$sumAmountPembelian = Keranjang::where('user_id', auth()->user()->id)->where('is_payment_approve', 1)->sum('grandtotal');
		$suratRiset         = SuratRiset::where('idCustomer', Auth::user()->id)->get();
		return view('frontend.suratriset.surat-riset', compact('suratRiset', 'sumAmountPembelian'));
	}
	
	public function createSuratRiset()
	{
		$jnsTugasAkhir = [
			'Tugas Akhir' => 'Tugas Akhir',
			'Skripsi'     => 'Skripsi',
			'Thesis'      => 'Thesis',
			'Disertasi'   => 'Disertasi'
		];
		
		return view('frontend.suratriset.surat-riset-create', compact('jnsTugasAkhir'));
	}
	
	public function storeSuratRiset(SuratRisetRequest $request)
	{
		$suratRiset = new SuratRiset($request->all());
		$user       = Auth::user();
		
		/**
		 * Uploading surat pengantar
		 */
		if ($request->hasFile('surat_pengantar')) {
			if ($request->file('surat_pengantar')->isValid()) {
				/** @var  $image  Get The Image */
				$image                      = $request->file('surat_pengantar');
				$filename                   = 'surat-pengantar' . '-' . $user->id . '-' . date('Y-m-d') . '.' . $image->getClientOriginalExtension();
				$suratRiset->suratPengantar = $filename;
				$uploadPath                 = public_path('upload/surat-pengantar/');
				$request->file('surat_pengantar')->move($uploadPath, $filename);
			}
		}
		
		if ($request->hasFile('dokumenskripsi')) {
			if ($request->file('dokumenskripsi')->isValid()) {
				/** @var  $image  Get The Image */
				$image               = $request->file('dokumenskripsi');
				$filename            = 'skripsi' . '-' . $user->id . '-' . date('Y-m-d') . '.' . $image->getClientOriginalExtension();
				$suratRiset->skripsi = $filename;
				$uploadPath          = public_path('upload/skripsi/');
				$request->file('dokumenskripsi')->move($uploadPath, $filename);
			}
		}
		
		/**
		 * Lengkapi data isian
		 */
		if ($user->roles()->first()->name == 'register') {
			$suratRiset->price           = 100000;
			$suratRiset->virtual_account = CartHelper::getVirtualAccount('031');
		} else {
			$suratRiset->price           = 0;
			$suratRiset->virtual_account = 0;
		}
		
		$suratRiset->invoiceNumber = CartHelper::getSuratRisetInvoice();
		$suratRiset->namaMhs       = $user->name;
		$suratRiset->idCustomer    = $user->id;
		$suratRiset->isApproved    = 1;
		$suratRiset->createdDate   = date('Y-m-d H:i:s');
		$suratRiset->approvedBy    = 1;
		$suratRiset->approvedDate  = date('Y-m-d H:i:s');
		$suratRiset->countEdited   = 0;
		$suratRiset->isDeleted     = false;
		
		if ($suratRiset->save()) {
			flash()->success('Pengajuan Surat Riset berhasil disimpan');
			return redirect()->route('member.suratriset');
		} else {
			flash()->error('Pengajuan Surat Riset gagal disimpan');
			return redirect()->withInput()->route('member.suratriset.create');
		}
	}
	
	public function editSuratRiset(SuratRiset $suratRiset)
	{
		/** Cek jika surat riset sudah disetujui atau tidak */
		if ($suratRiset->approveSuperAdmin == 1) {
			flash()->error('Pengajuan sudah tidak bisa diubah');
			return redirect()->route('member.suratriset');
		}
		$jnsTugasAkhir = [
			'Tugas Akhir' => 'Tugas Akhir',
			'Skripsi'     => 'Skripsi',
			'Thesis'      => 'Thesis',
			'Disertasi'   => 'Disertasi'
		];
		
		return view('frontend.suratriset.surat-riset-edit', compact('jnsTugasAkhir', 'suratRiset'));
	}
	
	public function updateSuratRiset(SuratRisetRequest $request, SuratRiset $suratRiset)
	{
		/** Cek jika surat riset sudah disetujui atau tidak */
		if ($suratRiset->approveSuperAdmin == 1) {
			flash()->error('Pengajuan sudah tidak bisa diubah');
			return redirect()->route('member.suratriset');
		}
		$update = [];
		$user   = Auth::user();
		
		/**
		 * Uploading surat pengantar
		 */
		if ($request->hasFile('surat_pengantar')) {
			if ($request->file('surat_pengantar')->isValid()) {
				/** @var  $image  Get The Image */
				$image                    = $request->file('surat_pengantar');
				$filename                 = 'surat-pengantar' . '-' . $user->id . '-' . date('Y-m-d') . '.' . $image->getClientOriginalExtension();
				$update['suratPengantar'] = $filename;
				$uploadPath               = public_path('upload/surat-pengantar/');
				$request->file('surat_pengantar')->move($uploadPath, $filename);
			}
		}
		
		if ($request->hasFile('dokumenskripsi')) {
			if ($request->file('dokumenskripsi')->isValid()) {
				/** @var  $image  Get The Image */
				$image             = $request->file('dokumenskripsi');
				$filename          = 'skripsi' . '-' . $user->id . '-' . date('Y-m-d') . '.' . $image->getClientOriginalExtension();
				$update['skripsi'] = $filename;
				$uploadPath        = public_path('upload/skripsi/');
				$request->file('dokumenskripsi')->move($uploadPath, $filename);
			}
		}
		
		/**
		 * Lengkapi data isian
		 */
		$update['countEdited'] = $suratRiset->countEdited + 1;
		$update                = array_merge($update, $request->all());
		
		if ($suratRiset->update($update)) {
			flash()->success('Pengajuan surat riset berhasil diubah');
			return redirect()->route('member.suratriset');
		} else {
			flash()->error('Pengajuan surat riset gagal diubah');
			return redirect()->route('member.suratriset.edit', ['suratriset' => $suratRiset]);
		}
	}
	
	public function destroySuratRiset(SuratRiset $suratRiset)
	{
		/** Cek jika surat riset sudah disetujui atau tidak */
		if ($suratRiset->approveSuperAdmin == 1) {
			flash()->error('Pengajuan sudah tidak bisa dihapus');
			return redirect()->route('member.suratriset');
		}
		if ($suratRiset->update(['isDeleted' => 1])) {
			flash()->success('Pengajuan surat riset berhasil dihapus');
		} else {
			flash()->error('Pengajuan surat riset gagal dihapus');
		}
		return redirect()->route('member.suratriset');
	}
	
	public function infoSuratRiset(SuratRiset $suratRiset)
	{
		if ($suratRiset->idCustomer == auth()->user()->id) {
			return view('frontend.suratriset.surat-riset-info', compact('suratRiset'));
		} else {
			abort(404, 'Halaman yang anda cari tidak ditemukan');
		}
	}
	
	public function career()
	{
		return view('frontend.career.approval');
	}
	
	public function cekEmailCareer(Request $request)
	{
		$this->validate($request, [
			'email' => 'email|required'
		]);
		
		$email  = trim($request->get('email'));
		$career = $request->get('career');
//		$career = ($career == 'yes') ? 1 : 2;
		$career = ($career == 'yes') ? "y" : "n";
		$admisi = Admisi::where('email', $email)->where('aktif', 'y')->first();
//		return $admisi;
		if ($admisi && $admisi->count() > 0) {
			if (empty($admisi->karir) || $admisi->karir == 'n') {
				if ($admisi->update(['karir' => $career])) {
					flash()->success('Terima kasih atas konfirmasi Ibu/ Bapak.');
					echo 'true ' . $career;
				} else {
					return response()->json(['email' => ["Maaf perubahan data gagal, silahkan ulangi sekali lagi"]], 422);
				}
			} else {
				return response()->json(['email' => ["Maaf Anda sudah pernah melakukan konfirmasi sebelumnya.Jika Anda ingin melakukan perubahan silahkan kontak cdc@ticmi.co.id"]], 422);
			}
		} else {
			return response()->json(['email' => ["Maaf Kami tidak menemukan peserta dengan email <strong>$email</strong>"]], 422);
		}
	}
	
	public function simulasiSaham()
	{
		$pageTitle      = "Simulasi Perdagangan Saham";
		$dashboardTitle = "Simulasi Saham";
		$userWinGamers  = UsersWinGamers::where('user_id', auth()->user()->id)->first();
		return view('frontend.member.simulasi-saham', compact('pageTitle', 'dashboardTitle', 'userWinGamers'));
	}
	
	public function seminarWorkshop()
	{
		$pageTitle = "Seminar dan Workshop";
		
		$pesertaWorkshop = PesertaWorkshop::where('email',auth()->user()->email)->get();
		$workshops = Workshop::whereHas('peserta', function ($query){
			$query->where('email',auth()->user()->email);
		})->orderBy('tgl_mulai','DESC')->get();
		return view('frontend.member.seminar-workshop', compact('pageTitle', 'workshops'));
	}
	
	public function seminarWorkshopView(Workshop $workshop)
	{
		if ($workshop->peserta->is_payment_approved) {
			$pageTitle = "Seminar dan Workshop";
			return view('frontend.member.seminar-workshop-view', compact('pageTitle','workshop'));
		} else {
			return redirect()->back();
		}
	}
	
	public function virtualAccount()
	{
		$kode_perusahaan = '04437';
		$kode_program    = '050';
		$res             = null;
		$vc              = null;
		for ($i = 0; $i <= 9999; $i++) {
			$value      = str_pad($i, 4, '0', STR_PAD_LEFT);
			$account_no = $kode_perusahaan . $kode_program . $value;
//			echo $i."<br/>";

//			$vc               = new VirtualAccount;
//			$vc->account_type = 'SEKOLAH PASAR MODAL';
//			$vc->account_code = $kode_program;
//			$vc->account_no   = $account_no;
//			$vc->save();

//			if () {
////				$res[$account_no] = "OK";
//			} else {
////				$res[$account_no] = "NOT OK";
//			}
//			$data = [
//				'account_type' => 'WPPE',
//			    'account_code' => '001',
//			    'account_no'    => $account_no
//			];
		}
		echo "Done";
//		return $res;
	}
}
