<?php

namespace App\Http\Controllers;

use App\Model\Admisi;
use App\Model\PesertaWisuda;
use App\Model\Program;
use Illuminate\Http\Request;

use App\Http\Requests;

class WisudaController extends Controller
{
	public function index()
	{
		$pageTitle = 'Wisuda Akbar TICMI';
		$program = Program::whereIn('nm_program',['WPPE','WPPE Pemasaran','WPPE Pemasaran Terbatas','ASPM','WMI'])->get();
		$program = array_pluck($program,'nm_program','nm_program');
		return view('frontend.wisuda.index', compact('program', 'pageTitle'));
    }
	
	public function store(Requests\DaftarWisudaRequest $request)
	{
		$program = Program::where('nm_program',$request->get('program'))->first();
		$cek = Admisi::where('email',$request->get('email'))->where('kd_program',$program->id)->whereRaw("tanggal > UNIX_TIMESTAMP('2017-04-01') AND tanggal < UNIX_TIMESTAMP('2017-10-31 23:59:59')")->first();
		if ($cek && $cek->count() > 0) {
			if ($cek->nip != 0) {
				$pesertaWisuda = new PesertaWisuda($request->all());
				if ($pesertaWisuda->save()) {
					if ($pesertaWisuda->is_confirm == 1) {
						flash()->success('Konfirmasi kehadiran berhasil. Sampai jumpa di acara Wisuda Akbar TICMI.');
					} else {
						flash()->success('Konfirmasi kehadiran berhasil. Anda memilih untuk tidak dapat menghadiri acara Wisuda Akbar TICMI.');
					}
					return redirect()->route('wisuda.message',['status'=>'success']);
				} else {
					flash()->error('Maaf Konfirmasi kehadiran gagal tersimpan, mohon untuk mengulangi konfirmasi sekali lagi');
				}
			} else {
				flash()->error('Maaf Anda belum masuk kedalam daftar lulusan Kami.');
			}
		} else {
			flash()->error('Maaf, Kami tidak menemukan data dengan email '.$request->get('email').' pada data lulusan Kami yang lulus di periode April 2017 s/d Oktober 2017');
		}
		return redirect()->route('wisuda');
    }
	
	public function message($status='')
	{
		return view('frontend.wisuda.message', compact('status'));
    }
}
