<?php

namespace App\Http\Controllers;

use App\Model\Admisi;
use App\Model\Cabang;
use App\Model\DaftarUjian;
use App\Model\Program;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use App\Http\Requests;

class DaftarUjianController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('frontend.ujianulang.index');
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(Request $request)
	{
		$cabang     = $request->get('cabang');
		$kd_program = $request->get('kd_prgoram');
		
		$allCabang = Cabang::where('aktif','y')->get()->pluck('nama','nama');
//		$allCabang = ['Jakarta' => 'Jakarta', 'Surabaya' => 'Surabaya', 'Banda Aceh' => 'Banda Aceh', 'Balikpapan' => 'Balikpapan', 'Pontianak' => 'Pontianak', 'Bandung' => 'Bandung'
//			, 'Pekanbaru' => 'Pekanbaru', 'Jambi' => 'Jambi', 'Makassar' => 'Makassar', 'Semarang' => 'Semarang', 'Padang' => 'Padang'
//		];
		$allProgram = Program::where('aktif', 'y')->orderBy('id')->get()->pluck('nm_program', 'id');
		return view('frontend.ujianulang.create', compact('allCabang', 'cabang', 'kd_program', 'allProgram'));
	}
	
	/**
	 * @param string $cabang
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create_old($cabang = 'Jakarta')
	{
		$kd_program = 1;
		/** Jakarta */
		$jkt11  = DaftarUjian::where('tanggal', 'March 1 2017')->where('program', $kd_program)->where('gelombang', '10:00-12:00')->where('cabang', 'Jakarta')->get();
		$jkt12  = DaftarUjian::where('tanggal', 'March 1 2017')->where('program', $kd_program)->where('gelombang', '14:00-16:00')->where('cabang', 'Jakarta')->get();
		$jkt21  = DaftarUjian::where('tanggal', 'March 2 2017')->where('program', $kd_program)->where('gelombang', '10:00-12:00')->where('cabang', 'Jakarta')->get();
		$jkt22  = DaftarUjian::where('tanggal', 'March 2 2017')->where('program', $kd_program)->where('gelombang', '14:00-16:00')->where('cabang', 'Jakarta')->get();
		$jkt61  = DaftarUjian::where('tanggal', 'March 6 2017')->where('program', $kd_program)->where('gelombang', '10:00-12:00')->where('cabang', 'Jakarta')->get();
		$jkt62  = DaftarUjian::where('tanggal', 'March 6 2017')->where('program', $kd_program)->where('gelombang', '14:00-16:00')->where('cabang', 'Jakarta')->get();
		$jkt71  = DaftarUjian::where('tanggal', 'March 7 2017')->where('program', $kd_program)->where('gelombang', '10:00-12:00')->where('cabang', 'Jakarta')->get();
		$jkt72  = DaftarUjian::where('tanggal', 'March 7 2017')->where('program', $kd_program)->where('gelombang', '14:00-16:00')->where('cabang', 'Jakarta')->get();
		$jkt81  = DaftarUjian::where('tanggal', 'March 8 2017')->where('program', $kd_program)->where('gelombang', '10:00-12:00')->where('cabang', 'Jakarta')->get();
		$jkt82  = DaftarUjian::where('tanggal', 'March 8 2017')->where('program', $kd_program)->where('gelombang', '14:00-16:00')->where('cabang', 'Jakarta')->get();
		$jkt91  = DaftarUjian::where('tanggal', 'March 9 2017')->where('program', $kd_program)->where('gelombang', '10:00-12:00')->where('cabang', 'Jakarta')->get();
		$jkt92  = DaftarUjian::where('tanggal', 'March 9 2017')->where('program', $kd_program)->where('gelombang', '14:00-16:00')->where('cabang', 'Jakarta')->get();
		$jkt131 = DaftarUjian::where('tanggal', 'March 13 2017')->where('program', $kd_program)->where('gelombang', '10:00-12:00')->where('cabang', 'Jakarta')->get();
		$jkt132 = DaftarUjian::where('tanggal', 'March 13 2017')->where('program', $kd_program)->where('gelombang', '14:00-16:00')->where('cabang', 'Jakarta')->get();
		$jkt141 = DaftarUjian::where('tanggal', 'March 14 2017')->where('program', $kd_program)->where('gelombang', '10:00-12:00')->where('cabang', 'Jakarta')->get();
		$jkt142 = DaftarUjian::where('tanggal', 'March 14 2017')->where('program', $kd_program)->where('gelombang', '14:00-16:00')->where('cabang', 'Jakarta')->get();
		$jkt151 = DaftarUjian::where('tanggal', 'March 15 2017')->where('program', $kd_program)->where('gelombang', '10:00-12:00')->where('cabang', 'Jakarta')->get();
		$jkt152 = DaftarUjian::where('tanggal', 'March 15 2017')->where('program', $kd_program)->where('gelombang', '14:00-16:00')->where('cabang', 'Jakarta')->get();
		$jkt161 = DaftarUjian::where('tanggal', 'March 16 2017')->where('program', $kd_program)->where('gelombang', '10:00-12:00')->where('cabang', 'Jakarta')->get();
		$jkt162 = DaftarUjian::where('tanggal', 'March 16 2017')->where('program', $kd_program)->where('gelombang', '14:00-16:00')->where('cabang', 'Jakarta')->get();
		$jkt201 = DaftarUjian::where('tanggal', 'March 20 2017')->where('program', $kd_program)->where('gelombang', '10:00-12:00')->where('cabang', 'Jakarta')->get();
		$jkt202 = DaftarUjian::where('tanggal', 'March 20 2017')->where('program', $kd_program)->where('gelombang', '14:00-16:00')->where('cabang', 'Jakarta')->get();
		$jkt211 = DaftarUjian::where('tanggal', 'March 21 2017')->where('program', $kd_program)->where('gelombang', '10:00-12:00')->where('cabang', 'Jakarta')->get();
		$jkt212 = DaftarUjian::where('tanggal', 'March 21 2017')->where('program', $kd_program)->where('gelombang', '14:00-16:00')->where('cabang', 'Jakarta')->get();
		$jkt221 = DaftarUjian::where('tanggal', 'March 22 2017')->where('program', $kd_program)->where('gelombang', '10:00-12:00')->where('cabang', 'Jakarta')->get();
		$jkt222 = DaftarUjian::where('tanggal', 'March 22 2017')->where('program', $kd_program)->where('gelombang', '14:00-16:00')->where('cabang', 'Jakarta')->get();
		$jkt231 = DaftarUjian::where('tanggal', 'March 23 2017')->where('program', $kd_program)->where('gelombang', '10:00-12:00')->where('cabang', 'Jakarta')->get();
		$jkt232 = DaftarUjian::where('tanggal', 'March 23 2017')->where('program', $kd_program)->where('gelombang', '14:00-16:00')->where('cabang', 'Jakarta')->get();
		$jkt271 = DaftarUjian::where('tanggal', 'March 27 2017')->where('program', $kd_program)->where('gelombang', '10:00-12:00')->where('cabang', 'Jakarta')->get();
		$jkt272 = DaftarUjian::where('tanggal', 'March 27 2017')->where('program', $kd_program)->where('gelombang', '14:00-16:00')->where('cabang', 'Jakarta')->get();
		$jkt281 = DaftarUjian::where('tanggal', 'March 28 2017')->where('program', $kd_program)->where('gelombang', '10:00-12:00')->where('cabang', 'Jakarta')->get();
		$jkt282 = DaftarUjian::where('tanggal', 'March 28 2017')->where('program', $kd_program)->where('gelombang', '14:00-16:00')->where('cabang', 'Jakarta')->get();
		$jkt291 = DaftarUjian::where('tanggal', 'March 29 2017')->where('program', $kd_program)->where('gelombang', '10:00-12:00')->where('cabang', 'Jakarta')->get();
		$jkt292 = DaftarUjian::where('tanggal', 'March 29 2017')->where('program', $kd_program)->where('gelombang', '14:00-16:00')->where('cabang', 'Jakarta')->get();
		$jkt301 = DaftarUjian::where('tanggal', 'March 30 2017')->where('program', $kd_program)->where('gelombang', '10:00-12:00')->where('cabang', 'Jakarta')->get();
		$jkt302 = DaftarUjian::where('tanggal', 'March 30 2017')->where('program', $kd_program)->where('gelombang', '14:00-16:00')->where('cabang', 'Jakarta')->get();
		
		/** Surabaya */
		$sby1Sesi1 = DaftarUjian::where('tanggal', 'March 20 2017')->where('program', $kd_program)->where('gelombang', '09:00-11:00')->where('cabang', 'Surabaya')->get();
		
		/** Aceh */
		$aceh1 = DaftarUjian::where('tanggal', 'March 17 2017')->where('program', 19)->where('gelombang', '09:00-11:00')->where('cabang', 'Banda Aceh')->get();
		$aceh2 = DaftarUjian::where('tanggal', 'March 18 2017')->where('program', 19)->where('gelombang', '15:00-17:00')->where('cabang', 'Banda Aceh')->get();
		$aceh3 = DaftarUjian::where('tanggal', 'March 25 2017')->where('program', 19)->where('gelombang', '09:00-11:00')->where('cabang', 'Banda Aceh')->get();
		
		/** Balikpapan */
		$balikpapan1 = DaftarUjian::where('tanggal', 'March 25 2017')->where('program', 19)->where('gelombang', '15:00-17:00')->where('cabang', 'Balikpapan')->get();
		$balikpapan2 = DaftarUjian::where('tanggal', 'March 26 2017')->where('program', 19)->where('gelombang', '15:00-17:00')->where('cabang', 'Balikpapan')->get();
		
		/** Pontianak */
		$pontianak1 = DaftarUjian::where('tanggal', 'March 17 2017')->where('program', 19)->where('gelombang', '13:00-15:00')->where('cabang', 'Pontianak')->get();
		$pontianak2 = DaftarUjian::where('tanggal', 'March 18 2017')->where('program', 19)->where('gelombang', '13:00-15:00')->where('cabang', 'Pontianak')->get();
		
		/** Bandung */
		$bandung1 = DaftarUjian::where('tanggal', 'March 12 2017')->where('program', 19)->where('gelombang', '14:00-16:00')->where('cabang', 'Bandung')->get();
		$bandung2 = DaftarUjian::where('tanggal', 'March 13 2017')->where('program', 19)->where('gelombang', '14:00-16:00')->where('cabang', 'Bandung')->get();
		$bandung3 = DaftarUjian::where('tanggal', 'March 17 2017')->where('program', 19)->where('gelombang', '10:00-12:00')->where('cabang', 'Bandung')->get();
		$bandung4 = DaftarUjian::where('tanggal', 'March 18 2017')->where('program', 19)->where('gelombang', '10:00-12:00')->where('cabang', 'Bandung')->get();
		$bandung5 = DaftarUjian::where('tanggal', 'March 19 2017')->where('program', 19)->where('gelombang', '14:00-16:00')->where('cabang', 'Bandung')->get();
		
		/** Riau */
		$riau11 = DaftarUjian::where('tanggal', 'March 25 2017')->where('program', 19)->where('gelombang', '09:00-11:00')->where('cabang', 'Pekanbaru')->get();
		$riau12 = DaftarUjian::where('tanggal', 'March 25 2017')->where('program', 19)->where('gelombang', '14:00-16:00')->where('cabang', 'Pekanbaru')->get();
		$riau21 = DaftarUjian::where('tanggal', 'March 26 2017')->where('program', 19)->where('gelombang', '09:00-11:00')->where('cabang', 'Pekanbaru')->get();
		$riau22 = DaftarUjian::where('tanggal', 'March 26 2017')->where('program', 19)->where('gelombang', '14:00-16:00')->where('cabang', 'Pekanbaru')->get();
		$riau31 = DaftarUjian::where('tanggal', 'March 27 2017')->where('program', 19)->where('gelombang', '09:00-11:00')->where('cabang', 'Pekanbaru')->get();
		$riau32 = DaftarUjian::where('tanggal', 'March 27 2017')->where('program', 19)->where('gelombang', '14:00-16:00')->where('cabang', 'Pekanbaru')->get();
		
		/** Jambi */
		$jambi1 = DaftarUjian::where('tanggal', 'March 13 2017')->where('program', 19)->where('gelombang', '14:00-16:00')->where('cabang', 'Jambi')->get();
		$jambi2 = DaftarUjian::where('tanggal', 'March 14 2017')->where('program', 19)->where('gelombang', '09:00-11:00')->where('cabang', 'Jambi')->get();
		
		/** Makassar */
		$makassar11 = DaftarUjian::where('tanggal', 'March 18 2017')->where('program', 19)->where('gelombang', '13:00-15:00')->where('cabang', 'Makassar')->get();
		$makassar12 = DaftarUjian::where('tanggal', 'March 18 2017')->where('program', 19)->where('gelombang', '15:00-17:00')->where('cabang', 'Makassar')->get();
		$makassar21 = DaftarUjian::where('tanggal', 'March 19 2017')->where('program', 19)->where('gelombang', '13:00-15:00')->where('cabang', 'Makassar')->get();
		$makassar22 = DaftarUjian::where('tanggal', 'March 19 2017')->where('program', 19)->where('gelombang', '15:00-17:00')->where('cabang', 'Makassar')->get();
		$makassar31 = DaftarUjian::where('tanggal', 'March 20 2017')->where('program', 19)->where('gelombang', '13:00-15:00')->where('cabang', 'Makassar')->get();
		$makassar32 = DaftarUjian::where('tanggal', 'March 20 2017')->where('program', 19)->where('gelombang', '15:00-17:00')->where('cabang', 'Makassar')->get();
		
		/** Semarang */
		$semarang11 = DaftarUjian::where('tanggal', 'March 19 2017')->where('program', 19)->where('gelombang', '09:00-11:00')->where('cabang', 'Semarang')->get();
		$semarang12 = DaftarUjian::where('tanggal', 'March 19 2017')->where('program', 19)->where('gelombang', '14:00-16:00')->where('cabang', 'Semarang')->get();
		$semarang21 = DaftarUjian::where('tanggal', 'March 20 2017')->where('program', 19)->where('gelombang', '09:00-11:00')->where('cabang', 'Semarang')->get();
		$semarang22 = DaftarUjian::where('tanggal', 'March 20 2017')->where('program', 19)->where('gelombang', '14:00-16:00')->where('cabang', 'Semarang')->get();
		$semarang31 = DaftarUjian::where('tanggal', 'March 26 2017')->where('program', 19)->where('gelombang', '09:00-11:00')->where('cabang', 'Semarang')->get();
		$semarang32 = DaftarUjian::where('tanggal', 'March 26 2017')->where('program', 19)->where('gelombang', '14:00-16:00')->where('cabang', 'Semarang')->get();
		
		/** Padang */
		$padang1 = DaftarUjian::where('tanggal', 'March 12 2017')->where('program', 19)->where('gelombang', '10:00-12:00')->where('cabang', 'Padang')->get();
		$padang2 = DaftarUjian::where('tanggal', 'March 19 2017')->where('program', 19)->where('gelombang', '10:00-12:00')->where('cabang', 'Padang')->get();
		$padang3 = DaftarUjian::where('tanggal', 'March 26 2017')->where('program', 19)->where('gelombang', '10:00-12:00')->where('cabang', 'Padang')->get();

//	    $allCabang = [''=>'','Jakarta'=>'Jakarta','Surabaya'=>'Surabaya','Banda Aceh'=>'Banda Aceh','Balikpapan'=>'Balikpapan','Pontianak'=>'Pontianak','Bandung'=>'Bandung'
//	                  ,'Pekanbaru'=>'Pekanbaru','Jambi'=>'Jambi','Makassar'=>'Makassar','Semarang'=>'Semarang','Padang'=>'Padang'];
		$allCabang = ['' => '', 'Jakarta' => 'Jakarta', 'Surabaya' => 'Surabaya'];
		return view('frontend.ujianulang.create',
			compact('jkt11', 'jkt12', 'jkt21', 'jkt22', 'jkt61', 'jkt62', 'jkt71', 'jkt72', 'jkt81', 'jkt82', 'jkt91', 'jkt92', 'jkt131', 'jkt132', 'jkt141', 'jkt142',
				'jkt151', 'jkt152', 'jkt161', 'jkt162', 'jkt201', 'jkt202', 'jkt211', 'jkt212', 'jkt221', 'jkt222', 'jkt231', 'jkt232', 'jkt271', 'jkt272', 'jkt281', 'jkt282',
				'jkt291', 'jkt292', 'jkt301', 'jkt302', 'cabang', 'allCabang', 'kd_program',
				'sby1Sesi1', 'sby1Sesi2', 'sby2Sesi1', 'sby2Sesi2', 'aceh1', 'aceh2', 'aceh3', 'balikpapan1', 'balikpapan2', 'pontianak1', 'pontianak2',
				'bandung1', 'bandung2', 'bandung3', 'bandung4', 'bandung5', 'riau11', 'riau12', 'riau21', 'riau22', 'riau31', 'riau32', 'jambi1', 'jambi2',
				'makassar11', 'makassar12', 'makassar21', 'makassar22', 'makassar31', 'makassar32', 'semarang11', 'semarang12', 'semarang21', 'semarang22',
				'semarang31', 'semarang32', 'padang1', 'padang2', 'padang3')
		);
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'email'  => 'required|email',
			'sesi'   => 'required',
			'cabang' => 'required'
		]);
		$email      = $request->get('email');
		$sesi       = $request->get('sesi');
		$cabang     = $request->get('cabang');
		$tanggal     = $request->get('tanggal');
		$kd_program = $request->get('kd_program');
		
		$admisi = Admisi::where('email', $email)->where('kd_program', $kd_program)->where('aktif', 'y')->first();
		if ($admisi && count($admisi) > 0) {
			$daftarUjian = new DaftarUjian();
			
			$daftarUjian->tanggal   = date('F d Y',strtotime($tanggal));
			$daftarUjian->gelombang = $sesi;
			$daftarUjian->email     = $email;
			$daftarUjian->nama      = $admisi->nm_peserta;
			$daftarUjian->program   = $kd_program;
			$daftarUjian->aktif     = 'y';
			$daftarUjian->cabang    = trim($cabang);
			
			if ($daftarUjian->save()) {
				$admisi->update(['status' => '']);
				flash()->success('Pendaftaran ujian ulang telah sukses.');
			} else {
				flash()->error('Maaf pendaftaran ujian ulang gagal, silahkan mencoba mendaftar kembali');
			}
		} else {
			flash()->error('Maaf Kami tidak dapat menemukan data peserta dengan email <strong>' . $email . '</strong>')->important();
		}
		return redirect()->route('daftarujianulang');
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 * @internal param int $id
	 */
	public function cekPeserta(Request $request)
	{
		$this->validate($request, [
			'email'      => 'required|email',
			'cabang'     => 'required',
			'kd_program' => 'required'
		]);
		
		$email      = $request->get('email');
		$cabang     = $request->get('cabang');
		$kd_program = $request->get('kd_program');
		$tanggal    = $request->get('tanggal');
		$admisi     = Admisi::where('email', $email)->where('aktif', 'y')->where('cabang', $cabang)->where('kd_program', $kd_program)->first();
		
		if ($admisi && count($admisi) > 0) {
			if (trim($admisi->kd_program) == $kd_program) {
				if (trim($admisi->cabang) == $cabang) {
					$tanggal = date('F d Y', strtotime($tanggal));
					$sesi1   = DaftarUjian::where('tanggal', $tanggal)->where('program', $kd_program)->where('gelombang', '10:00-12:00')->where('cabang', $cabang)->get();
					$sesi2   = DaftarUjian::where('tanggal', $tanggal)->where('program', $kd_program)->where('gelombang', '14:00-16:00')->where('cabang', $cabang)->get();
					
					$result = [
						'status' => true,
					    'sesi_1' => (20 - $sesi1->count()),
					    'sesi_2' => (20 - $sesi2->count())
					];
					echo json_encode($result);
				} else {
					return response()->json(['email' => [" Maaf Pendaftaran ini masih untuk cabang $cabang. Silahkan hubungi Kantor Perwakilan BEI dimasing-masing daerah domisili Anda."]], 422);
				}
			} else {
				return response()->json(['email' => ["Maaf Anda tidak dapat mengikuti ujian ulang ini."], 'admisi' => $admisi], 422);
			}
		} else {
//    		echo "tes";
//    		throw response()->json('tes',422);
			return response()->json(['email' => ["Maaf Kami tidak menemukan peserta dengan email <strong>$email</strong>"]], 422);
		}
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 * @internal param int $id
	 */
	public function cekPeserta_old(Request $request)
	{
		$this->validate($request, [
			'email'      => 'required|email',
			'cabang'     => 'required',
			'kd_program' => 'required'
		]);
		
		$email      = $request->get('email');
		$cabang     = $request->get('cabang');
		$kd_program = $request->get('kd_program');
		$tanggal    = $request->get('tanggal');
		$admisi     = Admisi::where('email', $email)->where('aktif', 'y')->where('cabang', $cabang)->where('kd_program', $kd_program)->first();
		
		if ($admisi && count($admisi) > 0) {
			if ($admisi->nip == 0) {
				if (trim($admisi->kd_program) == $kd_program) {
					if (trim($admisi->cabang) == $cabang) {
						$tanggal = date('F j Y', strtotime($tanggal));
						$sesi1   = DaftarUjian::where('tanggal', $tanggal)->where('program', $kd_program)->where('gelombang', '10:00-12:00')->where('cabang', $cabang)->get();
						$sesi2   = DaftarUjian::where('tanggal', $tanggal)->where('program', $kd_program)->where('gelombang', '14:00-16:00')->where('cabang', $cabang)->get();
						
						$result = [
							'status' => true,
						    'sesi_1' => $sesi1->count(),
						    'sesi_2' => $sesi2->count()
						];
						echo json_encode($result);
					} else {
						return response()->json(['email' => [" Maaf Pendaftaran ini masih untuk cabang $cabang. Silahkan hubungi Kantor Perwakilan BEI dimasing-masing daerah domisili Anda."]], 422);
					}
				} else {
					return response()->json(['email' => ["Maaf Anda tidak dapat mengikuti ujian ulang ini."], 'admisi' => $admisi], 422);
				}
			} else {
				return response()->json(['email' => ["Maaf peserta dengan email <strong>$email</strong> sudah lulus."]], 422);
			}
		} else {
//    		echo "tes";
//    		throw response()->json('tes',422);
			return response()->json(['email' => ["Maaf Kami tidak menemukan peserta dengan email <strong>$email</strong>"]], 422);
		}
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
