<?php

namespace App\Http\Controllers;

use App\Model\Batch;
use App\Model\Workshop;
use Illuminate\Http\Request;


/**
 * Class BatchController
 * @package App\Http\Controllers
 */
class BatchController extends Controller
{
	/**
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function findBatch(Request $request)
	{
		$kd_cabang  = $request->input("cabang");
		$bulan   = $request->input("bulan");
		$program = $request->input("kd_program");

		$time = strtotime("-7 days");
		$batch = Batch::where("mulai",'!=',"y")->where("approve","y")->where("tanggal",">",$time);
		$cabang = "";
		if ($kd_cabang)	$batch->where("cabang", $kd_cabang);
		if ($program) $batch->where("kd_program", $program);
		if ($bulan) $batch->whereRaw("DATE_FORMAT(FROM_UNIXTIME(tanggal),'%m') = '$bulan'");
		$resbatch = $batch->get();
		
		$workshops = Workshop::where('is_publish', 1)->where('tgl_mulai', '>', date('Y-m-d'));
		$workshops->when($program, function ($query) use ($program) {
			return $query->where('t_program_id', $program);
		})->when($bulan, function ($query) use ($bulan) {
			return $query->whereRaw("DATE_FORMAT(FROM_UNIXTIME(tgl_mulai),'%m') = '$bulan'");
		});
		$workshops = $workshops->get();

//		return $resbatch;
		return view('frontend.batch', compact('resbatch','kd_cabang','id_cabang','workshops'));
	}
}
