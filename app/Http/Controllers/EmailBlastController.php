<?php

namespace App\Http\Controllers;

use App\Model\Admisi;
use Illuminate\Http\Request;

use App\Http\Requests;

class EmailBlastController extends Controller
{
	public function emailBlastCDC()
	{
		\Mail::send('email-blast.cdc.cdc-karir',[],function ($m) {
//			$m->to('akmal.bashan@ticmi.co.id','Akmal Bashan');
//			$m->cc('octaviantika.kumala@ticmi.co.id','Octaviantika');
			
			$m->from('cdc@ticmi.co.id','TICMI Career Development Center');
			$m->subject('Ticmi CDC - Info Lowongan Karir Pasar Modal');
			
			$admisi = Admisi::select('email','nm_peserta')->where('karir','y')->where('nip','!=',0)->groupBy('email')->skip(0)->take(500)->get();
			
			foreach ($admisi as $item) {
				if (filter_var($item->email,FILTER_VALIDATE_EMAIL)) {
					$m->bcc($item->email);
				}
			}
		});

		return view('email-blast.cdc.cdc-karir');
    }
}
