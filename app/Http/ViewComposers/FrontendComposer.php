<?php
/**
 * Created by PhpStorm.
 * User: akmalbashan
 * Date: 12-Jul-16
 * Time: 1:26 PM
 */

namespace App\Http\ViewComposers;


use App\Model\Batch;
use Illuminate\View\View;
use Route;

class FrontendComposer
{
	public function compose($view)
	{
		$batchname = '';
//		if (is_object(Route::current()))
//			$batchname = Route::current()->parameter('batchname');
		
		$routename = Route::currentRouteName();
		
		$waktu      = strtotime("-7 days");
		$batch      = Batch::where("mulai", '!=', "y")
						->where("approve", "y")
						->where("pasang", "y");
		$batch->where("tanggal", ">", strtotime("-7 days"))->get();

		if (auth()->check()) {
		    $view->with('roles',auth()->user()->roles()->first());
        }

		$view->with('batchOngoing',$batch)->with('routename',$routename);
	}
}