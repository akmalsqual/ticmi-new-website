<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegistrationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'no_hp' => 'required|max:255',
            'email' => 'required|email|confirmed|unique:users,email',
            'password' => 'required|confirmed',
            'no_sid' => 'required_with:sid_card|unique:users,no_sid',
            'sid_card' => 'required_with:no_sid|mimes:jpg,jpeg,png',
            'profesi' => 'required|max:255',
            'profesi_lainnya' => 'required_if:profesi,Lainnya',
            'tanggal_lahir' => 'required',
            'bulan_lahir' => 'required',
            'tahun_lahir' => 'required'
        ];
    }
}
