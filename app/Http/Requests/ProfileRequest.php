<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$user = \Auth::user();
    	if ($user->is_valid_sid) {
		    return [
			    'name' => 'required|max:255',
			    'no_hp' => 'required|max:255',
			    'no_sid' => 'unique:users,no_sid,'.$user->no_sid.',no_sid',
			    'profesi' => 'required|max:255',
			    'profesi_lainnya' => 'required_if:profesi,Lainnya',
			    'tgl_lahir' => 'required'
		    ];
	    } else {
		    return [
			    'name' => 'required|max:255',
			    'no_hp' => 'required|max:255',
			    'no_sid' => 'unique:users,no_sid,'.$user->no_sid.',no_sid',
			    'sid_card' => 'required_with:no_sid|mimes:jpg,jpeg,png',
			    'profesi' => 'required|max:255',
			    'profesi_lainnya' => 'required_if:profesi,Lainnya',
			    'tgl_lahir' => 'required'
		    ];
	    }
    }
}
