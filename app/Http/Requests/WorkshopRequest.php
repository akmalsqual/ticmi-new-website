<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class WorkshopRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$workshop = $this->route()->getParameter('workshopslug');
    	
    	if ($workshop->program->id == 34) {
		    return [
			    'nama' => 'required|max:255',
			    'email' => 'required|email|unique:ticmi_peserta_workshop,email,NULL,id,workshop_id,'.$workshop->id,
			    //            'email' => 'required|email',
			    'company' => 'required|max:255',
			    'jabatan' => 'required|max:255',
			    'no_hp' => 'required|max:64',
//			    'no_sk_ojk' => 'required|max:64',
//			    'tgl_sk_ojk' => 'required|max:64',
		    ];
	    } else {
		    return [
			    'nama' => 'required|max:255',
			    'email' => 'required|email|unique:ticmi_peserta_workshop,email,NULL,id,workshop_id,'.$workshop->id,
			    //            'email' => 'required|email',
			    'company' => 'required|max:255',
			    'jabatan' => 'required|max:255',
			    'no_hp' => 'required|max:64'
		    ];
	    }
    }
}
