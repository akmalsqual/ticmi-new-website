<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PelatihanBsnRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required|max:255',
            'email' => 'required|email|unique:pelatihan_bsn,email',
            'no_hp' => 'required|max:64',
            'institusi' => 'required|max:255',
            'jabatan' => 'required|max:255',
            'kelas' => "required",
        ];
    }
}
