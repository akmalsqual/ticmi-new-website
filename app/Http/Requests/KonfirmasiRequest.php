<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class KonfirmasiRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'invoice_no' => 'required',
            'virtual_account' => 'required|size:12',
            'nominal' => 'required',
            'no_rekening' => 'required',
            'nama_rekening' => 'required',
            'tgl_transfer' => 'required|date_format:Y-m-d',
            'jam_transfer' => 'required',
            'bukti_transfer' => 'mimes:jpeg,jpg,png'
        ];
    }
}
