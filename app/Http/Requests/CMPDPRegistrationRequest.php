<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CMPDPRegistrationRequest extends Request
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'nama_lengkap'    => 'required|max:255',
			'email'           => 'required|email|max:255|unique:cmpdp_2017,email',
			'no_hp'           => 'required|max:32',
			'tmp_lahir'       => 'required|max:255',
			'tgl_lahir'       => 'required|date_format:Y-m-d',
			'no_ktp'          => 'required|max:32|unique:cmpdp_2017,no_ktp',
			'jns_kelamin'     => 'required',
			'agama'           => 'required',
			'alamat_ktp'      => 'required|max:255',
			'provinsi_ktp'    => 'required',
			'kabupaten_ktp'   => 'required|max:255',
			'kecamatan_ktp'   => 'required|max:255',
			'alamat_skrng'    => 'required|max:255',
			'provinsi_skrng'  => 'required|max:255',
			'kabupaten_skrng' => 'required|max:255',
			'kecamatan_skrng' => 'required|max:255',
			'status_kawin'    => 'required',
			'kebangsaan'      => 'required',
			'pas_foto'        => 'required|mimes:jpg,jpeg,png|max:512',
			'cv'              => 'sometimes|mimes:pdf|max:2048',
			'transkrip_nilai' => 'required|mimes:pdf|max:2048',
			'link_video'      => 'url',
			'periode_s1'      => 'required',
			'jurusan_s1'      => 'required',
			'universitas_s1'  => 'required',
			'ipk_s1'          => 'required',
			'is_lulus_s1'     => 'required',
		    'kota_seleksi'    => 'required'
		];
	}
}
