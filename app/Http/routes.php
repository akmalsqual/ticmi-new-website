<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'FrontendController@welcome');

/**
 * Ajax Route
 */
Route::group(['prefix'=>'ajax'], function (){
	Route::post('getkabupatenbyprovinsi','AjaxController@getKabupatenByProvinsi')->name('ajax.getkabupatenbyprovinsi');
	Route::post('getkecamatanbykabupaten','AjaxController@getKecamatanByKabupaten')->name('ajax.getkecamatanbykabupaten');
});

/**
 * Frontend Route
 */
Route::get('/profil-perusahaan','FrontendController@profilPerusahaan')->name('profil-perusahaan');
Route::get('/visi-misi','FrontendController@visiMisi')->name('visi-misi');
Route::get('/sejarah','FrontendController@sejarah')->name('sejarah');
Route::get('/struktur','FrontendController@struktur')->name('struktur');
Route::get('/wppe','FrontendController@wppeProduct')->name('wppe-product');
Route::get('/wmi','FrontendController@wmiProduct')->name('wmi-product');
Route::get('/aspm','FrontendController@aspmProduct')->name('aspm-product');
Route::get('/wpee','FrontendController@wpeeProduct')->name('wpee-product');
//Route::get('/waiver-product','FrontendController@waiverProduct')->name('waiver-product');
Route::get('/data-historis','FrontendController@dataHistoris')->name('data-historis');
Route::get('/perpustakaan','FrontendController@perpustakaan')->name('perpustakaan');
Route::get('/permintaan-data','FrontendController@permintaanData')->name('permintaan-data');
Route::get('/standar-kelulusan','FrontendController@standarKelulusan')->name('standar-kelulusan');
Route::get('/faq','FrontendController@faq')->name('faq');
Route::get('/karir','FrontendController@karir')->name('karir');
Route::get('/hubungi-kami','FrontendController@hubungiKami')->name('hubungi-kami');
Route::get('cmk','FrontendController@cmk')->name('cmk');
Route::get('rivan-kurniawan','FrontendController@rivan')->name('rivan');
Route::get('ppl','FrontendController@ppl')->name('ppl');
Route::get('login','FrontendController@login')->name('login');

Route::get('karir_old', function () {
	return view('frontend.karir_old');
});

//Route::get('gratis','FrontendController@bimbelUjian')->name('bimbel-ujian');
//Route::post('gratis','FrontendController@bimbelUjianStore')->name('bimbel-ujian.store');
//Route::get('gratis/message','FrontendController@bimbelMessage')->name('bimbel-ujian.message');
//Route::post('gratis/cek-jadwal','FrontendController@cekJadwalBimbel')->name('bimbel-ujian.cekjadwal');
Route::get('gratis/report','FrontendController@reportBimbel')->name('bimbel-ujian.report');

Route::get('/wppe-product', function() {
	return redirect('wppe');
});
Route::get('/wmi-product',function () {
	return redirect('wmi');
});
Route::get('/aspm-product',function () {
	return redirect('aspm');
});

/**
 * Login dan Logout proses
 */
//Route::post('login','Auth\AuthController@postLogin')->name('login');
Route::get('logout','Auth\AuthController@getLogout')->name('logout');

Route::post('login','MemberController@loginProses')->name('login');

Route::get('cekemail','FrontendController@cekemail')->name('cekemail');
Route::get('resendemail/{email}/{fromlaporan?}','FrontendController@resendemail')->name('resendemail');

Route::post("batch/find","BatchController@findBatch");
Route::get('daftar/{batchid}/{batchname}',"FrontendController@pendaftaranDetail")->name('daftar.detail');
//Route::get('pengajar/{id}/{nama}/view','FrontendController@showPengajar')->name('pengajar.view');

/**
 *  Registrasi Member
 */
Route::post('member/login','MemberController@loginProcess')->name('member.login');
Route::get('register','MemberController@registration')->name('registration');
Route::post('register','MemberController@storeRegistration')->name('registration.store');
Route::get('register/{email}/success','MemberController@successRegistration')->name('registration.success');
Route::get('register/{email}/resend-verification','MemberController@resendVerificationEmail')->name('registration.resendverification');
Route::get('verify/{useremail}/{user}','MemberController@verifyRegistration')->name('registration.verify');
Route::get('forgot-password','MemberController@forgotPassword')->name('forgotpassword');
Route::post('forgot-password','MemberController@prosesForgotPassword')->name('forgotpassword.proses');
Route::get('reset-password/{useremail}/{user}','MemberController@resetPassword')->name('resetpassword');
Route::post('reset-password/{useremail}/{user}','MemberController@resetPasswordProses')->name('resetpassword.proses');

Route::get('virtual-account','MemberController@virtualAccount');
Route::get('deletemember/{email}','MemberController@deleteMember');

/**
 * Data Emiten
 */

Route::get('dataemiten/{doctype}/{type}','DataEmitenController@index')->name('dataemiten');
Route::get('dataemiten/{doctype}/{type}/viewpdf/{emitendata}','DataEmitenController@viewpdf')->name('dataemiten.viewpdf');
Route::get('dataemiten/{doctype}/{type}/getpdf/{emitendata}','DataEmitenController@getpdf')->name('dataemiten.getpdf');
Route::get('dataemiten/{doctype}/{type}/getthumbnail/{emitendata}','DataEmitenController@getthumbnail')->name('dataemiten.gethumbnail');
Route::get('dataemiten/{doctype}/{type}/pengumuman','DataEmitenController@pengumuman')->name('dataemiten.pengumuman');
Route::get('dataemiten/{doctype}/{type}/viewpdfpengumuman/{pengumuman}','DataEmitenController@viewPengumumanPdf')->name('dataemiten.pengumuman.viewpdf');
Route::get('dataemiten/{doctype}/{type}/peraturan','DataEmitenController@peraturan')->name('dataemiten.peraturan');
Route::get('dataemiten/{doctype}/{type}/viewpdfperaturan/{peraturan}','DataEmitenController@viewPeraturanPdf')->name('dataemiten.peraturan.viewpdf');

/**
 * Keranjang Belanja
 */

Route::get('cart/view','CartController@viewcart')->name('cart.view');
Route::post('cart/add_item','CartController@addItem')->name('cart.add');
Route::get('cart/remove_item/{id}','CartController@removeItem')->name('cart.remove');
Route::get('cart/remove_all','CartController@removeAllItem')->name('cart.removeall');
Route::post('cart/store', 'CartController@store')->name('cart.store');

/**
 * Ujian Ulang Wppe Pemasaran
 */
//Route::get('daftarujianulang','DaftarUjianController@index')->name('daftarujianulang');
Route::get('daftarujianulang','DaftarUjianController@create')->name('daftarujianulang');
Route::post('store-daftarujianulang','DaftarUjianController@store')->name('daftarujianulang.store');
Route::post('cek-daftarujianulang','DaftarUjianController@cekPeserta')->name('daftarujianulang.cek');

/**
 * Career Development
 */
Route::get('career-development','MemberController@career')->name('cdc');
Route::post('cek-approval-career','MemberController@cekEmailCareer')->name('cdc.cekemail');

/**
 * CMPDP 2017
 */

Route::get('cmpdp/2017',function () {
	return redirect()->route('cmpdp');
});
Route::get('cmpdp','CMPDPController@cmpdp')->name('cmpdp');
Route::get('cmpdp/pendaftaran','CMPDPController@pendaftaran')->name('cmpdp.pendaftaran');
Route::get('cmpdp/pendaftaran2','CMPDPController@pendaftaran2')->name('cmpdp.pendaftaran2');
Route::post('cmpdp/pendaftaran/store','CMPDPController@pendaftaranStore')->name('cmpdp.pendaftaran.store');
Route::get('cmpdp/pendaftaran/info/{status}','CMPDPController@pendaftaranSukses')->name('cmpdp.pendaftaran.sukses');
Route::get('cmpdp/pengumuman','CMPDPController@pengumuman')->name('cmpdp.pengumuman');
Route::get('cmpdp/pengumuman/lokasi-ujian','CMPDPController@lokasiUjian')->name('cmpdp.lokasiujian');

/**
 * Wisuda TICMI
 */
Route::get('wisuda/2017','WisudaController@index')->name('wisuda');
Route::post('wisuda/2017/store','WisudaController@store')->name('wisuda.store');
Route::get('wisuda/2017/message/{status?}','WisudaController@message')->name('wisuda.message');


Route::get('konfirmasi_kehadiran','FrontendController@konfirmasi_kedatangan')->name('konfirmasi.daftar');
Route::post('KonfirmasiAcara','FrontendController@KonfirmasiAcara')->name('konfirmasi.acara');

Route::group(['middleware' => 'auth'], function (){
	Route::get('dashboard','MemberController@dashboard')->name('dashboard');
	Route::get('cart/checkout','CartController@checkout')->name('cart.checkout');
	Route::post('cart/store','CartController@store')->name('cart.store');

	Route::get('transaksi/pembelian/{invoice}/confirmation','CartController@viewKeranjang')->name('transaksi.pembelian');
	Route::get('transaksi/konfirmasi/{keranjang}','CartController@konfirmasi')->name('transaksi.konfirmasi');
	Route::post('transaksi/konfirmasi/{keranjang}/store','CartController@storeKonfirmasi')->name('transaksi.konfirmasi.store');
	Route::get('transaksi/tagihan/{keranjang}/view','CartController@viewTagihanMember')->name('transaksi.tagihan.view');

	Route::get('dataemiten/{doctype}/{type}/getfile/{emitendataencrypt}','DataEmitenController@getFile')->name('dataemiten.getfile');
	Route::get('dataemiten/{doctype}/{type}/getfiledownload/{emitendataencrypt}','DataEmitenController@getFileDownload')->name('dataemiten.downloadfile');
	
	Route::get('dataemiten/{doctype}/{type}/corporate-action','DataEmitenController@corporateAction')->name('dataemiten.corporateaction');
	Route::get('dataemiten/{doctype}/{type}/getpengumumanfile/{pengumumanencrypt}','DataEmitenController@getPengumumanFile')->name('dataemiten.pengumuman.getfile');
	Route::get('dataemiten/{doctype}/{type}/getpengumumandownload/{pengumumanencrypt}','DataEmitenController@getPengumumanDownload')->name('dataemiten.pengumuman.download');
	
	/**
	 * Routing untuk Halaman Member
	 */
	Route::get('member/profile','MemberController@profile')->name('member.profile');
	Route::post('member/profile/store','MemberController@storeProfile')->name('member.profile.store');
	Route::get('member/profile/change-password','MemberController@changePassword')->name('member.profile.password');
	Route::post('member/profile/change-password','MemberController@updatePassword')->name('member.profile.password.update');
	Route::get('member/riwayat-pembelian-data','MemberController@riwataPembelianData')->name('member.pembeliandata');
	Route::get('member/daftar-konfirmasi','MemberController@daftarKonfirmasi')->name('member.konfirmasi');
	Route::get('member/surat-riset','MemberController@suratRiset')->name('member.suratriset');
	Route::get('member/surat-riset/create','MemberController@createSuratRiset')->name('member.suratriset.create');
	Route::post('member/surat-riset/store','MemberController@storeSuratRiset')->name('member.suratriset.store');
	Route::get('member/surat-riset/{suratriset}/edit','MemberController@editSuratRiset')->name('member.suratriset.edit');
	Route::put('member/surat-riset/{suratriset}/update','MemberController@updateSuratRiset')->name('member.suratriset.update');
	Route::get('member/surat-riset/{suratriset}/destroy','MemberController@destroySuratRiset')->name('member.suratriset.destroy');
	Route::get('member/surat-riset/{invoicesuratriset}/info','MemberController@infoSuratRiset')->name('member.suratriset.info');
	Route::get('member/simulasi-saham','MemberController@simulasiSaham')->name('member.simulasisaham');
	Route::post('member/check-password','MemberController@checkPassword')->name('member.checkpassword');
	Route::get('member/seminar','MemberController@seminarWorkshop')->name('member.seminar');
	Route::get('member/seminar/{workshopslug}/view','MemberController@seminarWorkshopView')->name('member.seminar.view');
	
	Route::get('testapi','MemberController@testapi')->name('testapi');
	Route::get('testpost','MemberController@testpost')->name('testpost');
	
	/**
	 * Routing untuk Seminar / Workshop
	 */
	Route::group(['prefix'=>'seminar'], function (){
		Route::get('{workshopslug}/checkout','WorkshopController@checkout')->name('pelatihan.checkout');
		Route::post('{workshopslug}/store','WorkshopController@storePendaftaran')->name('pelatihan.store');
		Route::get('{workshopslug}/info/{pesertaworkshopinvoice}','WorkshopController@afterPendaftaran')->name('pelatihan.info');
		Route::get('{workshopslug}/konfirmasi/{pesertaworkshopinvoice}','WorkshopController@konfirmasi')->name('pelatihan.konfirmasi');
		Route::post('{workshopslug}/konfirmasi/{pesertaworkshop}/store','WorkshopController@storeKonfirmasi')->name('pelatihan.konfirmasi.store');
		Route::get('{workshopslug}/resendemailregister/{pesertaworkshop}','WorkshopController@resendEmailRegister')->name('pelatihan.resendemailregister');
		Route::post('{workshopslug}/absensi/{pesertaworkshop}','WorkshopController@absensi')->name('pelatihan.konfirmasi.absensi');
		
		Route::post('{workshopslug}/checkreferral','WorkshopController@addNewReferral')->name('pelatihan.checkreferral');
		Route::get('{workshopslug}/removereferral/{idreferral}','WorkshopController@removeReferral')->name('pelatihan.removereferral');
	});
	
	/**
	 * Routing untuk email blast
	 */
	Route::get('blast-email-cdc', 'EmailBlastController@emailBlastCDC');
	
});

/**
 * Corporate Secretary
 */

//Route::get('corsec','WorkshopController@index')->name('corsec.index');
//Route::get('corsec/pendaftaran','WorkshopController@pendaftaran')->name('corsec.daftar');
//Route::post('corsec/pendaftaran','WorkshopController@storePendaftaran')->name('corsec.store');
//Route::get('corsec/pendaftaran/{status}','WorkshopController@afterPendaftaran')->name('corsec.message');

/**
 * Routing untuk Pelatihan
 */

Route::group(['prefix'=>'seminar'], function (){
	Route::get('{workshopslug}','WorkshopController@index')->name('pelatihan');
	Route::get('{workshopslug}/laporan','WorkshopController@workshopReport')->name('pelatihan.laporan');
//	Route::get('{workshopslug}/daftar','WorkshopController@pendaftaran')->name('pelatihan.daftar');
});

Route::group(['prefix'=>'ppl'], function (){
	Route::get('{workshopslug}','WorkshopController@index')->name('ppl.index');
//	Route::get('{workshopslug}/daftar','WorkshopController@pendaftaran')->name('pelatihan.daftar');
});
//Route::get('/pelatihanbsn', 'FrontendController@riskManagement')->name('pelatihan.index');
//Route::get('daftar-pelatihan-bsn',function() {
//	return redirect('/');
//});
//Route::get('daftar-pelatihan-bsn','FrontendController@pendaftaranRiskManagement')->name('pelatihan.daftar');
//Route::post('daftar-pelatihan-bsn','FrontendController@storePendaftaranRiskManagement')->name('pelatihan.daftar');
//Route::get('daftar-pelatihan-bsn/{pelatihanbsn}/success', function ($pelatihanbsn){
//	return view('risk.success');
//})->name('pelatihan.daftar.success');
//Route::get('daftar-pelatihan-bsn/{pelatihanbsn}/failed', function ($pelatihanbsn){
//	return view('risk.failed');
//})->name('pelatihan.daftar.failed');
//Route::get('absenpelatihanbsn/{email}','FrontendController@absenPesertaPelatihanBsn')->name('pelatihan.absen');
//Route::get('absenpelatihanbsn/{email}/store/{fromlaporan?}','FrontendController@storeAbsenPelatihanBsn')->name('pelatihan.absen.store');
//Route::get('konfirmasikehadiran/{email}/{status}','FrontendController@konfirmasiKehadiranBsn')->name('pelatihan.konfirmasi.kehadiran');
//Route::get('blastemailkonfirmasikeharianbsn','FrontendController@blastEmailKehadiranBsn');
//
//Route::get('laporanbsn','FrontendController@laporanBsn')->name('laporanbsn.index');
//Route::get('laporanbsn2','FrontendController@laporanBsn2')->name('laporanbsn2.index');
//Route::get('laporanbsn/{kelas}','FrontendController@laporanBsnDetail')->name('laporanbsn.detail');
//
Route::get('tespdf','FrontendController@tespdf')->name('tespdf');
//Route::get('sendsertifikat/{email}/{fromlaporan?}','FrontendController@sendSertifikat')->name('pelatihan.send.sertifikat');
//Route::get('blastsertifikat','FrontendController@sendBlastSertifikat')->name('pelatihan.blast.sertifikat');
