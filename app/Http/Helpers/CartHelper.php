<?php  
namespace App\Http\Helpers;

use App\Model\Keranjang;
use App\Model\PesertaWorkshop;
use App\Model\SuratRiset;
use App\Model\VirtualAccount;
use App\Model\Workshop;

/**
* 
*/
class CartHelper
{
	public static function getInvoiceNo()
	{
		$maxId = Keranjang::whereRaw('YEAR(created_at) = ?', [date('Y')])->max('id');
        if ($maxId) {
            $nextId = $maxId + 1;
            $newId  = str_pad($nextId, 4, "0", STR_PAD_LEFT);
            $newId  = date("ymd") . $newId;
            return $newId;
        } else {
            return date('ymd') . "0001";
        }
	}
	
	public static function getVirtualAccount($code='030')
	{
		$vc = VirtualAccount::where('account_code',strval($code))->where('account_status',0)->orderBy('id')->first();
		if ($vc && $vc->count() > 0) {
			$no_vc = $vc->account_no;
			$vc->update(['account_status'=>1]);
		} else {
			$no_vc = '4581349992';
		}
		return $no_vc;
	}
	
	public static function getStatusTransaksi(Keranjang $keranjang)
	{
		$status = "Menunggu Pembayaran";
		if ($keranjang->is_cancel == 1) {
			$status = "Transaksi dibatalkan";
		} else {
			if ($keranjang->is_confirm == 1 && $keranjang->is_payment_approve == 0) {
				$status = "Menunggu persetujuan pembayaran";
			} elseif ($keranjang->is_confirm == 1 && $keranjang->is_payment_approve == 1) {
				$status = "Dibayar";
			}
		}
		return $status;
	}
	
	public static function getSuratRisetInvoice()
	{
		 $maxId = SuratRiset::whereRaw('YEAR(createdDate) = ?', [date('Y')])->count();
		if ($maxId) {
			$nextId = $maxId + 1;
			$newId  = str_pad($nextId, 4, "0", STR_PAD_LEFT);
			$newId  = date("ymd") . $newId;
			return 'SR'.$newId;
		} else {
			return 'SR'.date('ymd') . "0001";
		}
	}

	public static function getWorkshopInvoice()
	{
        $maxId = PesertaWorkshop::whereRaw('YEAR(created_at) = ?', [date('Y')])->count();
		if ($maxId) {
			$nextId = $maxId + 1;
			$newId  = str_pad($nextId, 4, "0", STR_PAD_LEFT);
			$newId  = date("ymd") . $newId;
			return 'WO'.$newId;
		} else {
			return 'WO'.date('ymd') . "0001";
		}
	}
	
	public static function getUrutanPeserta(Workshop $workshop)
	{
		$lastNo = PesertaWorkshop::orderBy('urutan')->where('workshop_id',$workshop->id)->get();
		if ($lastNo && $lastNo->count() > 0) {
			$nextNo = $lastNo->count() + 1;
		} else {
			$nextNo = 1;
		}
		return $nextNo;
	}
}