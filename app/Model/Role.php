<?php
namespace App\Model;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
	protected $fillable = [
		'name',
		'display_name',
		'description'
	];
	
	/**
	 * Get the route key for the model.
	 *
	 * @return string
	 */
	public function getRouteKeyName()
	{
		return 'name';
	}
}
