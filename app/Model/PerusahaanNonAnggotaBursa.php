<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PerusahaanNonAnggotaBursa extends Model
{
    protected $table = 'perusahaan1';
    
    protected $fillable = [
        'name',
        'aktif',
        'cabang'
    ];
	
	public function karir()
	{
		return $this->hasMany('App\Model\Karir','perusahaan1_id','id');
	}
}
