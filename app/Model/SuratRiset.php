<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class SuratRiset extends Model
{
    protected $table = 'cmeds_researchapplication';

    protected $primaryKey = 'idResearchApplication';

    protected $fillable = [
        'invoiceNumber',
        'idCustomer',
        'namaMhs',
        'noIndukMhs',
        'judulTugasAkhir',
        'isApproved',
        'createdDate',
        'suratPengantar',
        'approvedBy',
        'approvedDate',
        'noSuratPengantar',
        'tglSuratPengantar',
        'universitas',
        'jenisTugasAkhir',
        'programStudi',
        'skripsi',
        'thesis',
        'countEdited',
        'noSurat',
        'remarks',
        'price',
        'approveSuperAdmin',
        'approveSuperBy',
        'approveSuperDate',
        'sendEmailDate',
        'isDeleted',
        'isConfirm',
        'confirmDate',
        'virtual_account'
    ];

    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('suratRisetDeleted', function (Builder $builder){
            $builder->where('cmeds_researchapplication.isDeleted',0);
        });
    }
	
	public function user()
	{
		return $this->belongsTo('App\User','idCustomer','id');
    }
}
