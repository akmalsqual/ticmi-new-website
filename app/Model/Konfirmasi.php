<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Konfirmasi extends Model
{
    protected $table = 'cmeds_konfirmasi';

    protected $fillable = [
        'user_id',
        'keranjang_id',
        'invoice_no',
        'virtual_account',
        'nominal',
        'no_rekening',
        'nama_rekening',
        'tgl_transfer',
        'jam_transfer',
        'bukti_transfer',
        'jns_konfirmasi'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function keranjang()
    {
        return $this->belongsTo('App\Model\Keranjang');
    }
	
	public function pesertaworkshop()
	{
		return $this->belongsTo('App\Model\PesertaWorkshop','user_id','id');
    }
}
