<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    protected $table = 'ticmi_districts';
    
    protected $fillable = [
        'district_id',
    	'name'
    ];
	
	public function kecamatan()
	{
		return $this->belongsTo('\App\Kecamatan','district_id','id');
    }
    
}
