<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DaftarUjian extends Model
{
    protected $table = 'ujian';
    
    public $timestamps = false;
    
    protected $fillable = [
        'tanggal',
        'gelombang',
        'email',
        'nama',
        'program',
        'aktif',
        'cabang',
        'remark'
    ];
	
	public function nama_program()
	{
		return $this->belongsTo('App\Model\Program','program','id');
    }
}
