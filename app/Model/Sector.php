<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
	protected $table = 'cmeds_sector';
	
	protected $primaryKey = 'idSector';
	
	public $timestamps = false;
	
	protected $fillable = [
		'idParentSector',
		'name',
		'description',
		'idSector',
		'idSubsector',
		'createdDate',
		'createdBy',
		'updatedDate',
		'updatedBy',
		'isActive',
		'isDeleted'
	];
	
	public function emitensector()
	{
		return $this->hasMany('App\Model\EmitenToSector');
	}
	
	public function emiten()
	{
		return $this->belongsTo('App\Model\Emiten','idSector','idSector');
	}
	
	public function subemiten()
	{
		return $this->belongsTo('App\Model\Emiten','idSector','idSubsector');
	}
	
	public function emitendata()
	{
		return $this->belongsTo('App\Model\EmitenData','idSector','idSector');
	}
}
