<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tipe extends Model
{
    protected $table = 't_type';

	protected $fillable = [
		'nama',
	    'aktif'
	];

	public function kelas() {
		return $this->hasMany('App\Model\Kelas','tipe','id');
	}
}
