<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FileMateri extends Model
{
    protected $table = 'ticmi_file_materi';
    
    protected $fillable = [
        'workshop_id',
        'nama_materi',
        'keterangan',
        'link_file',
        'jns_file',
        'is_publish'
    ];
	
	public function workshop()
	{
		return $this->belongsTo('\App\Model\Workshop','workshop_id','id');
    }
}
