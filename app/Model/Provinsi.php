<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    protected $table = 'ticmi_provinces';
    
    protected $fillable = [
        'name'
    ];
	
	public function kabupaten()
	{
		return $this->hasMany('\App\Kabupaten','province_id','id');
    }
}
