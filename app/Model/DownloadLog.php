<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DownloadLog extends Model
{
    protected $table = 'cmeds_download_log';
    
    protected $fillabe = [
        'user_id',
        'idEmitenData',
        'emiten_code',
        'file_title',
        'file_type',
        'file_doctype',
        'year',
        'quarter',
        'date_file',
        'institusi',
        'download_date',
        'counter',
        'from_web',
        'from_email'
    ];
	
	public function user()
	{
		return $this->belongsTo('App\User');
    }
}
