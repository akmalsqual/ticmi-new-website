<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PesertaWisuda extends Model
{
    protected $table = 'ticmi_peserta_wisuda';
    
    protected $fillable = [
        'nama',
        'email',
        'no_hp',
        'is_confirm',
        'program'
    ];
}
