<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CMPDPLolos extends Model
{
	protected $table = "cmpdp_2017_lolos";
	
	protected $fillable = [
		'no_daftar',
		'nama_lengkap',
		'nama_panggilan',
		'email',
		'no_hp',
		'tmp_lahir',
		'tgl_lahir',
		'no_ktp',
		'jns_kelamin',
		'agama',
		'alamat_ktp',
		'provinsi_ktp',
		'kabupaten_ktp',
		'kecamatan_ktp',
		'alamat_skrng',
		'provinsi_skrng',
		'kabupaten_skrng',
		'kecamatan_skrng',
		'status_kawin',
		'kebangsaan',
		'pas_foto',
		'cv',
		'link_video',
		'periode_s1',
		'jurusan_s1',
		'universitas_s1',
		'ipk_s1',
		'is_lulus_s1',
		'transkrip_nilai',
		'periode_s2',
		'jurusan_s2',
		'universitas_s2',
		'ipk_s2',
		'is_lulus_s2',
		'nm_perusahaan_1',
		'jabatan_1',
		'periode_pekerjaan_1',
		'nonimal_gaji_1',
		'pendapatan_pertahun_1',
		'nm_perusahaan_2',
		'jabatan_2',
		'periode_pekerjaan_2',
		'nonimal_gaji_2',
		'pendapatan_pertahun_2',
		'nm_perusahaan_3',
		'jabatan_3',
		'periode_pekerjaan_3',
		'nonimal_gaji_3',
		'pendapatan_pertahun_3',
		'gambaran_layak',
		'harapan_karir',
		'kekuatan',
		'kelemahan',
		'kegiatan',
		'nama_saudara_1',
		'hubungan_saudara_1',
		'perusahaan_saudara_1',
		'nama_saudara_2',
		'hubungan_saudara_2',
		'perusahaan_saudara_2',
		'nama_saudara_3',
		'hubungan_saudara_3',
		'perusahaan_saudara_3',
		'is_statement',
		'kota_seleksi',
		'send_email',
		'is_lulus_admin',
		'hari_ujian',
		'jam_ujian',
	    'alamat_ujian'
	];
	
	public function get_provinsi_ktp()
	{
		return $this->belongsTo('\App\Model\Provinsi', 'provinsi_ktp', 'id');
	}
	
	public function get_kabupaten_ktp()
	{
		return $this->belongsTo('\App\Model\Kabupaten', 'kabupaten_ktp', 'id');
	}
	
	public function get_kecamatan_ktp()
	{
		return $this->belongsTo('\App\Model\Kecamatan', 'kecamatan_ktp', 'id');
	}
	
	public function get_provinsi_skrng()
	{
		return $this->belongsTo('\App\Model\Provinsi', 'provinsi_skrng', 'id');
	}
	
	public function get_kabupaten_skrng()
	{
		return $this->belongsTo('\App\Model\Kabupaten', 'kabupaten_skrng', 'id');
	}
	
	public function get_kecamatan_skrng()
	{
		return $this->belongsTo('\App\Model\Kecamatan', 'kecamatan_skrng', 'id');
	}
}
