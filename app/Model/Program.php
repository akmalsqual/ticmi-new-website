<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $table = 't_program';

	protected $fillable = [
		'nm_program',
	    'full',
	    'akses',
	    'aktif'
	];

	public function batch() {
		return $this->hasMany('App\Model\Batch','kd_program','id');
	}
	
	public function workshop()
	{
		return $this->hasMany('App\Model\Workshop','t_program_id','id');
	}
	
	public function ujianulang()
	{
		return $this->hasMany('App\Model\DaftarUjian','program','id');
	}
	
	public function admisi()
	{
		return $this->hasMany('App\Model\Admisi','kd_program','id');
	}
}
