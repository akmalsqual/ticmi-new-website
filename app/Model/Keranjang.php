<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Keranjang extends Model
{
    use SoftDeletes;

    protected $table = 'cmeds_keranjang';

    protected $fillable = [
        'user_id',
        'invoice_no',
        'subtotal',
        'biaya_admin',
        'diskon',
        'grandtotal',
        'virtual_account',
        'is_cancel',
        'cancel_date',
        'is_confirm',
        'confirm_date',
        'is_payment_approve',
        'payment_approve_by',
        'payment_approve_date',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at','confirm_date','cancel_date','payment_approve_date'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function keranjang_detail() {
        return $this->hasMany('App\Model\KeranjangDetail','cmeds_keranjang_id','id');
    }

    public function konfirmasi()
    {
        return $this->hasOne('App\Model\Konfirmasi');
    }
}
