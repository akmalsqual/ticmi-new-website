<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Emiten extends Model
{
	protected $table = 'cmeds_emiten';
	
	protected $primaryKey = 'idEmiten';
	
	public $timestamps = false;
	
	protected $dates = ['createdDate','updatedDate','deletedDate'];
	
	protected $fillable = [
		'name',
		'code',
		'description',
		'idSector',
		'idSubsector',
		'createdDate',
		'createdBy',
		'updatedDate',
		'updatedBy',
		'isActive',
		'isDeleted',
		'deletedDate',
		'deletedBy'
	];
	
	public static function boot()
	{
		parent::boot();
		static::addGlobalScope('emitenDeleted', function (Builder $builder){
			$builder->where('isDeleted',0);
		});
	}
	
	public function emitenData()
	{
		return $this->hasOne('App\Model\EmitenData','idEmiten','idEmiten');
	}
}
