<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
	protected $table = 'batch';

	protected $fillable = [
		'tanggal',
		'nama',
		'keterangan',
		'kd_program',
		'kd_kelas',
		'kd_type',
		't_kategori',
		'retail',
		'harga',
		'detail',
		'hari',
		'approve',
		'approve2',
		'pasang',
		'mulai',
		'cabang',
		'front',
		'aktif'
	];

	protected $dates = ['tanggal'];

	public function program() {
		return $this->belongsTo('App\Model\Program','kd_program','id');
	}

	public function kelas() {
		return $this->belongsTo('App\Model\Kelas','kd_kelas','id');
	}

	public function kodecabang()
	{
		return $this->belongsTo('App\Model\Cabang','cabang','nama');
	}
}
