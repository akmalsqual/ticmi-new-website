<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kabupaten extends Model
{
    protected $table = 'ticmi_regencies';
    
    protected $fillable = [
        'province_id',
        'name'
    ];
	
	public function provinsi()
	{
		return $this->belongsTo('\App\Provinsi','province_id','id');
    }
	
	public function kecamatan()
	{
		return $this->hasMany('\App\Kecamatan','regency_id','id');
    }
}
