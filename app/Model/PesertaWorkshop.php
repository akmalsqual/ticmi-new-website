<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PesertaWorkshop extends Model
{
    protected $table = 'ticmi_peserta_workshop';
    
    protected $fillable = [
    	'invoice_no',
    	'workshop_id',
	    'urutan',
	    'nama',
	    'email',
	    'email_corp',
	    'company',
	    'jabatan',
	    'telp',
	    'no_hp',
	    'no_sk_ojk',
	    'tgl_sk_ojk',
	    'biaya',
	    'diskon',
	    'total_bayar',
	    'has_referral',
	    'referral_from',
	    'virtual_account',
        'is_confirm',
        'confirm_at',
        'is_payment_approved',
        'payment_approved_at',
        'payment_approved_by',
	    'is_payment_cancel',
	    'payment_cancel_at',
	    'payment_cancel_by'
    ];
	
	public function workshop()
	{
		return $this->belongsTo('App\Model\Workshop','workshop_id','id');
    }
	
	public function konfirmasi()
	{
		return $this->hasOne('App\Model\Konfirmasi','invoice_no','invoice_no');
    }
	
	public function referral()
	{
		return $this->belongsTo('App\Model\PesertaWorkshop','referral_from','id');
    }
	
	public function child_referral()
	{
		return $this->hasMany('App\Model\PesertaWorkshop','referral_from','id');
    }
}
