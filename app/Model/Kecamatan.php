<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $table = 'ticmi_districts';
    
    protected $fillable = [
        'regency_id',
        'name'
    ];
	
	public function kabupaten()
	{
		return $this->belongsTo('\App\Kabupaten','regency_id','id');
    }
	
	public function kelurahan()
	{
		return $this->hasMany('\App\Kelurahan','district_id','id');
    }
}
