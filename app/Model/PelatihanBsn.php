<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PelatihanBsn extends Model
{
	use SoftDeletes;
	
    protected $table = 'pelatihan_bsn';
	
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['deleted_at'];
	
	protected $fillable = [
		'nama',
	    'email',
	    'no_hp',
	    'institusi',
	    'jabatan',
	    'kelas',
	    'absen',
	    'hadir'
	];
}
