<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VirtualAccount extends Model
{
    protected $table = 'virtual_account';
    
    protected $fillable = [
    	'account_type',
        'account_code',
        'account_no',
        'account_status',
        'account_balance'
    ];
}
