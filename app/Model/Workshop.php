<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Workshop extends Model
{
	use SoftDeletes;
	
    protected $table = 'ticmi_workshop';
	
	protected $fillable = [
		't_program_id',
		'workshop_name',
		'sub_title',
	    'slugs',
	    'tgl_mulai',
	    'tgl_selesai',
	    'waktu',
	    'lokasi',
	    'ruangan',
	    'biaya',
	    'minimal_peserta',
	    'deskripsi',
	    'is_publish',
	    'jml_peserta_diskon',
	    'jml_group_referral',
	    'persen_diskon',
	    'nominal_diskon',
	    'pesan_email_umum',
	    'pesan_email_khusus'
	];
	
	public function peserta()
	{
		return $this->hasMany('App\Model\PesertaWorkshop','workshop_id','id');
	}
	
	public function program()
	{
		return $this->belongsTo('App\Model\Program','t_program_id','id');
	}
	
	public function materi()
	{
		return $this->hasMany('\App\Model\FileMateri','workshop_id','id');
	}
}
