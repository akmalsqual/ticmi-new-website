<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UsersWinGamers extends Model
{
    protected $table = 'users_wingamers';
    
    protected $fillable = [
        'user_id',
        'loginid',
        'cif',
        'url_conf',
        'accountcode',
        'date_expired',
    ];
	
	public function user()
	{
		return $this->belongsTo('App\User');
    }
}
