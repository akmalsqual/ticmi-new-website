<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Acara extends Model
{
	use SoftDeletes;
	
	protected $table = 'kehadiran_acara';
	
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	// protected $dates = ['deleted_at'];
	
	protected $fillable = [
		'nama',
		'email',
		'alamat',
		'no_hp',
		'institusi',
		'alumni',
	];
}