<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pengajar extends Model
{
    protected $table = 'pengajar';
	
	protected $fillable = [
		'nama',
	    'foto',
	    'jabatan',
	    'institusi',
	    'kelembagaan',
	    'kelamin',
	    'ttl',
	    'alamat',
	    'email',
	    'email2',
	    'telp',
	    'sertifikat',
	    'sekretaris',
	    'email3',
	    'telp2',
	    'norek',
	    'bank',
	    'namarek',
	    'history',
	    'synopsis',
	    'share_telp',
	    'share_fb',
	    'share_twitter',
	    'share_linkedin',
	    'share_email',
	    'cabang',
	    'aktif',
	];
}
