<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Karir extends Model
{
    protected $table = 'ticmi_karir';
    
    protected $fillable = [
        'perusahaan_id',
        'perusahaan1_id',
        'posisi',
        'keterangan',
        'pic_name',
        'pic_email',
        'valid_until',
        'is_publish'
    ];
	
	public function perusahaan_ab()
	{
		return $this->belongsTo('App\Model\PerusahaanAnggotaBursa','perusahaan_id','id');
    }
	
	public function perusahaan_non_ab()
	{
		return $this->belongsTo('App\Model\PerusahaanNonAnggotaBursa','perusahaan1_id','id');
    }
}
