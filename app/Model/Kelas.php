<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = 't_kelas';

	protected $fillable = [
		'kd_kelas',
	    'nm_kelas',
	    'nick',
	    'tipe',
	    'keterangan',
	    'aktif',
	    'cabang'
	];

	public function type() {
		return $this->belongsTo('App\Model\Tipe','tipe','id');
	}

	public function batch() {
		return $this->hasMany('App\Model\Batch','kd_kelas','id');
	}

	public function admisi() {
		return $this->hasMany('App\Model\Admisi','kd_kelas','id');
	}
}
