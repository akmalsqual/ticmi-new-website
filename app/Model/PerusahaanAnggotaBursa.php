<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PerusahaanAnggotaBursa extends Model
{
    protected $table = 'perusahaan';
    
    protected $fillable = [
        'name',
        'aktif',
        'cabang'
    ];
	
	public function karir()
	{
		return $this->hasMany('App\Model\Karir','perusahaan_id','id');
    }
}
