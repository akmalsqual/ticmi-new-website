<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Cabang extends Model
{
	protected $table = 'cabang';

	protected $fillable = [
		'nama',
	    'urutan',
	    'aktif'
	];

	public function batch()
	{
		return $this->hasMany('App\Model\Batch','cabang','nama');
	}
}
