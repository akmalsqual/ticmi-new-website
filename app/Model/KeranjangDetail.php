<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KeranjangDetail extends Model
{
    protected $table = 'cmeds_keranjang_detail';

    protected $fillable = [
        'cmeds_keranjang_id',
        'user_id',
        'data_emiten_id',
        'item_name',
        'item_doctype',
        'item_type',
        'item_year',
        'item_filename',
        'price',
        'qty',
        'diskon',
        'total_price',
        'downloaded',
    ];

    public function keranjang() {
        return $this->belongsTo('App\Model\Keranjang','cmeds_keranjang_id','id');
    }
}
