var elixir = require('laravel-elixir');
require('laravel-elixir-imagemin');
require('laravel-elixir-browser-sync-simple');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    // mix.sass('app.scss');

    mix.imagemin();

    mix.styles(
        [
            'resources/assets/css/lib/bootstrap.min.css',
            'resources/assets/css/lib/animate.min.css',
            'resources/assets/css/lib/font-awesome.min.css',
            'resources/assets/css/lib/univershicon.css',
            'resources/assets/css/lib/owl.carousel.css',
            'resources/assets/css/lib/prettyPhoto.css',
            'resources/assets/css/lib/alertify.min.css',
            'resources/assets/css/lib/alertify-default.min.css',
            'resources/assets/css/lib/menu.css',
            'resources/assets/css/lib/timeline.css',
            'resources/assets/css/lib/pickaday/pikaday.css',
            'resources/assets/css/lib/pickaday/theme.css',
            'resources/assets/css/lib/pickaday/triangle.css',
        ]
        , 'public/assets/css/lib/libs.css'
    );
    
    mix.styles(
        [
            'resources/assets/css/theme.css',
            'resources/assets/css/theme-responsive.css',
            'resources/assets/css/default.css',
            'resources/assets/css/custom.css'
        ]
        , 'public/assets/css/style.css'
    );

    mix.scripts(
        [
            'resources/assets/js/lib/jquery.js',
            'resources/assets/js/lib/bootstrap.min.js',
            'resources/assets/js/lib/bootstrapValidator.min.js',
            'resources/assets/js/lib/jquery.appear.js',
            'resources/assets/js/lib/jquery.easing.min.js',
            'resources/assets/js/lib/owl.carousel.min.js',
            'resources/assets/js/lib/countdown.js',
            'resources/assets/js/lib/counter.js',
            'resources/assets/js/lib/isotope.pkgd.min.js',
            'resources/assets/js/lib/jquery.easypiechart.min.js',
            'resources/assets/js/lib/jquery.mb.YTPlayer.min.js',
            'resources/assets/js/lib/jquery.prettyPhoto.js',
            'resources/assets/js/lib/jquery.stellar.min.js',
            'resources/assets/js/lib/alertify.min.js',
            'resources/assets/js/lib/menu.js',
            'resources/assets/js/lib/moment.min.js',
            'resources/assets/js/lib/pikaday.js',
            'resources/assets/js/lib/pikaday.jquery.js',
            'resources/assets/js/custom.js',
            'resources/assets/js/ticmi-script.js'
        ]
        , 'public/assets/js/lib/libs.js'
    );

    mix.copy("resources/assets/css/ie.css", "public/assets/css/ie.css")
        .copy("resources/assets/js/lib/modernizr.js", "public/assets/js/lib/modernizr.js")
        .copy("resources/assets/js/lib/jquery.themepunch.tools.min.js", "public/assets/js/lib/jquery.themepunch.tools.min.js")
        .copy("resources/assets/js/lib/jquery.themepunch.revolution.min.js", "public/assets/js/lib/jquery.themepunch.revolution.min.js")
        .copy("resources/assets/js/lib/theme-rs.js", "public/assets/js/lib/theme-rs.js")
        .copy("resources/assets/js/theme.js", "public/assets/js/theme.js")
        .copy("resources/assets/js/lib/extensions/", "public/assets/js/lib/extensions");

    mix.browserSync({
        files: [
            'public/assets/css/**/*.css',                     // This is the one required to get the CSS to inject
            'public/assets/js/**/*.js',                     // This is the one required to get the CSS to inject
            'resources/views/**/*.blade.php',       // Watch the views for changes & force a reload
            'app/**/*.php'                      // Watch the app files for changes & force a reload
        ],
        proxy: 'ticminew.app'
    });
});
