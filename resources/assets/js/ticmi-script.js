/**
 * Created by akmalbashan on 24-Jun-16.
 */
var base_url = $(".base_url").val();
$.ajaxPrefilter(function(options, originalOptions, xhr) { // this will run before each request
    //var token = $('meta[name="csrf-token"]').attr('content'); // or _token, whichever you are using
    var token = $("input[name='_token']").val();

    if (token) {
        return xhr.setRequestHeader('X-CSRF-TOKEN', token); // adds directly to the XmlHttpRequest Object
    }
});
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $("input[name='_token']").val()
    }
});

alertify.defaults.glossary.title = "TICMI";

$(document).ready(function () {

    $('body').on('change','.cari-kabupaten', function (e) {
        e.preventDefault();
        var prefix = $(this).data('prefix-code');
        var id_provinsi = $(this).val();

        $('.kabupaten_'+prefix).html('<option value="">LOADING</option>');
        $.post(base_url + '/ajax/getkabupatenbyprovinsi',{id_provinsi:id_provinsi}, function (data) {
            if (data) {
                $('.kabupaten_'+prefix).html(data);
            }
        });
    });

    $('body').on('change','.cari-kecamatan', function (e) {
        e.preventDefault();
        var prefix = $(this).data('prefix-code');
        var id_kabupaten = $(this).val();

        $('.kecamatan_'+prefix).html('<option value="">LOADING</option>');
        $.post(base_url + '/ajax/getkecamatanbykabupaten',{id_kabupaten:id_kabupaten}, function (data) {
            if (data) {
                $('.kecamatan_'+prefix).html(data);
            }
        });
    });

    /**
     * General
     */
    $('[data-toggle="popover"]').on('click',function(e){
        e.preventDefault();
    });
    $('.showtooltip').tooltip({
        track: true
    });

    // $('[data-toggle="loadpdf"]').loadPDF();
    $('[data-toggle="loadpdf"]').loadThumbnail();

    $('.datepicker').pikaday({
        format: 'YYYY-MM-DD',
        firstDay: 1,
        minDate: new Date(1940, 0, 1),
        maxDate: new Date(2020, 12, 31),
        yearRange: [1940,2020],
        theme: 'triangle-theme'
    });

    var disable = false;
    var picker = new Pikaday({
        field: document.getElementById('datepicker-ujian'),
        trigger: document.getElementById('datepicker-button'),
        format: 'YYYY-MM-DD',
        firstDay: 1,
        minDate: new Date(),
        maxDate: new Date(2018, 12, 31),
        yearRange: [2017,2018],
        disableWeekends: true,
        bound: true,
        disableDayFn: function(theDate) {
            if (theDate.getDay() == 5) return true;
            else return false;
        },
        onSelect: function() {
            // console.log(this.getMoment().format('Do MMMM YYYY'));
            var url = base_url + '/cek-daftarujianulang';

            $.post(url,$('#form-ujianulang').serialize(), function (data) {
                // console.log(data.status);
                if (data.status) {
                    $('.pilih-sesi').fadeIn(100);
                    if (data.sesi_1 == 0) {
                        $('.full-sesi1').fadeIn(100);
                        $('input[type="radio"].sesi_1').attr('disabled',true);
                        $('.btn-ujianulang').attr('disabled',true);
                    } else {
                        $('.kuota-sesi1').html('<strong>('+data.sesi_1+' kursi tersisa)</strong>');
                        $('input[type="radio"].sesi_1').attr('disabled',false);
                        $('.btn-ujianulang').attr('disabled',false);
                    }
                    if (data.sesi_2 == 0) {
                        $('.full-sesi2').fadeIn(100);
                        $('input[type="radio"].sesi_2').attr('disabled',true);
                        $('.btn-ujianulang').attr('disabled',true);
                    } else {
                        $('.kuota-sesi2').html('<strong>('+data.sesi_2+' kursi tersisa)</strong>');
                        $('input[type="radio"].sesi_2').attr('disabled',false);
                        $('.btn-ujianulang').attr('disabled',false);
                    }
                }
            },'json').done(function(){

            }).fail(function (data) {
                var errors = data.responseJSON;
                var errorsHtml= '<ul class="list-unstyled">';
                $.each( errors, function( key, value ) {
                    errorsHtml += '<li>' + value[0] + '</li>';
                });
                errorsHtml += '</ul>';
                alertify.alert('Notifikasi', errorsHtml);
                $('.datepicker-ujian').val('');
            });
        }
    });

    /**
     * Bimbel Ujian
     */
    $('body').on('click','.btn-bimbel', function (e) {
        e.preventDefault();
        var $btn = $(this).button('loading');
        var url  = base_url + '/cek-bimbel';
        $.post(url,$('#form-bimbel').serialize(), function (data) {
            if (data.status) {
                document.formbimbel.submit();
            }
            $btn.button('reset');
        },'json').done(function () {
            $btn.button('reset');
        }).fail(function (data) {
            var errors = data.responseJSON;
            var errorsHtml= '<ul class="list-unstyled">';
            $.each( errors, function( key, value ) {
                errorsHtml += '<li>' + value[0] + '</li>';
            });
            errorsHtml += '</ul>';
            alertify.alert('Notifikasi', errorsHtml);
            $btn.button('reset');
        });
    });

    $('body').on('change','select.sel-cabang', function (e) {
        e.preventDefault();
        $('.sel-program').val('');
        $('.sel-tanggal').html("<option value=''>Pilih Tanggal dan Jam</option>").val('');
        $('.btn-bimbel').attr('disabled',true);
    })

    $('body').on('change','select.sel-program', function (e) {
        e.preventDefault();
        var url = base_url + '/gratis/cek-jadwal';
        var cabang = $('.sel-cabang');
        var tanggal_jam = $('.sel-tanggal');
        var img_loader = $('.img-loader');

        if (cabang.val() == '') {
            var errorsHtml = '<ul class="list-unstyled">';
                errorsHtml += '<li>Pilih Cabang terlebih dahulu</li>';
                errorsHtml += '</ul>';
            alertify.alert('Notifikasi', errorsHtml);
            $(this).val('');
            $('.btn-bimbel').attr('disabled',true);
        } else {
            tanggal_jam.hide(100);
            img_loader.show(100);
            $.post(url,$('#form-bimbel').serialize(), function (data) {
                // tanggal_jam.hide(100);
                // img_loader.show(100);
                tanggal_jam.html("<option value=''>Pilih Tanggal dan Jam</option>");
                tanggal_jam.append(data);
                tanggal_jam.show(100);
                img_loader.hide(100);
                // $('.tgl_wrapper').show(100);
            });
        }
    });

    $('body').on('change','.sel-tanggal', function (e) {
        if ($(this).val() != '') {
            $('.btn-bimbel').attr('disabled',false);
        } else {
            $('.btn-bimbel').attr('disabled',true);
        }
    });

    $('body').on('click','a.delconfirm', function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        var msg = $(this).data('del-message');
        if (msg == '') {
            msg = "Apakah Anda yakin untuk mengahpus data ?";
        }
        alertify.confirm('Konfirmasi hapus item',msg, function () {
            window.location.href = link;
        },function () {
            alertify.notify('Batal Hapus items','warning',10);
        });
    });

    /**
     * Flash Modal Overlayed
     */
    // $('#flash-overlay-modal').modal();

    $('[data-toggle="loading"]').on('click', function (e) {
        var $btn = $(this).button('loading');
        var confirm = $(this).data('confirm');
        var form = $(this).parents('form');
        if (confirm == true) {
            alertify.confirm("Apakah Anda Yakin ?.",
                function(){
                    form.submit();
                    alertify.success('Ok');
                },
                function(){
                    $btn.button('reset');
                    alertify.error('Cancel');
                }
            );
        }
    });

    /**
     * Home Program search
     */
    $(".searchProgram").on("click", function (e) {
        var $btn = $(this).button('loading');
        $(".loadingSearch").show(300);
        
        $.post(base_url+'/batch/find',$("#formSearch").serialize(), function (data) {
            $(".defaultBatch").hide(200);
            $(".searchResult").html(data);
            $btn.button('reset');
        });
        ga('send', 'event', 'Search', 'Home Page Search', $("#formSearch").serialize() );
    });

    /**
     * Registration
     */
    $('select[name=profesi]').on('change', function (e) {
        if ($(this).val() == 'Lainnya') {
            $('.profesi_lainnya').show();
        } else {
            $('.profesi_lainnya').hide();
            $('.profesi_lainnya').find('input').val('');
        }
    });

    /**
     * Ujian Ulang WPPE Pemasaran
     */
    // $('.btn-ujianulang').on('click', function (e) {
    //     e.preventDefault();
    //     var $btn = $(this).button('loading');
    //     var url = base_url + '/cek-daftarujianulang';
    //     var cabang = $('.sel-cabang').val();
    //     var cabangClass = cabang.split(" ");
    //     $.post(url,$('#form-ujianulang').serialize(), function (data) {
    //         // console.log(data.status);
    //         if (data.status == 'true') {
    //             $btn.button('reset');
    //             $('.sel-email').attr('readonly',true);
    //             $('.sel-cabang').attr('readonly',true);
    //             $('.btn-ujianulang').fadeOut(100);
    //             $('.form-ujianulang').fadeIn(100);
    //             $('.'+cabangClass[0]).fadeIn(100);
    //         }
    //     },'json').done(function(){
    //
    //     }).fail(function (data) {
    //         var errors = data.responseJSON;
    //         var errorsHtml= '<ul class="list-unstyled">';
    //         $.each( errors, function( key, value ) {
    //             errorsHtml += '<li>' + value[0] + '</li>';
    //         });
    //         errorsHtml += '</ul>';
    //         alertify.alert('Notifikasi', errorsHtml);
    //         $btn.button('reset');
    //     });
    // });

    /**
     * Career Development
     */
    $('.btn-career').on('click', function (e) {
        e.preventDefault();
        var $btn = $(this).button('loading');
        var url = base_url + '/cek-approval-career';
        var email = $('.career-email').val();
        var career = $(this).data('career');
        if (email == '') {
            alertify.alert('Notifikasi', 'Harap masukkan email Anda.');
            $btn.button('reset');
        } else {
            $.post(url, {email:email,career:career}, function (data) {
                // console.log(data);
                $btn.button('reset');
                if (data) {
                    window.location.reload();
                }
            }).fail(function (data) {
                var errors = data.responseJSON;
                var errorsHtml= '<ul class="list-unstyled">';
                $.each( errors, function( key, value ) {
                    errorsHtml += '<li>' + value[0] + '</li>';
                });
                errorsHtml += '</ul>';
                alertify.alert('Notifikasi', errorsHtml);
                $btn.button('reset');
            });
        }
    });

    /**
     * Shopping Script
     */
    setTimeout(function(){
        $('.shop-img-loader').fadeOut(500);
    },2000);
    $('body').on('click','a.addDocToCart', function(e){
        e.preventDefault();
        var idEmitenData = $(this).data('id');
        var title = $(this).data('title');
        var doctype = $(this).data('doctype');
        var type = $(this).data('type');
        var year = $(this).data('year');
        var message = 'Tambahkan <strong>'+title+'</strong> ke dalam keranjang belanja ?';
        alertify.confirm('Konfirmasi Pembelian Data',message,function(){
            $('#pageloader').fadeIn(100);
            $(".loader-inner").fadeIn(150);
            $.post(base_url+'/cart/add_item',{idEmitenData:idEmitenData,title:title,doctype:doctype,type:type,year:year}, function (data)
            {
                $('#pageloader').fadeOut(100);
                $(".loader-inner").fadeOut(100);
                alertify.alert('Notifikasi',data, function ()
                {
                    $('#pageloader').fadeIn(100);
                    $(".loader-inner").fadeIn(150);
                    window.location.reload();
                }).set('resizable',true).resizeTo('70%',250);
            }).fail(function (data) {
                $('#pageloader').fadeOut(100);
                $(".loader-inner").fadeOut(100);
                var errors = data.responseJSON;
                var errorsHtml= '<ul class="list-unstyled">';
                $.each( errors, function( key, value ) {
                    errorsHtml += '<li>' + value[0] + '</li>';
                });
                errorsHtml += '</ul>';
                alertify.alert('Notifikasi', errorsHtml);
            });
        },
        function ()
        {
            console.log('cancel');
        }).set('resizable',true).resizeTo('70%',250).setting('labels',{'ok':'Tambah Ke Keranjang', 'cancel': 'Batal'});;
    });

    $('body').on('click','a.delfromcart', function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        alertify.confirm('Konfirmasi hapus item','Apakah Anda yakin untuk manghapus item ini ?', function () {
            window.location.href = link;
        },function () {
            alertify.notify('Batal Hapus items','warning',10);
        });
    });

    $('body').on('click','button.buttonCheckout', function(e){
        e.preventDefault();
        var msg = $(this).data('message');
        if (msg == '') {
            msg = 'Lanjutkan proses pembelian ?';
        }
        alertify.confirm('Konfirmasi',msg,function(){
            document.formCheckout.submit();
        }, function(){
            alertify.notify('Batal ke proses selanjutnya','warning',10);     
        }).setting('labels',{'ok':'Lanjutkan','cancel':'Batal'});
    });

    $('body').on('click','button.btnAddReferral', function (e) {
        e.preventDefault();
        var add_referral = $('.add_referral_email');
        var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
        var act_url = $(this).data('url');

        if (add_referral.val() != "") {
            if (pattern.test(add_referral.val())) {
                document.formCheckout.action = act_url;
                document.formCheckout.submit();
            } else {
                alertify.alert('Harap isi kolom Email Peserta Tambahan dengan Email yang benar.');
            }
        } else {
            alertify.alert('Harap isi kolom Email Peserta Tambahan dahulu.');
        }
    })

    /**
     * Online Trading Simulation
     */
    $('body').on('click','a.onlineTradingSimulation', function (el) {
       el.preventDefault();



        alertify.prompt( 'Masukkan Password', '', ''
            , function(evt, value) {
                $('#pageloader').fadeIn(100);
                $(".loader-inner").fadeIn(150);
                $.post(base_url+'/member/check-password',{password:value}, function (response) {
                    if (response.status) {
                        $('#pageloader').fadeOut(100);
                        $(".loader-inner").fadeOut(100);
                        $.prettyPhoto.open(response.url_conf + '&iframe=true&width=100%&height=100%','Online Trading Simulation','Online Trading Simulation');
                        // alertify.alert('Sukses','Sukses');
                    } else {
                        $('#pageloader').fadeOut(100);
                        $(".loader-inner").fadeOut(100);
                        alertify.alert('Notifikasi',response.message);
                    }
                },'json');
            }, function() {
                $('#pageloader').fadeOut(100);
                $(".loader-inner").fadeOut(100);
                alertify.error('Cancel');
            }).set('type', 'password');
    });

    /**
     * Modal
     */


    $("body").on("click","a.modalIframe",function(e) {
        e.preventDefault();
        var url = $(this).attr("href");
        var title = $(this).attr("title");
        $('#mod-iframe-large').on('show.bs.modal', function (e) {
            var iframeWindow = $(this).find('iframe');
            $(this).find('iframe').attr('src',url);

            // console.log($( document ).height()+ ' - ' + $( window ).height());
            var frameHeight = $( document ).height() - $( window ).height();
            // var frameHeight = "80%";
            $(this).find('.modal-title').html(title);

            // console.log(iframeWindow.contents('html').outerHeight());
            iframeWindow.height(frameHeight);
        });
        $('#mod-iframe-large').modal({show:true});
    });
    $("body").on("click","a.modalIframeDocument",function(e) {
        console.log("aaa");
        e.preventDefault();
        var idEmitenData = $(this).data('id');
        var url = $(this).attr("href");
        var title = $(this).attr("title");
        var company = $(this).data('emiten');
        var code = $(this).data('code');
        var type = $(this).data('type');
        var year = $(this).data('year');
        var reftable = $(this).data('reftable');
        $('#mod-iframe-document').on('show.bs.modal', function (e) {
            var iframeWindow = $(this).find('iframe');
            $(this).find('iframe').attr('src',url);

            $('#nm-perusahaan').html(company);
            $('#kode-perusahaan').html(code);
            $('#tipe-dokumen').html(type);
            $('#tahun-dokumen').html(year);
            $('#show-id').val(idEmitenData);
            $('#show-reftable').val(reftable);

            // console.log($( document ).height()+ ' - ' + $( window ).height());
            // var frameHeight = $( document ).height() - $( window ).height();
            // var frameHeight = "80%";
            $(this).find('.modal-title').html(title);

            // console.log(iframeWindow.contents('html').outerHeight());
            // iframeWindow.height(frameHeight);
        });
        $('#mod-iframe-document').modal({show:true});
    });
    $('#mod-iframe-large').on('shown.bs.modal', function (e) {
        var iframeWindow = $(this).find('iframe');
        // $(this).find('iframe').attr('src',url);
        var h = $(this).find('.modal-body').outerWidth();
        // console.log(h);
        // iframeWindow.width(h);
    });
    $('.modal').on('hide.bs.modal', function() {
        $('.modal').removeData('bs.modal');
        console.log('hide');
        // Table.destroy();
    });

    /**
     * Absensi Pelatihan BSN
     */
    $('.absenPelatihanBsn').on('click', function (e) {
        e.preventDefault();
        var $btn = $(this).button('loading');
        var url = $(this).data('url');
        alertify.prompt( 'Verifikasi', 'Masukkan Password Verifikasi', ''
            , function(evt, value) {
                if (value == 5501) {
                    window.location.href = url;
                    // alertify.alert(url);
                } else  {
                    alertify.error('Password salah, <br> Masukkan Password yang benar');
                    $btn.button('reset');
                }
            }
            , function() {
                alertify.error('Cancel');
                $btn.button('reset');
            }).set('type', 'password').setting({'closable':false});
    });
    $('.tesabsenPelatihanBsn').on('click', function (e) {
        e.preventDefault();
        var $btn = $(this).button('loading');
        var url = $(this).data('url');
        alertify.prompt( 'Verifikasi', 'Masukkan Password Verifikasi', ''
            , function(evt, value) {
                if (value == 5501) {
                    window.location.href = url;
                    // alertify.alert(url);
                } else  {
                    alertify.error('Password salah, <br> Masukkan Password yang benar');
                    $btn.button('reset');
                }
            }
            , function() {
                alertify.error('Cancel');
                $btn.button('reset');
            }).set('type', 'password').setting({'closable':false});
    });
    $('body').on('click','.btn-form-sid', function (e) {
        e.preventDefault();
        var $btn = $(this).button('loading');
        var sid = $('.cekSid').val();
        var tanggal_lahir = $('.tanggal_lahir').val();
        var bulan_lahir = $('.bulan_lahir').val();
        var tahun_lahir = $('.tahun_lahir').val();

        if (sid.length  == 13) {
            if (bulan_lahir.toString().length == 1) {
                bulan_lahir = '0'+bulan_lahir;
            }
            if (tanggal_lahir.toString().length == 1) {
                tanggal_lahir = '0'+tanggal_lahir;
            }
            var tglbulan = tanggal_lahir+bulan_lahir;

            if (tglbulan == sid.substr(3,4)) {
                document.formRegInvestor.submit();
                $('#pageloader').fadeIn(100);
                $(".loader-inner").fadeIn(150);
                $btn.button('reset');
            } else {
                alertify.confirm('Notifikasi SID','SID yang Anda masukkan tidak valid, mohon masukkan SID yang valid atau Anda dapat mendaftar sebagai <strong>Non Pemilik SID</strong>',function(){
                    $('.cekSid').val('');
                    $('#sid_card').val('');
                    document.formRegInvestor.submit();
                    $('#pageloader').fadeIn(100);
                    $(".loader-inner").fadeIn(150);
                }, function(){
                    alertify.notify('Batal ke proses selanjutnya','warning',10);
                    $btn.button('reset');
                }).setting({'labels':{'ok':'Daftar sebagai Non Pemilik SID','cancel':'Perbaiki Isian SID'},'defaultFocus':'cancel'});
            }
        } else {
            alertify.confirm('Notifikasi SID','SID yang Anda masukkan tidak valid, mohon masukkan SID yang valid atau Anda dapat mendaftar sebagai <strong>Non Pemilik SID</strong>',function(){
                $('.cekSid').val('');
                $('#sid_card').val('');
                document.formRegInvestor.submit();
                $('#pageloader').fadeIn(100);
                $(".loader-inner").fadeIn(150);
            }, function(){
                alertify.notify('Batal ke proses selanjutnya','warning',10);
                $btn.button('reset');
            }).setting({'labels':{'ok':'Daftar sebagai Non Pemilik SID','cancel':'Perbaiki Isian SID'},'defaultFocus':'cancel'});
        }

    });

    /**
     * Load PDF
     */

    // $('data-load-pdf').loadPDF();

    window.sr = ScrollReveal({reset : false, duration : 2000});
    sr.reveal(".item-batch",{
        distance : '40px',
        origin : 'left',
        scale: 0.7
    });
    sr.reveal('.sekilas-title',{
        distance : '20px',
        origin : 'top'
    });
    sr.reveal('.counter-data',{
        distance : '40px',
        origin : 'bottom',
        scale: 0.7
    });
    sr.reveal('ul.sro-widget div.thumb-wrap',{
        distance : '40px',
        origin : 'right',
        scale: 0.5,
        delay: 5
    });
    sr.reveal('.trainer-list', {
        distance : '60px',
        origin : 'top',
    });



});


function LoadPDF(pdfDoc,pdfLoc) //pdfLoc is CANVAS, not div
{
    PDFJS.getDocument(pdfDoc).then(function(pdf)
    {
        // Using promise to fetch the page
        pdf.getPage(1).then(function(page) //Only grab first page
        {
            var scale = .5; // MODIFY THIS to change thumbnail size
            var viewport = page.getViewport(scale);

            //
            // Prepare canvas using PDF page dimensions
            //
            var canvas = document.getElementById(pdfLoc);
            var context = canvas.getContext('2d');
            canvas.height = viewport.height;
            canvas.width = viewport.width;

            //
            // Render PDF page into canvas context
            //
            var renderContext =
                {
                    canvasContext: context,
                    viewport: viewport
                };
            page.render(renderContext);
        });
    });
}