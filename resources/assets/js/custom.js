/* Add here all your JS customizations */
var base_url = $(".base_url").val();
(function ($) {

    $.fn.loadPDF = function(options) {
        options = options || {};

        var defaults = {
            datapdf:'',
            scale: .4,
            idData: 0,
            dataType: 'ED'
        };

        return $(this).each(function () {
            var setting = $.extend({}, defaults,{
                idData: $(this).data('id-data'),
                type: $(this).data('type'),
                docType: $(this).data('doctype')
            }, options);
            var _this = $(this);

            initialize();

            function getPDF() {
                $.post('http://download.icamel.id/viewpdf/view/75338/ED', function (data) {
                    setting.datapdf = data;
                }).done(function (data) {
                    $.fn.loadPDF().initialize();
                })
            }

            function initialize() {
                // PDFJS.getDocument().then(function (pdf) {
                // PDFJS.getDocument('http://dev.ticmi.co.id/dataemiten/'+ setting.docType+'/'+setting.type+'/getthumbnail/'+setting.idData).then(function (pdf) {
                PDFJS.getDocument(base_url+'/dataemiten/'+ setting.docType+'/'+setting.type+'/getthumbnail/'+setting.idData).then(function (pdf) {
                    pdf.getPage(1).then(function (page) {

                        var canvas = document.getElementById(setting.idData);
                        var viewport = page.getViewport(canvas.width / page.getViewport(1.0).width);
                        // var viewport = page.getViewport(setting.scale);

                        // var canvas = document.getElementById('75865');
                        var context = canvas.getContext('2d');
                        // canvas.height = 262;
                        // canvas.width = 262;

                        var renderContext =
                            {
                                canvasContext: context,
                                viewport: viewport
                            };
                        page.render(renderContext);
                        _this.parent('.shop-img-wrap').find('.img-def').addClass('canvas-shop');
                        _this.parent('.shop-img-wrap').find('.shop-img-loader').addClass('canvas-shop');
                        _this.parent('.shop-img-wrap').find('#'+setting.idData).removeClass('canvas-shop');
                    })
                });
            }
        })

    }

    $.fn.loadThumbnail = function (options) {
        options = options || {};

        var defaults = {
            datapdf:'',
            scale: .4,
            idData: 0,
            dataType: 'ED'
        };


        return $(this).each(function () {
            var setting = $.extend({}, defaults,{
                idData: $(this).data('id-data'),
                type: $(this).data('type'),
                docType: $(this).data('doctype')
            }, options);
            var _this = $(this);

            initialize();

            function initialize() {
                var canvas = document.getElementById(setting.idData);
                var context = canvas.getContext('2d');
                var imageObj = new Image();

                imageObj.onload = function() {
                    context.drawImage(imageObj, 69, 50);
                };
                imageObj.src = base_url+'/dataemiten/'+ setting.docType+'/'+setting.type+'/getthumbnail/'+setting.idData;
            }

        })

    }


}(jQuery));




function LoadPDF(pdfDoc,pdfLoc) //pdfLoc is CANVAS, not div
{
    PDFJS.getDocument(pdfDoc).then(function(pdf)
    {
        // Using promise to fetch the page
        pdf.getPage(1).then(function(page) //Only grab first page
        {
            var scale = .5; // MODIFY THIS to change thumbnail size
            var viewport = page.getViewport(scale);

            //
            // Prepare canvas using PDF page dimensions
            //
            var canvas = document.getElementById(pdfLoc);
            var context = canvas.getContext('2d');
            canvas.height = viewport.height;
            canvas.width = viewport.width;

            //
            // Render PDF page into canvas context
            //
            var renderContext =
                {
                    canvasContext: context,
                    viewport: viewport
                };
            page.render(renderContext);
        });
    });
}
