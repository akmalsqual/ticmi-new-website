@extends('risk.layout.home')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('./assets/images/banner/profile.jpg') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Pendaftaran Pelatihan SNI ISO 31000 Manajemen Risiko</h3>
                        <h6 class="sub-title"></h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-lgrey">
        <!-- Container -->
        <div class="container">
            <div class="row course-single content-box bg-white shadow ">
                @include('errors.list')
                @include('flash::message')

                <div class="col-sm-12 text-center">
                    <h5>Maaf pendaftaran Anda masih gagal, silahkan melakukan <a href={{ route('pelatihan.daftar') }}"">Pendaftaran</a> Kembali</h5>
                </div>
            </div>
        </div>
    </div>
@endsection