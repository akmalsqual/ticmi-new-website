@extends('risk.layout.home')
@section('content')
    <div class="page-default bg-lgrey">
        <!-- Container -->
        <div class="container">
            <div class="row">
                <div class="row course-single content-box bg-white shadow">
                    <div class="title-container text-left sm">
                        <div class="title-wrap">
                            <h5 class="title">Daftar Peserta Kelas : {{ $peserta->first()->kelas }}</h5>
                            <span class="separator line-separator"></span>
                            <a href="{{ route('laporanbsn.index') }}" class="btn pull-right">Kembali</a>
                        </div>
                    </div><!-- Name -->

                    @include('errors.list')
                    @include('flash::message')
                    <table class="table table-bordered table-striped table-condensed table-hover">
                        <thead>
                        <tr>
                            <th width="">Aksi</th>
                            <th width="25%">Nama</th>
                            <th width="20%">Email</th>
                            <th width="10%">No HP</th>
                            <th width="20%">Institusi</th>
                            <th>Jabatan</th>
                            <th>Kehadiran</th>
                            <th>Absen</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($no =1)
                        @if($peserta && $peserta->count() > 0)
                            @foreach($peserta as $item)
                                <tr>
                                    <td>
                                        <a href="{{ route('pelatihan.absen.store',['email'=>$item->email,'fromlaporan'=>$kelas]) }}"><i class="fa fa-pencil" style="font-size: 14px;"></i></a> |
                                        <a href="{{ route('resendemail',['email'=>$item->email,'fromlaporan'=>$kelas]) }}"><i class="fa fa-envelope-o" style="font-size: 14px;"></i></a> |
                                        @if($item->survey == 1)
                                            <a href="{{ route('pelatihan.send.sertifikat',['email'=>$item->email,'fromlaporan'=>$kelas]) }}"><i class="fa fa-file" style="font-size: 14px;"></i></a>
                                        @endif
                                    </td>
                                    <td>{{ $item->nama }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->no_hp }}</td>
                                    <td>{{ $item->institusi }}</td>
                                    <td>{{ $item->jabatan }}</td>
                                    <td>{{ $item->hadir }}</td>
                                    <td>{{ $item->absen }}</td>
                                </tr>
                                @php($no++)
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                    <a href="{{ route('laporanbsn.index') }}" class="btn">Kembali</a>
                </div>

            </div>
        </div>
    </div>
@endsection