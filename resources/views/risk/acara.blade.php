@extends('risk.layout.home')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('./assets/images/banner/profile.jpg') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Lembar Konfirmasi Kehadiran Peserta
                        </h3>
                        <h6 class="sub-title">Launching Perpustakaan Digital, Launching Ikatan Dosen Pasar Modal Indonesia (IDPMI), Peresmian Ikatan Alumni TICMI</h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-lgrey">
        <!-- Container -->
        <div class="container">

            <!-- Form Begins -->
            {{ Form::open(['url'=>route('konfirmasi.acara')]) }}

            <div class="row course-single content-box bg-google-red shadow ">
                <div class="col-md-6 col-md-offset-3">
                    <div class="title-container text-left sm typo-light">
                        <div class="title-wrap nomargin" style="padding-bottom: 0;">
                            <h4 class="title typo-light">Data Pribadi</h4>
                            <div class="">
                                <ul>
                                    <li>Form konfirmasi hanya berlaku untuk 1 (satu) orang</li>
                                    <li>Acara ini tidak dipungut biaya apapun</li>
                                </ul>
                            </div>
                        </div>
                    </div><!-- Name -->
                @include('errors.list')
                @include('flash::message')
                <!-- Field 1 -->
                    <div class="input-text form-group">
                        {{--<input type="text" name="nama" class="input-name form-control" autocomplete='off' required placeholder="Nama Lengkap" />--}}
                        {{ Form::text('nama',null,['class'=>'form-control','placeholder'=>'Nama Lengkap','autocomplete'=>'off']) }}

                    </div>
                    <!-- Field 2 -->
                    <div class="input-email form-group">
                        {{--<input type="text" name="alamat" class="input-alamat form-control" autocomplete='off' required placeholder="Alamat"/>--}}
                        {{ Form::text('alamat',null,['class'=>'form-control','placeholder'=>'Alamat','autocomplete'=>'off']) }}
                    </div>
                    <!-- Field 3 -->
                    <div class="input-email form-group">
                        {{--<input type="email" name="email" class="input-email form-control" autocomplete='off' required placeholder="Email"/>--}}
                        {{ Form::email('email',null,['class'=>'form-control','placeholder'=>'Email','autocomplete'=>'off']) }}
                    </div>
                    <!-- Field 4 -->
                    <div class="input-email form-group">
                        {{--<input type="text" name="no_hp" class="input-phone form-control" autocomplete='off' required placeholder="Nomor HP"/>--}}
                        {{ Form::text('no_hp',null,['class'=>'form-control','placeholder'=>'No HP','autocomplete'=>'off']) }}
                    </div>
                    <!-- Field 5 -->
                    <div class="input-institusi form-group">
                        {{--<input type="text" name="institusi" class="input-institusi form-control" autocomplete='off' required placeholder="Institusi"/>--}}
                        {{ Form::text('institusi',null,['class'=>'form-control','placeholder'=>'Institusi','autocomplete'=>'off']) }}
                    </div>

                    <!--div class="input-email form-group">
                      <p style="color:white">Bersedia menghadiri acara Launching Perpustakaan Digital & Launching Ikatan Dosen Pasar Modal Indonesia (IDPMI) yang akan diselenggarakan pada Rabu, 4 Oktober 2017. Mohon untuk submit lembar konfirmasi paling lambat Jumat, 25 September 2017 </p>
                    </div>
                    <!-- Button -->

                    <button class="btn" data-toggle="loading" type="submit"><i class="fa fa-save" style="font-size: 14px;color: #fff;"></i> Submit</button>
                </div><!-- Column -->

            </div>


            {{ Form::close() }}
        </div>
    </div>
@endsection