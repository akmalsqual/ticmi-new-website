@extends("risk.layout.home")

@section('content')

    <!-- Section -->
    <section class="pad-none hero">
        <div class="owl-carousel"
             data-animatein=""
             data-animateout=""
             data-items="1" data-margin=""
             data-loop="true"
             data-merge="true"
             data-nav="false"
             data-dots="false"
             data-stagepadding=""
             data-mobile="0"
             data-tablet="0"
             data-desktopsmall="1"
             data-desktop="1"
             data-autoplay="true"
             data-delay="8000"
             data-navigation="true" data-autoheight="false">
            <div class="item">
                <img src="{{ url('assets/images/risk/header-1.jpg') }}" alt="" class="img-responsive" height="550" width="2000">
                <div class="container slider-content vmiddle text-center">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3 class="text-uppercase animated" data-animate="fadeInUp" data-animation-delay="1200">PELATIHAN SNI ISO 31000 Manajemen Risiko</h3>
                            <p class="animated" data-animate="fadeInUp" data-animation-delay="1700">SNI ISO 31000 Manajemen Risiko dapat digunakan sebagai referensi dalam penerapan manajemen risiko secara enterprise untuk mendukung Governance atau tata kelola perusahaan, baik di sektor keuangan dan pasar modal, maupun berbagai sektor industri lainnya secara nasional.</p>
                            <p class="animated" data-animate="fadeInUp" data-animation-delay="2300"><a href="{{ route('pelatihan.daftar') }}" class="btn hero-btn">Silahkan Daftar disini</a></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item">
                <img src="{{ url('assets/images/risk/header-1.jpg') }}" alt="" class="img-responsive" height="550" width="2000">
                <div class="container slider-content vmiddle text-center">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3 class="text-uppercase animated" data-animate="fadeInUp" data-animation-delay="1200">PELATIHAN SNI ISO 31000 Manajemen Risiko</h3>
                            <p class="animated" data-animate="fadeInUp" data-animation-delay="1700">SNI ISO 31000 Manajemen Risiko dapat digunakan sebagai referensi dalam penerapan manajemen risiko secara enterprise untuk mendukung Governance atau tata kelola perusahaan, baik di sektor keuangan dan pasar modal, maupun berbagai sektor industri lainnya secara nasional.</p>
                            <p class="animated" data-animate="fadeInUp" data-animation-delay="2300"><a href="{{ route('pelatihan.daftar') }}" class="btn hero-btn">Silahkan Daftar disini</a></p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section><!-- Section -->

    <!-- Section -->
    <section id="latarbelakang" class="typo-dark">
        <div class="container">
            <div class="row visible-xs">
                <div class="col-sm-12 text-center">
                    <a href="{{ route('pelatihan.daftar') }}" class="btn">Silahkan Daftar Disini</a>
                </div>
                <br>
            </div>
            <!-- Row -->
            <div class="row">
                <!-- Title -->
                <div class="col-sm-12">
                    <div class="title-container sm">
                        <div class="title-wrap">
                            <h3 class="title">Latar Belakang</h3>
                            <span class="separator line-separator"></span>
                        </div>
                    </div>
                </div>
                <!-- Title -->
            </div>

            <div class="row">
                <!-- Column -->
                <div class="col-sm-12">
                    <!-- Content Box -->
                    <div class="content-box icon-box box-size-sm shadow active">
                        <!-- Content Wraper -->
                        <div class="content-wrap">
                            <p>
                                Keberadaaan Standardisasi Manajemen Risiko di level nasional sampai saat ini belum dikelola dengan intensif, dan masih bersifat umum,
                                fundamental dan sebatas merujuk pada standar internasional yang telah ada, yaitu ISO 31000 <em>Risk Management — Principles and Guidelines</em>.
                                Meskipun standar tersebut saat ini telah diadopsi menjadi SNI ISO 31000 <em>Manajemen Risiko – Prinsip dan Panduan</em>, dengan metode adopsi identik
                                secara <em>Republikasi-Reprint</em>.
                            </p>
                            <p>
                                Akan tetapi lahirnya SNI ISO 31000 sebagai suatu dokumen nasional tidak akan memberikan kontribusi optimal bagi bangsa dan negara bila tidak disertai
                                suatu usaha untuk mensosialisasikan standar tersebut sehingga dikenal oleh berbagai lapisan masyarakat, diterima secara terbuka sebagai suatu alat bantu
                                untuk tetap berdaya saing, dan dapat dijadikan pertimbangan dan rujukan bagi para regulator dalam pembuatan peraturan-peraturan yang terkait dengan bagaimana
                                sebaiknya manajemen risiko diterapkan secara efektif, baik di lembaga pemerintahan itu sendiri atau di industri yang menjadi tanggung jawab mereka misal
                                industri jasa keuangan, industri kesehatan, industri perhubungan, industri maritim, BUMN, dan lain sebagainya.
                            </p>
                            <p>
                                Dalam rangka kegiatan Bulan Mutu Nasional (BMN), dimana periode ini dijadikan sebagai momentum peningkatan produktivitas dan mutu nasional, Badan Standardisasi Nasional (BSN),
                                PT Bursa Efek Indonesia (BEI), dan The Indonesian Capital Market Institute (TICMI) menyelengarakan kegiatan Seminar SNI ISO 31000 Manajemen Risiko.
                            </p>
                        </div><!-- Content Wraper -->
                    </div><!-- Content Box -->
                </div><!-- Column -->

            </div><!-- Row -->

        </div><!-- Container -->
    </section><!-- Section -->

    <!-- Section -->
    <section id="agenda" class="bg-dark-red typo-dark">
        <div class="container">
            <!-- Row -->
            <div class="row">
                <!-- Title -->
                <div class="col-sm-12">
                    <div class="title-container sm">
                        <div class="title-wrap typo-light">
                            <h3 class="title">Detail Kegiatan</h3>
                            <span class="separator line-separator"></span>
                        </div>
                    </div>
                </div>
                <!-- Title -->
            </div>

            <div class="row">
                <!-- Title -->
                <div class="col-sm-12">
                    <div class="content-box bg-white box-size-sm shadow active">
                        <!-- Content Wraper -->
                        <div class="content-wrap">

                            <!-- LOKASI -->
                            <div class="title-container sm white bg-google-red">
                                <div class="title-wrap typo-light" style="padding-bottom: 20px;">
                                    <h5 class="title text-info" style="padding-top: 5px;">Lokasi</h5>
                                    <span class="separator line-separator" style="bottom: 10px;"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2" style="vertical-align: top;"><i class="fa fa-map-marker" style="font-size: 20px; width: 20px;"></i> Tempat </div>
                                <div class="col-sm-10">Ruang Seminar 1, 2, 3 dan Auditorium, PT Bursa Efek Indonesia, Gedung Bursa Efek Indonesia, Tower 2, Lantai 1 SCBD Jl. Jend. Sudirman Kav 52-53 Jakarta Selatan 12190, Indonesia</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2"><i class="fa fa-calendar-o" style="font-size: 20px; width: 20px;"></i> Tanggal </div>
                                <div class="col-sm-10">Senin, 21 November 2016</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2"><i class="fa fa-clock-o" style="font-size: 20px; width: 20px;"></i> Waktu </div>
                                <div class="col-sm-10">
                                    <table>
                                        <tr>
                                            <td>1. Registrasi Sesi I &nbsp; </td>
                                            <td> : 08:30 - 09:00</td>
                                        </tr>
                                        <tr>
                                            <td>2. Pembukaan dan Sambutan &nbsp; </td>
                                            <td> : 09:00 - 09:30</td>
                                        </tr>
                                        <tr>
                                            <td>3. Pelatihan Sesi I </td>
                                            <td> : 09:30 - 11:30</td>
                                        </tr>
                                        <tr>
                                            <td>4. Registrasi Sesi II &nbsp; </td>
                                            <td> : 13:00 - 13:30</td>
                                        </tr>
                                        <tr>
                                            <td>5. Pelatihan Sesi II </td>
                                            <td> : 13:30 - 15:30</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- LOKASI END -->
                            <br>
                            <!-- PEMBICARA -->
                            <div class="title-container sm white bg-google-red">
                                <div class="title-wrap typo-dark" style="padding-bottom: 20px;">
                                    <h5 class="title" style="padding-top: 5px;">Pembicara</h5>
                                    <span class="separator line-separator" style="bottom: 10px;"></span>
                                </div>
                            </div>
                            <!-- Team Container -->
                            <div class="container">
                                <div class="row">
                                    <!-- Column -->
                                    <div class="col-sm-4">
                                        <!-- Content Box -->
                                        <div class="content-box  icon-inline text-center">
                                            <!-- Icon Wraper -->
                                            <div class="icon-wrap">
                                                <i class="fa uni-business-man"></i>
                                            </div><!-- Icon Wraper -->
                                            <!-- Content Wraper -->
                                            <div class="content-wrap">
                                                <h4 class="heading">Arif Budiman ST, CRGP, CERG</h4>
                                                <p>CSUL Finance</p>
                                            </div><!-- Content Wraper -->
                                        </div><!-- Content Box -->
                                    </div><!-- Column -->

                                    <!-- Column -->
                                    <div class="col-sm-4">
                                        <!-- Content Box -->
                                        <div class="content-box  icon-inline text-center">
                                            <!-- Icon Wraper -->
                                            <div class="icon-wrap">
                                                <i class="fa uni-business-woman"></i>
                                            </div><!-- Icon Wraper -->
                                            <!-- Content Wraper -->
                                            <div class="content-wrap">
                                                <h4 class="heading">Dr. Miryam Lilian Wijaya</h4>
                                                <p>UNPAR</p>
                                            </div><!-- Content Wraper -->
                                        </div><!-- Content Box -->
                                    </div><!-- Column -->
                                    <!-- Column -->
                                    <div class="col-sm-4">
                                        <!-- Content Box -->
                                        <div class="content-box  icon-inline text-center">
                                            <!-- Icon Wraper -->
                                            <div class="icon-wrap">
                                                <i class="fa uni-business-man"></i>
                                            </div><!-- Icon Wraper -->
                                            <!-- Content Wraper -->
                                            <div class="content-wrap">
                                                <h4 class="heading">Bernado Agustono Mochtar SE, AAAIK, ANZIF, ERMCP, CRMP, CIPMP</h4>
                                                <p>ABM Investama</p>
                                            </div><!-- Content Wraper -->
                                        </div><!-- Content Box -->
                                    </div><!-- Column -->
                                </div><!-- Row -->

                                <div class="row">
                                    <!-- Column -->
                                    <div class="col-sm-4">
                                        <!-- Content Box -->
                                        <div class="content-box  icon-inline text-center">
                                            <!-- Icon Wraper -->
                                            <div class="icon-wrap">
                                                <i class="fa uni-business-man"></i>
                                            </div><!-- Icon Wraper -->
                                            <!-- Content Wraper -->
                                            <div class="content-wrap">
                                                <h4 class="heading">Nursepdal Verliandri S.Si, MSM, ERMCP</h4>
                                                <p>LPS</p>
                                            </div><!-- Content Wraper -->
                                        </div><!-- Content Box -->
                                    </div><!-- Column -->

                                    <!-- Column -->
                                    <div class="col-sm-4">
                                        <!-- Content Box -->
                                        <div class="content-box  icon-inline text-center">
                                            <!-- Icon Wraper -->
                                            <div class="icon-wrap">
                                                <i class="fa uni-business-man"></i>
                                            </div><!-- Icon Wraper -->
                                            <!-- Content Wraper -->
                                            <div class="content-wrap">
                                                <h4 class="heading">Charles Reinier Vorst, ERMCP</h4>
                                                <p>IRMAPA</p>
                                            </div><!-- Content Wraper -->
                                        </div><!-- Content Box -->
                                    </div><!-- Column -->
                                    <!-- Column -->
                                    <div class="col-sm-4">
                                        <!-- Content Box -->
                                        <div class="content-box  icon-inline text-center">
                                            <!-- Icon Wraper -->
                                            <div class="icon-wrap">
                                                <i class="fa uni-business-man"></i>
                                            </div><!-- Icon Wraper -->
                                            <!-- Content Wraper -->
                                            <div class="content-wrap">
                                                <h4 class="heading">Roy Urich, ST, MM, CRMP</h4>
                                                <p>SUCOFINDO</p>
                                            </div><!-- Content Wraper -->
                                        </div><!-- Content Box -->
                                    </div><!-- Column -->
                                </div><!-- Row -->

                                <div class="row">
                                    <!-- Column -->
                                    <div class="col-sm-4">
                                        <!-- Content Box -->
                                        <div class="content-box  icon-inline text-center">
                                            <!-- Icon Wraper -->
                                            <div class="icon-wrap">
                                                <i class="fa uni-business-man"></i>
                                            </div><!-- Icon Wraper -->
                                            <!-- Content Wraper -->
                                            <div class="content-wrap">
                                                <h4 class="heading">Mohammad Mukhlis ST, MT</h4>
                                                <p>BEI</p>
                                            </div><!-- Content Wraper -->
                                        </div><!-- Content Box -->
                                    </div><!-- Column -->

                                    <!-- Column -->
                                    <div class="col-sm-4">
                                        <!-- Content Box -->
                                        <div class="content-box  icon-inline text-center">
                                            <!-- Icon Wraper -->
                                            <div class="icon-wrap">
                                                <i class="fa uni-business-man"></i>
                                            </div><!-- Icon Wraper -->
                                            <!-- Content Wraper -->
                                            <div class="content-wrap">
                                                <h4 class="heading">Johan Candra, ST, ERMCP, CRMP</h4>
                                                <p>XL AXIATA</p>
                                            </div><!-- Content Wraper -->
                                        </div><!-- Content Box -->
                                    </div><!-- Column -->

                                    <!-- Column -->
                                    <div class="col-sm-4">
                                        <!-- Content Box -->
                                        <div class="content-box  icon-inline text-center">
                                            <!-- Icon Wraper -->
                                            <div class="icon-wrap">
                                                <i class="fa uni-business-man"></i>
                                            </div><!-- Icon Wraper -->
                                            <!-- Content Wraper -->
                                            <div class="content-wrap">
                                                <h4 class="heading">Prof. Dr. D. S. Priyarsono, ERMCP, CERG</h4>
                                                <p>IPB</p>
                                            </div><!-- Content Wraper -->
                                        </div><!-- Content Box -->
                                    </div><!-- Column -->

                                    <!-- Column -->
                                    <div class="col-sm-4">
                                        <!-- Content Box -->
                                        <div class="content-box  icon-inline text-center">
                                            <!-- Icon Wraper -->
                                            <div class="icon-wrap">
                                                <i class="fa uni-business-man"></i>
                                            </div><!-- Icon Wraper -->
                                            <!-- Content Wraper -->
                                            <div class="content-wrap">
                                                <h4 class="heading">Ridwan Hendra, MM, ERMCP</h4>
                                                <p>IRMAPA</p>
                                            </div><!-- Content Wraper -->
                                        </div><!-- Content Box -->
                                    </div><!-- Column -->

                                    <!-- Column -->
                                    <div class="col-sm-4">
                                        <!-- Content Box -->
                                        <div class="content-box  icon-inline text-center">
                                            <!-- Icon Wraper -->
                                            <div class="icon-wrap">
                                                <i class="fa uni-business-woman"></i>
                                            </div><!-- Icon Wraper -->
                                            <!-- Content Wraper -->
                                            <div class="content-wrap">
                                                <h4 class="heading">Wening Kusharjani, SE, MM</h4>
                                                <p>KPEI</p>
                                            </div><!-- Content Wraper -->
                                        </div><!-- Content Box -->
                                    </div><!-- Column -->

                                    <!-- Column -->
                                    <div class="col-sm-4">
                                        <!-- Content Box -->
                                        <div class="content-box  icon-inline text-center">
                                            <!-- Icon Wraper -->
                                            <div class="icon-wrap">
                                                <i class="fa uni-business-woman"></i>
                                            </div><!-- Icon Wraper -->
                                            <!-- Content Wraper -->
                                            <div class="content-wrap">
                                                <h4 class="heading">Dwi Sulistyorini Amidjono, SE, MM</h4>
                                                <p>TICMI</p>
                                            </div><!-- Content Wraper -->
                                        </div><!-- Content Box -->
                                    </div><!-- Column -->

                                    <!-- Column -->
                                    <div class="col-sm-4">
                                        <!-- Content Box -->
                                        <div class="content-box  icon-inline text-center">
                                            <!-- Icon Wraper -->
                                            <div class="icon-wrap">
                                                <i class="fa uni-business-man"></i>
                                            </div><!-- Icon Wraper -->
                                            <!-- Content Wraper -->
                                            <div class="content-wrap">
                                                <h4 class="heading">Kristian Manulang</h4>
                                                <p>BEI</p>
                                            </div><!-- Content Wraper -->
                                        </div><!-- Content Box -->
                                    </div><!-- Column -->

                                    <!-- Column -->
                                    <div class="col-sm-4">
                                        <!-- Content Box -->
                                        <div class="content-box  icon-inline text-center">
                                            <!-- Icon Wraper -->
                                            <div class="icon-wrap">
                                                <i class="fa uni-business-woman"></i>
                                            </div><!-- Icon Wraper -->
                                            <!-- Content Wraper -->
                                            <div class="content-wrap">
                                                <h4 class="heading">Hilda Rossieta, PhD</h4>
                                                <p>-</p>
                                            </div><!-- Content Wraper -->
                                        </div><!-- Content Box -->
                                    </div><!-- Column -->

                                </div><!-- Row -->
                            </div>
                            <!-- PEMBICARA END -->

                            <!-- KELAS -->
                            <div class="title-container sm white bg-google-red">
                                <div class="title-wrap typo-dark" style="padding-bottom: 20px;">
                                    <h5 class="title" style="padding-top: 5px;">Kelas</h5>
                                    <span class="separator line-separator" style="bottom: 10px;"></span>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr class="success">
                                        <th width="20%">Gugus Kerja Komisi Teknis</th>
                                        <th width="50%">Segmen Peserta</th>
                                        {{--<th width="30%">Narasumber</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="info">
                                        <td>Pasar Modal</td>
                                        <td>Anggota Bursa, SRO dan Afiliasi</td>
                                        {{--<td>--}}
                                            {{--<ol>--}}
                                                {{--<li>Bapak Mukhlis (BEI)</li>--}}
                                                {{--<li>Ibu Wening (KPEI)</li>--}}
                                                {{--<li>Bapak Munawar (OJK)</li>--}}
                                                {{--<li>Bapak Krisitan Manulang (BEI)</li>--}}
                                            {{--</ol>--}}
                                        {{--</td>--}}
                                    </tr>
                                    <tr>
                                        <td>Keuangan</td>
                                        <td>Perbankan, Perasuransian, Pengelola Dana Pensiun, Lembaga Pembiayaan, Pergadaian, Lembaga Penjaminan.</td>
                                        {{--<td>--}}
                                            {{--<ol>--}}
                                                {{--<li>Bapak Bernardo</li>--}}
                                                {{--<li>Bapak Arif Budiman</li>--}}
                                                {{--<li>Bapak Hidayat Prabowo</li>--}}
                                            {{--</ol>--}}
                                        {{--</td>--}}
                                    </tr>
                                    <tr class="info">
                                        <td>Pendidikan</td>
                                        <td>Universitas Negeri dan Universitas Swasta.</td>
                                        {{--<td>--}}
                                        {{--<ol>--}}
                                        {{--<li>Bapak D.S. Priyatsono</li>--}}
                                        {{--<li>Ibu Miriam</li>--}}
                                        {{--<li>Ibu Hilda</li>--}}
                                        {{--</ol>--}}
                                        {{--</td>--}}
                                    </tr>
                                    <tr>
                                        <td>Telekomunikasi - IT</td>
                                        <td>Perusahaan IT, Penyelenggara Jaringan Komunikasi, Penyelenggara Jasa Komunikasi, Penyelenggara Telekomunikasi Khusus.</td>
                                        {{--<td>--}}
                                            {{--<ol>--}}
                                                {{--<li>Bapak Johan Chandra</li>--}}
                                                {{--<li>Bapak Charles</li>--}}
                                            {{--</ol>--}}
                                        {{--</td>--}}
                                    </tr>
                                    <tr class="info">
                                        <td>Tambang Mineral</td>
                                        <td>Pemilik Izin Usaha Pertambangan, Pemilik Usaha Pertambangan Operasi Produksi, perorangan yang berhubungan dengan usaha pertambangan (Rakyat, Khusus, Eksplorasi; Sesuai dengan UU No 6 Tahun 1999).</td>
                                        {{--<td>--}}
                                            {{--<ol>--}}
                                                {{--<li>Bapak Roy</li>--}}
                                                {{--<li>Bapak Ridwan Hendra</li>--}}
                                            {{--</ol>--}}
                                        {{--</td>--}}
                                    </tr>
                                    <tr>
                                        <td>Infrastruktur</td>
                                        <td>Penanggung Jawab Proyek Kerjasama, Badan Usaha dalam Peyediaan Infrastruktur.</td>
                                        {{--<td>--}}
                                            {{--<ol>--}}
                                                {{--<li>Bapak Bernado</li>--}}
                                                {{--<li>Bapak Arif Budiman</li>--}}
                                            {{--</ol>--}}
                                        {{--</td>--}}
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- KELAS END -->

                            <!-- SUSUNAN ACARA -->
                            <div class="title-container sm white bg-google-red">
                                <div class="title-wrap typo-dark" style="padding-bottom: 20px;">
                                    <h5 class="title" style="padding-top: 5px;">Susunan Acara</h5>
                                    <span class="separator line-separator" style="bottom: 10px;"></span>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr class="success">
                                        <th>Jam</th>
                                        <th>Kegiatan</th>
                                        <th>Lokasi</th>
                                        <th>Kelas</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="info">
                                        <td>08:30 - 09:00</td>
                                        <td>Registrasi Sesi I</td>
                                        <td>
                                            <ol>
                                                <li>Auditorium</li>
                                                <li>Ruang Seminar 1</li>
                                                <li>Ruang Seminar 2</li>
                                                <li>Ruang Seminar 3</li>
                                            </ol>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>09:00 - 09:30</td>
                                        <td>Pembukaan & Sambutan</td>
                                        <td>Ruang Seminar 1,2 & 3</td>
                                        <td></td>
                                    </tr>
                                    <tr class="info">
                                        <td>09:30 - 11:30</td>
                                        <td>Pelatihan Sesi I SNI ISO 31000 Manajemen Risiko</td>
                                        <td>
                                            <ol>
                                                <li>Auditorium</li>
                                                <li>Ruang Seminar 1</li>
                                                <li>Ruang Seminar 2</li>
                                                <li>Ruang Seminar 3</li>
                                            </ol>
                                        </td>
                                        <td>
                                            <ol>
                                                <li>Pasar Modal</li>
                                                <li>Pendidikan Tinggi</li>
                                                <li>Keuangan</li>
                                                <li>Tambang Mineral</li>
                                            </ol>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>13:00 - 13:30</td>
                                        <td>Registrasi Sesi II</td>
                                        <td>
                                            <ol>
                                                <li>Ruang Seminar 1</li>
                                                <li>Ruang Seminar 2</li>
                                                <li>Ruang Seminar 3</li>
                                            </ol>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr class="info">
                                        <td>13:30 - 15:30</td>
                                        <td>Pelatihan Sesi II SNI ISO 31000 Manajemen Risiko</td>
                                        <td>
                                            <ol>
                                                <li>Ruang Seminar 1</li>
                                                <li>Ruang Seminar 2</li>
                                                <li>Ruang Seminar 3</li>
                                            </ol>
                                        </td>
                                        <td>
                                            <ol>
                                                <li>Telekomunikasi - IT</li>
                                                <li>Infrastruktur</li>
                                                <li>Pasar Modal</li>
                                            </ol>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <!-- SUSUNAN ACARA END -->

                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <a href="{{ route('pelatihan.daftar') }}" class="btn btn-lg">Silahkan Daftar Disini</a>
                                </div>
                            </div>

                        </div><!-- Content Wraper -->
                    </div>
                </div>
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Section -->
@endsection


