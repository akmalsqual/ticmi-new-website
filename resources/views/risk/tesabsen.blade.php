@extends('risk.layout.home')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('./assets/images/banner/profile.jpg') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Registrasi Pelatihan SNI ISO 31000 Manajemen Risiko</h3>
                        <h6 class="sub-title"></h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-lgrey">
        <!-- Container -->
        <div class="container">
            <div class="row course-single content-box bg-white shadow ">
                @include('errors.list')
                @include('flash::message')
                <div class="col-sm-12 text-center">
                    @if($peserta)
                        <div class="row">
                            @if(!empty($peserta->kelas))
                                <div class="col-sm-6 col-sm-offset-3">
                                    <table class="table table-bordered table-striped table-condensed">
                                        <tbody>
                                        <tr>
                                            <td>Nama</td>
                                            <td>:</td>
                                            <td>{{ $peserta->nama }}</td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td>:</td>
                                            <td>{{ $peserta->email }}</td>
                                        </tr>
                                        <tr>
                                            <td>No HP</td>
                                            <td>:</td>
                                            <td>{{ $peserta->no_hp }}</td>
                                        </tr>
                                        <tr>
                                            <td>Institusi</td>
                                            <td>:</td>
                                            <td>{{ $peserta->institusi }}</td>
                                        </tr>
                                        <tr>
                                            <td>Jabatan</td>
                                            <td>:</td>
                                            <td>{{ $peserta->jabatan }}</td>
                                        </tr>
                                        <tr>
                                            <td>Kelas</td>
                                            <td>:</td>
                                            <td>{{ $peserta->kelas }}</td>
                                        </tr>
                                        <tr>
                                            <td>Kehadiran</td>
                                            <td>:</td>
                                            <td>{{ $peserta->hadir }}</td>
                                        </tr>
                                        <tr>
                                            <td>Absen</td>
                                            <td>:</td>
                                            <td>{{ $peserta->absen }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    @if(empty($peserta->absen))
                                        <a href="" class="btn btn-lg btn-block tesabsenPelatihanBsn" data-url="{{ url('tesabsen/'.$peserta->email.'/store') }}" data-loading-text="Loading">Absen</a>
                                    @endif
                                </div>
                            @endif
                        </div>
                    @else
                        <div class="alert alert-warning" role="alert">
                            <strong>Warning!</strong> Maaf data dengan akun email {{ $email }} tidak Kami temukan. Silahkan cek kembali penulisan email
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection