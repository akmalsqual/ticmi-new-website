@extends('risk.layout.home')
@section('content')
    @php
        /** @var  $totalPeserta $totalPeserta = $pendidikan->count() + $keuangan->count() + $tambang->count() + $pasarmodal1->count() +
                        $telekom->count() + $infrastruktur->count() + $pasarmodal2->count(); */
        $totalPesertaSesi1 = $pendidikan->count() + $keuangan->count() + $tambang->count() + $pasarmodal1->count();
        $totalPesertaSesi2 = $telekom->count() + $infrastruktur->count() + $pasarmodal2->count();
        $totalPeserta = $totalPesertaSesi1 + $totalPesertaSesi2;

        $pendidikanHadir = $pendidikan->count() - $pendidikan->where('absen',null)->count();
        $keuanganHadir = $keuangan->count() - $keuangan->where('absen',null)->count();
        $tambangHadir = $tambang->count() - $tambang->where('absen',null)->count();
        $pasarmodal1Hadir = $pasarmodal1->count() - $pasarmodal1->where('absen',null)->count();
        $telekomHadir = $telekom->count() - $telekom->where('absen',null)->count();
        $infrastrukturHadir = $infrastruktur->count() - $infrastruktur->where('absen',null)->count();
        $pasarmodal2Hadir = $pasarmodal2->count() - $pasarmodal2->where('absen',null)->count();

        $totalHadirSesi1 = $pendidikanHadir + $keuanganHadir + $tambangHadir + $pasarmodal1Hadir;
        $totalHadirSesi2 = $telekomHadir + $infrastrukturHadir + $pasarmodal2Hadir;

    @endphp
    <div class="page-default bg-white">
        <!-- Container -->
        <div class="container">
            <div class="row">

                <div class="title-container text-left sm">
                    <div class="title-wrap">
                        <h5 class="title">Laporan Peserta Pelatihan SNI ISO 31000 Manajemen Risiko</h5>
                        <span class="separator line-separator"></span>
                    </div>
                </div><!-- Name -->
                <div class="col-sm-12 text-center">
                    <h5>Total Jumlah Pendaftar ({{ date('d F Y') }}) : {{ $totalPeserta }} Orang</h5>
                    <h5>Peserta yang sudah absen : {{ $pesertaAbsen->count() }} Orang</h5>
                </div>

                <div class="col-sm-6 col-sm-offset-3">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Kelas</th>
                            <th>Pendaftar</th>
                            <th>Hadir</th>
                            <th>Persentase</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="info">
                            <td colspan="4" class="text-center"><strong>SESI I</strong></td>
                        </tr>
                        <tr>
                            <td>Pendidikan</td>
                            <td class="text-center">{{ $pendidikan->count() }}</td>
                            <td class="text-center">{{ $pendidikanHadir }}</td>
                            <td class="text-center">{{ number_format(($pendidikanHadir * 100) / $pendidikan->count(),2) }} %</td>
                        </tr>
                        <tr>
                            <td>Keuangan</td>
                            <td class="text-center">{{ $keuangan->count() }}</td>
                            <td class="text-center">{{ $keuanganHadir }}</td>
                            <td class="text-center">{{ number_format(($keuanganHadir * 100 ) / $keuangan->count(),2) }} %</td>
                        </tr>
                        <tr>
                            <td>Tambang Mineral</td>
                            <td class="text-center">{{ $tambang->count() }}</td>
                            <td class="text-center">{{ $tambangHadir }}</td>
                            <td class="text-center">{{ number_format(($tambangHadir * 100) / $tambang->count(),2 ) }} %</td>
                        </tr>
                        <tr>
                            <td>Pasar Modal 1</td>
                            <td class="text-center">{{ $pasarmodal1->count() }}</td>
                            <td class="text-center">{{ $pasarmodal1Hadir }}</td>
                            <td class="text-center">{{ number_format(($pasarmodal1Hadir * 100) / $pasarmodal1->count(),2 ) }} %</td>
                        </tr>
                        <tr class="warning">
                            <td class="text-right"><strong>Total Sesi I</strong></td>
                            <td class="text-center">{{ $totalPesertaSesi1 }}</td>
                            <td class="text-center">{{ $totalHadirSesi1 }}</td>
                            <td class="text-center">{{ number_format(($totalHadirSesi1 * 100) / $totalPesertaSesi1,2) }} %</td>
                        </tr>
                        <tr class="info">
                            <td colspan="4" class="text-center"><strong>SESI II</strong></td>
                        </tr>
                        <tr>
                            <td>Telekomunikasi IT</td>
                            <td class="text-center">{{ $telekom->count() }}</td>
                            <td class="text-center">{{ $telekom->count() - $telekom->where('absen',null)->count() }}</td>
                            <td class="text-center">{{ number_format((($telekom->count() - $telekom->where('absen',null)->count()) * 100) / $telekom->count(),2) }} %</td>
                        </tr>
                        <tr>
                            <td>Infrastruktur</td>
                            <td class="text-center">{{ $infrastruktur->count() }}</td>
                            <td class="text-center">{{ $infrastruktur->count() - $infrastruktur->where('absen',null)->count() }}</td>
                            <td class="text-center">{{ number_format((($infrastruktur->count() - $infrastruktur->where('absen',null)->count()) * 100) / $infrastruktur->count(),2) }} %</td>
                        </tr>
                        <tr>
                            <td>Pasar Modal 2</td>
                            <td class="text-center">{{ $pasarmodal2->count() }}</td>
                            <td class="text-center">{{ $pasarmodal2->count() - $pasarmodal2->where('absen',null)->count() }}</td>
                            <td class="text-center">{{ number_format((($pasarmodal2->count() - $pasarmodal2->where('absen',null)->count()) * 100) / $pasarmodal2->count(),2) }} %</td>
                        </tr>
                        <tr class="warning">
                            <td class="text-right"><strong>Total Sesi II</strong></td>
                            <td class="text-center">{{ $totalPesertaSesi2 }}</td>
                            <td class="text-center">{{ $totalHadirSesi2 }}</td>
                            <td class="text-center">{{ number_format(($totalHadirSesi2 * 100) / $totalPesertaSesi2,2) }} %</td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr class="success">
                            <td class="text-right"><strong>TOTAL KESELURUHAN</strong></td>
                            <td class="text-center"><strong>{{ $totalPeserta }}</strong></td>
                            <td class="text-center"><strong>{{ $pesertaAbsen->count() }}</strong></td>
                            <td class="text-center">{{ number_format(($pesertaAbsen->count() * 100) / $totalPeserta,2) }} %</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <br>
        </div> <!-- Container -->
    </div>


@endsection