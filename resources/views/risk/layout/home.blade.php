@extends('risk.layout.base')
@include('risk.layout.header')

@section('contentWrapper')

    @yield('content')

@endsection