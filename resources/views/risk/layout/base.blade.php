<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Basic -->

    <title>
        The Indonesia Capital Market Institute -
        @if(isset($pageTitle) && !empty($pageTitle))
            {{ $pageTitle }}
        @else
            Pusat Edukasi dan Data Pasar Modal Indonesia
        @endif
    </title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.png">
    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic" rel="stylesheet" type="text/css">

    <!-- Lib CSS -->
<!--     <link rel="stylesheet" href="./assets/css/lib/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/lib/animate.min.css">
    <link rel="stylesheet" href="./assets/css/lib/font-awesome.min.css">
    <link rel="stylesheet" href="./assets/css/lib/univershicon.css">
    <link rel="stylesheet" href="./assets/css/lib/owl.carousel.css">
    <link rel="stylesheet" href="./assets/css/lib/prettyPhoto.css">
    <link rel="stylesheet" href="./assets/css/lib/menu.css">
    <link rel="stylesheet" href="./assets/css/lib/timeline.css"> -->

    <link rel="stylesheet" href="{{ url('assets/css/lib/libs.css') }}">

    <!-- Revolution CSS -->
    <link rel="stylesheet" href="{{ url('assets/css/settings.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/layers.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/navigation.css') }}">


    <link rel="stylesheet" href="{{ url('assets/css/style.css') }}">
    <!-- Theme CSS -->
    <!-- <link rel="stylesheet" href="assets/css/theme.css"> -->
    <!-- <link rel="stylesheet" href="assets/css/theme-responsive.css"> -->

    <!--[if IE]>
    <link rel="stylesheet" href="{{ url('assets/css/ie.css') }}">
    <![endif]-->

    <!-- Head Libs -->
    <script src="{{ url('assets/js/lib/modernizr.js') }}"></script>

    <!-- Skins CSS -->
    <!-- <link rel="stylesheet" href="./assets/css/default.css"> -->

    <!-- Theme Custom CSS -->
    <!-- <link rel="stylesheet" href="./assets/css/custom.css"> -->
</head>
<body class="one-page" data-target=".single-menu" data-spy="scroll" data-offset="200">
<!-- Page Loader -->
{{--<div id="pageloader" style="">--}}
    {{--<div class="loader-inner" style="">--}}
        {{--<img src="{{ url('assets/images/default/logo-ticmi.jpg') }}" alt="" width="250">--}}
        {{--<img src="{{ url('assets/images/preloader.gif') }}" alt="">--}}
    {{--</div>--}}
{{--</div>--}}
<!-- Page Loader -->


<!-- Back to top -->
<a href="#0" class="cd-top cd-is-visible">Top</a>

@yield('header')

<!-- Page Main -->
<div role="main" class="main">
    @yield('contentWrapper')

<!-- Section -->
    <section class="pad-tb-40">
    <div class="container">
        <div class="row">
            <!-- Column -->
            <div class="col-sm-9 col-sm-offset-3">
                <div class="callto-action text-center">
                    {{--<div class="widget no-box text-center" style="margin: 0 auto;">--}}
                        {{--<ul class="sro-widget">--}}
                            {{--<li style="float: left;">--}}
                                {{--<div class="thumb-wrap">--}}
                                    {{--<a href="http://www.bsn.go.id" target="_blank" title="Badan Standarisasi Nasional"><img width="100" height="60" src="{{ url('assets/images/risk/logo/Logo-BSNI.png') }}" class="img-responsive" alt="Badan Standarisasi Nasional" style="margin-top: 30px;"></a>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                            {{--<li style="float: left;">--}}
                                {{--<div class="thumb-wrap">--}}
                                    {{--<a href="http://idx.co.id" target="_blank" title="IDX BEI"><img width="60" height="60" src="{{ url('assets/images/partner/idx.png') }}" class="img-responsive" alt="IDX" style="width: 90px;"></a>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                            {{--<li style="float: left;">--}}
                                {{--<div class="thumb-wrap">--}}
                                    {{--<a href="http://yuknabungsaham.idx.co.id" target="_blank" title="Yuk Nabung Saham"><img width="60" height="60" src="{{ url('assets/images/risk/logo/Yuk-Nabung-Saham.png') }}" class="img-responsive" alt="Yuk Nabung Saham" style="width: 60px;"></a>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                            {{--<li style="float: left;">--}}
                                {{--<div class="thumb-wrap">--}}
                                    {{--<a href="http://ticmi.co.id" target="_blank" title="TICMI"><img width="60" height="60" src="{{ url('assets/images/default/logo-ticmi.png') }}" class="img-responsive" alt="TICMI" style="width: 120px;margin-top: 15px"></a>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                        {{--</ul><!-- Thumbnail Widget -->--}}
                    {{--</div><!-- Widget -->--}}

                    <!--<h5 class="title"><span class="call-title-text"><span class="call-title-text">Need a copy of this beautiful Html5 so what are you waiting?</span></span><a href="http://glorythemes.in/html/universh/demo/index.html#" class="btn">Buy now</a></h5>-->
                </div>
            </div><!-- Column -->
        </div><!-- row -->
    </div><!-- Container -->
</section><!-- Section -->
</div><!-- Page Main -->

<!-- Footer -->
<footer id="footer" class="footer-1">
    <!-- Footer Copyright -->
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <!-- Copy Right Logo -->
                <!--<div class="col-md-2">-->
                <!--<a class="logo" href="#">-->
                <!--<img src="./assets/images/default/logo-ticmi.jpg" width="120" height="0" class="img-rounded img-responsive" alt="Universh Education HTML5 Website Template">-->
                <!--</a>-->
                <!--</div>-->
                <!-- Copy Right Logo -->
                <!-- Copy Right Content -->
                <div class="col-md-9">
                    <p>© Copyright 2016. All Rights Reserved. | By <a href="http://ticmi.co.id" title="TICMI">The Indonesia Capital Market Electronic Library</a></p>
                </div><!-- Copy Right Content -->
                <!-- Copy Right Content -->
                <div class="col-md-3">
                    <nav class="sm-menu text-center">
                        {{--<ul>--}}
                            {{--<li><a href="{{ url('faq') }}">FAQ's</a></li>--}}
                            {{--<li><a href="#">Sitemap</a></li>--}}
                            {{--<li><a href="{{ url('hubungi-kami') }}">Hubungi Kami</a></li>--}}
                        {{--</ul>--}}
                    </nav><!-- Nav -->
                </div><!-- Copy Right Content -->
            </div><!-- Footer Copyright -->
        </div><!-- Footer Copyright container -->
    </div><!-- Footer Copyright -->
</footer>
<!-- Footer -->

<input type="hidden" name="base_url" class="base_url" value="{!! url('/') !!}">
<input type="hidden" name="_token" class="_token" value="{!! csrf_token() !!}">
<!-- library -->
<script src="{{ url('assets/js/lib/libs.js') }}"></script>
<script src="https://cdn.jsdelivr.net/scrollreveal.js/3.2.0/scrollreveal.min.js"></script>

<!-- Revolution Js -->
<script src="{{ url('assets/js/lib/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ url('assets/js/lib/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ url('assets/js/lib/theme-rs.js') }}"></script>
<!-- Theme Base, Components and Settings -->
<script src="{{ url('assets/js/theme.js') }}"></script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-78360717-1', 'auto');
    ga('send', 'pageview');

</script>
<script type="text/javascript">
    var clicky_site_ids = clicky_site_ids || [];
    clicky_site_ids.push(100957363);
    (function() {
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = '//static.getclicky.com/js';
        ( document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0] ).appendChild( s );
    })();
</script>

<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100957363ns.gif" /></p></noscript>
</body></html>