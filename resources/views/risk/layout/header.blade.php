@section("header")
    <!-- Header Begins -->
    <header id="header" class="default-header colored flat-menu">
        <div class="header-top">
            <div class="container">
                <nav>
                    <ul class="nav nav-pills nav-top">
                        <li class="phone">
                            <span><i class="fa fa-envelope"></i> <a href="mailto:info@ticmi.co.id?subject=Hello From Website">info@ticmi.co.id</a> </span>
                        </li>
                        <li class="phone">
                            <span><i class="fa fa-phone"></i>(021) 515 - 2318</span>
                        </li>
                    </ul>
                </nav>
                <ul class="social-icons color">
                    <li class="facebook"><a href="http://www.facebook.com/TICMI.ID" target="_blank" title="Facebook">Facebook</a></li>
                    <li class="twitter"><a href="https://twitter.com/TICMI_ID" target="_blank" title="Twitter">Twitter</a></li>
                    <li class="youtube"><a href="https://www.youtube.com/channel/UCBGMquAIsG_LVYYbMtb3rdQ" target="_blank" title="Youtube">Youtube</a></li>
                    <li class="instagram"><a href="https://www.instagram.com/ticmi_id" target="_blank" title="Instagram">Instagram</a></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="logo">
                <a href="{{ url('/') }}">
                    <img alt="TICMI" width="103" height="60" data-sticky-width="95" data-sticky-height="55" data-sticky-padding="20" src="{{ url('assets/images/default/logo-ticmi.png') }}">
                </a>
            </div>
            <button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse"><i class="fa fa-bars"></i></button>
        </div>

        <div class="navbar-collapse nav-main-collapse collapse">
            <div class="container">
                <nav class="nav-main mega-menu">
                    <ul class="nav nav-pills nav-main" id="mainMenu">
                        <li class="active">
                            <a class="disabled active sticky-menu-active" href=""> Home </a>
                        </li>
                        {{--<li class="">--}}
                            {{--<a class="" data-hash href="#latarbelakang">Latar Belakang</a>--}}
                        {{--</li>--}}
                        {{--<li class=" mega-menu-item mega-menu-fullwidth">--}}
                            {{--<a class="" data-hash href="#agenda">Detail Kegiatan</a>--}}
                        {{--</li>--}}
                        {{--<li class="">--}}
                            {{--<a href="{{ route('pelatihan.daftar') }}" class="">Pendaftaran</a>--}}
                        {{--</li>--}}
                    </ul>


                </nav>
            </div>
        </div>
    </header><!-- Header Ends -->
@endsection