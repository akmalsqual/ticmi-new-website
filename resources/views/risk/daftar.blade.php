@extends('risk.layout.home')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('./assets/images/banner/profile.jpg') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Pendaftaran Pelatihan SNI ISO 31000 Manajemen Risiko</h3>
                        <h6 class="sub-title"></h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-lgrey">
        <!-- Container -->
        <div class="container">

            <!-- Form Begins -->
            {{ Form::open(['url'=>route('pelatihan.daftar')]) }}

            <div class="row course-single content-box bg-google-red shadow ">

                <div class="col-md-6 col-md-offset-3">
                    <div class="title-container text-left sm typo-light">
                        <div class="title-wrap">
                            <h4 class="title typo-light">Data Pribadi</h4>
                            <span class="separator line-separator"></span>
                        </div>
                    </div><!-- Name -->
                        @include('errors.list')
                        @include('flash::message')
                        <!-- Field 1 -->
                        <div class="input-text form-group">
                            {{--<input type="text" name="contact_name" class="input-name form-control" placeholder="Nama Lengkap" />--}}
                            {{ Form::text('nama',null,['class'=>'form-control','placeholder'=>'Nama Lengkap']) }}
                        </div>
                        <!-- Field 2 -->
                        <div class="input-email form-group">
                            {{--<input type="email" name="contact_email" class="input-email form-control" placeholder="Email"/>--}}
                            {{ Form::email('email',null,['class'=>'form-control','placeholder'=>'Email']) }}
                        </div>
                        <!-- Field 3 -->
                        <div class="input-email form-group">
                            {{--<input type="text" name="contact_phone" class="input-phone form-control" placeholder="Nomor HP"/>--}}
                            {{ Form::text('no_hp',null,['class'=>'form-control','placeholder'=>'No HP']) }}
                        </div>
                        <!-- Field 3 -->
                        <div class="input-email form-group">
                            {{--<input type="text" name="contact_phone" class="input-phone form-control" placeholder="Institusi (Perusahaan / Universitas / Instansi)"/>--}}
                            {{ Form::text('institusi',null,['class'=>'form-control','placeholder'=>'Institusi (Perusahaan / Universitas / Instansi)']) }}
                        </div>
                        <!-- Field 3 -->
                        <div class="input-email form-group">
                            {{--<input type="text" name="contact_phone" class="input-phone form-control" placeholder="Posisi / Jabatan"/>--}}
                            {{ Form::text('jabatan',null,['class'=>'form-control','placeholder'=>'Jabatan / Posisi']) }}
                        </div>
                        <!-- Button -->
                        {{--<button class="btn" type="submit">Send Now <i class="fa fa-paper-plane" style="font-size: 14px;color: #fff;"></i></button>--}}
                </div><!-- Column -->

            </div>
            <br>
            <div class="row course-single content-box bg-white shadow ">

                <div class="col-md-6 col-md-offset-3">
                    <div class="title-container text-left sm">
                        <div class="title-wrap">
                            <h4 class="title typo-light">Pemilihan Kelas</h4>
                            <span class="separator line-separator"></span>
                        </div>

                        <div role="alert" class="alert alert-info typo-dark">
                            <strong>Informasi</strong> Anda hanya bisa memilih 1 (satu) Kelas di Sesi I atau  Sesi II
                        </div>

                    </div><!-- Name -->

                    <h5>Sesi I ( 09:30 - 11:30 )</h5>
                    <div class="radio">
                        <label>
                            @if($pendidikan && $pendidikan->count() >= 100)
                                {{ Form::radio('kelas','Pendidikan (Sesi I, Ruang Seminar 1)',null,['disabled'=>'disabled']) }}
                                Pendidikan (Ruang Seminar 1) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                            @else
                                {{ Form::radio('kelas','Pendidikan (Sesi I, Ruang Seminar 1)',null,['class'=>'']) }}
                                Pendidikan (Ruang Seminar 1) <strong>({{ (100 - $pendidikan->count()) }} Kursi Tersisa)</strong>
                            @endif
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            @if($keuangan && $keuangan->count() >= 100)
                                {{ Form::radio('kelas','Keuangan (Sesi I, Ruang Seminar 2)',null,['disabled'=>'disabled']) }}
                                Keuangan (Ruang Seminar 2) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                            @else
                                {{ Form::radio('kelas','Keuangan (Sesi I, Ruang Seminar 2)',null,['class'=>'']) }}
                                Keuangan (Ruang Seminar 2) <strong>({{ (100 - $keuangan->count()) }} Kursi Tersisa)</strong>
                            @endif
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            @if($tambang && $tambang->count() >= 100)
                                {{ Form::radio('kelas','Tambang Mineral (Sesi I, Ruang Seminar 3)',null,['disabled'=>'disabled']) }}
                                Tambang Mineral (Ruang Seminar 3) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                            @else
                                {{ Form::radio('kelas','Tambang Mineral (Sesi I, Ruang Seminar 3)',null,['class'=>'']) }}
                                Tambang Mineral (Ruang Seminar 3) <strong>({{ (100 - $tambang->count()) }} Kursi Tersisa)</strong>
                            @endif
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            @if($pasarmodal1 && $pasarmodal1->count() >= 100)
                                {{ Form::radio('kelas','Pasar Modal (Sesi I, Ruang Auditorium)',null,['disabled'=>'disabled']) }}
                                Pasar Modal (Ruang Auditorium) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                            @else
                                {{ Form::radio('kelas','Pasar Modal (Sesi I, Ruang Auditorium)',null,['class'=>'']) }}
                                Pasar Modal (Ruang Auditorium) <strong>({{ (100 - $pasarmodal1->count()) }} Kursi Tersisa)</strong>
                            @endif
                        </label>
                    </div>
                    <h5>Sesi II ( 13:30 - 15:30 )</h5>
                    <div class="radio">
                        <label>
                            @if($telekom && $telekom->count() >= 100)
                                {{ Form::radio('kelas','Telekomunikasi IT (Sesi II, Ruang Seminar 1)',null,['disabled'=>'disabled']) }}
                                Telekomunikasi IT (Ruang Seminar 1) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                            @else
                                {{ Form::radio('kelas','Telekomunikasi IT (Sesi II, Ruang Seminar 1)',null,['class'=>'']) }}
                                Telekomunikasi IT (Ruang Seminar 1) <strong>({{ (100 - $telekom->count()) }} Kursi Tersisa)</strong>
                            @endif
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            @if($infrastruktur && $infrastruktur->count() >= 100)
                                {{ Form::radio('kelas','Infrastruktur (Sesi II, Ruang Seminar 2)',null,['disabled'=>'disabled']) }}
                                Infrastruktur (Ruang Seminar 2) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                            @else
                                {{ Form::radio('kelas','Infrastruktur (Sesi II, Ruang Seminar 2)',null,['class'=>'']) }}
                                Infrastruktur (Ruang Seminar 2) <strong>({{ (100 - $infrastruktur->count()) }} Kursi Tersisa)</strong>
                            @endif
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            @if($pasarmodal2 && $pasarmodal2->count() >= 100)
                                {{ Form::radio('kelas','Pasar Modal (Sesi II, Ruang Seminar 3)',null,['disabled'=>'disabled']) }}
                                Pasar Modal (Ruang Seminar 3) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                            @else
                                {{ Form::radio('kelas','Pasar Modal (Sesi II, Ruang Seminar 3)',null,['class'=>'']) }}
                                Pasar Modal (Ruang Seminar 3) <strong>({{ (100 - $pasarmodal2->count()) }} Kursi Tersisa)</strong>
                            @endif
                        </label>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-4">
                            <br>
                            <button class="btn btn-loading" data-toggle="loading" data-loading-text="Loading" type="submit">Daftar <i class="fa fa-paper-plane" style="font-size: 14px;color: #fff;"></i></button>
                        </div>
                    </div>
                </div><!-- Column -->

            </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection