@extends('risk.layout.home')
@section('content')
    @php
        $totalPeserta = $pendidikan->count() + $keuangan->count() + $tambang->count() + $pasarmodal1->count() +
                        $telekom->count() + $infrastruktur->count() + $pasarmodal2->count();
    @endphp
    <div class="page-default bg-white">
        <!-- Container -->
        <div class="container">
            <div class="row">
                @include('errors.list')
                @include('flash::message')

                <div class="title-container text-left sm">
                    <div class="title-wrap">
                        <h5 class="title">Laporan Peserta Pelatihan SNI ISO 31000 Manajemen Risiko</h5>
                        <span class="separator line-separator"></span>
                    </div>
                </div><!-- Name -->
                <div class="col-sm-12 text-center">
                    <h5>Total Jumlah Pendaftar ({{ date('d F Y') }}) : {{ $totalPeserta }} Orang</h5>
                    <h5>Peserta yang sudah absen : {{ $pesertaAbsen->count() }} Orang</h5>
                </div>

                <div class="col-sm-3">
                    <!-- Count Block -->
                    <a href="{{ route('laporanbsn.detail',['kelas'=>'pendidikan']) }}">
                        <div class="count-block dark bg-yellow">
                            <h5>Pendidikan ({{ $pendidikan->count() - $pendidikan->where('absen',null)->count() }})</h5>
                            <h3 class="count-number" data-count="{{ $pendidikan->count() }}" data-speed="200"><span class="counter"></span></h3>
                        </div><!-- Counter Block -->
                    </a>
                </div><!-- Column -->
                <div class="col-sm-3">
                    <!-- Count Block -->
                    <a href="{{ route('laporanbsn.detail',['kelas'=>'keuangan']) }}">
                        <div class="count-block dark bg-green">
                            <h5>Keuangan ({{ $keuangan->count() - $keuangan->where('absen',null)->count() }})</h5>
                            <h3 class="count-number" data-count="{{ $keuangan->count() }}" data-speed="200"><span class="counter"></span></h3>
                        </div><!-- Counter Block -->
                    </a>
                </div><!-- Column -->
                <div class="col-sm-3">
                    <!-- Count Block -->
                    <a href="{{ route('laporanbsn.detail',['kelas'=>'tambang']) }}">
                        <div class="count-block dark bg-pink">
                            <h5>Tambang Mineral ({{ $tambang->count() - $tambang->where('absen',null)->count()}})</h5>
                            <h3 class="count-number" data-count="{{ $tambang->count() }}" data-speed="200"><span class="counter"></span></h3>
                        </div><!-- Counter Block -->
                    </a>
                </div><!-- Column -->
                <div class="col-sm-3">
                    <!-- Count Block -->
                    <a href="{{ route('laporanbsn.detail',['kelas'=>'pasarmodal1']) }}">
                        <div class="count-block dark bg-voilet">
                            <h5>Pasar Modal 1 ({{ $pasarmodal1->count() - $pasarmodal1->where('absen',null)->count() }})</h5>
                            <h3 class="count-number" data-count="{{ $pasarmodal1->count() }}" data-speed="200"><span class="counter"></span></h3>
                        </div><!-- Counter Block -->
                    </a>
                </div><!-- Column -->

            </div>
            <br>
            <div class="row clearfix">

                <div class="col-sm-4">
                    <!-- Count Block -->
                    <a href="{{ route('laporanbsn.detail',['kelas'=>'telekom']) }}">
                        <div class="count-block dark bg-green">
                            <h5>Telekomunikasi IT ({{ $telekom->count() - $telekom->where('absen',null)->count() }})</h5>
                            <h3 class="count-number" data-count="{{ $telekom->count() }}" data-speed="200"><span class="counter"></span></h3>
                        </div><!-- Counter Block -->
                    </a>
                </div><!-- Column -->
                <div class="col-sm-4">
                    <!-- Count Block -->
                    <a href="{{ route('laporanbsn.detail',['kelas'=>'infrastruktur']) }}">
                        <div class="count-block dark bg-pink">
                            <h5>Infrastruktur {{ $infrastruktur->count() - $infrastruktur->where('absen',null)->count() }}</h5>
                            <h3 class="count-number" data-count="{{ $infrastruktur->count() }}" data-speed="200"><span class="counter"></span></h3>
                        </div><!-- Counter Block -->
                    </a>
                </div><!-- Column -->
                <div class="col-sm-4">
                    <!-- Count Block -->
                    <a href="{{ route('laporanbsn.detail',['kelas'=>'pasarmodal2']) }}">
                        <div class="count-block dark bg-voilet">
                            <h5>Pasar Modal 2 ({{ $pasarmodal2->count() - $pasarmodal2->where('absen',null)->count()}})</h5>
                            <h3 class="count-number" data-count="{{ $pasarmodal2->count() }}" data-speed="200"><span class="counter"></span></h3>
                        </div><!-- Counter Block -->
                    </a>
                </div><!-- Column -->

            </div>
        </div> <!-- Container -->
    </div>


@endsection