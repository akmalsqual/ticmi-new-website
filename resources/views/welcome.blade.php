@extends("layout.home")
@include('layout.googlesnippet')
@section('content')
        <!-- Home Slider -->
        <div class="rs-container light rev_slider_wrapper">
            <div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options='{"delay": 9000, "gridwidth": 1170, "gridheight": 220}'>
                <ul>
                    {{--<li data-transition="fade" class="typo-dark heavy">--}}
                        {{--<img src="./assets/images/banner/banner-schroders2.jpg"--}}
                             {{--alt=""--}}
                             {{--data-bgposition="top center"--}}
                             {{--data-bgfit="cover"--}}
                             {{--data-bgrepeat="no-repeat"--}}
                             {{--class="rev-slidebg">--}}
                    {{--</li>--}}
                    <li data-transition="fade" class="typo-dark heavy">
                        <img src="./assets/images/banner/banner_2.jpg"
                             alt=""
                             data-bgposition="top center"
                             data-bgfit="cover"
                             data-bgrepeat="no-repeat"
                             class="rev-slidebg">

                        <div class="tp-caption big-text"
                             style="background: rgba(0,0,0,.5);width: 600px;height: 100px;"
                             data-x="left" data-hoffset="67"
                             data-y="top" data-voffset="30"
                             data-start="590"
                             data-transform_in="y:[-300%];opacity:0;s:500;">&nbsp;</div>

                        <div class="tp-caption sm-text"
                             data-x="left" data-hoffset="70"
                             data-y="95" data-voffset="-70"
                             data-start="600"
                             data-transform_in="y:[-300%];opacity:0;s:500;"><span class="color-white">Pusat Edukasi Pasar Modal Indonesia</span></div>

                        <div class="tp-caption big-text"
                             data-x="left" data-hoffset="70"
                             data-y="center" data-voffset="-50"
                             data-start="1500"
                             data-whitespace="nowrap"
                             data-transform_in="y:[100%];s:500;"
                             data-transform_out="opacity:0;s:500;"
                             data-mask_in="x:0px;y:0px;"><span class="color-white">Pelatihan & Sertifikasi</span></div>

                        <!--                    <div class="tp-caption sm-text"-->
                        <!--                         data-x="left" data-hoffset="67"-->
                        <!--                         data-y="135"-->
                        <!--                         data-start="2000"-->
                        <!--                         data-transform_in="y:[100%];opacity:0;s:500;"><a href="#" class="btn">Lihat Semua Program Kami</a></div>-->
                    </li>

                    <li data-transition="fade" class="typo-dark heavy">

                        <img src="./assets/images/banner/banner_3.jpg"
                             alt=""
                             data-bgposition="center center"
                             data-bgfit="cover"
                             data-bgrepeat="no-repeat"
                             class="rev-slidebg">

                        <div class="tp-caption big-text"
                             style="background: rgba(0,0,0,.5);width: 925px;height: 130px;"
                             data-x="right" data-hoffset="67"
                             data-y="top" data-voffset="15"
                             data-start="590"
                             data-transform_in="y:[-300%];opacity:0;s:500;">&nbsp;</div>

                        <div class="tp-caption md-text"
                             data-x="right" data-hoffset="70"
                             data-y="top" data-voffset="10"
                             data-start="500"
                             data-whitespace="nowrap"
                             data-transform_in="y:[100%];s:500;"
                             data-transform_out="opacity:0;s:500;"
                             data-mask_in="x:0px;y:0px;"><span class="color-white">Promo Diskon 20% Program Reguler WPPE dan WMI</span>
                        </div>

                        <div class="tp-caption sm-text"
                             data-x="right" data-hoffset="70"
                             data-y="top" data-voffset="70"
                             data-start="1000"
                             data-whitespace="nowrap"
                             data-transform_in="y:[100%];s:500;"
                             data-transform_out="opacity:0;s:500;"
                             data-mask_in="x:0px;y:0px;"><span class="color-white">Segera daftarkan diri Anda untuk mendapatkan Promo Diskon 20% untuk <br/> Program Reguler WPPE dan WMI untuk 10 pendaftar pertama.</span>
                        </div>

                        <!--                    <div class="tp-caption big-text"-->
                        <!--                         data-x="right" data-hoffset="67"-->
                        <!--                         data-y="top" data-voffset="130"-->
                        <!--                         data-start="2000"-->
                        <!--                         data-whitespace="nowrap"-->
                        <!--                         data-transform_in="y:[100%];s:500;"-->
                        <!--                         data-transform_out="opacity:0;s:500;"-->
                        <!--                         data-mask_in="x:0px;y:0px;"><a href="#" class="btn">View All Courses</a>-->
                        <!--                    </div>-->

                    </li>

                    <li data-transition="fade" class="typo-dark heavy">
                        <img src="./assets/images/banner/banner_1.jpg"
                             alt=""
                             data-bgposition="center center"
                             data-bgfit="cover"
                             data-bgrepeat="no-repeat"
                             class="rev-slidebg">

                        <div class="tp-caption big-text"
                             style="background: rgba(0,0,0,.5);width: 900px;height: 120px;"
                             data-x="left" data-hoffset="67"
                             data-y="top" data-voffset="10"
                             data-start="590"
                             data-transform_in="y:[-300%];opacity:0;s:500;">&nbsp;</div>

                        <div class="tp-caption md-text"
                             data-x="left" data-hoffset="70"
                             data-y="0" data-voffset="30"
                             data-start="1500"
                             data-whitespace="nowrap"
                             data-transform_in="y:[-300%];s:500;"
                             data-transform_out="opacity:0;s:500;"
                             data-mask_in="x:0px;y:0px;"><span class="color-white">Unduh Data Pasar Modal </span></div>

                        <div class="tp-caption sm-text"
                             data-x="left" data-hoffset="70"
                             data-y="60"
                             data-start="500"
                             data-transform_in="y:[-300%];opacity:0;s:500;"><span class="color-white">Anda dapat mengunduh data pasar modal seperti LapKeu dan Prospectus <br/> dengan gratis jika Anda memiliki SID.</span></div>


                        <div class="tp-caption sm-text"
                             data-x="left" data-hoffset="67"
                             data-y="160"
                             data-start="2000"
                             data-transform_in="y:[100%];opacity:0;s:500;"><a href="https://ticmi.co.id/register" class="btn"><span class="color-white">Silahkan registrasi disini.</span></a></div>
                    </li>
                </ul>
            </div>
        </div><!-- Home Slider -->



        <!-- Section -->
        <section class="slider-below-section" style="">
            <div class="container">
                <div class="slider-below-wrap bg-color typo-light">
                    <div class="row">

                        <!-- Column -->
                        <div class="col-sm-3">
                            <!-- Content Box -->
                            <div class="content-box text-center">
                                <!-- Icon Wraper -->
                                <div class="icon-wrap">
                                    <a href="http://library.ticmi.co.id" onclick="ga('send', 'event', 'Banner Menu', 'perpustakaan', '{{ url()->current() }}' );" target="_blank"><i class="uni-address-book"></i></a>
                                </div><!-- Icon Wraper -->
                                <!-- Content Wraper -->
                                <div class="content-wrap">
                                    <a href="http://library.ticmi.co.id" onclick="ga('send', 'event', 'Banner Menu', 'perpustakaan', '{{ url()->current() }}' );" target="_blank"><h5 class="heading">Perpustakaan</h5></a>
                                    <p>Perpustakaan Pasar Modal yang berisi buku-buku untuk pengetahuan dan belajar Pasar Modal.</p>
                                </div><!-- Content Wraper -->
                            </div><!-- Content Box -->
                        </div><!-- Column -->

                        <!-- Column -->
                        <div class="col-sm-3">
                            <!-- Content Box -->
                            <div class="content-box text-center">
                                <!-- Icon Wraper -->
                                <div class="icon-wrap">
                                    <a href="https://ticmi.co.id" onclick="ga('send', 'event', 'Banner Menu', 'sertifikasi', '{{ url()->current() }}' );"><i class="uni-medal-3"></i></a>
                                </div><!-- Icon Wraper -->
                                <!-- Content Wraper -->
                                <div class="content-wrap">
                                    <a href="https://ticmi.co.id" onclick="ga('send', 'event', 'Banner Menu', 'sertifikasi', '{{ url()->current() }}' );"><h5 class="heading">Sertifikasi Profesi</h5></a>
                                    <p>Sertifikasi Profesi WPPE, WMI dan WPEE untuk pelaku pasar modal</p>
                                </div><!-- Content Wraper -->
                            </div><!-- Content Box -->
                        </div><!-- Column -->

                        <!-- Column -->
                        <div class="col-sm-3">
                            <!-- Content Box -->
                            <div class="content-box text-center">
                                <!-- Icon Wraper -->
                                <div class="icon-wrap">
                                    <a href="{{ route('member.simulasisaham') }}" onclick="ga('send', 'event', 'Banner Menu', 'simulasi-saham', '{{ url()->current() }}' );"><i class="uni-line-chart4"></i></a>
                                </div><!-- Icon Wraper -->
                                <!-- Content Wraper -->
                                <div class="content-wrap">
                                    <a href="{{ route('member.simulasisaham') }}" onclick="ga('send', 'event', 'Banner Menu', 'simulasi-saham', '{{ url()->current() }}' );"><h5 class="heading">Online Trading Simulation</h5></a>
                                    <p>Simulasi Perdagangan Saham untuk investor pemula dan calon investor</p>
                                </div><!-- Content Wraper -->
                            </div><!-- Content Box -->
                        </div><!-- Column -->

                        <!-- Column -->
                        <div class="col-sm-3">
                            <!-- Content Box -->
                            <div class="content-box text-center">
                                <!-- Icon Wraper -->
                                <div class="icon-wrap">
                                    <a href="{{ route('member.suratriset') }}" onclick="ga('send', 'event', 'Banner Menu', 'surat-riset', '{{ url()->current() }}' );"><i class="uni-letter-open"></i></a>
                                </div><!-- Icon Wraper -->
                                <!-- Content Wraper -->
                                <div class="content-wrap">
                                    <a href="{{ route('member.suratriset') }}" onclick="ga('send', 'event', 'Banner Menu', 'surat-riset', '{{ url()->current() }}' );"><h5 class="heading">Permohonan Surat Riset</h5></a>
                                    <p>Untuk mahasiswa yang ingin skripsi dengan data yang ada di TICMI.</p>
                                </div><!-- Content Wraper -->
                            </div><!-- Content Box -->
                        </div><!-- Column -->

                    </div><!-- Slider Below Wrapper -->
                </div><!-- Row -->
            </div><!-- Container -->
        </section><!-- Section -->

        <!-- Section -->
        <section class="bg-lgrey typo-dark" style="padding: 20px 0;">
            <div class="container" style="padding: 0 15px ; margin: 0;max-width: 1200px;">
                <div class="shadow active bg-white typo-dark" style="margin-top: -5px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-sm-12">
                                <div class="title-container sm">
                                    <div class="title-wrap">
                                        <h3 class="title" style="padding-top: 30px;">Program Unggulan Kami</h3>
                                        <span class="separator line-separator  pad-tb-none"></span>
                                    </div>
                                </div>
                            </div>
                            <!-- Search -->
                            <div class="search margin-top-30">
                                <form id="formSearch" class="light form-horizontal" action="" method="post">
                                    <div class="form-group">
                                        <div class=" col-sm-2 col-sm-offset-2">
                                            <select name="cabang" id="" class="form-control">
                                                <option value="">Pilih Kota</option>
                                                @foreach($cabang as $item)
                                                    <option value="{{ $item->nama }}">{{ $item->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class=" col-sm-2">
                                            <select name="bulan" id="" class="form-control">
                                                <option value="">Pilih Bulan</option>
                                                <option value="01">Januari</option>
                                                <option value="02">Februari</option>
                                                <option value="03">Maret</option>
                                                <option value="04">April</option>
                                                <option value="05">Mei</option>
                                                <option value="06">Juni</option>
                                                <option value="07">Juli</option>
                                                <option value="08">Agustus</option>
                                                <option value="09">September</option>
                                                <option value="10">Oktober</option>
                                                <option value="11">November</option>
                                                <option value="12">Desember</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <select name="kd_program" id="" class="form-control">
                                                <option value="">Pilih Program</option>
                                                @foreach($program as $value)
                                                    <option value="{{ $value->id }}">{{ $value->nm_program }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <button class="form-control btn search searchProgram" type="button" autocomplete="off" data-loading-text="<i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i> Loading..."><i class="fa fa-search"></i> CARI</button>
                                        </div>
                                    </div>
                                </form>
                            </div><!-- Search -->
                        </div><!-- Column -->
                    </div><!-- Slider Below Wrapper -->

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="text-center searchResult">
                                <img src="{{ url('assets/images/loading2.gif') }}" style="display: none;" class="loadingSearch" alt="TICMI">
                            </div>

                            <div class="defaultBatch">
                                <!-- Mannualy Created Batch -->
                                {{--<div class="col-sm-3 item-batch">--}}
                                    {{--<div class="event-wrap">--}}
                                        {{--<div class="event-img-wrap course-banner-wrap bg-pin-red color-white text-center" style="">--}}
                                            {{--<img alt="Event" class="img-responsive" src="./assets/images/course/wisuda-ticmi.jpg" width="600" height="220">--}}
                                            {{--<span class="cat bg-yellow">WISUDA</span>--}}
                                        {{--</div>--}}
                                        {{--<!-- Event Detail Wrapper -->--}}
                                        {{--<div class="event-details" style="height: 450px;">--}}
                                            {{--<h4 class="batch-title" style="font-size: 16px;"><a href="{{ url('corsec') }}">Wisuda Lulusan TICMI</a></h4>--}}
                                            {{--<ul class="events-meta">--}}
                                                {{--<li><i class="fa fa-calendar-o"></i>Rabu, 17 Mi 2017</li>--}}
                                                {{--<li><i class="fa fa-map-marker"></i>TICMI Gd. BEI Tower 2 Lt. 1 Jl. Jend. Sudirman Kav. 52-53 SCBD Jakarta Selatan </li>--}}
                                                {{--<li><i class="fa fa-users"></i> 14:30 - 16:30</li>--}}
                                            {{--</ul>--}}
                                            {{--<p>Wisuda Akbar TICMI Periode I untuk semua lulusan TICMI periode November 2016 s/d Maret 2017</p>--}}
                                            {{--<a href="{{ route('wisuda') }}" class="btn btn-lg" style="position: absolute;bottom: 10px;"><i class="fa fa-angle-double-right"></i> Saya ingin daftar</a>--}}
                                        {{--</div><!-- Event Meta -->--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <!-- Mannualy Created Batch End -->

                            <!-- Program Seminar / Workshop -->
                            @if($workshops && $workshops->count() > 0)
                                @foreach($workshops as $workshop)
                                    <div class="col-sm-3 item-batch">
                                        <div class="event-wrap">
                                            <div class="event-img-wrap course-banner-wrap bg-pin-red color-white text-center" style="">
                                                @if($workshop->program->nm_program == "Forum")
                                                    <img alt="Event" class="img-responsive" src="./assets/images/course/forum-course-ticmi.jpg" width="600" height="220">
                                                @else
                                                    <img alt="Event" class="img-responsive" src="./assets/images/course/workshop-umum.jpg" width="600" height="220">
                                                @endif
                                                <span class="cat bg-yellow">{{ $workshop->program->nm_program }}</span>
                                            </div>
                                            <!-- Event Detail Wrapper -->
                                            <div class="event-details" style="height: 450px;">
                                                <h4 class="batch-title" style="font-size: 16px;"><a href="{{ route('pelatihan',['workshopslug'=>$workshop->slugs]) }}" onclick="ga('send', 'event', 'Pendaftaran Workshop', '{{ $workshop->workshop_name }}', '{{ url()->current() }}' )">{{ $workshop->workshop_name }}</a></h4>
                                                <ul class="events-meta">
                                                    <li><i class="fa fa-calendar-o"></i>{{ strftime('%A, %#d %B %Y',strtotime($workshop->tgl_mulai)) }}</li>
                                                    <li><i class="fa fa-map-marker"></i>{{ $workshop->lokasi }}</li>
                                                    <li><i class="fa fa-users"></i> {{ $workshop->waktu }}</li>
                                                </ul>
                                                <p>{!! substr(strip_tags($workshop->deskripsi),0,200) !!}</p>
                                                <a href="{{ route('pelatihan',['workshopslug'=>$workshop->slugs]) }}" onclick="ga('send', 'event', 'Pendaftaran Workshop', '{{ $workshop->workshop_name }}', '{{ url()->current() }}' )" class="btn btn-lg" style="position: absolute;bottom: 10px;"><i class="fa fa-angle-double-right"></i> Saya ingin daftar</a>
                                            </div><!-- Event Meta -->
                                        </div>
                                    </div>
                                @endforeach
                            @endif

                            <div class="clearfix"></div>
                            <!-- Program Sertifikasi -->
                            @if($batch && $batch->count() > 0)
                                @php($no = 1)
                                @foreach($batch as $value)
                                    <!-- Event Column -->
                                    <div class="col-sm-3 item-batch">
                                        <!-- Event Wrapper -->
                                        <div class="event-wrap">
                                            <div class="event-img-wrap course-banner-wrap bg-pin-red color-white text-center" style="">
                                                @if($value->program->nm_program == "WPPE")
                                                    @if($value->kelas->type->nama == "Regular")
                                                        <img alt="Event" class="img-responsive" src="./assets/images/course/wppe-reguler-3.jpg" width="600" height="220">
                                                    @elseif($value->kelas->type->nama == "Waiver")
                                                        <img alt="Event" class="img-responsive" src="./assets/images/course/wppe-waiver-3.jpg" width="600" height="220">
                                                    @else
                                                    @endif
                                                @elseif($value->program->nm_program == "WPPE Pemasaran")
                                                    @if($value->kelas->type->nama == "Regular")
                                                        <img alt="Event" class="img-responsive" src="./assets/images/course/wppe-pemasaran-reguler.jpg" width="600" height="220">
                                                    @elseif($value->kelas->type->nama == "Waiver")
                                                        <img alt="Event" class="img-responsive" src="./assets/images/course/wppe-pemasaran-waiver.jpg" width="600" height="220">
                                                    @else
                                                    @endif
                                                @elseif($value->program->nm_program == "WPPE Pemasaran Terbatas")
                                                        <img alt="Event" class="img-responsive" src="./assets/images/course/wppe-pemasaran-terbatas.jpg" width="600" height="220">
                                                @elseif($value->program->nm_program == "WMI")
                                                    @if($value->kelas->type->nama == "Regular")
                                                        <img alt="Event" class="img-responsive" src="./assets/images/course/wmi-reguler-3.jpg" width="600" height="220">
                                                    @elseif($value->kelas->type->nama == "Waiver")
                                                        <img alt="Event" class="img-responsive" src="./assets/images/course/wmi-waiver-3.jpg" width="600" height="220">
                                                    @else
                                                    @endif
                                                @elseif($value->program->nm_program == "WPEE")
                                                    @if($value->kelas->type->nama == "Regular")
                                                        <img alt="Event" class="img-responsive" src="./assets/images/course/wpee-reguler.jpg" width="600" height="220">
                                                    @elseif($value->kelas->type->nama == "Waiver")
                                                        <img alt="Event" class="img-responsive" src="./assets/images/course/wpee-waiver.jpg" width="600" height="220">
                                                    @else
                                                    @endif
                                                @elseif($value->program->nm_program == "ASPM")
                                                    @if($value->kelas->type->nama == "Regular")
                                                        <img alt="Event" class="img-responsive" src="./assets/images/course/aspm-reguler-3.jpg" width="600" height="220">
                                                    @elseif($value->kelas->type->nama == "Waiver")
                                                        <img alt="Event" class="img-responsive" src="./assets/images/course/aspm-akselerasi-3.jpg" width="600" height="220">
                                                    @else
                                                    @endif
                                                @elseif($value->program->nm_program == "Placement Test ASPM")
                                                    <img alt="Event" class="img-responsive" src="./assets/images/course/aspm-akselerasi-3.jpg" width="600" height="220">
                                                @else
                                                    <img alt="Event" class="img-responsive" src="./assets/images/course/wppe-waiver-3.jpg" width="600" height="220">
                                                @endif

                                                <span class="cat bg-yellow">{{ $value->program->nm_program }}</span>
                                            </div>
                                            <!-- Event Image Wrapper -->
                                            <!-- Event Detail Wrapper -->
                                            <div class="event-details" style="height: 450px;">
                                                <h4 class="batch-title" style="font-size: 16px;"><a href="{{ url('daftar/'.$value->id.'/'.$value->nama) }}" onclick="ga('send', 'event', 'Pendaftaran Sertifikasi', '{{ $value->nama }}', '{{ url()->current() }}' )">{{ $value->nama }} </a></h4>
                                                <ul class="events-meta">
                                                    <li><i class="fa fa-calendar-o"></i> {{ $value->tanggal->formatLocalized('%A, %#d %B %Y') }}</li>
                                                    {{--<li><i class="fa fa-calendar-o"></i> {{ date('l, d M Y',strtotime($value->tanggal)) }}</li>--}}
                                                    <li><i class="fa fa-map-marker"></i> {{ !empty($value->lokasi) ? $value->lokasi:"Gedung Bursa Efek Indonesia" }}</li>
                                                    <li><i class="fa fa-users"></i> Minimal Peserta {{ !empty($value->minimal) ? $value->minimal:10 }} Orang</li>
                                                </ul>
                                                <p>{{ substr($value->keterangan,0,150) }}</p>

                                                <a href="{{ url('daftar/'.$value->id.'/'.$value->nama) }}" class="btn btn-lg" onclick="ga('send', 'event', 'Pendaftaran Sertifikasi', '{{ $value->nama }}', '{{ url()->current() }}' )" style="position: absolute;bottom: 10px;"><i class="fa fa-angle-double-right"></i> Saya ingin daftar</a>

                                                {{--@if($value->kd_type != 1)--}}
                                                    {{--<a href="http://akademik.ticmi.co.id/index/daftar{{ $value->kd_type }}/{{ $value->id }}/Pendaftaran {{ $value->nama }}.html" class="btn">Saya ingin daftar</a>--}}
                                                {{--@else--}}
                                                    {{--<a href="http://akademik.ticmi.co.id/index/daftar/{{ $value->cabang }}/{{ $value->id }}/Pendaftaran {{ $value->nama }}.html" class="btn">Saya ingin daftar</a>--}}
                                                {{--@endif--}}
                                            </div><!-- Event Meta -->
                                            <br/>
                                        </div><!-- Event details -->
                                    </div><!-- Column -->
                                        @if($no%4 == 0)
                                            <div class="clearfix"></div>
                                        @endif
                                    @php($no++)
                                @endforeach
                            @else
                                    <div class="col-sm-12">
                                        <div class="alert alert-warning" role="alert"><h4>Maaf, belum ada jadwal tersedia.</h4></div>
                                    </div>
                            @endif

                            @if($batch && $batch->count() == 0 && $workshops && $workshops->count() == 0)
                            @endif
                            </div>
                        </div>
                        <br>
                    </div>
                </div><!-- Row -->
            </div><!-- Container -->
        </section><!-- Section -->

        {{--<section class="bg-lgrey typo-dark">--}}
            {{--<div class="container">--}}
                {{--<!-- Teacher Column -->--}}

                {{--<!-- Team Row -->--}}
                {{--<div class="row">--}}
                    {{--<!-- Title Block -->--}}
                    {{--<div class="col-md-12">--}}
                        {{--<div class="title-container">--}}
                            {{--<div class="title-wrap">--}}
                                {{--<h4 class="title">Beberapa Pengajar Kami</h4>--}}
                                {{--<span class="separator line-separator"></span>--}}
                            {{--</div>--}}
                        {{--</div><!-- Title Container -->--}}
                    {{--</div><!-- Title Block -->--}}

                    {{--<!-- Team Container -->--}}
                    {{--<div class="team-container small">--}}

                        {{--@if($pengajar->count() > 0)--}}
                            {{--@foreach($pengajar as $item)--}}
                                {{--<div class="col-sm-6 col-md-3 trainer-list">--}}
                                    {{--<!-- Member Wrap -->--}}
                                    {{--<div class="member-wrap">--}}
                                        {{--<!-- Member Image Wrap -->--}}
                                        {{--<div class="member-img-wrap">--}}
                                            {{--<img class="img-responsive" alt="Member" src="http://203.148.84.28/images/{{ explode(':',$item->foto)[0] }}.jpg" style="" width="400" height="500">--}}
                                            {{--<img class="img-responsive" alt="Member" src="{{ url('assets/images/teacher/teacher-01.jpg') }}" width="400" height="500">--}}
                                        {{--</div>--}}
                                        {{--<!-- Member detail Wrapper -->--}}
                                        {{--<div class="member-detail-wrap bg-grey">--}}
                                            {{--<h5 class="member-name"><a href="{{ route('pengajar.view',['id'=>$item->id,'nama'=>str_replace(' ','-',strtolower($item->nama))]) }}">{{ $item->nama }}</a></h5>--}}
                                            {{--<span>{{ $item->jabatan }}</span>--}}
                                            {{--<p>{{ $item->institusi }}</p>--}}
                                            {{--<ul class="social-icons">--}}
                                                {{--@if($item->share_fb)--}}
                                                    {{--<li class="facebook"><a href="{{ $item->share_fb }}" target="_blank" title="Facebook">Facebook</a></li>--}}
                                                {{--@endif--}}
                                                {{--@if($item->share_twitter)--}}
                                                        {{--<li class="twitter"><a href="{{ $item->share_twitter }}" target="_blank" title="Twitter">Twitter</a></li>--}}
                                                {{--@endif--}}
                                                {{--@if($item->share_linkedin)--}}
                                                        {{--<li class="linkedin"><a href="{{ $item->share_linkedin }}" target="_blank" title="Linkedin">Linkedin</a></li>--}}
                                                {{--@endif--}}
                                                {{--@if($item->share_email)--}}
                                                        {{--<li class="mail"><a href="mailto:{{ $item->share_email }}" target="_blank" title="{{ $item->share_email }}">{{ $item->share_email }}</a></li>--}}
                                                {{--@endif--}}
                                            {{--</ul><!-- Member Social -->--}}
                                        {{--</div><!-- Member detail Wrapper -->--}}
                                    {{--</div><!-- Member Wrap -->--}}
                                {{--</div><!-- Column -->--}}
                            {{--@endforeach--}}
                        {{--@endif--}}
                    {{--</div><!-- Team Container -->--}}
                {{--</div><!-- Row -->--}}

                {{--<!-- End Teacher Column -->--}}
                {{--<!-- Divider -->--}}
                {{--<hr class="lg" />--}}
            {{--</div>--}}

        {{--</section>--}}


        <!-- Section -->
        <section class="bg-dark-red typo-light">
            <div class="container">
                <div class="row counter-sm">
                    <!-- Title -->
                    <div class="col-sm-12">
                        <div class="title-container">
                            <div class="title-wrap">
                                <h3 class="title sekilas-title">Sekilas tentang TICMI</h3>
                                <span class="separator line-separator"></span>
                            </div>
                            <p class="description">Berikut beberapa data kami </p>
                        </div>
                    </div>
                    <!-- Title -->
                    <div class="col-sm-6 col-md-3 counter-data">
                        <!-- Count Block -->
                        <div class="count-block bg-white color-dark">
                            <h5>Lulusan WPPE</h5>
                            <h3 data-count="1730" class="count-number"><span class="counter">1730</span></h3>
                            <i class="uni-fountain-pen"></i>
                        </div><!-- Counter Block -->
                    </div><!-- Column -->
                    <div class="col-sm-6 col-md-3 counter-data">
                        <!-- Count Block -->
                        <div class="count-block bg-white color-dark">
                            <h5>Lulusan WPPE PT</h5>
                            <h3 data-count="630" class="count-number"><span class="counter">630</span></h3>
                            <i class="uni-talk-man"></i>
                        </div><!-- Counter Block -->
                    </div><!-- Column -->
                    <div class="col-sm-6 col-md-3 counter-data">
                        <!-- Count Block -->
                        <div class="count-block bg-white color-dark">
                            <h5>Lulusan ASPM</h5>
                            <h3 data-count="78" class="count-number"><span class="counter">78</span></h3>
                            <i class="uni-fountain-pen"></i>
                        </div><!-- Counter Block -->
                    </div><!-- Column -->
                    <div class="col-sm-6 col-md-3 counter-data">
                        <!-- Count Block -->
                        <div class="count-block bg-white color-dark">
                            <h5>Lulusan WMI</h5>
                            <h3 data-count="731" class="count-number"><span class="counter">731</span></h3>
                            <i class="uni-download counter-data"></i>
                        </div><!-- Counter Block -->
                    </div><!-- Column -->
                </div><!-- Row -->
                <br>
                <div class="row counter-sm">
                    <div class="col-sm-6 col-md-4 counter-data">
                        <!-- Count Block -->
                        <div class="count-block bg-white  color-dark">
                            <h5 style="">Lulusan WPPE Pemasaran</h5>
                            <h3 data-count="2011" class="count-number"><span class="counter">2011</span></h3>
                            <i class="uni-medal-3"></i>
                        </div><!-- Counter Block -->
                    </div><!-- Column -->
                    <div class="col-sm-6 col-md-4 counter-data">
                        <!-- Count Block -->
                        <div class="count-block bg-white color-dark">
                            <h5>Total Pengunduh</h5>
                            <h3 data-count="3646" class="count-number"><span class="counter">3646</span></h3>
                            <i class="uni-talk-man"></i>
                        </div><!-- Counter Block -->
                    </div><!-- Column -->
                    <div class="col-sm-6 col-md-4 counter-data">
                        <!-- Count Block -->
                        <div class="count-block bg-white color-dark">
                            <h5>Total Data Diunduh</h5>
                            <h3 data-count="598282" class="count-number"><span class="counter">598282</span></h3>
                            <i class="uni-download counter-data"></i>
                        </div><!-- Counter Block -->
                    </div><!-- Column -->
                </div><!-- Row -->
            </div><!-- Container -->
        </section><!-- Section -->
@endsection


