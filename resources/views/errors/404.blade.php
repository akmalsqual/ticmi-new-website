@extends('layout.home')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('./assets/images/banner/profile.jpg') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Maaf, Halaman tidak ditemukan</h3>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <!-- Blog Detail Wrapper -->
                        <div class="text-center content-box bg-white shadow">
                            <h4><a href="#">The Indonesia Capital Market Institute</a></h4>
                            <p>
                                Halaman yang Anda cari sudah tidak ada atau sudah dipindahkan ke halaman lain.
                            </p>
                            <a href="{{ url('/') }}">Kembali ke halaman utama</a>
                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->


                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Default -->
@endsection