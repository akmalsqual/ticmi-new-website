@extends('layout.base')
@include('layout.header')

@section('contentWrapper')

    @yield('content')

@endsection