@extends('layout.home')
@section('content')

    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('{{ url('assets/images/banner/profile.jpg') }}') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">{{ isset($dashboardTitle) ? $dashboardTitle:"Dashboard" }}</h3>
                        <h4 class="sub-title">Hallo {{ Auth::user()->name }}</h4>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-lgrey team-single typo-dark">
        <!-- Container -->
        <div class="container">
            <div class="row">
                <!-- Sidebar -->
                <div class="col-md-3">
                    <!-- aside -->
                    <aside class="sidebar">
                        <!-- Widget -->
                        <div class="widget">
                            {{--<h5 class="widget-title">Ringkasan Akun<span></span></h5>--}}
                            <ul class="go-widget">
                                <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                                <li><a href="{{ route('member.profile') }}">Profil</a></li>
                                <li><a href="{{ route('member.profile.password') }}">Ubah Password</a></li>
                                <li><a href="{{ route('member.seminar') }}">Seminar & Workshop</a></li>
                                <li><a href="{{ route('member.pembeliandata') }}">Riwayat Pembelian Data</a></li>
                                <li><a href="{{ route('member.konfirmasi') }}">Konfirmasi Pembayaran</a></li>
                                <li><a href="{{ route('member.suratriset') }}">Surat Riset</a></li>
                                <li><a href="{{ route('member.simulasisaham') }}">Online Trading Simulation</a></li>
                                <li><a href="{{ route('logout') }}">Logout</a></li>
                            </ul>
                        </div><!-- Widget -->

                    </aside>
                </div>
                <div class="col-md-9">
                    @yield('member-content')
                </div>
            </div>
        </div>
    </div>
@endsection