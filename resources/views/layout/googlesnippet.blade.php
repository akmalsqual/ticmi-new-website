@section('googlesnippet')

    <!-- Google Structured Data -->
    <script type="application/ld+json">
        {
            "@@context": "https://schema.org",
            "@@type": "Organization",
            "url": "https://ticmi.co.id",
            "description": "TICMI merupakan satu-satunya lembaga pendidikan dan pelatihan pasar modal yang sekaligus menyelenggarakan ujian sertifikasi profesi pasar modal di Indonesia. Seluruh lulusan TICMI diharapkan dapat membantu menciptakan dan mengembangkan pasar modal yang dinamis.",
            "name": "The Indonesia Capital Market Institute",
            "sameAs" : [ "https://www.facebook.com/TICMI.ID",
                        "https://twitter.com/TICMI_ID",
                        "https://www.instagram.com/ticmi_id",
                        "https://www.youtube.com/channel/UCBGMquAIsG_LVYYbMtb3rdQ"]
        }
    </script>
    <script type="application/ld+json">
        {
            "@@context": "https://schema.org",
            "@@type": "LocalBusiness",
            "url": "https://ticmi.co.id",
            "logo": "{{ url('assets/images/default/logo-ticmi.jpg') }}",
            "email":"mailto:info@ticmi.co.id",
            "address": {
                "@@type": "PostalAddress",
                "addressLocality": "Kawasan SCBD",
                "addressRegion": "Jakarta Selatan - DKI Jakarta",
                "postalCode":"12190",
                "streetAddress": "Gedung Bursa Efek Indonesia Tower II Lt 1, Jl. Jend. Sudirman Kav 52 52, Kebayoran Baru, RT.5/RW.3, Senayan, Jakarta Selatan"
            },
            "description": "TICMI merupakan satu-satunya lembaga pendidikan dan pelatihan pasar modal yang sekaligus menyelenggarakan ujian sertifikasi profesi pasar modal di Indonesia. Seluruh lulusan TICMI diharapkan dapat membantu menciptakan dan mengembangkan pasar modal yang dinamis.",
            "name": "The Indonesia Capital Market Institute",
            "telephone": "(021) 515-2318 ",
            "openingHours": "Mo,Tu,We,Th,Fr 08:00-19:00",
            "hasMap": "https://www.google.co.id/maps/place/The+Indonesia+Capital+Market+Institute/@@-6.2234954,106.8084537,15z/data=!4m5!3m4!1s0x0:0x5d818586aa817fd!8m2!3d-6.2234954!4d106.8084537",
            "geo": {
                "@@type": "GeoCoordinates",
                "latitude": "-6.2233945",
                "longitude": "106.8082617"
                },
            "sameAs" : [ "https://www.facebook.com/TICMI.ID",
                        "https://twitter.com/TICMI_ID",
                        "https://www.instagram.com/ticmi_id",
                        "https://www.youtube.com/channel/UCBGMquAIsG_LVYYbMtb3rdQ"],
            "image": {
                "@@context": "https://schema.org",
                "@@type": "ImageObject",
                "author": "TICMI",
                "contentLocation": "Library TICMI",
                "contentUrl": "{{ url('assets/images/blog/1.jpg') }}",
                "url": "{{ url('assets/images/blog/1.jpg') }}",
                "datePublished": "2014-12-25",
                "description": "Ruangan Library Ticmi",
                "name": "Ticmi Library"
            }
        }
    </script>
    <script type="application/ld+json">
    {
      "@@context": "http://schema.org/",
      "@@type": "Product",
      "name": "WPPE Reguler",
      "image": "https://ticmi.co.id/assets/images/course/wppe-reguler-3.jpg",
      "logo": "https://ticmi.co.id/assets/images/course/wppe-reguler-3.jpg",
      "description": "Pelatihan dan Ujian untuk mendapatkan sertifikasi WPPE (Wakil Perantara Pedagang Efek) untuk mengajukan Lisensi WPPE ke OJK",
      "mpn": "wppe-reguler",
      "award": "Lisensi dari OJK",
      "category" : "Sertifikasi > WPPE Reguler",
      "brand": {
        "@type": "Organization",
        "name": "The Indonesia Capital Market Institute (TICMI)"
      },
      "aggregateRating": {
        "@type": "AggregateRating",
        "ratingValue": "4.9",
        "reviewCount": "144"
      },
      "offers": {
        "@type": "Offer",
        "priceCurrency": "IDR",
        "price": "12500000.00",
        "priceValidUntil": "2017-12-31",
        "itemCondition": "http://schema.org/NewCondition",
        "availability": "http://schema.org/InStock",
        "seller": {
          "@type": "Organization",
          "name": "The Indonesia Capital Market Institute (TICMI)"
        }
      }
    }
    </script>

    <script type="application/ld+json">
    {
      "@@context": "http://schema.org/",
      "@@type": "Product",
      "name": "WPPE Waiver",
      "image": "https://ticmi.co.id/assets/images/course/wppe-reguler-3.jpg",
      "logo": "https://ticmi.co.id/assets/images/course/wppe-reguler-3.jpg",
      "description": "Pelatihan dan Ujian untuk mendapatkan sertifikasi WPPE (Wakil Perantara Pedagang Efek) untuk mengajukan Lisensi WPPE ke OJK",
      "mpn": "wppe-pemasaran-reguler",
      "award": "Lisensi dari OJK",
      "category" : "Sertifikasi > WPPE Waiver",
      "brand": {
        "@type": "Organization",
        "name": "The Indonesia Capital Market Institute (TICMI)"
      },
      "aggregateRating": {
        "@type": "AggregateRating",
        "ratingValue": "4.9",
        "reviewCount": "144"
      },
      "offers": {
        "@type": "Offer",
        "priceCurrency": "IDR",
        "price": "1500000.00",
        "priceValidUntil": "2017-12-31",
        "itemCondition": "http://schema.org/NewCondition",
        "availability": "http://schema.org/InStock",
        "seller": {
          "@type": "Organization",
          "name": "The Indonesia Capital Market Institute (TICMI)"
        }
      }
    }
    </script>
@endsection