@extends("layout.home")

@section('content')


    <!-- Home Slider -->
    <div class="rs-container light rev_slider_wrapper">
        <div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options='{"delay": 9000, "gridwidth": 1170, "gridheight": 220}'>
            <ul>
                <li data-transition="fade" class="typo-dark heavy">
                    <img src="./assets/images/banner/banner_2.jpg"
                         alt=""
                         data-bgposition="top center"
                         data-bgfit="cover"
                         data-bgrepeat="no-repeat"
                         class="rev-slidebg">

                    <div class="tp-caption big-text"
                         style="background: rgba(0,0,0,.5);width: 600px;height: 100px;"
                         data-x="left" data-hoffset="67"
                         data-y="top" data-voffset="30"
                         data-start="590"
                         data-transform_in="y:[-300%];opacity:0;s:500;">&nbsp;</div>

                    <div class="tp-caption sm-text"
                         data-x="left" data-hoffset="70"
                         data-y="95" data-voffset="-70"
                         data-start="600"
                         data-transform_in="y:[-300%];opacity:0;s:500;"><span class="color-white">Pusat Edukasi Pasar Modal Indonesia</span></div>

                    <div class="tp-caption big-text"
                         data-x="left" data-hoffset="70"
                         data-y="center" data-voffset="-50"
                         data-start="1500"
                         data-whitespace="nowrap"
                         data-transform_in="y:[100%];s:500;"
                         data-transform_out="opacity:0;s:500;"
                         data-mask_in="x:0px;y:0px;"><span class="color-white">Pelatihan & Sertifikasi</span></div>

                    <!--                    <div class="tp-caption sm-text"-->
                    <!--                         data-x="left" data-hoffset="67"-->
                    <!--                         data-y="135"-->
                    <!--                         data-start="2000"-->
                    <!--                         data-transform_in="y:[100%];opacity:0;s:500;"><a href="#" class="btn">Lihat Semua Program Kami</a></div>-->
                </li>

                <li data-transition="fade" class="typo-dark heavy">

                    <img src="./assets/images/banner/banner_3.jpg"
                         alt=""
                         data-bgposition="center center"
                         data-bgfit="cover"
                         data-bgrepeat="no-repeat"
                         class="rev-slidebg">

                    <div class="tp-caption big-text"
                         style="background: rgba(0,0,0,.5);width: 925px;height: 130px;"
                         data-x="right" data-hoffset="67"
                         data-y="top" data-voffset="15"
                         data-start="590"
                         data-transform_in="y:[-300%];opacity:0;s:500;">&nbsp;</div>

                    <div class="tp-caption md-text"
                         data-x="right" data-hoffset="70"
                         data-y="top" data-voffset="10"
                         data-start="500"
                         data-whitespace="nowrap"
                         data-transform_in="y:[100%];s:500;"
                         data-transform_out="opacity:0;s:500;"
                         data-mask_in="x:0px;y:0px;"><span class="color-white">Promo Diskon 25% Program Reguler WPPE dan WMI</span>
                    </div>

                    <div class="tp-caption sm-text"
                         data-x="right" data-hoffset="70"
                         data-y="top" data-voffset="70"
                         data-start="1000"
                         data-whitespace="nowrap"
                         data-transform_in="y:[100%];s:500;"
                         data-transform_out="opacity:0;s:500;"
                         data-mask_in="x:0px;y:0px;"><span class="color-white">Segera daftarkan diri Anda untuk mendapatkan Promo Diskon 25% untuk <br/> Program Reguler WPPE dan WMI untuk 25 pendaftar pertama.</span>
                    </div>

                    <!--                    <div class="tp-caption big-text"-->
                    <!--                         data-x="right" data-hoffset="67"-->
                    <!--                         data-y="top" data-voffset="130"-->
                    <!--                         data-start="2000"-->
                    <!--                         data-whitespace="nowrap"-->
                    <!--                         data-transform_in="y:[100%];s:500;"-->
                    <!--                         data-transform_out="opacity:0;s:500;"-->
                    <!--                         data-mask_in="x:0px;y:0px;"><a href="#" class="btn">View All Courses</a>-->
                    <!--                    </div>-->

                </li>

                <li data-transition="fade" class="typo-dark heavy">
                    <img src="./assets/images/banner/banner_1.jpg"
                         alt=""
                         data-bgposition="center center"
                         data-bgfit="cover"
                         data-bgrepeat="no-repeat"
                         class="rev-slidebg">

                    <div class="tp-caption big-text"
                         style="background: rgba(0,0,0,.5);width: 900px;height: 90px;"
                         data-x="left" data-hoffset="67"
                         data-y="top" data-voffset="30"
                         data-start="590"
                         data-transform_in="y:[-300%];opacity:0;s:500;">&nbsp;</div>

                    <div class="tp-caption sm-text"
                         data-x="left" data-hoffset="70"
                         data-y="85"
                         data-start="500"
                         data-transform_in="y:[-300%];opacity:0;s:500;"><span class="color-white">Anda dapat mengunduh data pasar modal seperti LapKeu, Prospectus dengan gratis.</span></div>

                    <div class="tp-caption md-text"
                         data-x="left" data-hoffset="70"
                         data-y="top" data-voffset="30"
                         data-start="1500"
                         data-whitespace="nowrap"
                         data-transform_in="y:[100%];s:500;"
                         data-transform_out="opacity:0;s:500;"
                         data-mask_in="x:0px;y:0px;"><span class="color-white">Unduh Data Pasar Modal GRATIS! </span></div>

                    <div class="tp-caption sm-text"
                         data-x="left" data-hoffset="67"
                         data-y="130"
                         data-start="2000"
                         data-transform_in="y:[100%];opacity:0;s:500;"><a href="http://cmeds.ticmi.co.id" class="btn"><span class="color-white">Silahkan registrasi disini.</span></a></div>
                </li>
            </ul>
        </div>
    </div><!-- Home Slider -->



    <!-- Section -->
    <section class=" slider-below-section" style="padding-bottom: 20px">
        <div class="container">
            <div class="slider-below-wrap bg-color typo-light">
                <div class="row">
                    <!-- Column -->
                    <div class="col-sm-3">
                        <!-- Content Box -->
                        <div class="content-box text-center">
                            <!-- Icon Wraper -->
                            <div class="icon-wrap">
                                <a href="http://akademik.ticmi.co.id"><i class="uni-medal-3"></i></a>
                            </div><!-- Icon Wraper -->
                            <!-- Content Wraper -->
                            <div class="content-wrap">
                                <a href="http://akademik.ticmi.co.id"><h5 class="heading">Sertifikasi Profesi</h5></a>
                                <p>Sertifikasi Profesi WPPE, WMI dan WPEE untuk pelaku pasar modal</p>
                            </div><!-- Content Wraper -->
                        </div><!-- Content Box -->
                    </div><!-- Column -->

                    <!-- Column -->
                    <div class="col-sm-3">
                        <!-- Content Box -->
                        <div class="content-box text-center">
                            <!-- Icon Wraper -->
                            <div class="icon-wrap">
                                <a href="http://cmeds.ticmi.co.id"><i class="uni-line-chart4"></i></a>
                            </div><!-- Icon Wraper -->
                            <!-- Content Wraper -->
                            <div class="content-wrap">
                                <a href="http://cmeds.ticmi.co.id/EmitenNew/AnnualReport"><h5 class="heading">Data Pasar Modal</h5></a>
                                <p>Pusat Data pasar modal Indonesia , Laporan Keuangan, Prospectus dan Data Statistic</p>
                            </div><!-- Content Wraper -->
                        </div><!-- Content Box -->
                    </div><!-- Column -->

                    <!-- Column -->
                    <div class="col-sm-3">
                        <!-- Content Box -->
                        <div class="content-box text-center">
                            <!-- Icon Wraper -->
                            <div class="icon-wrap">
                                <a href="http://library.ticmi.co.id"><i class="uni-letter-open"></i></a>
                            </div><!-- Icon Wraper -->
                            <!-- Content Wraper -->
                            <div class="content-wrap">
                                <a href="http://cmeds.ticmi.co.id"><h5 class="heading">Permohonan Surat Riset</h5></a>
                                <p>Untuk mahasiswa yang ingin skripsi dengan data yang ada di TICMI.</p>
                            </div><!-- Content Wraper -->
                        </div><!-- Content Box -->
                    </div><!-- Column -->

                    <!-- Column -->
                    <div class="col-sm-3">
                        <!-- Content Box -->
                        <div class="content-box text-center">
                            <!-- Icon Wraper -->
                            <div class="icon-wrap">
                                <a href="http://library.ticmi.co.id"><i class="uni-address-book"></i></a>
                            </div><!-- Icon Wraper -->
                            <!-- Content Wraper -->
                            <div class="content-wrap">
                                <a href="http://library.ticmi.co.id"><h5 class="heading">Koleksi Buku</h5></a>
                                <p>Perpustakaan TICMI yang berisi koleksi buku-buku seputar Pasar Modal.</p>
                            </div><!-- Content Wraper -->
                        </div><!-- Content Box -->
                    </div><!-- Column -->

                </div><!-- Slider Below Wrapper -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Section -->

    <!-- Section -->
    <section class="pad-top-none typo-dark">
        <div class="container">
            <!-- Row -->
            <div class="row">
                <!-- Title -->
                <div class="col-sm-12">
                    <div class="title-container sm">
                        <div class="title-wrap">
                            <h3 class="title pad-tb-none">Program Unggulan Kami</h3>
                            <span class="separator line-separator  pad-tb-none"></span>
                        </div>
                    </div>
                </div>
                <!-- Title -->

                <!-- Event Column -->
                <div class="col-sm-3">
                    <!-- Event Wrapper -->
                    <div class="event-wrap">
                        <div class="event-img-wrap course-banner-wrap bg-pin-red color-white text-center" style="">
                            <!--<i class="uni-notepad" style="font-size: 90px;"></i>-->
                            <img alt="Event" class="img-responsive" src="./assets/images/course/wppe-waiver-3.jpg" width="600" height="220">
                            <!--<span class="cat bg-green">Open | tersisa 15 kuota</span>-->
                        </div>
                        <!-- Event Image Wrapper -->
                        <!-- Event Detail Wrapper -->
                        <div class="event-details" style="height: 420px;">
                            <h4><a href="">WPPE Waiver</a></h4>
                            <ul class="events-meta">
                                <li><i class="fa fa-calendar-o"></i> Sabtu 23 Juli 2016</li>
                                <li><i class="fa fa-map-marker"></i> R. Seminar 1 Gedung BEI</li>
                                <li><i class="fa fa-users"></i> Kuota 15 Peserta</li>
                            </ul>
                            <p>Program percepatan untuk kelas WPPE bagi Profesionnal yang sudah berkecimpung di dunia Pasar Modal...</p>
                            <a href="http://akademik.ticmi.co.id/index/daftar2/111/Pendaftaran%20Sertifikasi%20WPPE%20Program%20Waiver.html" class="btn">Saya ingin daftar</a>
                        </div><!-- Event Meta -->
                    </div><!-- Event details -->
                </div><!-- Column -->

                <!-- Event Column -->
                <div class="col-sm-3">
                    <!-- Event Wrapper -->
                    <div class="event-wrap">
                        <div class="event-img-wrap course-banner-wrap bg-primary color-white text-center" style="">
                            <!--<i class="uni-notepad" style="font-size: 90px;"></i>-->
                            <img alt="Event" class="img-responsive" src="./assets/images/course/wmi-waiver-3.jpg" width="600" height="220">
                            <!--<span class="cat bg-green">Open | tersisa 19 kuota</span>-->
                        </div><!-- Event Image Wrapper -->
                        <!-- Event Detail Wrapper -->
                        <div class="event-details" style="height: 420px;">
                            <h4><a href="">WMI Waiver</a></h4>
                            <ul class="events-meta">
                                <li><i class="fa fa-calendar-o"></i> Sabtu 23 Juli 2016</li>
                                <li><i class="fa fa-map-marker"></i> R. Seminar 3 Gedung BEI</li>
                                <li><i class="fa fa-users"></i> Kuota 15 Peserta</li>
                            </ul>
                            <p>Program percepatan untuk kelas WMI bagi Profesionnal yang sudah berkecimpung di dunia Pasar Modal...</p>
                            <a href="http://akademik.ticmi.co.id/index/daftar2/112/Pendaftaran%20Sertifikasi%20WMI%20Program%20Waiver.html" class="btn">Saya ingin daftar</a>
                        </div><!-- Event Meta -->
                    </div><!-- Event details -->
                </div><!-- Column -->

                <!-- Event Column -->
                <div class="col-sm-3">
                    <!-- Event Wrapper -->
                    <div class="event-wrap">
                        <div class="event-img-wrap course-banner-wrap color-white text-center" style="">
                            <!--<i class="uni-notepad" style="font-size: 90px;"></i>-->
                            <img alt="Event" class="img-responsive" src="./assets/images/course/wppe-reguler-3.jpg" width="600" height="220">
                            <!--<span class="cat bg-green">Open | tersisa 19 kuota</span>-->
                        </div><!-- Event Image Wrapper -->
                        <!-- Event Detail Wrapper -->
                        <div class="event-details" style="height: 420px;">
                            <h4><a href="">WPPE Reguler #38</a></h4>
                            <ul class="events-meta">
                                <li><i class="fa fa-calendar-o"></i> Jum'at 12 Agustus 2016</li>
                                <li><i class="fa fa-map-marker"></i> R. Seminar 3 Gedung BEI</li>
                                <li><i class="fa fa-users"></i> Kuota 10 Peserta</li>
                            </ul>
                            <p>Program Regular WPPE untuk Anda yang ingin mendapatkan Sertifikasi untuk bekerja di Pasar Modal...</p>
                            <a href="http://akademik.ticmi.co.id/index/daftar/Jakarta/133/Pendaftaran%20WPPE%20Batch%2038.html" class="btn">Saya ingin daftar</a>
                        </div><!-- Event Meta -->
                    </div><!-- Event details -->
                </div><!-- Column -->

                <!-- Event Column -->
                <div class="col-sm-3">
                    <!-- Event Wrapper -->
                    <div class="event-wrap">
                        <div class="event-img-wrap course-banner-wrap">
                            <img alt="Event" class="img-responsive" src="./assets/images/course/wmi-reguler-3.jpg" width="600" height="220">
                            <!--<span class="cat bg-green">Open | tersisa 15 kuota</span>-->
                        </div><!-- Event Image Wrapper -->
                        <!-- Event Detail Wrapper -->
                        <div class="event-details" style="height: 420px;">
                            <h4><a href="">WMI Batch #47</a></h4>
                            <ul class="events-meta">
                                <li><i class="fa fa-calendar-o"></i> Jum'at 12 Agustus 2016</li>
                                <li><i class="fa fa-map-marker"></i> R. Seminar 1 Gedung BEI</li>
                                <li><i class="fa fa-users"></i> Kuota 10 Peserta</li>
                            </ul>
                            <p>Program percepatan untuk kelas WPPE bagi Profesionnal yang sudah berkecimpung di dunia Pasar Modal...</p>
                            <a href="http://akademik.ticmi.co.id/index/daftar/Jakarta/134/Pendaftaran%20WMI%20Batch%2047.html" class="btn">Saya ingin daftar</a>
                        </div><!-- Event Meta -->
                    </div><!-- Event details -->
                </div><!-- Column -->

            </div><!-- Row -->

            <!-- Row -->
            <div class="row">
                <br/>

                <!-- Event Column -->
                <div class="col-sm-3">
                    <!-- Event Wrapper -->
                    <div class="event-wrap">
                        <div class="event-img-wrap course-banner-wrap">
                            <img alt="Event" class="img-responsive" src="./assets/images/course/aspm-reguler-3.jpg" style="" width="600" height="220">
                            <!--<span class="cat bg-green">Open | tersisa 19 kuota</span>-->
                        </div><!-- Event Image Wrapper -->
                        <!-- Event Detail Wrapper -->
                        <div class="event-details" style="height: 420px;">
                            <h4><a href="">Ahli Syariah Pasar Modal (ASPM)</a></h4>
                            <ul class="events-meta">
                                <li><i class="fa fa-calendar-o"></i> Jum'at 12 Agustus 2016</li>
                                <li><i class="fa fa-map-marker"></i> R. Seminar 3 Gedung BEI</li>
                                <li><i class="fa fa-users"></i> Kuota 15 Peserta</li>
                            </ul>
                            <p>Program Keahlian Syariah Pasar Modal untuk Anda yang ingin mendapatkan Sertifikasi untuk Pasar Modal dengan sistem Syariah...</p>
                            <a href="http://akademik.ticmi.co.id/index/daftar/Jakarta/152/Pendaftaran%20Ahli%20Syariah%20Pasar%20Modal%20(ASPM).html" class="btn">Saya ingin daftar</a>
                        </div><!-- Event Meta -->
                    </div><!-- Event details -->
                </div><!-- Column -->

                <!-- Event Column -->
                <div class="col-sm-3">
                    <!-- Event Wrapper -->
                    <div class="event-wrap">
                        <div class="event-img-wrap course-banner-wrap">
                            <img alt="Event" class="img-responsive" src="./assets/images/course/aspm-reguler-3.jpg" style="" width="600" height="220">
                            <!--<span class="cat bg-green">Open | tersisa 19 kuota</span>-->
                        </div><!-- Event Image Wrapper -->
                        <!-- Event Detail Wrapper -->
                        <div class="event-details" style="height: 420px;">
                            <h4><a href="">Akselerasi ASPM</a></h4>
                            <ul class="events-meta">
                                <li><i class="fa fa-calendar-o"></i> Jum'at 12 Agustus 2016</li>
                                <li><i class="fa fa-map-marker"></i> R. Seminar 3 Gedung BEI</li>
                                <li><i class="fa fa-users"></i> Kuota 15 Peserta</li>
                            </ul>
                            <p>Program Akselerasi Keahlian Syariah Pasar Modal untuk Anda yang ingin mendapatkan Sertifikasi untuk Pasar Modal dengan sistem Syariah...</p>
                            <a href="http://akademik.ticmi.co.id/index/daftar/Jakarta/156/Pendaftaran%20Akselerasi%20ASPM.html" class="btn">Saya ingin daftar</a>
                        </div><!-- Event Meta -->
                    </div><!-- Event details -->
                </div><!-- Column -->

            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Section -->

    <!-- Section -->
    <section class="bg-dark-red typo-light">
        <div class="container">
            <div class="row counter-sm">
                <!-- Title -->
                <div class="col-sm-12">
                    <div class="title-container">
                        <div class="title-wrap">
                            <h3 class="title">Sekilas tentang TICMI</h3>
                            <span class="separator line-separator"></span>
                        </div>
                        <p class="description">Berikut beberapa data kami </p>
                    </div>
                </div>
                <!-- Title -->
                <div class="col-sm-6 col-md-3">
                    <!-- Count Block -->
                    <div class="count-block bg-white color-dark">
                        <h5>Lulusan WPPE</h5>
                        <h3 data-count="642" class="count-number"><span class="counter">642</span></h3>
                        <i class="uni-fountain-pen"></i>
                    </div><!-- Counter Block -->
                </div><!-- Column -->
                <div class="col-sm-6 col-md-3">
                    <!-- Count Block -->
                    <div class="count-block bg-white  color-dark">
                        <h5>Lulusan WMI</h5>
                        <h3 data-count="463" class="count-number"><span class="counter">463</span></h3>
                        <i class="uni-medal-3"></i>
                    </div><!-- Counter Block -->
                </div><!-- Column -->
                <div class="col-sm-6 col-md-3">
                    <!-- Count Block -->
                    <div class="count-block bg-white color-dark">
                        <h5>Total Pengunduh</h5>
                        <h3 data-count="1680" class="count-number"><span class="counter">1680</span></h3>
                        <i class="uni-talk-man"></i>
                    </div><!-- Counter Block -->
                </div><!-- Column -->
                <div class="col-sm-6 col-md-3">
                    <!-- Count Block -->
                    <div class="count-block bg-white color-dark">
                        <h5>Total Data Diunduh</h5>
                        <h3 data-count="358204" class="count-number"><span class="counter">358204</span></h3>
                        <i class="uni-download"></i>
                    </div><!-- Counter Block -->
                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Section -->



@endsection