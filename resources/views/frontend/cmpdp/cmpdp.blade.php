@extends('layout.home')
@section('content')

    <section data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="/assets/images/ticmi/header-cmpdp.jpg" class="img-responsive" alt="Capital Market Professional Development Program">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            <div class="alert alert-info">
                                <p class="text-center" style="font-size: 16px;"><strong>Pemberitahuan</strong></p>
                                <p class="text-center">Hasil Seleksi Nasional 3-4 Juni 2017 telah dikirimkan ke email para peserta.  Silakan cek inbox masing-masing.</p>
                            </div>
                            {{--<div class="text-center">--}}
                                {{--<a href="{{ route('cmpdp.pengumuman') }}" class="btn btn-lg">Pengumuman Ujian Nasional CMPDP 2017</a>--}}
                                {{--<br>--}}
                                {{--<br>--}}
                            {{--</div>--}}
                            <h5><a href="#">I. Tentang Capital Market Professional Development Program</a></h5>
                            <p class="text-justify">
                                <strong>The Indonesia Capital Market Intitute (TICMI)</strong> mengajak talenta-talenta terbaik bangsa bergabung
                                melalui program <strong>Capital Market Professional Development Program (CMPDP)</strong> untuk menjadi
                                professional pasar modal yang kompeten, handal, dan mampu menjawab tantangan di masa depan.
                                Setiap lulusan CMPDP akan ditempatkan bekerja tidak hanya di <em>PT Bursa Efek Indonesia (BEI)</em>,
                                <em>PT Kliring Penjaminan Efek Indonesia (KPEI)</em>, <em>PT Kustodian Sentral Efek Indonesia (KSEI)</em> yang berfungsi sebagai <em>Self Regulatory
                                    Organization (SRO)</em>  melainkan juga di Anak Usaha <em>SRO</em> sebagai calon
                                pemimpin masa depan pasar modal Indonesia.
                            </p>
                            <br>
                            <h5><a href="#">II. Program Pengembangan</a></h5>
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="text-justify">
                                        Demi mempersiapkan talenta-talenta terpilih untuk menjadi penggerak industri pasar modal, kami
                                        senantiasa mengembangkan berbagai program peningkatan kompetensi agar terbentuk karakter
                                        para pemimpin yang tangguh.
                                        <br/><br/>Program CMPDP akan berlangsung dalam 2 fase dengan total selama 18 bulan, yaitu terdiri dari 12 bulan fase pertama yang merupakan
                                        fase pengembangan dan 6 bulan fase kedua yang merupakan <em>On The Job Training (OTJ)</em> di divisi-divisi SRO. Selama 18 bulan tersebut peserta akan mengikuti:
                                        <br><br>
                                        <strong><em>In Class Training</em></strong> <br>
                                        <em>In Class Training</em> terdiri dari pengenalan anak usaha SRO, <em>Professionalism & Grooming Training</em>, <em>Strive for Excellence & Diciplinary Training dan Entrepreneurship</em>.
                                        <br><br>
                                        <strong><em>Capital Market Certification</em></strong> <br>
                                        Sertifikasi pasar modal meliputi sertifikasi profesi Wakil Perantara Pedagang Efek dan Wakil Manager Investasi sebagai bekal di industri pasar modal
                                        <br><br>

                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <img src="/assets/images/ticmi/course-ticmi.jpg" alt="CMPDP" align="right" class="img-responsive">
                                    <div class="text-justify" style="margin-top: 10px; display: inline-block;">
                                        <strong><em>On The Job Training</em> (OJT)</strong> <br>
                                        <em>On The Job Training</em> pada fase pertama yang akan dilakukan sebanyak 3 kali masing-masing 3 bulan dan fase kedua sebanyak 1 kali selama 6 bulan pada divisi-divisi SRO.
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <img src="/assets/images/ticmi/cmpdp-training.jpg" alt="CMPDP" align="left" class="img-responsive" style="margin-top: 10px;">
                                </div>
                                <div class="col-md-6">
                                    <p class="text-justify">
                                        <strong><em>Individual Project Assignment: Monthly Presentation (in English)</em></strong> <br>
                                        Monthly Presentation merupakan sharing session secara rutin yang dilakukan oleh peserta CMPDP kepada peserta lainnya, dimana mereka harus melakukan transfer of knowledge terhadap apa yang telah mereka pelajari selama periode OJT tertentu di divisi terkait.
                                        <br><br>
                                        <strong><em>Job Coaching dan Career Coaching</em></strong>
                                        <br><br>
                                        <strong><em>Organizational Event Involvement</em></strong> <br>
                                        <em>Organizational Event Involvement</em> dan <em>Other Ad Hoc Project</em> merupakan tugas-tugas kelompok maupun individual yang diberikan kepada peserta CMPDP berdasarkan kebutuhan ad-hoc dalam waktu tertentu. Peserta CMPDP akan terlibat dalam event-event SRO mulai dari rancangan konsep, persiapan hingga pelaksanaan event seperti IPO, Festival Yuk Nabung Saham, dsb.
                                    </p>
                                </div>
                            </div>
                            <br>
                            <h5><a href="#">III. Kualifikasi dan Persyaratan Umum</a></h5>
                            <p class="text-justify">
                                <ol style="list-style-type: square;">
                                    <li>Minimal lulusan S1 dari semua jurusan baik dari Perguruan Tinggi di dalam maupun luar negeri dengan minimum GPA/IPK 3.0 dari skala 4.0</li>
                                    <li><em>Preferable</em> jika telah memiliki pengalaman kerja minimal 1 (satu) tahun</li>
                                    <li>Memiliki minat yang tinggi untuk mengembangkan industri pasar modal Indonesia</li>
                                    <li>Bersedia ditempatkan di mana saja setelah lulus program</li>
                                    <li>Memiliki kemampuan belajar yang cepat dan memiliki keinginan yang tinggi untuk mempelajari hal baru</li>
                                    <li>Memiliki jiwa entrepreneurship yang tinggi (<em>inovatif, salesmanship skill</em>)</li>
                                    <li>Memiliki perilaku: percaya diri, positive mindset, bertanggung jawab, <em>resilient, strong interpersonal skill</em>, komunikatif (tulisan dan verbal baik Bahasa Indonesia maupun Bahasa Inggris).</li>
                                </ol>
                            </p>
                            <br>
                            <h5><a href="#">IV. Pendaftaran Batch II Tahun 2017</a></h5>
                            <div class="text-justify">
                                Proses pendaftaran untuk mengikuti program CMPDP akan berlangsung hingga 25 Mei 2017 Pukul 23:58.
                                <br>
                                Kami informasikan bahwa tanggal-tanggal berikut merupakan jadwal pelaksanaan tahapan seleksi selanjutnya : <br/>
                                <ul style="list-style: disc;margin-left: 40px;">
                                    <li>3 & 4 Juni 2017: Seleksi Tertulis (Bahasa Inggris, Ekonomi, Pengetahuan Pasar Modal, Potensi Akademik dan Etika) bagi kandidat yang lolos dari tahap Seleksi Administrasi </li>
                                    <li>10 Juni 2017: Psikotest bagi kandidat yang lolos dari tahap Seleksi Tertulis </li>
                                    <li>17 Juni 2017: Interview dan Focus Group Discussion (FGD) bagi kandidat yang lolos dari tahap Psikotest</li>
                                </ul>
                                Mohon dapat dipahami bahwa tanggal-tanggal tersebut masih dapat berubah disesuaikan dengan kondisi yang ada. <br/>
                                Kandidat akan menerima email notifikasi jika kandidat memenuhi syarat dan lolos pada setiap tahapan seleksi seperti yang disebutkan di atas. Untuk itu mohon agar Anda dapat menandai kalender Anda, selalu terhubung dengan email dan mempersiapkan diri pada saat Anda mendapatkan notifikasi tersebut. <br/><br>
                                Untuk informasi lebih lanjut mengenai program ini dapat mengirimkan email ke <a href="mailto:cmpdp@ticmi.co.id">cmpdp@ticmi.co.id</a> atau melalui call center 08001009000
                            </div>

                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                    <!-- Blog Share Post -->
                    <div class="share">
                        <h5>Share Post : </h5>
                        <ul class="social-icons">
                            <li class="fb-share-button"  data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true">
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('cmpdp') }}&src=sdkpreparse" class="fb-xfbml-parse-ignore" target="_blank" title="Facebook">Facebook</a>
{{--                                <div class="fb-share-button" data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"></a></div>--}}
                                {{--<div class="fb-share-button" data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>--}}
                            </li>
                        </ul><!-- Blog Social Share -->
                    </div><!-- Blog Share Post -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Page Default -->
@endsection