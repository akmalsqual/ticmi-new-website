@extends('layout.home')
@section('content')

    <section data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <!-- Blog Detail Wrapper -->
                    <div class=" content-box bg-white shadow">
                        <h5 align="center">Pengumuman Peserta Ujian Nasional CMPDP 2017</h5>
                        <div class="text-center">
                            Berikut daftar peserta yang akan mengikuti Ujian Nasional Seleksi CMPDP 2017 yang akan diselenggarakan secara serentak
                            di 26 Provinsi diseluruh Indonesia. <br><br>

                            <a href="{{ route('cmpdp.lokasiujian',['iframe'=>'true','width'=>'100%','height'=>'100%']) }}" class="btn btn-danger btn-lg" rel="prettyPhoto[lokasi_ujian]">Informasi Lokasi Ujian</a>
                            <a href="{{ url('download/cmpdp/TATA TERTIB PESERTA UJIAN CMPDP 2017.pdf') }}?iframe=true&width=100%&height=100%" class="btn btn-danger btn-lg" rel="prettyPhoto[tatib_ujian]">Tata Tertib Ujian Nasional CMPDP</a>

                        </div>
                        <hr>
                        {!! Form::open(['url'=>route('cmpdp.pengumuman'),'method'=>'get','class'=>'form-horizontal']) !!}
                        <div class="row">
                            <div class="col-sm-4">
                                {!! Form::text('no_daftar',$no_daftar,['class'=>'form-control','placeholder'=>'Cari No Pendaftaran']) !!}
                            </div>
                            <div class="col-sm-6">
                                {!! Form::text('nama_lengkap',$nama,['class'=>'form-control','placeholder'=>'Cari Nama Lengkap']) !!}
                            </div>
                            <div class="col-sm-2">
                                {!! Form::submit('CARI',['class'=>'btn btn-md btn-primary'])!!}
                                <a href="{{ route('cmpdp.pengumuman') }}" class="btn  btn-warning">RESET</a>
                            </div>
                        </div>
                        {!! Form::close() !!}
                        <div class="table-responsive">
                            <table class="table table-condensed table-striped table-hover table-bordered">
                                <thead>
                                <tr class="success">
                                    <th rowspan="2" valign="top">No Pendaftaran</th>
                                    <th rowspan="2" valign="top">Nama Lengkap</th>
                                    <th rowspan="2" valign="top">Kota Domisili</th>
                                    <th colspan="3" valign="top" class="text-center">Jadwal Ujian</th>
                                </tr>
                                <tr class="success">
                                    <th>Lokasi Ujian</th>
                                    <th>Tanggal</th>
                                    <th>Waktu</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($cmpdp as $item)
                                    <tr>
                                        <td>{{ $item->no_daftar }}</td>
                                        <td>
                                            @if($nama)
                                                {!! strtoupper(str_ireplace($nama,"<span style='color: red;'>".$nama."</span>",$item->nama_lengkap)) !!}
                                            @else
                                                {{ strtoupper($item->nama_lengkap) }}
                                            @endif
                                        </td>
                                        <td>{{ $item->get_kabupaten_skrng->name }}</td>
                                        <td>{{ strtoupper($item->kota_seleksi) }}</td>
                                        <td>{{ $item->hari_ujian }}</td>
                                        <td>{{ $item->jam_ujian }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="infotable row">
                            <div class="col-sm-6">
                                Halaman {{ $cmpdp->currentPage() }} dari {{ $cmpdp->lastPage() }} |
                                Menampilkan {{ $cmpdp->count() }} dari {{ $cmpdp->total() }}
                            </div>
                            <div class="pull-right">
                                {{ $cmpdp->appends(['no_daftar'=>$no_daftar,'nama_lengkap'=>$nama])->links() }}
                            </div>
                        </div>
                    </div>
                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Page Default -->

@endsection