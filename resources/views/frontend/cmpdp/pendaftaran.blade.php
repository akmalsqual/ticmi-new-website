@extends('layout.home')
@section('content')

    <section data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="/assets/images/ticmi/header-cmpdp.jpg" class="img-responsive" alt="Capital Market Professional Development Program">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            <h5 class="text-center">Pendaftaran Capital Market Professional Development Program Angkatan 2017</h5>

                            {!! Form::open(['url'=>route('cmpdp.pendaftaran.store'),'files'=>true]) !!}
                            <div class="alert alert-info">
                                Isi data Anda selengkap-lengkapnya agar informasi mengenai diri Anda dapat kami proses dengan baik.
                            </div>
                            @include('errors.list')
                            @include('flash::message')
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    Data Pribadi
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="input-text form-group form-group-sm">
                                                <label for="nama" class="hidden-xs">Nama Lengkap <em class="text-red">*</em></label>
                                                {{ Form::text('nama_lengkap',null,['class'=>'form-control','placeholder'=>'Nama Lengkap (tanpa gelar)']) }}
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-text form-group form-group-sm">
                                                <label for="nama" class="hidden-xs">Nama Panggilan</label>
                                                {{ Form::text('nama_panggilan',null,['class'=>'form-control','placeholder'=>'Nama Panggilan']) }}
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-text form-group form-group-sm">
                                                <label for="nama" class="hidden-xs">Email <em class="text-red">*</em></label>
                                                {{ Form::text('email',null,['class'=>'form-control','placeholder'=>'Email']) }}
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-text form-group form-group-sm">
                                                <label for="nama" class="hidden-xs">No Telp. Genggam <em class="text-red">*</em></label>
                                                {{ Form::text('no_hp',null,['class'=>'form-control','placeholder'=>'No Telp. Genggam']) }}
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-text form-group form-group-sm">
                                                <label for="nama" class="hidden-xs control-label col-sm-12">Tempat / Tanggal Lahir <em class="text-red">*</em></label>
                                                <div class="col-sm-6">
                                                    {{ Form::text('tmp_lahir',null,['class'=>'form-control','placeholder'=>'Tempat Lahir']) }}
                                                </div>
                                                <div class="col-sm-6">
                                                    {{ Form::text('tgl_lahir',null,['class'=>'form-control datepicker','placeholder'=>'Tanggal Lahir (yyyy-mm-dd)']) }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-text form-group form-group-sm">
                                                <label for="nama" class="hidden-xs">No KTP <em class="text-red">*</em></label>
                                                {{ Form::text('no_ktp',null,['class'=>'form-control','placeholder'=>'No KTP']) }}
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-text form-group form-group-sm">
                                                <label for="nama" class="hidden-xs">Jenis Kelamin <em class="text-red">*</em></label>
                                                <div class="col-sm-12">
                                                    <label class="checkbox-inline">
                                                        {!! Form::radio('jns_kelamin','Laki-laki',null) !!} Laki-laki
                                                    </label>
                                                    <label class="checkbox-inline">
                                                        {!! Form::radio('jns_kelamin','Perempuan',null) !!} Perempuan
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-text form-group form-group-sm">
                                                <label for="nama" class="hidden-xs">Agama <em class="text-red">*</em></label>
                                                {{ Form::select('agama',['Islam'=>'Islam','Kristen Protestan'=>'Kristen Protestan','Katolik'=>'Katolik','Budha'=>'Budha','Hindu'=>'Hindu','Kong Hu Cu'=>'Kong Hu Cu'],null,['class'=>'form-control','placeholder'=>'Pilih Agama']) }}
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="input-text form-group form-group-sm">
                                                        <label for="nama" class="hidden-xs">Alamat Sesuai KTP <em class="text-red">*</em></label>
                                                        {{ Form::textarea('alamat_ktp',null,['class'=>'form-control','placeholder'=>'Alamat Sesuai KTP (masukkan alamat selengkap-lengkapnya)','rows'=>5]) }}
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="input-text form-group form-group-sm">
                                                        <label for="nama" class="hidden-xs">Provinsi (Sesuai KTP) <em class="text-red">*</em></label>
                                                        {{ Form::select('provinsi_ktp',$provinsi,null,['class'=>'form-control cari-kabupaten','placeholder'=>'Pilih Provinsi Sesuai KTP','data-prefix-code'=>'ktp']) }}
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="input-text form-group form-group-sm">
                                                        <label for="nama" class="hidden-xs">Kabupaten/Kota (Sesuai KTP) <em class="text-red">*</em></label>
                                                        {{ Form::select('kabupaten_ktp',[],null,['class'=>'form-control kabupaten_ktp cari-kecamatan','placeholder'=>'Pilih Kabupaten/Kota Sesuai KTP','data-prefix-code'=>'ktp']) }}
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="input-text form-group form-group-sm">
                                                        <label for="nama" class="hidden-xs">Kecamatan (Sesuai KTP) <em class="text-red">*</em></label>
                                                        {{ Form::select('kecamatan_ktp',[],null,['class'=>'form-control kecamatan_ktp','placeholder'=>'Pilih Kecamatan Sesuai KTP']) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-text form-group form-group-sm">
                                                <label for="nama" class="hidden-xs">Alamat Tinggal Saat Ini <em class="text-red">*</em></label>
                                                {{ Form::textarea('alamat_skrng',null,['class'=>'form-control','placeholder'=>'Alamat Tinggal Saat Ini (masukkan alamat selengkap-lengkapnya)','rows'=>5]) }}
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-text form-group form-group-sm">
                                                <label for="nama" class="hidden-xs">Provinsi (Tempat tinggal saat ini) <em class="text-red">*</em></label>
                                                {{ Form::select('provinsi_skrng',$provinsi,null,['class'=>'form-control cari-kabupaten','placeholder'=>'Pilih Provinsi Sesuai Tempat Tinggal Sekarang','data-prefix-code'=>'skrng']) }}
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-text form-group form-group-sm">
                                                <label for="nama" class="hidden-xs">Kabupaten/Kota (Tempat tinggal saat ini) <em class="text-red">*</em></label>
                                                {{ Form::select('kabupaten_skrng',[],null,['class'=>'form-control kabupaten_skrng cari-kecamatan','placeholder'=>'Pilih Kabupaten/Kota Sesuai Tempat Tinggal Sekarang','data-prefix-code'=>'skrng']) }}
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-text form-group form-group-sm">
                                                <label for="nama" class="hidden-xs">Kecamatan (Tempat tinggal saat ini) <em class="text-red">*</em></label>
                                                {{ Form::select('kecamatan_skrng',[],null,['class'=>'form-control kecamatan_skrng','placeholder'=>'Pilih Kecamatan Sesuai Tempat Tinggal Sekarang']) }}
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-text form-group form-group-sm">
                                                <label for="nama" class="hidden-xs">Status <em class="text-red">*</em></label>
                                                {{ Form::select('status_kawin',['Menikah'=>'Menikah','Belum Menikah'=>'Belum Menikah','Janda'=>'Janda','Duda'=>'Duda'],null,['class'=>'form-control','placeholder'=>'Status']) }}
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-text form-group form-group-sm">
                                                <label for="nama" class="hidden-xs">Kebangsaan <em class="text-red">*</em></label>
                                                {{ Form::text('kebangsaan',null,['class'=>'form-control','placeholder'=>'Kebangsaan']) }}
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-text form-group form-group-sm">
                                                <label for="nama" class="">Pas Foto (berpakaian rapi dan resmi) <em class="text-red">*</em></label>
                                                {{ Form::file('pas_foto',['class'=>'form-control','placeholder'=>'Foto']) }}
                                                <small class="form-note">File harus berukuran dibawah 500Kb, dan dalam format : jpg atau png</small>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-text form-group form-group-sm">
                                                <label for="nama" class="">Curriculum Vitae</label>
                                                {{ Form::file('cv',['class'=>'form-control','placeholder'=>'Curicullum Vitae']) }}
                                                <small class="form-note">File harus berukuran dibawah 2Mb, dan dalam format : pdf</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    Unggah Video <em>(Optional)</em>
                                </div>
                                <div class="panel-body">
                                    <ol>
                                        <li>
                                            Akan memberikan nilai tambah jika anda membuat video pendek yang menceritakan mengenai profil anda dan ceritakan juga mengapa TICMI harus
                                            memilih anda sebagai peserta CMPDP 2017.
                                        </li>
                                        <li>Video di unggah melalui <strong><i style="font-size: 18px;color: #f00000;" class="fa fa-youtube-play"></i> Youtube</strong> dan salin tautan (<em>link</em>) video anda pada kolom dibawah ini</li>
                                    </ol>
                                    <div class="col-sm-12">
                                        {!! Form::text('link_video',null,['class'=>'form-control','placeholder'=>'https://www.youtube.com/watch?v=47zVVAisqdg']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    Data Pendidikan
                                </div>
                                <div class="panel-body">
                                    <div class="row hidden-xs">
                                        <div class="col-sm-1 text-center">Tingkat Pendidikan</div>
                                        <div class="col-sm-2 text-center">Periode</div>
                                        <div class="col-sm-2 text-center">Jurusan/Fakultas</div>
                                        <div class="col-sm-3 text-center">Perguruan Tinggi</div>
                                        <div class="col-sm-2 text-center">Nilai/IPK</div>
                                        <div class="col-sm-2 text-center">Lulus/Belum</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-1 text-center">S1/Setara <em class="text-red">*</em></div>
                                        <div class="col-sm-2 text-center">
                                            {!! Form::text('periode_s1',null,['class'=>'form-control','placeholder'=>'Tahun Mulai - Tahun Selesai S1']) !!}
                                        </div>
                                        <div class="col-sm-2 text-center">
                                            {!! Form::text('jurusan_s1',null,['class'=>'form-control','placeholder'=>'Jurusan S1']) !!}
                                        </div>
                                        <div class="col-sm-3 text-center">
                                            {!! Form::text('universitas_s1',null,['class'=>'form-control','placeholder'=>'Perguruan Tinggi S1']) !!}
                                        </div>
                                        <div class="col-sm-2 text-center">
                                            {!! Form::text('ipk_s1',null,['class'=>'form-control','placeholder'=>'IPK S1']) !!}
                                        </div>
                                        <div class="col-sm-2 text-center">
                                            {!! Form::select('is_lulus_s1',['Sudah Lulus'=>'Sudah Lulus','Belum Lulus'=>'Belum Lulus'],null,['class'=>'form-control','placeholder'=>'Lulus/Blm Lulus S1']) !!}
                                        </div>
                                        <div class="col-sm-3 col-sm-offset-1">
                                            Upload Data Transkrip Nilai Anda
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="input-text form-group form-group-sm">
                                                {!! Form::file('transkrip_nilai',['class'=>'form-control','placeholder'=>'Transkrip Nilai']) !!}
                                                <small class="form-note">File harus berukuran dibawah 2Mb, dan dalam format : pdf</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-1 text-center">S2</div>
                                        <div class="col-sm-2 text-center">
                                            {!! Form::text('periode_s2',null,['class'=>'form-control','placeholder'=>'Tahun Mulai - Tahun Selesai S2']) !!}
                                        </div>
                                        <div class="col-sm-2 text-center">
                                            {!! Form::text('jurusan_s2',null,['class'=>'form-control','placeholder'=>'Jurusan S2']) !!}
                                        </div>
                                        <div class="col-sm-3 text-center">
                                            {!! Form::text('universitas_s2',null,['class'=>'form-control','placeholder'=>'Perguruan Tinggi S2']) !!}
                                        </div>
                                        <div class="col-sm-2 text-center">
                                            {!! Form::text('ipk_s2',null,['class'=>'form-control','placeholder'=>'IPK S2']) !!}
                                        </div>
                                        <div class="col-sm-2 text-center">
                                            {!! Form::select('is_lulus_s2',['Sudah Lulus'=>'Sudah Lulus','Belum Lulus'=>'Belum Lulus'],null,['class'=>'form-control','placeholder'=>'Lulus/Blm Lulus S2']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    Pengalaman Kerja
                                </div>
                                <div class="panel-body">
                                    <div class="row hidden-xs">
                                        <div class="col-sm-3 text-center">Nama Perusahaan</div>
                                        <div class="col-sm-3 text-center">Jabatan/Posisi</div>
                                        <div class="col-sm-2 text-center">Periode</div>
                                        <div class="col-sm-2 text-center">Gaji Per Bulan<br> <em style="font-size: 12px; color: #cf0404;">Setelah dipotong pajak</em></div>
                                        <div class="col-sm-2 text-center">Pendapatan Pertahun <br> <em style="font-size: 12px; color: #cf0404;">Setelah dipotong pajak</em></div>

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 visible-xs-block">Pengalaman Pekerjaan 1</div>
                                        <div class="col-sm-3 text-center">
                                            {!! Form::text('nm_perusahaan_1',null,['class'=>'form-control','placeholder'=>'Nama Perusahaan 1']) !!}
                                        </div>
                                        <div class="col-sm-3 text-center">
                                            {!! Form::text('jabatan_1',null,['class'=>'form-control','placeholder'=>'Jabatan / Posisi 1']) !!}
                                        </div>
                                        <div class="col-sm-2 text-center">
                                            {!! Form::text('periode_pekerjaan_1',null,['class'=>'form-control','placeholder'=>'Tahun Mulai - Tahun Selesai']) !!}
                                        </div>
                                        <div class="col-sm-2 text-center">
                                            {!! Form::text('nonimal_gaji_1',null,['class'=>'form-control','placeholder'=>'Nominal Gaji 1']) !!}
                                        </div>
                                        <div class="col-sm-2 text-center">
                                            {!! Form::text('pendapatan_pertahun_1',null,['class'=>'form-control','placeholder'=>'Pendapatan pertahun 1 setelah dipotong pajak']) !!}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 visible-xs-block">Pengalaman Pekerjaan 2</div>
                                        <div class="col-sm-3 text-center">
                                            {!! Form::text('nm_perusahaan_2',null,['class'=>'form-control','placeholder'=>'Nama Perusahaan 2']) !!}
                                        </div>
                                        <div class="col-sm-3 text-center">
                                            {!! Form::text('jabatan_2',null,['class'=>'form-control','placeholder'=>'Jabatan / Posisi 2']) !!}
                                        </div>
                                        <div class="col-sm-2 text-center">
                                            {!! Form::text('periode_pekerjaan_2',null,['class'=>'form-control','placeholder'=>'Tahun Mulai - Tahun Selesai']) !!}
                                        </div>
                                        <div class="col-sm-2 text-center">
                                            {!! Form::text('nonimal_gaji_2',null,['class'=>'form-control','placeholder'=>'Nominal Gaji 2']) !!}
                                        </div>
                                        <div class="col-sm-2 text-center">
                                            {!! Form::text('pendapatan_pertahun_2',null,['class'=>'form-control','placeholder'=>'Pendapatan pertahun 2 setelah dipotong pajak']) !!}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 visible-xs-block">Pengalaman Pekerjaan 3</div>
                                        <div class="col-sm-3 text-center">
                                            {!! Form::text('nm_perusahaan_3',null,['class'=>'form-control','placeholder'=>'Nama Perusahaan 3']) !!}
                                        </div>
                                        <div class="col-sm-3 text-center">
                                            {!! Form::text('jabatan_3',null,['class'=>'form-control','placeholder'=>'Jabatan / Posisi 3']) !!}
                                        </div>
                                        <div class="col-sm-2 text-center">
                                            {!! Form::text('periode_pekerjaan_3',null,['class'=>'form-control','placeholder'=>'Tahun Mulai - Tahun Selesai']) !!}
                                        </div>
                                        <div class="col-sm-2 text-center">
                                            {!! Form::text('nonimal_gaji_3',null,['class'=>'form-control','placeholder'=>'Nominal Gaji 3']) !!}
                                        </div>
                                        <div class="col-sm-2 text-center">
                                            {!! Form::text('pendapatan_pertahun_3',null,['class'=>'form-control','placeholder'=>'Pendapatan pertahun 3 setelah dipotong pajak']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    Kepribadian
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h6>I. Beri gambaran singkat mengapa Anda layak menjadi Peserta Capital Market Professional Development Program</h6>
                                        </div>
                                        <div class="col-sm-12">
                                            {!! Form::textarea('gambaran_layak',null,['class'=>'form-control','rows'=>5,'placeholder'=>'Beri gambaran singkat mengapa Anda layak menjadi Peserta Capital Market Professional Development Program']) !!}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h6>II. Beri gambaran singkat mengenai rencana karir Anda</h6>
                                        </div>
                                        <div class="col-sm-12">
                                            {!! Form::textarea('harapan_karir',null,['class'=>'form-control','rows'=>5,'placeholder'=>'Beri gambaran singkat mengenai rencana karir Anda']) !!}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h6>III. Beri gambaran singkat tentang diri Anda dalam hal Kekuatan dan Kelemahan</h6>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>Kekuatan</p>
                                            {!! Form::textarea('kekuatan',null,['class'=>'form-control','rows'=>5,'placeholder'=>'Kekuatan &#10;1. &#10;2. &#10;3.']) !!}
                                        </div>
                                        <div class="col-sm-6">
                                            <p>Kelemahan</p>
                                            {!! Form::textarea('kelemahan',null,['class'=>'form-control','rows'=>5,'placeholder'=>'Kelemahan &#10;1. &#10;2. &#10;3.']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    Lain-lain
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h6>IV. Kegiatan-kegiatan di luar pekerjaan</h6>
                                        </div>
                                        <div class="col-sm-12">
                                            {!! Form::textarea('kegiatan',null,['class'=>'form-control','rows'=>5,'placeholder'=>'Kegiatan diluar pekerjaan &#10;1. &#10;2. &#10;3.']) !!}
                                        </div>
                                        <div class="col-sm-12">
                                            <p>V. Untuk tes seleksi, mohon memilih kota terdekat di bawah ini:</p>
                                            {{ Form::select('kota_seleksi',$kota_seleksi,null,['class'=>'form-control','placeholder'=>'Pilih kota terdekat untuk tes seleksi']) }}
                                        </div>
                                    </div>
                                    {{--<div class="row">--}}
                                        {{--<div class="col-sm-12">--}}
                                            {{--<h6>V. Apakah Anda mempunyai saudara atau kawan yang bekerja di PT Indonesia Capital Market Electronic Library , PT Bursa Efek Indonesia, PT Kustodian Sentral Efek Indonesia, PT Kliring Penjamin Efek Indonesia, PT Pefindo Indonesia ? Jika ya, cantumkan nama dan perusahaannya</h6>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-4">Nama</div>--}}
                                        {{--<div class="col-sm-4">Hubungan</div>--}}
                                        {{--<div class="col-sm-4">Perusahaan</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-sm-4 visible-xs-block">--}}
                                            {{--Saudara / Kawan 1--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-4">--}}
                                            {{--{!! Form::text('nama_saudara_1',null,['class'=>'form-control','placeholder'=>'Nama Saudara / Kawan 1']) !!}--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-4">--}}
                                            {{--{!! Form::text('hubungan_saudara_1',null,['class'=>'form-control','placeholder'=>'Hubungan Saudara / Kawan 1']) !!}--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-4">--}}
                                            {{--{!! Form::text('perusahaan_saudara_1',null,['class'=>'form-control','placeholder'=>'Perusahaan Saudara / Kawan 1']) !!}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-sm-4 visible-xs-block">--}}
                                            {{--Saudara / Kawan 2--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-4">--}}
                                            {{--{!! Form::text('nama_saudara_2',null,['class'=>'form-control','placeholder'=>'Nama Saudara / Kawan 2']) !!}--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-4">--}}
                                            {{--{!! Form::text('hubungan_saudara_2',null,['class'=>'form-control','placeholder'=>'Hubungan Saudara / Kawan 2']) !!}--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-4">--}}
                                            {{--{!! Form::text('perusahaan_saudara_2',null,['class'=>'form-control','placeholder'=>'Perusahaan Saudara / Kawan 2']) !!}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-sm-4 visible-xs-block">--}}
                                            {{--Saudara / Kawan 3--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-4">--}}
                                            {{--{!! Form::text('nama_saudara_3',null,['class'=>'form-control','placeholder'=>'Nama Saudara / Kawan 3']) !!}--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-4">--}}
                                            {{--{!! Form::text('hubungan_saudara_3',null,['class'=>'form-control','placeholder'=>'Hubungan Saudara / Kawan 3']) !!}--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-4">--}}
                                            {{--{!! Form::text('perusahaan_saudara_3',null,['class'=>'form-control','placeholder'=>'Perusahaan Saudara / Kawan 3']) !!}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                </div>
                            </div>

                            <div class="panel panel-info">
                                <div class="panel-heading">Pernyataan Kesepakatan</div>
                                <div class="panel-body">
                                    <ol style="font-weight: bold;margin-left: -20px; text-align: justify;">
                                        <li>Dengan melakukan pendaftaran ini, saya menyatakan bahwa semua data yang saya isi adalah benar.  Saya bersedia apabila TICMI
                                            melakukan klarifikasi/verifikasi atas data-data tersebut dan saya bersedia tereliminasi dari proses seleksi atau diberhentikan dari program bila terdapat data saya yang tidak benar.</li>
                                        <li>Dengan melakukan pendaftaran ini, saya bersedia untuk ditempatkan di seluruh wilayah Indonesia saat menjalani program dan setelah lulus program.</li>
                                    </ol>
                                    {!! Form::submit('Daftar',['class'=>'btn btn-lg btn-block','data-loading-text'=>'Loading ...','data-toggle'=>'loading']) !!}

                                    <br/> <small><em>Keterangan : Kolom bertanda ini <em class="text-red">*</em> harus di isi</em></small>
                                </div>
                            </div>

                            {!! Form::close() !!}
                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                    <!-- Blog Share Post -->
                    <div class="share">
                        <h5>Share Post : </h5>
                        <ul class="social-icons">
                            <li class="fb-share-button"  data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true">
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('cmpdp') }}&src=sdkpreparse" class="fb-xfbml-parse-ignore" target="_blank" title="Facebook">Facebook</a>
                                {{--                                <div class="fb-share-button" data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"></a></div>--}}
                                {{--<div class="fb-share-button" data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>--}}
                            </li>
                        </ul><!-- Blog Social Share -->
                    </div><!-- Blog Share Post -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Page Default -->
@endsection