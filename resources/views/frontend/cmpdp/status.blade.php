@extends('layout.home')
@section('content')

    <section data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="/assets/images/ticmi/header-cmpdp.jpg" class="img-responsive" alt="Capital Market Professional Development Program">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            <h5 class="text-center">Pendaftaran Capital Market Professional Development Program Angkatan 2017</h5>
                            @include('errors.list')
                            @include('flash::message')
                            @if($status == 'sukses')
                                <h5 class="text-center">Terima kasih telah melakukan pendaftaran</h5>
                            @endif
                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Page Default -->
@endsection