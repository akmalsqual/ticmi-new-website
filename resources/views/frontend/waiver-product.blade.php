@extends("layout.home")
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('./assets/images/banner/profile.jpg') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Waiver</h3>
                        <h5 class="sub-title">Program Akselerasi untuk WPPE, WMI dan ASPM</h5>
                        <br/>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <!-- Course Wrapper -->
            <div class="row course-single">
                <!-- Course Banner Image -->
                <div class="col-sm-7">
                    <div class="owl-crousel">
                        <!--                        <img alt="Course" class="img-responsive" src="./assets/images/course/aspm-product.jpg" width="1920" height="966">-->
                    </div>
                </div><!-- Column -->
            </div><!-- Course Wrapper -->

            <div class="row course-single content-box bg-white shadow">
                <div class="col-sm-12">
                    <h4>Program Waiver 2016</h4>
                    <p class="text-justify">
                        <strong>Program Waiver 2016</strong> adalah program yang diselenggarakan oleh <strong>The Indonesia Capital Market Institute (TICMI)</strong> bagi WNI yang memenuhi ketentuan yang dipersyaratkan guna mendapatkan Sertifikasi Keahlian Pasar Modal sebagai <em>Wakil Perantara Pedagang Efek</em>, <em>Wakil Manajer Investasi</em> serta <em>Ahli Syariah Pasar Modal</em>.
                    </p>
                    <p class="text-justify">
                        Program ini diselenggarakan untuk memenuhi kebutuhan SDM Pasar Modal berlisensi WPPE dan WMI dari kalangan mahasiswa, alumni, dosen serta masyarakat luas guna menghadapi perkembangan ekonomi global, serta liberalisasi sektor jasa keuangan dan pasar modal yang terjadi dengan terbentuknya <em>Masyarakat Ekonomi ASEAN</em>.
                    </p>
                    <p class="text-justify">
                        Calon peserta yang memenuhi persyaratan langsung dapat mengikuti Program & Exam Review serta Ujian Keahlian WPPE dan/atau WMI yang diselenggarakan oleh TICMI. Penyelenggaraan dan pelaksanaan Program Waiver di daerah akan dikoordinasi oleh
                        <a href="http://www.idx.co.id/id-id/beranda/tentangbei/programbei/kantorperwakilankpbei.aspx" target="_blank">Kantor Perwakilan Bursa Efek Indonesia</a>.
                    </p>
                    <p class="text-justify">
                        Institusi atau Perguruan Tinggi yang ingin mengikut-sertakan karyawan atau mahasiswanya secara reguler dalam jumlah besar, silakan untuk mendaftarkan seorang PIC (<em>Person-In-Charge</em>) melalui laman <a href="http://www.ticmi.co.id/index/PIC.html">Pendaftaran PIC Institusi</a>  untuk segera kami tindak-lanjuti. PIC akan menjadi wakil resmi sebuah institusi untuk berkomunikasi dengan TICMI dan melakukan pendaftaran bagi para anggotanya.
                    </p>

                </div><!-- Column -->
            </div><!-- row -->
            <br>
            <div class="row course-single pad-tb-40 content-box bg-white shadow">
                <div class="col-sm-12">
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">

                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#keunggulanWaiver" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Apa keunggulan dari Program Waiver ini?
                                    </a>
                                </h4>
                            </div>
                            <div id="keunggulanWaiver" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <ol>
                                        <li>Biaya yang murah serta waktu pelaksanaan yang singkat dan cepat</li>
                                        <li>Program diselenggarakan di <strong>TICMI Jakarta</strong> serta di <strong>Kantor Perwakilan PT. Bursa Efek Indonesia</strong> yang ada di seluruh Indonesia</li>
                                        <li>Ujian dilaksanakan menggunakan teknologi berbasis WEB, dan diselenggarakan secara <em>online</em></li>
                                        <li>Hasil ujian dapat diketahui saat itu juga</li>
                                        <li>Jika Peserta tidak lulus ujian perdana, saat ujian ulang peserta hanya mengulang modul-modul yang gagal uji saja</li>
                                    </ol>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->

                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#biayaInvestasi" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Berapa Biaya Investasi untuk ikut Program Waiver 2016 ini?
                                    </a>
                                </h4>
                            </div>
                            <div id="biayaInvestasi" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <ol>
                                        <li>Biaya Program Waiver 2016: IDR 1.500.000,-</li>
                                        <li>
                                            Biaya Program sudah termasuk :
                                            <ul style="list-style: disc; padding-left: 30px;">
                                                <li>Modul</li>
                                                <li>8 Jam <em>Program & Exam Review</em></li>
                                                <li>Ujian Sertifikasi Profesi sebanyak 3 kali (1 kali Ujian Perdana dan 2 kali Ujian Ulang) yang berlaku selama masa promosi</li>
                                                <li>Makan siang dan 2 kali rehat kopi</li>
                                            </ul>
                                        </li>
                                    </ol>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->

                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#syaratPeserta" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Siapa yang dapat menjadi peserta Program Waiver ini dan apa persyaratannya
                                    </a>
                                </h4>
                            </div>
                            <div id="syaratPeserta" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p> Program Waiver 2016 terbuka bagi Warga Negara Indonesia :</p>
                                    <ol>
                                        <li>
                                            Mahasiswa dan Alumni Program S1/S2/S3 dari Perguruan Tinggi Negeri dan Swasta di seluruh Indonesia
                                            <ul style="list-style: disc; padding-left: 30px;">
                                                <li>
                                                    Mahasiswa dan Alumni <strong class="text-danger">harus berasal dari Universitas/ Jurusan/ Program Studi yang Berakreditasi B dengan bidang studi:</strong>
                                                    <ul style="list-style: circle; padding-left: 30px;">
                                                        <li>Pasar Modal</li>
                                                        <li>Ekonomi</li>
                                                        <li>Akuntansi</li>
                                                        <li>Manajemen</li>
                                                        <li>Bisnis</li>
                                                        <li>Perbankan</li>
                                                        <li>Administrasi Niaga</li>
                                                    </ul>
                                                </li>
                                                <li>Mahasiswa adalah mereka yang pada saat mendaftarkan diri pada Program Waiver 2016 ini masih masih terdaftar sebagai mahasiswa aktif pada Jurusan/ Progam Studi termaksud di atas</li>
                                                <li>Alumni adalah mereka yang pada saat mendaftarkan diri pada Program Waiver 2016 ini telah lulus dari Jurusan/ Progam Studi termaksud di atas, maksimal pada Januari 2011</li>
                                                <li>Mahasiswa dan Alumni harus <strong class="text-danger">"memenuhi seluruh matakuliah yang dipersyaratkan"</strong></li>
                                            </ul>
                                        </li>
                                        <li>
                                            Alumni TICMI atau PSP
                                            <ul style="list-style: disc; padding-left: 30px;">
                                                <li>Pemilik sertifikat keahlian WPPE/ WMI/ WPEE dari TICMI atau PSP, atau</li>
                                                <li>Pemilik Izin WPPE/ WPEE/ WMI dari OJK yang telah habis masa (kadaluarsa) berdasarkan ketentuan OJK</li>
                                            </ul>
                                        </li>
                                        <li>Karyawan Perusahaan Efek yang sudah memiliki pengalaman kerja selama 2 (dua) tahun berturut-turut di Perusahaan Efek. Khusus bagi karyawan Perusahaan Efek Perantara Pedagang Efek, calon peserta program diutamakan bagi mereka yang bekerja sebagai Broker/ Dealer/ Marketing</li>
                                        <li>Masyarakat Umum, pemilik sertifikat CA/ CPA/ CFA Level 1</li>
                                        <li>Dosen pengampu matakuliah untuk satu atau beberapa modul yang diujikan pada Ujian Sertifikasi WPPE dan/atau WMI</li>
                                    </ol>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->

                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#matakuliah" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Apa yang dimaksud "memenuhi seluruh matakuliah yang dipersyaratkan" bagi Mahasiswa dan Alumni Program S1/S2/S3 PTN/S Berakreditasi A & B?
                                    </a>
                                </h4>
                            </div>
                            <div id="matakuliah" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <ol>
                                        <li>
                                            Untuk Program Waiver WPPE, pada saat mendaftarkan diri, peserta telah LULUS dengan nilai minimal B untuk matakuliah*:
                                            <ul style="list-style: disc; padding-left: 30px;">
                                                <li>Pengantar Statistik</li>
                                                <li>Pengantar Ekonomi</li>
                                                <li>Pengantar Akuntansi</li>
                                                <li>Manajemen Keuangan</li>
                                                <li>Pengantar Hukum Bisnis (optional, tapi lebih disarankan)</li>
                                            </ul>
                                        </li>
                                        <li>
                                            Untuk Program Waiver WMI, pada saat mendaftarkan diri, peserta telah LULUS dengan nilai minimum B untuk matakuliah**:
                                            <ul style="list-style: disc; padding-left: 30px;">
                                                <li>Prasyarat Program Waiver WPPE*</li>
                                                <li>Statistik Ekonomi/ Bisnis</li>
                                                <li>Mikro Ekonomi</li>
                                                <li>Makro Ekonomi</li>
                                                <li>Manajemen Investasi/ Analisa Sekuritas</li>
                                                <li>Manajemen Portofolio (optional, tapi lebih disarankan)</li>
                                            </ul>
                                        </li>
                                    </ol>

                                    <p class="text-justify"><em>*Matakuliah prasyarat  untuk Sertifikasi Profesi WPPE di atas tidak berlaku apabila calon peserta telah (1) memiliki Sertifikat Keahlian WPPE/ WPEE/ WMI dari TICMI/ PSP atau (2) sudah pernah memiliki Izin Profesi sebagai WPPE/ WPEE/ WMI dari OJK dan telah habis masa (kadaluarsa) berdasarkan ketentuan OJK</em></p>
                                    <p class="text-justify"><em>**Matakuliah prasyarat untuk Sertifikasi Profesi WMI di atas tidak berlaku apabila calon peserta telah memiliki (1) Sertifikat Keahlian WMI dari TICMI/ PSP atau sudah pernah memiliki Izin Profesi sebagai WMI dari OJK dan telah habis masa (kadaluarsa) berdasarkan ketentuan OJK, atau (2) memiliki sertifikat CFA/ CA/ CPA</em></p>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->

                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#jumlahSoal" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Berapa jumlah soal yang akan diujikan dan apa saja modulnya?
                                    </a>
                                </h4>
                            </div>
                            <div id="jumlahSoal" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <ol>
                                        <li>Ujian Keahlian terdiri dari 100 soal dan harus      diselesaikan dalam waktu 2 jam</li>
                                        <li>Modul untuk Ujian Keahlian WPPE:
                                            <ul style="list-style: disc; padding-left: 30px;">
                                                <li>Mekanisme Perdagangan Efek</li>
                                                <li>Pengetahuan Tentang Efek</li>
                                                <li>Analisis Ekonomi, Keuangan dan Investasi</li>
                                                <li>Hukum &amp; Etika WPPE</li>
                                            </ul>
                                        </li>
                                        <li>Modul untuk Ujian Keahlian WMI:
                                            <ul style="list-style: disc; padding-left: 30px;">
                                                <li>Analisis Laporan Keuangan Perusahaan</li>
                                                <li>Analisis Efek Bersifat Ekuitas</li>
                                                <li>Analisis Efek Bersifat Utang</li>
                                                <li>Manajemen Portofolio</li>
                                                <li>Mikro &amp; Makro Ekonomi untuk Bisnis</li>
                                                <li>Hukum &amp; Etika WMI</li><li>Matematika &amp; Statistika Keuangan &amp; Bisnis<br></li>
                                            </ul>
                                        </li>
                                    </ol>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->

                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#standarNilai" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Berapa Standar Nilai Kelulusan untuk setiap Ujian Keahlian?
                                    </a>
                                </h4>
                            </div>
                            <div id="standarNilai" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <ol>
                                        <li>Standar Kelulusan Ujian Keahlian WPPE adalah 65% untuk setiap modul yang diujikan (bukan nilai rata-rata)</li>
                                        <li>Standar Kelulusan Ujian Keahlian WMI adalah 60% untuk setiap modul yang diujikan (bukan nilai rata-rata)</li>
                                    </ol>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->

                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#tidakLulus" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Bagaimana jika peserta tidak memenuhi Standar Nilai Kelulusan pada Ujian Perdana?
                                    </a>
                                </h4>
                            </div>
                            <div id="tidakLulus" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <ol type="disc">
                                        <li>Peserta dapat mengikuti ujian ulang di TICMI. <br></li>
                                        <li>Ujian dilakukan hanya untuk modul-modul yang gagal uji hanya dapat dilakukan maksimal 3 kali dalam 1 (satu) minggu, mulai Senin sampai Kamis<br></li>
                                        <li>Pelaksanaan dan penyelenggaraan teknis ujian ulang di daerah akan disesuaikan dengan kebijakan Kantor Perwakilan BEI setempat<br></li>
                                        <li>Seluruh modul yang  disyaratkan dalam setiap program sertifikasi harus sudah        ditempuh LULUS dalam&nbsp;tenggat waktu 18  bulan sejak ujian perdana        dilaksanakan, atau akan dinyatakan hangus (kadaluarsa) berdasarkan ketentuan OJK</li>
                                    </ol>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->

                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#biayaUjianUlang" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Berapa Biaya untuk Ujian Ulang?
                                    </a>
                                </h4>
                            </div>
                            <div id="biayaUjianUlang" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <ol>
                                        <li>Biaya Ujian Ulang adalah sebesar Rp200.000,- per ujian per modul atau maskimal Rp500.000,- jika mengulang lebih dari 2 modul.</li>
                                    </ol>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->

                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#sertifikatOJK" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Bilamana peserta bisa mendapatkan Sertifikat Keahlian dan Izin Profesi dari OJK?
                                    </a>
                                </h4>
                            </div>
                            <div id="sertifikatOJK" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <ol>
                                        <li>Sertifikat Keahlian akan diperoleh jika peserta sudah mengikuti ujian dan memenuhi Standar Nilai Kelulusan yang telah ditetapkan</li>
                                        <li>Izin Profesi dari OJK merupakan izin yang melekat pada orang per seorangan yang bersifat pribadi. Izin Profesi dari OJK dan hanya dapat diperoleh melalui proses pengajuan resmi kepada OJK yang dilakukan secara individual dengan melampirkan Sertifikat Keahlian Pasar Modal yang dikeluarkan TICMI</li>
                                    </ol>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->

                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#wavierMulai" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Kapan Program Waiver ini akan dimulai?
                                    </a>
                                </h4>
                            </div>
                            <div id="wavierMulai" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <ol>
                                        <li>Program Waiver 2016 akan dimulai tanggal 5 Maret 2016 </li>
                                        <li>Penyelenggaraan di daerah dapat saja berbeda, sesuai kebijakan Kantor Perwakilan Bursa Efek Indonesia setempat.</li>
                                    </ol>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->

                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#caraDaftar" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Bagaimana caranya  mendaftarkaan diri?
                                    </a>
                                </h4>
                            </div>
                            <div id="caraDaftar" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <div align="justify">
                                        Silakan kunjungi laman <a target="_blank" title="" href="http://www.ticmi.co.id/index/daftar2/111/Pendaftaran%20Sertifikasi%20WPPE%20Program%20Waiver.html">Pendaftaran Sertifikasi WPPE Program Waiver 2016</a> untuk Sertifikasi WPPE<br>
                                        Silakan kunjungi laman <a target="_blank" title="" href="http://www.ticmi.co.id/index/daftar2/112/Pendaftaran%20Sertifikasi%20WMI%20Program%20Waiver.html">Pendaftaran Sertifikasi WMI Program Waiver 2016</a> untuk Sertifikasi WMI<br>
                                        Silakan kunjungi laman <a target="_blank" title="" href="http://www.ticmi.co.id/index/PIC.html">Pendaftaran PIC Institusi</a> untuk pendaftaran secara masal oleh satu insitusi<br><br>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->

                    </div>
                </div>
            </div>
        </div><!-- Container -->
    </div><!-- Page Default -->
@endsection