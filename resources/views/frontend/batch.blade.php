
            @if($workshops && $workshops->count() > 0)
                @foreach($workshops as $workshop)
                    <div class="col-sm-3">
                        <div class="event-wrap">
                            <div class="event-img-wrap course-banner-wrap bg-pin-red color-white text-center" style="">
                                @if($workshop->program->nm_program == "Forum")
                                    <img alt="Event" class="img-responsive" src="./assets/images/course/forum-course-ticmi.jpg" width="600" height="220">
                                @else
                                    <img alt="Event" class="img-responsive" src="./assets/images/course/workshop-umum.jpg" width="600" height="220">
                                @endif
                                <span class="cat bg-yellow">{{ $workshop->program->nm_program }}</span>
                            </div>
                            <!-- Event Detail Wrapper -->
                            <div class="event-details" style="height: 450px;">
                                <h4 class="batch-title" style="font-size: 16px;"><a href="{{ route('pelatihan',['workshopslug'=>$workshop->slugs]) }}" onclick="ga('send', 'event', 'Pendaftaran Workshop', '{{ $workshop->workshop_name }}', '{{ url()->current() }}' )">{{ $workshop->workshop_name }}</a></h4>
                                <ul class="events-meta">
                                    <li><i class="fa fa-calendar-o"></i>{{ strftime('%A, %#d %B %Y',strtotime($workshop->tgl_mulai)) }}</li>
                                    <li><i class="fa fa-map-marker"></i>{{ $workshop->lokasi }}</li>
                                    <li><i class="fa fa-users"></i> {{ $workshop->waktu }}</li>
                                </ul>
                                <p>{!! substr(strip_tags($workshop->deskripsi),0,150) !!}</p>
                                <a href="{{ route('pelatihan',['workshopslug'=>$workshop->slugs]) }}" onclick="ga('send', 'event', 'Pendaftaran Workshop', '{{ $workshop->workshop_name }}', '{{ url()->current() }}' )" class="btn"><i class="fa fa-angle-double-right"></i> Saya ingin daftar</a>
                            </div><!-- Event Meta -->
                        </div>
                    </div>
                @endforeach
            @endif
            <div class="clearfix"></div>

            @if($resbatch && $resbatch->count() > 0)
                @php($no = 1)
                @foreach($resbatch as $value)
                <!-- Event Column -->
                    <div class="col-sm-3">
                        <!-- Event Wrapper -->
                        <div class="event-wrap">
                            <div class="event-img-wrap course-banner-wrap bg-pin-red color-white text-center" style="">
                                @if($value->program->nm_program == "WPPE")
                                    @if($value->kelas->type->nama == "Regular")
                                        <img alt="Event" class="img-responsive" src="./assets/images/course/wppe-reguler-3.jpg" width="600" height="220">
                                    @elseif($value->kelas->type->nama == "Waiver")
                                        <img alt="Event" class="img-responsive" src="./assets/images/course/wppe-waiver-3.jpg" width="600" height="220">
                                    @else
                                    @endif
                                @elseif($value->program->nm_program == "WPPE Pemasaran")
                                    @if($value->kelas->type->nama == "Regular")
                                        <img alt="Event" class="img-responsive" src="./assets/images/course/wppe-pemasaran-reguler.jpg" width="600" height="220">
                                    @elseif($value->kelas->type->nama == "Waiver")
                                        <img alt="Event" class="img-responsive" src="./assets/images/course/wppe-pemasaran-waiver.jpg" width="600" height="220">
                                    @else
                                    @endif
                                @elseif($value->program->nm_program == "WPPE Pemasaran Terbatas")
                                    <img alt="Event" class="img-responsive" src="./assets/images/course/wppe-pemasaran-terbatas.jpg" width="600" height="220">
                                @elseif($value->program->nm_program == "WMI")
                                    @if($value->kelas->type->nama == "Regular")
                                        <img alt="Event" class="img-responsive" src="./assets/images/course/wmi-reguler-3.jpg" width="600" height="220">
                                    @elseif($value->kelas->type->nama == "Waiver")
                                        <img alt="Event" class="img-responsive" src="./assets/images/course/wmi-waiver-3.jpg" width="600" height="220">
                                    @else
                                    @endif
                                @elseif($value->program->nm_program == "WPEE")
                                    @if($value->kelas->type->nama == "Regular")
                                        <img alt="Event" class="img-responsive" src="./assets/images/course/wpee-reguler.jpg" width="600" height="220">
                                    @elseif($value->kelas->type->nama == "Waiver")
                                        <img alt="Event" class="img-responsive" src="./assets/images/course/wpee-waiver.jpg" width="600" height="220">
                                    @else
                                    @endif
                                @elseif($value->program->nm_program == "ASPM")
                                    @if($value->kelas->type->nama == "Regular")
                                        <img alt="Event" class="img-responsive" src="./assets/images/course/aspm-reguler-3.jpg" width="600" height="220">
                                    @elseif($value->kelas->type->nama == "Waiver")
                                        <img alt="Event" class="img-responsive" src="./assets/images/course/aspm-akselerasi-3.jpg" width="600" height="220">
                                    @else
                                    @endif
                                @elseif($value->program->nm_program == "Placement Test ASPM")
                                    <img alt="Event" class="img-responsive" src="./assets/images/course/aspm-akselerasi-3.jpg" width="600" height="220">
                                @else
                                    <img alt="Event" class="img-responsive" src="./assets/images/course/wppe-waiver-3.jpg" width="600" height="220">
                                @endif
                            </div>
                            <!-- Event Image Wrapper -->
                            <!-- Event Detail Wrapper -->
                            <div class="event-details" style="height: 450px;">
                                <h4 class="batch-title" style="font-size: 16px;"><a href="{{ url('daftar/'.$value->id.'/'.$value->nama) }}">{{ $value->nama }} </a></h4>
                                <ul class="events-meta">
                                    <li><i class="fa fa-calendar-o"></i> {{ $value->tanggal->formatLocalized('%A, %#d %B %Y') }}</li>
                                    <li><i class="fa fa-map-marker"></i> {{ !empty($value->lokasi) ? $value->lokasi:"Gedung Bursa Efek Indonesia" }}</li>
                                    <li><i class="fa fa-users"></i> Minimal Peserta {{ !empty($value->minimal) ? $value->minimal:10 }} Orang</li>
                                </ul>
                                <p>{{ substr($value->keterangan,0,150) }}</p>
                                <a href="{{ url('daftar/'.$value->id.'/'.$value->nama) }}" class="btn"><i class="fa fa-angle-double-right"></i> Saya ingin daftar</a>
                                {{--@if($value->kd_type != 1)--}}
                                    {{--<a href="http://akademik.ticmi.co.id/index/daftar{{ $value->kd_type }}/{{ $value->id }}/Pendaftaran {{ $value->nama }}.html" class="btn">Saya ingin daftar</a>--}}
                                {{--@else--}}
                                    {{--<a href="http://akademik.ticmi.co.id/index/daftar/{{ $value->cabang }}/{{ $value->id }}/Pendaftaran {{ $value->nama }}.html" class="btn">Saya ingin daftar</a>--}}
                                {{--@endif--}}
                            </div><!-- Event Meta -->
                            <br/>
                        </div><!-- Event details -->
                    </div><!-- Column -->
                @if($no%4 == 0)
                    <div class="clearfix"></div>
                @endif
                @php($no++)
                @endforeach
            @endif


            @if($resbatch && $resbatch->count() == 0 && $workshops && $workshops->count() == 0)
                <div class="col-sm-12">
                    <div class="alert alert-warning" role="alert"><h4>Maaf, belum ada jadwal tersedia.</h4></div>
                </div>
            @endif