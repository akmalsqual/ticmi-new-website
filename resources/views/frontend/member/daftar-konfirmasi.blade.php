@extends('layout.member.base')
@section('member-content')
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">Konfirmasi Pembayaran</h3>
        </div>
        <div class="panel-body">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>No Invoice</th>
                    <th>Status</th>
                    <th class="text-right">Nominal</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                @php(\Carbon\Carbon::setLocale('id'))
                @if($keranjang && $keranjang->count() > 0)
                    @foreach($keranjang as $item)
                        <tr>
                            <td>{{ $item->created_at->diffForHumans() }}</td>
                            <td>#{{ $item->invoice_no }}</td>
                            <td>
                                @if($item->is_confirm == 0)
                                    Belum Konfirmasi
                                @else
                                    Menunggu Verifikasi Pembayaran
                                @endif
                            </td>
                            <td class="text-right">Rp{{ number_format($item->grandtotal,0,',','.') }}</td>
                            <td class="text-center">
                                @if($item->is_confirm == 0)
                                    <div class="btn-group" role="group">
                                        <a href="{{ route('transaksi.konfirmasi',['keranjang'=>$item->id]) }}" title="Konfirmasi Pembayaran" class="btn btn-xs showtootltip"><span class="fa fa-money"></span> </a>
                                    </div>
                                @endif
                                <a href="{{ route('transaksi.tagihan.view',['keranjang'=>$item->id]) }}" title="Lihat Data Transaksi" class="btn btn-xs btn-warning showtootltip"><span class="fa fa-search"></span> </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                @if($seminars && $seminars->count() > 0)
                    @foreach($seminars as $seminar)
                        <tr>
                            <td>{{ $seminar->created_at->diffForHumans() }}</td>
                            <td>{{ $seminar->invoice_no }}</td>
                            <td>
                                @if($seminar->is_confirm == 0)
                                    Belum Konfirmasi
                                @else
                                    Menunggu Verifikasi Pembayaran
                                @endif
                            </td>
                            <td class="text-right">Rp{{ number_format($seminar->total_bayar,0,',','.') }}</td>
                            <td>
                                @if($seminar->is_confirm == 0)
                                    <div class="btn-group" role="group">
                                        <a href="{{ route('pelatihan.konfirmasi',['workshopslug'=>$seminar->workshop->slugs,'pesertaworkshopinvoice'=>$seminar->invoice_no]) }}" title="Konfirmasi Pembayaran" class="btn btn-xs showtootltip"><span class="fa fa-money"></span> </a>
                                    </div>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif

                @if($seminars->count() == 0 && $keranjang->count() == 0)
                    <tr>
                        <td colspan="5">Belum ada tagihan terbaru</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection