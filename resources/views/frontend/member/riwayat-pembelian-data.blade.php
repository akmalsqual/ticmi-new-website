@extends('layout.member.base')
@section('member-content')
    {{--<div class="content-box shadow bg-white">--}}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Riwayat Pembelian Data</h3>
            </div>
            <div class="panel-body">
                @include('errors.list')
                @include('flash::message')
                <table class="table">
                    <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>No Invoice</th>
                        <th>Status</th>
                        <th class="text-right">Nominal</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php(\Carbon\Carbon::setLocale('id'))
                    @if($keranjang && $keranjang->count() > 0)
                        @foreach($keranjang as $item)
                            <tr>
                                <td>{{ $item->created_at->diffForHumans() }}</td>
                                <td>#{{ $item->invoice_no }}</td>
                                <td>
                                    @if($item->is_confirm == 0)
                                        <span class="label label-warning">Belum Konfirmasi</span>
                                    @else
                                        @if($item->is_payment_approve == 0)
                                            <span class="label label-success">Menunggu Verifikasi Pembayaran</span>
                                        @else
                                            <span class="label label-info">Sudah dibayar</span>
                                        @endif
                                    @endif
                                </td>
                                <td class="text-right">Rp{{ number_format($item->grandtotal,0,',','.') }}</td>
                                <td class="text-center">
                                    @if($item->is_confirm == 0)
                                        <div class="btn-group" role="group">
                                            <a href="{{ route('transaksi.konfirmasi',['keranjang'=>$item->id]) }}" title="Konfirmasi Pembayaran" class="btn btn-xs showtootltip"><span class="fa fa-money"></span> </a>
                                        </div>
                                    @endif
                                    @if($item->is_payment_approve == 1)
                                            <a href="{{ route('transaksi.tagihan.view',['keranjang'=>$item->id]) }}" title="Download Data" class="btn btn-xs btn-info showtootltip"><span class="fa fa-download"></span> </a>
                                    @else
                                            <a href="{{ route('transaksi.tagihan.view',['keranjang'=>$item->id]) }}" title="Lihat Data Transaksi" class="btn btn-xs btn-warning showtootltip"><span class="fa fa-search"></span> </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">Belum ada tagihan terbaru</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>

    {{--</div>--}}
@endsection