@extends('layout.home')
@section('content')

    <!-- Page Main -->
    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">

            <div class="row shop-forms">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 content-box shadow bg-white">
                            <ul class="text-center">
                                <!-- Page Template Logo -->
                                <li class="logo-wrap">
                                    <a href="" class="logo">
                                        <h5>Reset Password</h5>
                                    </a>
                                </li><!-- Page Template Logo -->
                            </ul>
                            @include('errors.list')
                            @if(session()->has('flash_notification.message'))
                                @include('flash::message')
                            @else
                                {{ Form::open(['url'=>route('forgotpassword.proses')]) }}
                                <div class="input-text form-group text-left">
                                    {!! Form::text('email',null,['class'=>'input-name form-control','placeholder'=>'Masukkan Email Anda']) !!}
                                    <small class="form-note">Cek kembali Email Anda, pastikan email yang Anda masukkan benar</small>
                                </div>
                                {!! Form::submit('Reset Password',['class'=>'btn btn-block','data-loading-text'=>'Loading ...','data-toggle'=>'loading','data-confirm'=>'false']) !!}
                                {{ Form::close() }}
                            @endif

                            <br>
                            <br>
                            <ul class="text-center">
                                <li>
                                    Kembali ke halaman <a href="{{ route('login') }}">Login</a> atau <a href="{{ route('registration') }}">Daftar</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- Row -->

        </div><!-- Container -->
    </div><!-- Page Default -->
    <!-- Page Main -->


@endsection