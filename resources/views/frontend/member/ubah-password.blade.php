@extends('layout.member.base')
@section('member-content')
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">Ubah Password</h3>
        </div>
        <div class="panel-body">
            @include('errors.list')
            @include('flash::message')
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    {!! Form::open(['url'=>route('member.profile.password.update'),'files'=>true]) !!}
                    <div class="form-group">
                        {!! Form::label('old_password','Password Saat Ini') !!} <strong class="text-red">*</strong>
                        {!! Form::password('old_password',['class'=>'form-control','placeholder'=>'Masukkan Password Anda Saat Ini']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('new_password','Password Baru') !!} <strong class="text-red">*</strong>
                        {!! Form::password('new_password',['class'=>'form-control','placeholder'=>'Masukkan Password Baru']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('new_password_confirmation','Konfirmasi Password Baru') !!} <strong class="text-red">*</strong>
                        {!! Form::password('new_password_confirmation',['class'=>'form-control','placeholder'=>'Konfirmasi Password Baru']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::submit('Ubah Password',['class'=>'btn btn-block','data-loading-text'=>'Loading ...','data-toggle'=>'loading','data-confirm'=>'false']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection