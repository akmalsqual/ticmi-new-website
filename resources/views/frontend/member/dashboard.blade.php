@extends('layout.member.base')
@section('member-content')

                    <div class="content-box shadow bg-white">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td valign="middle" class="text-center">
                                    <img src="{{ GeneralHelper::getGravatar(Auth::user()->email) }}" align="center" alt="{{ Auth::user()->name }}" class="img-responsive img-circle img-center">
                                    <strong>{{ Auth::user()->name }}</strong>
                                </td>
                                <td>
                                    <table class="table course-table no-margin">
                                        <tr>
                                            <td>Email</td>
                                            <td>: {{ Auth::user()->email }}</td>
                                            <td>No HP</td>
                                            <td>: {{ Auth::user()->no_hp }}</td>
                                            <td rowspan="2" valign="middle">
                                                <a href="{{ route('member.profile') }}" class="btn btn-sm btn-default">Ubah</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>No SID</td>
                                            <td>: {{ Auth::user()->no_sid }}</td>
                                            <td>Profesi</td>
                                            <td>: {{ Auth::user()->profesi }}</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Tagihan Pembayaran</h3>
                            </div>
                            <div class="panel-body">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>No Invoice</th>
                                        <th>Status</th>
                                        <th class="text-right">Nominal</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php(\Carbon\Carbon::setLocale('id'))
                                    @if($keranjang && $keranjang->count() > 0)
                                        @foreach($keranjang as $item)
                                            <tr>
                                                <td>{{ $item->created_at->diffForHumans() }}</td>
                                                <td>#{{ $item->invoice_no }}</td>
                                                <td>
                                                    @if($item->is_confirm == 0)
                                                        Belum Konfirmasi
                                                    @else
                                                        Sudah Konfirmasi
                                                    @endif
                                                </td>
                                                <td class="text-right">Rp{{ number_format($item->grandtotal,0,',','.') }}</td>
                                                <td class="text-center">
                                                    @if($item->is_confirm == 0)
                                                    <div class="btn-group" role="group">
                                                        <a href="{{ route('transaksi.konfirmasi',['keranjang'=>$item->id]) }}" title="Konfirmasi Pembayaran" class="btn btn-xs showtootltip"><span class="fa fa-money"></span> </a>
                                                    </div>
                                                    @endif
                                                    <a href="{{ route('transaksi.tagihan.view',['keranjang'=>$item->id]) }}" title="Lihat Data Transaksi" class="btn btn-xs btn-warning showtootltip"><span class="fa fa-search"></span> </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                    <tr>
                                        <td colspan="3">Belum ada tagihan terbaru</td>
                                    </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
@endsection