@extends('layout.home')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('{{ url('assets/images/banner/profile.jpg') }}') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Pendaftaran user baru</h3>
                        <h6 class="sub-title">Silahkan daftar disini</h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <!-- Page Main -->
    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">

            <div class="row shop-forms">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 content-box shadow bg-white">
                            @include('errors.list')
                            @include('flash::message')
                            <div>
                                <hr>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading text-center">
                                    <h3 class="panel-title">Terima kasih sudah melakukan registrasi, silahkan cek Email Anda untuk melakukan verifikasi email.</h3>
                                    {{--Jika Anda belum menerima email konfirmasi, silahkan klik tombol dibawah <br/><br/>--}}
                                    {{--<a href="{{ route('registration.resendverification',['email'=>$user->email]) }}" class="btn btn-primary">Kirim kembali email verifikasi</a>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- Row -->

        </div><!-- Container -->
    </div><!-- Page Default -->
    <!-- Page Main -->


@endsection