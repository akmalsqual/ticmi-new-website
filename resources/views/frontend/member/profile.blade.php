@extends('layout.member.base')
@section('member-content')

    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">Profile</h3>
        </div>
        <div class="panel-body">
            @include('errors.list')
            @include('flash::message')
            {!! Form::open(['url'=>route('member.profile.store'),'files'=>true]) !!}
            <div class="form-group">
                {!! Form::label('name','Nama') !!} <strong class="text-red">*</strong>
                {!! Form::text('name',Auth::user()->name,['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('no_hp','No Handphone') !!} <strong class="text-red">*</strong>
                {!! Form::text('no_hp',Auth::user()->no_hp,['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('tgl_lahir','Tanggal Lahir') !!} <strong class="text-red">*</strong>
                {!! Form::text('tgl_lahir',Auth::user()->tgl_lahir,['class'=>'form-control datepicker','placeholder'=>'yyyy-mm-dd']) !!}
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-12">
                        {!! Form::label('profesi','Profesi Anda') !!}
                        {!! Form::select('profesi',$profesi,(in_array(Auth::user()->profesi,$profesi) ? Auth::user()->profesi:(!empty(Auth::user()->profesi) ? 'Lainnya':'')),['class'=>'form-control','placeholder'=>'Pilih profesi Anda']) !!}
                    </div>
                    <div class="col-md-12 profesi_lainnya" style="display: {{ (in_array(Auth::user()->profesi,$profesi) ? 'none':(!empty(Auth::user()->profesi) ? 'block':'none')) }};">
                        {!! Form::text('profesi_lainnya',(!in_array(Auth::user()->profesi,$profesi) ? Auth::user()->profesi:''),['class'=>'form-control','placeholder'=>'Sebutkan profesi Anda']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    {!! Form::label('no_sid','Nomor SID') !!}
                    @if(Auth::user()->is_valid_sid)
                        {!! Form::text('no_sid',Auth::user()->no_sid,['class'=>'form-control','readonly'=>'readonly','placeholder'=>'Contoh : IDD2601EB4556']) !!}
                    @else
                        {!! Form::text('no_sid',Auth::user()->no_sid,['class'=>'form-control','placeholder'=>'Contoh : IDD2601EB4556']) !!}
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 form-group">
                    {!! Form::label('sid_card','Kartu Akses') !!}
                    <div class="row">
                        <div class="col-sm-4">
                            @if(Auth::user()->sid_card)
                                <img src="{{ url('upload/kartu_akses/'.Auth::user()->sid_card) }}" class="img-responsive" alt="">
                            @else
                                <img src="{{ url('assets/images/ticmi/SID.jpg') }}" class="img-responsive" alt="Contoh Kartu SID">
                            @endif
                        </div>
                        <div class="col-sm-8">
                            {!! Form::file('sid_card',['class'=>'form-control']) !!}
                            <small class="form-note">Format : Jpeg,Jpg,Png</small>
                        </div>
                    </div>
                    <small class="form-note text-left" style="text-align: left;">Masukkan scan bagian depan Kartu Akses Anda <a href="#" onclick="return false" data-container="body" data-toggle="popover" data-html="true" data-trigger="focus" data-content="Kartu Akses merupakan kartu yang diperoleh jika Anda menjadi Investor yang dikeluarkan oleh KSEI. <a href='http://akses.ksei.co.id/info/faq' target='_blank'>Info lebih lanjut</a> "><span class="fa fa-question-circle"></span></a> </small>
                    <br>
                    <br>
                </div>
            </div>
            <div class="form-group">
                {!! Form::button('Ubah Profil',['class'=>'btn btn-lg btn-block','type'=>'submit','data-toggle'=>'loading','data-loading-text'=>'Loading ...']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection