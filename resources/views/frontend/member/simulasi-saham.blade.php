@extends('layout.member.base')
@section('member-content')

    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">Online Trading Simulation</h3>
        </div>
        <div class="panel-body">
            @include('errors.list')
            @include('flash::message')
            <div class="alert alert-info">
                <p>Untuk tampilan dan performa optimal dalam mengakses <strong>Online Trading Simulation</strong>, Kami menyarankan untuk menggunakan <em>Web Browser</em> <a href="https://ftp.mozilla.org/pub/firefox/releases/34.0b9/win32/en-US/Firefox%20Setup%2034.0b9.exe" target="_blank">Mozilla Firefox Versi 34</a></p>
                <p><strong>Online Trading Simulation</strong> hanya bisa di akses pada hari dan jam Perdagangan, yang mengikuti jadwal Perdagangan Bursa Efek Indonesia. Berikut jadwal perdagangan dari Bursa Efek Indonesia : </p>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Hari</th>
                        <th>Sesi I</th>
                        <th>Sesi II</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Senin s/d Kamis</td>
                        <td>Pukul 09:00:00 s/d 12:00:00</td>
                        <td>Pukul 13:30:00 s/d 15:49:59</td>
                    </tr>
                    <tr>
                        <td>Jum'at</td>
                        <td>Pukul 09:00:00 s/d 11:30:00</td>
                        <td>Pukul 14:00:00 s/d 15:49:59</td>
                    </tr>
                    </tbody>
                </table>
                <p>Online Trading Simulation tidak bisa diakses pada hari sabtu, minggu dan hari libur nasional</p>
            </div>
            @if($userWinGamers && $userWinGamers->count() > 0)
                <a href="http://www.winsimulation.com?iframe=true&width=100%&height=100%" rel="prettyPhoto[{{ auth()->user()->email }}]" title="Online Trading Simulation" onclick="ga('send', 'event', 'OLTS', 'Login', 'Relogin' )" class="btn btn-lg btn-block">Buka Online Trading Simulation</a>
            @else
                <a href="#" title="Online Trading Simulation" class="btn btn-lg btn-block onlineTradingSimulation" onclick="ga('send', 'event', 'OLTS', 'Login', 'New login' )">Buka Online Trading Simulation</a>
            @endif
        </div>
    </div>
@endsection