@extends('layout.home')
@section('content')

    <!-- Page Main -->
    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">

            <div class="row shop-forms">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 content-box shadow bg-white">
                            <ul class="text-center">
                                <!-- Page Template Logo -->
                                <li class="logo-wrap">
                                    <a href="" class="logo">
                                        <h5>Reset Password</h5>
                                    </a>
                                </li><!-- Page Template Logo -->
                            </ul>
                                @include('errors.list')
                                @include('flash::message')

                                {{ Form::open(['url'=>route('resetpassword.proses',['useremail'=>trim($useremail),'userid'=>$userid])]) }}
                                <div class="alert alert-info">
                                    Silahkan cek email Anda untuk mendapatkan Token Password dan Masukkan Token dan Password baru Anda pada form dibawah ini. Cek folder Spam pada email Anda
                                    jika email reset password belum Anda terima.
                                </div>
                                <div class="input-text form-group text-left">
                                    {!! Form::text('token',null,['class'=>'input-name form-control','placeholder'=>'Masukkan Token Password Anda yang terdapat di email']) !!}
                                </div>
                                <div class="input-text form-group text-left">
                                    {!! Form::password('password',['class'=>'input-name form-control','placeholder'=>'Masukkan Password Baru Anda']) !!}
                                </div>
                                <div class="input-text form-group text-left">
                                    {!! Form::password('password_confirmation',['class'=>'input-name form-control','placeholder'=>'Masukkan Kembali Password Baru Anda']) !!}
                                </div>
                                {!! Form::submit('Reset Password',['class'=>'btn btn-block','data-loading-text'=>'Loading ...','data-toggle'=>'loading','data-confirm'=>'false']) !!}
                                {{ Form::close() }}
                            <br>
                            <br>
                            <ul class="text-center">
                                <li>
                                    Kembali ke halaman <a href="{{ route('login') }}">Login</a> atau <a href="{{ route('registration') }}">Daftar</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- Row -->

        </div><!-- Container -->
    </div><!-- Page Default -->
    <!-- Page Main -->


@endsection