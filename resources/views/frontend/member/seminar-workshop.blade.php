@extends('layout.member.base')
@section('member-content')
    {{--<div class="content-box shadow bg-white">--}}
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Seminar dan Workshop</h3>
        </div>
        <div class="panel-body">
            <div class="alert alert-info">
                Semua Seminar dan Workshop yang sudah Anda ikuti.
            </div>
            @include('errors.list')
            @include('flash::message')
            <table class="table">
                <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Seminar/Workshop</th>
                    <th class="text-right">Biaya</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                @if($workshops && $workshops->count() > 0)
                    @foreach($workshops as $item)
                        <tr>
                            <td>{{ strftime('%d %B %Y',strtotime($item->tgl_mulai)) }}</td>
                            <td>
                                @if(strlen($item->workshop_name) > 50)
                                    <abbr title="{{ $item->workshop_name }}" class="showtooltip">{{ substr($item->workshop_name,0,50) }}</abbr>
                                @else
                                    {{ $item->workshop_name }}
                                @endif
                            </td>
                            <td class="text-right">Rp{{ number_format($item->peserta->total_bayar,0) }}</td>
                            @if($item->peserta->is_payment_approved)
                                <td><label for="" class="label label-success">Sudah di bayar</label></td>
                                <td>
                                    <a href="{{ route('member.seminar.view',['workshopslug'=>$item->slugs]) }}" class="btn btn-success btn-xs showtooltip" title="Detail Seminar/Workshop & Unduh Materi Dan Modul"><i class="fa fa-book"></i></a>
                                </td>
                            @else
                                @if($item->peserta->is_confirm)
                                    <td><label for="" class="label label-warning">Proses Konfirmasi</label></td>
                                    <td>
                                        <a href="#" class="btn btn-default btn-xs disabled"><i class="fa fa-book"></i></a>
                                    </td>
                                @else
                                    <td><label for="" class="label label-danger">Belum di bayar</label></td>
                                    <td>
                                        <a href="#" class="btn btn-default btn-xs disabled"><i class="fa fa-book"></i></a>
                                    </td>
                                @endif
                            @endif
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

    {{--</div>--}}
@endsection