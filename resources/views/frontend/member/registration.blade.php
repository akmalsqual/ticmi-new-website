@extends('layout.home')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('{{ url('assets/images/banner/profile.jpg') }}') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Pendaftaran user baru</h3>
                        <h6 class="sub-title">Silahkan daftar disini</h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <!-- Page Main -->
        <div class="page-default bg-grey typo-dark">
            <!-- Container -->
            <div class="container">

                <div class="row shop-forms">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-8 col-sm-offset-2 content-box shadow bg-white">
                                <!-- Tab -->
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    @if(old('non_investor') == 'yes')
                                        <li role="presentation"><a href="#investor" aria-controls="home" role="tab" data-toggle="tab">PEMILIK SID</a></li>
                                        <li role="presentation" class="active"><a href="#noninvestor" aria-controls="profile" role="tab" data-toggle="tab">NON PEMILIK SID</a></li>
                                    @else
                                        <li role="presentation" class="active"><a href="#investor" aria-controls="home" role="tab" data-toggle="tab">PEMILIK SID</a></li>
                                        <li role="presentation"><a href="#noninvestor" aria-controls="profile" role="tab" data-toggle="tab">NON PEMILIK SID</a></li>
                                    @endif
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade {{ (old('non_investor') != 'yes') ? 'in active':'' }}" id="investor">

                                        {!! Form::open(['url'=>route('registration.store'),'files'=>true,'name'=>'formRegInvestor']) !!}
                                        @if (count($errors) == 0)
                                            <div class="panel panel-info">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Silahkan mendaftar disini.</h3>
                                                </div>
                                            </div>
                                        @endif
                                        @include('errors.list')
                                        @include('flash::message')
                                        {{--<h4>Pendaftaran user baru</h4>--}}
                                        <div>
                                            <hr>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    {!! Form::label('nama','Nama Lengkap') !!}
                                                    {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Masukkan Nama Anda']) !!}
                                                    <small class="form-note">Masukkan nama sesuai dengan Kartu Identitas Anda <a href="#" onclick="return false" data-container="body" data-toggle="popover" data-trigger="focus" data-content="KTP/SIM/Passport"><span class="fa fa-question-circle"></span></a> </small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    {!! Form::label('no_hp','Nomor Handphone') !!}
                                                    {!! Form::text('no_hp',null,['class'=>'form-control','placeholder'=>'Masukkan No Handphone Anda']) !!}
                                                    <small class="form-note">Masukkan no HP Aktif Anda </small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-4">
                                                    {!! Form::label('tgl_lahir','Tanggal Lahir') !!}
                                                    {!! Form::selectRange('tanggal_lahir',1,31,null,['class'=>'form-control tanggal_lahir','placeholder'=>'Tanggal']) !!}
                                                </div>
                                                <div class="col-md-4">
                                                    {!! Form::label('tgl_lahir','&nbsp;') !!}
                                                    {!! Form::selectMonth('bulan_lahir',null,['class'=>'form-control bulan_lahir','placeholder'=>'Bulan']) !!}
                                                </div>
                                                <div class="col-md-4">
                                                    {!! Form::label('tgl_lahir','&nbsp;') !!}
                                                    {!! Form::selectRange('tahun_lahir',1940,date('Y'),null,['class'=>'form-control tahun_lahir','placeholder'=>'Tahun']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    {!! Form::label('profesi','Profesi Anda') !!}
                                                    {!! Form::select('profesi',$profesi,null,['class'=>'form-control','placeholder'=>'Pilih profesi Anda']) !!}
                                                </div>
                                                <div class="col-md-12 profesi_lainnya" style="display: none;">
                                                    {!! Form::text('profesi_lainnya',null,['class'=>'form-control','placeholder'=>'Sebutkan profesi Anda']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    {!! Form::label('email','Email') !!}
                                                    {!! Form::text('email',null,['class'=>'form-control','autocomplete'=>'off','onpaste'=>'return false','placeholder'=>'Masukkan Email Anda']) !!}
                                                    <small class="form-note">Pastikan e-mail yang Anda tuliskan valid <a href="#" onclick="return false" data-container="body" data-toggle="popover" data-trigger="focus" data-content="Kami akan mengirimkan e-mail konfirmasi ke e-mail Anda"><span class="fa fa-question-circle"></span></a> </small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    {!! Form::label('email_confirmation','Ulangi Email') !!}
                                                    {!! Form::text('email_confirmation',null,['class'=>'form-control','autocomplete'=>'off','onpaste'=>'return false','placeholder'=>'Masukkan kembali Email Anda']) !!}
                                                    <small class="form-note">Pastikan e-mail yang Anda tuliskan valid <a href="#" onclick="return false" data-container="body" data-toggle="popover" data-trigger="focus" data-content="Kami akan mengirimkan e-mail konfirmasi ke e-mail Anda"><span class="fa fa-question-circle"></span></a> </small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    {!! Form::label('password','Password') !!}
                                                    {!! Form::password('password',['class'=>'form-control','placeholder'=>'Password']) !!}
                                                </div>
                                                <div class="col-md-6">
                                                    {!! Form::label('password_confirmation','Ulangi Password') !!}
                                                    {!! Form::password('password_confirmation',['class'=>'form-control','placeholder'=>'Ulangi Password']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <hr>
                                                Jika Anda memiliki SID, maka layanan data TICMI dapat diperoleh secara gratis.
                                                Apakah Anda sudah memiliki SID ? Silahkan masukkan <abbr title="Single Investor Identification">SID</abbr> Anda dan Upload Kartu Akses Anda.
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                {!! Form::label('no_sid','Nomor SID') !!}
                                                {!! Form::text('no_sid',null,['class'=>'form-control cekSid','placeholder'=>'Contoh : IDD2601EB4556']) !!}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 form-group">
                                                {!! Form::label('sid_card','Kartu Akses') !!}
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <img src="{{ url('assets/images/ticmi/SID.jpg') }}" class="img-responsive" alt="Contoh Kartu SID">
                                                    </div>
                                                    <div class="col-sm-8">
                                                        {!! Form::file('sid_card',['class'=>'form-control']) !!}
                                                        <small class="form-note">Format : Jpeg,Jpg,Png</small>
                                                    </div>
                                                </div>
                                                <small class="form-note text-left" style="text-align: left;">Masukkan scan bagian depan Kartu Akses Anda <a href="#" onclick="return false" data-container="body" data-toggle="popover" data-html="true" data-trigger="focus" data-content="Kartu Akses merupakan kartu yang diperoleh jika Anda menjadi Investor yang dikeluarkan oleh KSEI. <a href='http://akses.ksei.co.id/info/faq' target='_blank'>Info lebih lanjut</a> "><span class="fa fa-question-circle"></span></a> </small>
                                                <br>
                                                <br>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                {!! Form::checkbox('newsletter',1,true) !!} Saya ingin menerima newsletter melalui e-mail
                                                <br>
                                                <small>Dengan mendaftar Anda berarti sudah menyetujui <a href="#">Kebijakan privasi TICMI</a></small>
                                            </div>
                                            <div class="col-md-12 text-center">
                                                <br>
                                                {{--<input type="submit" value="Daftar" class="btn" data-loading-text="Loading...">--}}
                                                {!! Form::button('Daftar',['class'=>'btn btn-lg btn-block btn-form-sid','data-loading-text'=>'Loading ...']) !!}
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade {{ (old('non_investor') == 'yes') ? 'in active':'' }}" id="noninvestor">
                                        {!! Form::open(['url'=>route('registration.store'),'files'=>true]) !!}
                                        {!! Form::hidden('non_investor','yes') !!}
                                        @if (count($errors) == 0)
                                            <div class="panel panel-info">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Silahkan mendaftar disini untuk kemudahan proses unduh data.</h3>
                                                </div>
                                            </div>
                                        @endif
                                        @include('errors.list')
                                        @include('flash::message')
                                        {{--<h4>Pendaftaran user baru</h4>--}}
                                        <div>
                                            <hr>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    {!! Form::label('nama','Nama Lengkap') !!}
                                                    {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Masukkan Nama Anda']) !!}
                                                    <small class="form-note">Masukkan nama sesuai dengan Kartu Identitas Anda <a href="#" onclick="return false" data-container="body" data-toggle="popover" data-trigger="focus" data-content="KTP/SIM/Passport"><span class="fa fa-question-circle"></span></a> </small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    {!! Form::label('no_hp','Nomor Handphone') !!}
                                                    {!! Form::text('no_hp',null,['class'=>'form-control','placeholder'=>'Masukkan No Handphone Anda']) !!}
                                                    <small class="form-note">Masukkan no HP Aktif Anda </small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-4">
                                                    {!! Form::label('tgl_lahir','Tanggal Lahir') !!}
                                                    {!! Form::selectRange('tanggal_lahir',1,31,null,['class'=>'form-control','placeholder'=>'Tanggal']) !!}
                                                </div>
                                                <div class="col-md-4">
                                                    {!! Form::label('tgl_lahir','&nbsp;') !!}
                                                    {!! Form::selectMonth('bulan_lahir',null,['class'=>'form-control','placeholder'=>'Bulan']) !!}
                                                </div>
                                                <div class="col-md-4">
                                                    {!! Form::label('tgl_lahir','&nbsp;') !!}
                                                    {!! Form::selectRange('tahun_lahir',1940,date('Y'),null,['class'=>'form-control','placeholder'=>'Tahun']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    {!! Form::label('profesi','Profesi Anda') !!}
                                                    {!! Form::select('profesi',$profesi,null,['class'=>'form-control','placeholder'=>'Pilih profesi Anda']) !!}
                                                </div>
                                                <div class="col-md-12 profesi_lainnya" style="display: none;">
                                                    {!! Form::text('profesi_lainnya',null,['class'=>'form-control','placeholder'=>'Sebutkan profesi Anda']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    {!! Form::label('email','Email') !!}
                                                    {!! Form::text('email',null,['class'=>'form-control','autocomplete'=>'off','onpaste'=>'return false','placeholder'=>'Masukkan Email Anda']) !!}
                                                    <small class="form-note">Pastikan e-mail yang Anda tuliskan valid <a href="#" onclick="return false" data-container="body" data-toggle="popover" data-trigger="focus" data-content="Kami akan mengirimkan e-mail konfirmasi ke e-mail Anda"><span class="fa fa-question-circle"></span></a> </small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    {!! Form::label('email_confirmation','Ulangi Email') !!}
                                                    {!! Form::text('email_confirmation',null,['class'=>'form-control','autocomplete'=>'off','onpaste'=>'return false','placeholder'=>'Masukkan kembali Email Anda']) !!}
                                                    <small class="form-note">Pastikan e-mail yang Anda tuliskan valid <a href="#" onclick="return false" data-container="body" data-toggle="popover" data-trigger="focus" data-content="Kami akan mengirimkan e-mail konfirmasi ke e-mail Anda"><span class="fa fa-question-circle"></span></a> </small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    {!! Form::label('password','Password') !!}
                                                    {!! Form::password('password',['class'=>'form-control','placeholder'=>'Password']) !!}
                                                </div>
                                                <div class="col-md-6">
                                                    {!! Form::label('password_confirmation','Ulangi Password') !!}
                                                    {!! Form::password('password_confirmation',['class'=>'form-control','placeholder'=>'Ulangi Password']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                {!! Form::checkbox('newsletter',1,true) !!} Saya ingin menerima newsletter melalui e-mail
                                                <br>
                                                <small>Dengan mendaftar Anda berarti sudah menyetujui <a href="#">Kebijakan privasi TICMI</a></small>
                                            </div>
                                            <div class="col-md-12 text-center">
                                                <br>
                                                {{--<input type="submit" value="Daftar" class="btn" data-loading-text="Loading...">--}}
                                                {!! Form::submit('Daftar',['class'=>'btn btn-lg btn-block','data-loading-text'=>'Loading ...','data-toggle'=>'loading']) !!}
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div><!-- Row -->

            </div><!-- Container -->
        </div><!-- Page Default -->
    <!-- Page Main -->


@endsection