@extends('layout.home')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('{{ url('assets/images/banner/profile.jpg') }}') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Verifikasi Email</h3>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <!-- Page Main -->
    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">

            <div class="row shop-forms">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 content-box shadow bg-white">
                            @include('errors.list')
                            @include('flash::message')
                            <div>
                                <hr>
                            </div>
                            @if($user && count($user) > 0)
                                @if($user->is_verify == 0)
                                    <div class="panel panel-info">
                                        <div class="panel-heading text-center">
                                            <h3 class="panel-title">Terima kasih sudah melakukan verifikasi email. Silahkan login <a href="{{ route('login') }}">disini</a> </h3>
                                        </div>
                                    </div>
                                @else
                                    <div class="panel panel-info">
                                        <div class="panel-heading text-center">
                                            <h3 class="panel-title">Silahkan login <a href="{{ route('login') }}">disini</a> </h3>
                                        </div>
                                    </div>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div><!-- Row -->

        </div><!-- Container -->
    </div><!-- Page Default -->
    <!-- Page Main -->


@endsection