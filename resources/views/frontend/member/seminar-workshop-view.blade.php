@extends('layout.member.base')
@section('member-content')
    {{--<div class="content-box shadow bg-white">--}}
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{ $workshop->workshop_name }}</h3>
        </div>
        <div class="panel-body">
            @include('errors.list')
            @include('flash::message')

            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td width="25%">Tanggal</td>
                    <td width="25%">{{ GeneralHelper::waktuPelaksanaan($workshop->tgl_mulai,$workshop->tgl_selesai) }}</td>
                    <td width="25%">Biaya</td>
                    <td>Rp{{ number_format($workshop->peserta->total_bayar,0) }}</td>
                </tr>
                <tr>
                    <td>Waktu</td>
                    <td>{{ $workshop->waktu }}</td>
                    <td>Lokasi / Ruangan</td>
                    <td>{{ $workshop->lokasi }} / {{ $workshop->ruangan }}</td>
                </tr>
                </tbody>
            </table>

            <h6 class="title-bg-line">Materi dan Dokumen</h6>
            <table class="table">
                <thead>
                <tr>
                    <th>Materi</th>
                    <th>Unduh</th>
                </tr>
                </thead>
                <tbody>
                @if($workshop && $workshop->materi->where('is_publish',1)->count() > 0)
                    @foreach($workshop->materi->where('is_publish',1) as $item)
                        <tr>
                            <td>
                                <strong>{{ $item->nama_materi }}</strong>
                                <p style="font-size: 12px;">{{ $item->keterangan }}</p>
                            </td>
                            <td width="15%">
                                <a href="{{ $item->link_file }}" class="btn btn-xs" target="_blank">Unduh <i class="fa fa-download"></i></a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

    {{--</div>--}}
@endsection