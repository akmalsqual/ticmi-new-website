@extends('layout.member.base')
@section('member-content')

    <div class="content-box shadow bg-white  shop-forms">
        <table class="table table-bordered bg-google-red typo-light" style="">
            <tr>
                <td width="25%"><span>No Tagihan <br> <strong>#{{ $keranjang->invoice_no }}</strong></span></td>
                <td width="25%"><span>Total Tagihan <br> <strong>Rp{{ number_format($keranjang->grandtotal,0,',','.') }}</strong></span></td>
                <td width="25%"><span>Status Pembelian <br> <strong>{{ CartHelper::getStatusTransaksi($keranjang) }}</strong></span></td>
                <td width="25%"><span>Metode Pembayaran <br> <strong>Transfer</strong></span></td>
            </tr>
        </table>

        <div class="text-center invoice-view">

            @if($keranjang->is_confirm == 0)
                Mohon lakukan pembayaran sebesar : <br><br>

                <div class="inv-amount">Rp{{ number_format($keranjang->grandtotal,0,',','.') }}</div>

                <p class="text-center">
                    <br>
                    Pembayaran dapat dilakukan melalui transfer ke <em>Virtual Account</em> A/N TICMI ke :
                    <br>

                    <img src="{{ url('assets/images/shop/logo-bca.gif') }}" alt="BCA"> <br>
                    <span class="bank__name">Bank BCA, Bursa Efek Indonesia</span> <br>
                    <span class="virtual__account">
                                        @php
                                            $vc = !empty($keranjang->virtual_account) ? str_split($keranjang->virtual_account,3):$keranjang->virtual_account;
                                            $vc = implode(" ",$vc);
                                        @endphp
                        No : {{ $vc }}
                                    </span> <br>
                    <span class="account__owner">a/n TICMI</span>
                </p>

                <p>
                    Terima kasih telah melakukan pembelian data di The Indonesia Capital Market Institute.
                    <br>
                    Jika sudah melakukan pembayaran, mohon untuk <strong>Konfirmasi Pembayaran</strong> Anda agar transaksi Anda bisa diproses.
                </p>
                <a href="{{ route('transaksi.konfirmasi',['keranjang'=>$keranjang->id]) }}" class="btn btn-primary">Konfirmasi Pembayaran</a>
                <hr>
            @else
                @if($keranjang->is_payment_approve == 0)
                <p>
                    Anda sudah melakukan konfirmasi pembayaran
                </p>
                <hr>
                @endif
            @endif


            <strong>Detail Transaksi</strong>

            <table cellspacing="0" class="shop_table cart table-bordered">
                <thead>
                <tr>
                    <th class="product-name">
                        Nama
                    </th>
                    <th class="product-price">
                        Price
                    </th>
                    <th class="product-quantity">
                        Quantity
                    </th>
                    <th class="product-subtotal">
                        Total
                    </th>
                </tr>
                </thead>
                <tbody>
                @if($keranjang->is_payment_approve == 0)

                    @php($total=0)
                    @if($keranjang->keranjang_detail)
                        @foreach($keranjang->keranjang_detail as $key => $item)
                            <tr>
                                <td>{{ $item->item_name }}</td>
                                <td>{{ number_format($item->price) }}</td>
                                <td>1</td>
                                <td align="right">Rp{{ number_format($item->price) }}</td>
                            </tr>
                            @php($total += $item->price)
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4" class="text-center">Keranjang Masih Kosong</td>
                        </tr>
                    @endif
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="3" align="right"><strong>Sub Total</strong></td>
                    <td align="right"><strong>Rp{{ number_format($keranjang->subtotal,0,',','.') }}</strong></td>
                </tr>
                <tr>
                    <td colspan="3" align="right"><strong>Biaya Administrasi</strong></td>
                    <td align="right"><strong>Rp{{ number_format($keranjang->biaya_admin,0,',','.') }}</strong></td>
                </tr>
                <tr>
                    <td colspan="3" align="right"><strong>TOTAL</strong></td>
                    <td align="right"><strong>Rp{{ number_format($keranjang->grandtotal,0,',','.') }}</strong></td>
                </tr>
                </tfoot>

                @else

                    @php($total=0)
                    @if($keranjang->keranjang_detail)
                        @foreach($keranjang->keranjang_detail as $key => $item)
                            <tr>
                                <td>{{ $item->docType }}<a href="{{ route('dataemiten.getfile',['doctype'=>$item->item_doctype,'type'=>$item->item_type,'emitendataencrypt'=>encrypt($item->data_emiten_id)]) }}">{{ $item->item_name }} <i class="fa fa-download" style="font-size: 12px;"></i></a> </td>
                                <td>{{ number_format($item->price) }}</td>
                                <td>1</td>
                                <td align="right">Rp{{ number_format($item->price) }}</td>
                            </tr>
                            @php($total += $item->price)
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4" class="text-center">Keranjang Masih Kosong</td>
                        </tr>
                    @endif

                @endif
            </table>
        </div>
    </div>
    <br>
    <div class="content-box shadow bg-white">

    </div>

@endsection
