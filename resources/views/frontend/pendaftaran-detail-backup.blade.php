@extends("layout.home")
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('./assets/images/banner/profile.jpg') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">{{ $batch->nama }}</h3>
                        <h6 class="sub-title">{{ $batch->keterangan }}</h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->


    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <!-- Course Wrapper -->
            <div class="row course-single">
                <!-- Course Banner Image -->
                <div class="col-sm-7">
                    <div class="owl-crousel">
                        <!--                        <img alt="Course" class="img-responsive" src="./assets/images/course/aspm-product.jpg" width="1920" height="966">-->
                    </div>
                </div><!-- Column -->
            </div><!-- Course Wrapper -->

            <div class="row course-single content-box  bg-white shadow">
                {{--<p class="text-center">--}}
                    {{--@if($batch->kd_type != 1)--}}
                        {{--<a href="http://akademik.ticmi.co.id/index/daftar{{ $batch->kd_type }}/{{ $batch->id }}/Pendaftaran {{ $batch->nama }}.html" class="btn btn-block"><i class="fa fa-angle-double-right"></i> Saya ingin daftar</a>--}}
                    {{--@else--}}
                        {{--<a href="http://akademik.ticmi.co.id/index/daftar/{{ $batch->cabang }}/{{ $batch->id }}/Pendaftaran {{ $batch->nama }}.html" class="btn btn-block"><i class="fa fa-angle-double-right"></i> Saya ingin daftar</a>--}}
                    {{--@endif--}}
                {{--</p>--}}
                <!-- Course Banner Image -->
                <div class="col-sm-6">
                    <div class="owl-crousel">
                        <img alt="Course" class="img-responsive" src="{{ url('assets/images/ticmi/course-ticmi.jpg') }}" width="1920" height="966">
                    </div>
                </div><!-- Column -->

                <!-- Course Detail -->
                <div class="col-sm-6 ">
                    <div class="course-detail">
                        <!-- Course Content -->
                        <div class="course-meta">
                            <h4>{{ $batch->program->nm_program." ".$batch->kelas->type->nama }}</h4>
                            <span class="cat bg-yellow">{{ $batch->program->nm_program }}</span><span class="cat bg-green">Sertifikasi</span>
                            <!--<h4>Wakil Perantara Pedagang Efek</h4>-->
                            <ul class="course-meta-icons">
                                <li><i class="fa fa-dollar"></i><span>Biaya Investasi</span><h5>IDR{{ number_format($batch->harga,0,".",",") }}</h5></li>
                                <li><i class="fa fa-clock-o"></i><span>Waktu Pelaksanaan</span><h5>{{ $batch->tanggal->formatLocalized('%A, %#d %B %Y') }}</h5></li>
                                <li><i class="fa fa-map-marker"></i><span>Tempat</span><h5>TICMI, Gedung Bursa Efek Indonesia Tower II Lantai 1 <br/>Jl. Jenderal Sudirman kav 52-53 Jakarta 12190</h5></li>
                                @if($batch->program->nm_program == "WPPE" || $batch->program->nm_program == "WMI" )
                                    @if($batch->kelas->type->nama == "Waiver")
                                        <li><i class="fa fa-users"></i><span>Minimum Peserta</span><h5>10+ Orang</h5></li>
                                        <li><i class="fa fa-clock-o"></i><span>Durasi Pelatihan</span><h5>8.5 Jam</h5></li>
                                        <li><i class="fa fa-clock-o"></i><span>Durasi Ujian</span><h5>Ujian Perdana 2 Jam </h5></li>
                                    @else
                                        <li><i class="fa fa-users"></i><span>Minimum Peserta</span><h5>8+ Orang</h5></li>
                                        <li><i class="fa fa-clock-o"></i><span>Durasi Pelatihan</span><h5>75 Jam</h5></li>
                                        <li><i class="fa fa-clock-o"></i><span>Durasi Ujian</span><h5>Ujian Perdana 2 Jam </h5></li>
                                    @endif
                                @elseif($batch->program->nm_program == "ASPM")
                                    @if($batch->kelas->type->nama == "Waiver")
                                        <li><i class="fa fa-users"></i><span>Minimum Peserta</span><h5>10+ Orang</h5></li>
                                        <li><i class="fa fa-clock-o"></i><span>Durasi Pelatihan</span><h5>25 Jam</h5></li>
                                        <li><i class="fa fa-clock-o"></i><span>Durasi Ujian</span><h5>Ujian Perdana 2 Jam </h5></li>
                                    @else
                                        <li><i class="fa fa-users"></i><span>Minimum Peserta</span><h5>10+ Orang</h5></li>
                                        <li><i class="fa fa-clock-o"></i><span>Durasi Pelatihan</span><h5>75 Jam</h5></li>
                                        <li><i class="fa fa-clock-o"></i><span>Durasi Ujian</span><h5>Ujian Perdana 2 Jam </h5></li>
                                    @endif
                                @elseif($batch->program->nm_program == "PT-ASPM")
                                    <li><i class="fa fa-users"></i><span>Minimum Peserta</span><h5>10+ Orang</h5></li>
                                    <li><i class="fa fa-clock-o"></i><span>Durasi Pelatihan</span><h5>Tidak ada</h5></li>
                                    <li><i class="fa fa-clock-o"></i><span>Durasi Ujian</span><h5>Ujian 2 Jam </h5></li>
                                @endif
                            </ul>
                        </div>
                    </div><!-- Course Detail -->
                </div><!-- Column -->
            </div>

            <div class="row course-full-detail content-box  bg-white shadow">
                <div class="col-sm-12">
                    <div class="">
                        {!! $batch->program->keterangan !!}

                        <h4>Persyaratan mengikuti kelas {{ $batch->program->nm_program." ".$batch->kelas->type->nama }}</h4>
                        @if($batch->program->nm_program == "WPPE")
                            <ol>
                                <li>Berusia minimal 18 Tahun</li>
                                <li>Untuk <strong>WPPE REGULER</strong> persyaratan untuk dapat mengikuti program ini adalah berpendidikan paling rendah setingkat SMTA dan atau yang Sederajat</li>
                                <li>Untuk <strong>WPPE Waiver</strong> persyaratan untuk dapat mengikutinya dapat dilihat di <a href="{{ route('waiver-product') }}">sini</a> </li>
                                <li>Peserta diwajibkan mendaftar online di website ticmi.co.id </li>
                                <li>Melakukan konfirmasi pembayaran melalui menu <a href="http://akademik.ticmi.co.id/index/confirm.html" title="konfirmasi pembayaran">konfirmasi pembayaran</a> di website TICMI</li>
                                <li>Melakukan pendaftaran, pembayaran dan menyerahkan fotokopi KTP dan fotokopi ijazah terakhir, ke kantor admisi TICMI</li>
                            </ol>

                            <h4>Materi yang diajarkan</h4>
                            <p>Berdasarkan fungsi tersebut di atas, materi yang diberikan meliputi modul-modul sebagai berikut:</p>
                            <table class="table course-table">
                                <thead>
                                <tr>
                                    <th colspan="3" class="text-center">KELAS REGULER</th>
                                </tr>
                                <tr class="warning">
                                    <th> Modul  </th>
                                    <th> Jumlah Pertemuan </th>
                                    <th> Total Jam </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="info">
                                    <td> Pasar Modal Indonesia & Mekanisme Perdagangan Efek </td>
                                    <td class="text-left"> 7 Pertemuan </td>
                                    <td class="text-left"> 17.5 Jam </td>
                                </tr>
                                <tr class="info">
                                    <td> Pengetahuan Tentang Efek </td>
                                    <td class="text-left"> 2 Pertemuan </td>
                                    <td class="text-left"> 5 Jam </td>
                                </tr>
                                <tr class="info">
                                    <td> Analisis Ekonomi, Keuangan Perusahaan dan Investasi </td>
                                    <td class="text-left"> 8 Pertemuan </td>
                                    <td class="text-left"> 20 Jam </td>
                                </tr>
                                <tr class="info">
                                    <td> Hukum dan Etika WPPE </td>
                                    <td class="text-left"> 7 Pertemuan </td>
                                    <td class="text-left"> 17.5 Jam </td>
                                </tr>
                                <tr class="info">
                                    <td> Aplikasi Praktis </td>
                                    <td class="text-left"> 6 Pertemuan </td>
                                    <td class="text-left"> 15 Jam </td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr class="danger">
                                    <td class="text-right"><strong>TOTAL</strong></td>
                                    <td class="text-left"><strong>30 Pertemuan</strong></td>
                                    <td class="text-left"><strong>75 Jam</strong></td>
                                </tr>
                                </tfoot>
                            </table>


                            <table class="table course-table">
                                <thead>
                                <tr>
                                    <th colspan="2" class="text-center">KELAS WAIVER</th>
                                </tr>
                                <tr class="warning">
                                    <th> Modul  </th>
                                    <th> Durasi Jam </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="info">
                                    <td> Pasar Modal Indonesia & Mekanisme Perdagangan Efek </td>
                                    <td class="text-left"> 1 Jam </td>
                                </tr>
                                <tr class="info">
                                    <td> Pengetahuan Tentang Efek </td>
                                    <td class="text-left"> 1 Jam </td>
                                </tr>
                                <tr class="info">
                                    <td> Analisis Ekonomi, Keuangan Perusahaan dan Investasi </td>
                                    <td class="text-left"> 2 Jam </td>
                                </tr>
                                <tr class="info">
                                    <td> Hukum dan Etika WPPE </td>
                                    <td class="text-left"> 4 Jam </td>
                                </tr>
                                <tr class="info">
                                    <td> Aplikasi Praktis </td>
                                    <td class="text-left"> - </td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr class="danger">
                                    <td class="text-right"><strong>TOTAL</strong></td>
                                    <td class="text-left"><strong>8 Jam</strong></td>
                                </tr>
                                </tfoot>
                            </table>

                        @elseif($batch->program->nm_program == "WMI")
                            <ol>
                                <li>Berusia minimal 18 Tahun</li>
                                <li>Untuk <strong>WMI Reguler</strong> persyaratan untuk dapat mengikuti program ini adalah berpendidikan paling rendah lulusan Diploma III dan /atau yang sederajat</li>
                                <li>Untuk <strong>WMI Waiver</strong> persyaratan untuk dapat mengikutinya dapat dilihat di <a href="{{ route('waiver-product') }}">sini</a> </li>
                                <li>Peserta diwajibkan mendaftar online di website ticmi.co.id </li>
                                <li>Melakukan konfirmasi pembayaran melalui menu <a href="http://akademik.ticmi.co.id/index/confirm.html" title="konfirmasi pembayaran">konfirmasi pembayaran</a> di website TICMI</li>
                                <li>Melakukan pendaftaran, pembayaran dan menyerahkan fotokopi KTP dan fotokopi ijazah terakhir, ke kantor admisi TICMI</li>
                            </ol>
                            <h4>Materi yang diajarkan</h4>
                            <p>Berdasarkan fungsi tersebut di atas, materi yang diberikan meliputi modul-modul sebagai berikut:</p>
                            <table class="table course-table">
                                <thead>
                                <tr>
                                    <th colspan="3" class="text-center">Kelas Reguler</th>
                                </tr>
                                <tr class="warning">
                                    <th> Modul  </th>
                                    <th> Jumlah Pertemuan </th>
                                    <th> Total Jam </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="info">
                                    <td> Perekonomian </td>
                                    <td> 3 Pertemuan </td>
                                    <td> 9 Jam </td>
                                </tr>
                                <tr class="info">
                                    <td> Analisis Laporan Keuangan Perusahaan </td>
                                    <td> 6 Pertemuan </td>
                                    <td> 12 Jam </td>
                                </tr>
                                <tr class="info">
                                    <td> Analisis Kuantitatif </td>
                                    <td> 2 Pertemuan </td>
                                    <td> 5 Jam </td>
                                </tr>
                                <tr class="info">
                                    <td> Analisis Efek Pendapatan Tetap </td>
                                    <td> 2 Pertemuan </td>
                                    <td> 5.5 Jam </td>
                                </tr>
                                <tr class="info">
                                    <td> Analisis Efek Ekuitas </td>
                                    <td> 3 Pertemuan </td>
                                    <td> 8 Jam </td>
                                </tr>
                                <tr class="info">
                                    <td> Manajemen Portfolio </td>
                                    <td> 4 Pertemuan </td>
                                    <td> 10 Jam </td>
                                </tr>
                                <tr class="info">
                                    <td> Hukum & Etika WMI </td>
                                    <td> 2 Pertemuan </td>
                                    <td> 6 Jam </td>
                                </tr>
                                <tr class="info">
                                    <td> Aplikasi Praktis </td>
                                    <td> 8 Pertemuan </td>
                                    <td> 19.5 Jam </td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr class="danger">
                                    <td class="text-right"><strong>TOTAL</strong></td>
                                    <td><strong>30 Pertemuan</strong></td>
                                    <td><strong>75 Jam</strong></td>
                                </tr>
                                </tfoot>
                            </table>
                            <table class="table course-table">
                                <thead>
                                <tr>
                                    <th colspan="3" class="text-center">Kelas Waiver</th>
                                </tr>
                                <tr class="warning">
                                    <th> Modul  </th>
                                    <th> Pertemuan (1 hari)  </th>
                                    <th> Durasi Jam </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="info">
                                    <td> Hukum & Etika WMI </td>
                                    <td> 1 Pertemuan </td>
                                    <td> 2 Jam </td>
                                </tr>
                                <tr class="info">
                                    <td> Perekonomian </td>
                                    <td> 1 Pertemuan </td>
                                    <td> 1 Jam </td>
                                </tr>
                                <tr class="info">
                                    <td> Analisis Laporan Keuangan Perusahaan </td>
                                    <td> 1 Pertemuan </td>
                                    <td> 1.5 Jam </td>
                                </tr>
                                <tr class="info">
                                    <td> Analisis Kuantitatif </td>
                                    <td> 1 Pertemuan </td>
                                    <td> 2 Jam </td>
                                </tr>
                                <tr class="info">
                                    <td> Analisis Efek Pendapatan Tetap </td>
                                    <td> - </td>
                                    <td> - </td>
                                </tr>
                                <tr class="info">
                                    <td> Analisis Efek Ekuitas </td>
                                    <td> - </td>
                                    <td> - </td>
                                </tr>
                                <tr class="info">
                                    <td> Manajemen Portfolio </td>
                                    <td> - </td>
                                    <td> - </td>
                                </tr>
                                <tr class="info">
                                    <td> Aplikasi Praktis </td>
                                    <td> - </td>
                                    <td> - </td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr class="danger">
                                    <td class="text-right"><strong>TOTAL</strong></td>
                                    <td><strong>4 Pertemuan</strong></td>
                                    <td><strong>8 Jam</strong></td>
                                </tr>
                                </tfoot>
                            </table>

                        @elseif($batch->program->nm_program == "ASPM")
                            <h5>Kelas Reguler</h5>
                            <ol>
                                <li>Berusia minimal 18 Tahun</li>
                                <li>Persyaratan untuk dapat mengikuti program ini adalah berpendidikan paling rendah lulusan S1</li>
                                <li>Tidak memiliki sertifikasi WPEE, WPPE, WMI (atau sudah hangus masa)</li>
                                <li>Bukan Dewan Pengawas Syariah atau Tim Ahli Syariah</li>
                                <li>Peserta diwajibkan mendaftar online di website ticmi.co.id </li>
                                <li>Melakukan konfirmasi pembayaran melalui menu <a href="http://akademik.ticmi.co.id/index/confirm.html" title="konfirmasi pembayaran">konfirmasi pembayaran</a> di website TICMI</li>
                                <li>Melakukan pendaftaran, pembayaran dan menyerahkan fotokopi KTP dan fotokopi ijazah terakhir, ke kantor admisi TICMI</li>
                            </ol>
                            <h5>Kelas Akselerasi</h5>
                            <ol>
                                <li>Berusia minimal 18 Tahun</li>
                                <li>Persyaratan untuk dapat mengikuti program ini adalah berpendidikan paling rendah lulusan S1</li>
                                <li>Peserta diwajibkan mendaftar online dan membayar biaya placement test sebesar Rp200.000</li>
                                <li>Masih menjadi Dewan Pengawas Syariah aktif</li>
                                <li>Lulus <strong>Placement Test</strong> dengan Nilai Passing Grade 60</li>
                                <li>Peserta diwajibkan mendaftar online di website ticmi.co.id </li>
                                <li>Melakukan konfirmasi pembayaran melalui menu <a href="http://akademik.ticmi.co.id/index/confirm.html" title="konfirmasi pembayaran">konfirmasi pembayaran</a> di website TICMI</li>
                                <li>Melakukan pendaftaran, pembayaran dan menyerahkan fotokopi KTP dan fotokopi ijazah terakhir, ke kantor admisi TICMI</li>
                            </ol>
                        @elseif($batch->program->nm_program == "PT-ASPM")
                            <ol>
                                <li>Berusia minimal 18 Tahun</li>
                                <li>Persyaratan untuk dapat mengikuti program ini adalah berpendidikan paling rendah lulusan S1</li>
                                <li>Peserta diwajibkan mendaftar online dan membayar biaya placement test sebesar Rp200.000</li>
                                <li>Masih menjadi Dewan Pengawas Syariah aktif</li>
                                <li>Lulus <strong>Placement Test</strong> dengan Nilai Passing Grade 60</li>
                                <li>Peserta diwajibkan mendaftar online di website ticmi.co.id </li>
                                <li>Melakukan konfirmasi pembayaran melalui menu <a href="http://akademik.ticmi.co.id/index/confirm.html" title="konfirmasi pembayaran">konfirmasi pembayaran</a> di website TICMI</li>
                                <li>Melakukan pendaftaran, pembayaran dan menyerahkan fotokopi KTP dan fotokopi ijazah terakhir, ke kantor admisi TICMI</li>
                            </ol>
                            <h4>Materi yang akan di ujikan dalam Placement Test</h4>
                            <table class="table border">
                                <caption>Materi Placement Test</caption>
                                <thead>
                                <tr class="warning">
                                    <th width="10">No</th>
                                    <th>Modul</th>
                                    <th width="20%" class="text-center">Jumlah Soal</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="info">
                                    <td class="text-center">1</td>
                                    <td>
                                        <strong>Pasar Modal Syariah</strong>
                                        <ul style="list-style: disc; margin-left: 30px;">
                                            <li>Pengantar Ekonomi Syariah</li>
                                            <li><em>Ushul Fiqih</em></li>
                                            <li><em>Qawaid Fiqhiyah</em></li>
                                            <li><em>Fiqih Muammalah</em></li>
                                            <li>Akad Syariah</li>
                                            <li>Fatwa DSN MUI</li>
                                            <li>Dasar-dasar pasar modal syariah</li>
                                            <li>Produk pasar modal syariah</li>
                                            <li>Akuntansi syariah</li>
                                            <li>Regulasi pasar modal syariah</li>
                                        </ul>
                                    </td>
                                    <td class="text-center">60</td>
                                </tr>
                                <tr class="info">
                                    <td class="text-center">2</td>
                                    <td>
                                        <strong>Pasar Modal Konvensional</strong>
                                        <ul style="list-style: disc; margin-left: 30px;">
                                            <li>Basic knowledge keahlian WMI, WPPE dan WPEE</li>
                                        </ul>
                                    </td>
                                    <td class="text-center">40</td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr class="danger">
                                    <td colspan="2" class="text-right"><strong>TOTAL</strong></td>
                                    <td class="text-center"><strong>100</strong></td>
                                </tr>
                                </tfoot>
                            </table>
                        @endif

                        <p class="disclaimer">
                            <h4>Disclaimer</h4>
                            Kelas akan dimulai jika jumlah peserta sudah memenuhi kuota untuk minimum peserta masing-masing kelas. Jika kuota minimum peserta belum terpenuhi maka akan dimasukkan kedalam kelas yang selanjutnya.
                        </p>

                        <p class="text-center">
                        @if($batch->kd_type != 1)
                            <a href="http://akademik.ticmi.co.id/index/daftar{{ $batch->kd_type }}/{{ $batch->id }}/Pendaftaran {{ $batch->nama }}.html" class="btn btn-danger btn-lg"><span class="fa fa-angle-double-right"></span> Saya ingin daftar</a>
                        @else
                            <a href="http://akademik.ticmi.co.id/index/daftar/{{ $batch->cabang }}/{{ $batch->id }}/Pendaftaran {{ $batch->nama }}.html" class="btn btn-danger btn-lg"><span class="fa fa-angle-double-right"></span> Saya ingin daftar</a>
                        @endif
                        </p>
                    </div>


                </div><!-- Column -->
            </div><!-- row -->

            <div class="row course-full-detail content-box  bg-white shadow">
                <h4>Program Kami lainnya : </h4>
                <div class="owl-carousel show-nav-hover dots-dark nav-square dots-square navigation-color" data-animatein="zoomIn" data-animateout="slideOutDown" data-items="1" data-margin="30" data-loop="true" data-merge="true" data-nav="true" data-dots="false" data-stagepadding="" data-mobile="1" data-tablet="2" data-desktopsmall="2"  data-desktop="3" data-autoplay="true" data-delay="3000" data-navigation="true">

                    @if($otherbatch && $otherbatch->count() > 0)
                        @foreach($otherbatch as $value)
                            <div class="item">
                                <!-- Related Wrapper -->
                                <div class="related-wrap">
                                    <!-- Related Image Wrapper -->
                                    <div class="img-wrap">
                                        @if($value->program->nm_program == "WPPE")
                                            @if($value->kelas->type->nama == "Regular")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wppe-reguler-3.jpg') }}" width="600" height="220">
                                            @elseif($value->kelas->type->nama == "Waiver")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wppe-waiver-3.jpg') }}" width="600" height="220">
                                            @else
                                            @endif
                                        @elseif($value->program->nm_program == "WMI")
                                            @if($value->kelas->type->nama == "Regular")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wmi-reguler-3.jpg') }}" width="600" height="220">
                                            @elseif($value->kelas->type->nama == "Waiver")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wmi-waiver-3.jpg') }}" width="600" height="220">
                                            @else
                                            @endif
                                        @elseif($value->program->nm_program == "ASPM")
                                            @if($value->kelas->type->nama == "Regular")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/aspm-reguler-3.jpg') }}" width="600" height="220">
                                            @elseif($value->kelas->type->nama == "Waiver")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/aspm-akselerasi-3.jpg') }}" width="600" height="220">
                                            @else
                                            @endif
                                        @elseif($value->program->nm_program == "PT-ASPM")
                                            <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/aspm-akselerasi-3.jpg') }}" width="600" height="220">
                                        @else
                                            <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wppe-waiver-3.jpg') }}" width="600" height="220">
                                        @endif
                                    </div>
                                    <!-- Related Content Wrapper -->
                                    <div class="related-content">
                                        <a href="{{ url('daftar/'.$value->id.'/'.$value->nama) }}" title="{{ $value->nama }}">+</a><span>{{ $value->nama }}</span>
                                        <h5 class="title">{{ $value->nama }}</h5>
                                    </div><!-- Related Content Wrapper -->
                                </div><!-- Related Wrapper -->
                            </div><!-- Item -->
                        @endforeach
                    @else
                    @endif

                </div><!-- Related Post -->

            </div>
        </div><!-- Container -->
    </div><!-- Page Default -->
@endsection