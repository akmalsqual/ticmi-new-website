@extends('layout.home')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('./assets/images/banner/profile.jpg') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Career Development Center</h3>
                        <h6 class="sub-title"></h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-lgrey">
        <!-- Container -->
        <div class="container">

            <!-- Form Begins -->
            {{ Form::open(['url'=>route('cdc'),'id'=>'form-career']) }}
            <div class="row course-single content-box bg-google-red shadow ">

                <div class="col-md-8 col-md-offset-2">
                @include('errors.list')
                @include('flash::message')
                    @if(!session()->has('flash_notification.message'))
                    <div class="text-center typo-light clearfix" style="color: #FAFAFA;">
                        <strong>CDC</strong> dibentuk untuk mempertemukan pencari kerja dan pemberi kerja di industri pasar modal.  Salah satu pelayanan yang diberikan <strong>CDC</strong> adalah memberikan informasi data alumni kepada calon pemberi kerja. <br/>
                        <br>
                        Mohon konfirmasi persetujuan Ibu/ Bapak dengan mengetik alamat email Ibu/ Bapak di kolom berikut ini: <br> <br>
                    </div>
                <!-- Field 1 -->
                    <!-- Field 2 -->
                    <div class="input-email form-group">
                        {{ Form::email('email',null,['class'=>'form-control career-email','placeholder'=>'Masukkan Email Anda']) }}
                    </div>
                    <div class="form-group clearfix ">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button class="btn btn-loading btn-career" data-career="yes" data-toggle="loading" data-loading-text="<i class='fa fa-spinner fa-pulse fa-fw' style='font-size: 14px;color: #fff;'></i> Loading" type="button">Setuju <i class="fa fa-check-square-o" style="font-size: 14px;color: #fff;"></i></button>
                            <button class="btn btn-warning btn-loading btn-career" data-career="no" data-toggle="loading" data-loading-text="<i class='fa fa-spinner fa-pulse fa-fw' style='font-size: 14px;color: #fff;'></i> Loading" type="button">Tidak Setuju <i class="fa fa-times" style="font-size: 14px;color: #fff;"></i></button>
                        </div>
                    </div>
                    <div class="text-center typo-light clearfix" style="color: #FAFAFA">
                        Bila Ibu/ Bapak setuju maka Ibu/ Bapak akan mendapatkan kesempatan yang luas dalam memperoleh penawaran kerja dari berbagai Perusahaan calon Pemberi Kerja di industri Pasar Modal.
                    </div>
                    @endif
                    <!-- Button -->
                    {{--<button class="btn" type="submit">Send Now <i class="fa fa-paper-plane" style="font-size: 14px;color: #fff;"></i></button>--}}
                </div><!-- Column -->

            </div>
            <br>
            {{ Form::close() }}
        </div>
    </div>
@endsection