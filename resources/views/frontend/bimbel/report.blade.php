@extends('layout.home')
@section('content')
    <section data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="/assets/images/ticmi/ujian.jpg" class="img-responsive" alt="Capital Market Professional Development Program">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            <h5 style="text-align:center">LAPORAN BIMBINGAN & UJIAN ULANG GRATIS</h5>
                            <table class="table table-hover table-bordered table-condensed" style="font-size: 12px;">
                                <thead>
                                <tr>
                                    <th rowspan="2">Cabang</th>
                                    <th colspan="4" class="text-center">Program</th>
                                    <th rowspan="2" class="text-center">Total</th>
                                </tr>
                                <tr>
                                    @foreach($allProgram as $program)
                                        <th class="text-center" width="17%">{{ $program }}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @if($ujian && $ujian->count() > 0)
                                    @foreach($allCabang as $cabang)
                                        <tr>
                                            <td>{{ $cabang }}
                                            @foreach($allProgram as $key => $program)
                                                @if($cabang == 'Jakarta' && ($key == 1 || $key == 19))
                                                    <td class="text-center">
                                                        <strong>Tgl 23 :</strong> {{ $ujian->where('cabang',$cabang)->where('program',$key)->where('tanggal','July 23 2017')->count() }} |
                                                        <strong>Tgl 30 :</strong> {{ $ujian->where('cabang',$cabang)->where('program',$key)->where('tanggal','July 30 2017')->count() }}
                                                    </td>
                                                @else
                                                    <td class="text-center">{{ $ujian->where('cabang',$cabang)->where('program',$key)->count() }}</td>
                                                @endif
                                            @endforeach
                                            <th class="text-center danger">{{ $ujian->where('cabang',$cabang)->count() }}</th>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td class="text-right danger"><strong>TOTAL</strong></td>
                                    @foreach($allProgram as $key => $program)
                                        <th class="text-center danger">{{ $ujian->where('program',$key)->count() }}</th>
                                    @endforeach
                                    <th class="text-center warning">{{ $ujian->count() }}</th>
                                </tr>
                                </tfoot>
                            </table>
                            <br>
                            <table class="table table-hover table-bordered table-condensed" style="font-size: 12px;">
                                <thead>
                                <tr>
                                    <th rowspan="2">Tanggal</th>
                                    <th colspan="5" class="text-center">Program</th>
                                    <th rowspan="2" class="text-center">Total</th>
                                </tr>
                                <tr>
                                    <th class="text-center" width="15%">WPPE</th>
                                    <th class="text-center" width="15%">WPPE P</th>
                                    <th class="text-center" width="15%">WPPE PT</th>
                                    <th class="text-center" width="15%">WMI</th>
                                    <th class="text-center" width="15%">ASPM</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($tgl_ujian as $tgl)
                                        <tr>
                                            <td>{{ date('d M Y', strtotime($tgl)) }}</td>
                                            <td class="text-center">{{ $ujian2->where('cabang','Jakarta')->where('tanggal',$tgl)->where('program',1)->count() }}</td>
                                            <td class="text-center">{{ $ujian2->where('cabang','Jakarta')->where('tanggal',$tgl)->where('program',19)->count() }}</td>
                                            <td class="text-center">{{ $ujian2->where('cabang','Jakarta')->where('tanggal',$tgl)->where('program',18)->count() }}</td>
                                            <td class="text-center">{{ $ujian2->where('cabang','Jakarta')->where('tanggal',$tgl)->where('program',2)->count() }}</td>
                                            <td class="text-center">{{ $ujian2->where('cabang','Jakarta')->where('tanggal',$tgl)->where('program',22)->count() }}</td>
                                            <th class="text-center danger">{{ $ujian2->where('cabang','Jakarta')->where('tanggal',$tgl)->count() }}</th>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td class="text-right danger"><strong>TOTAL</strong></td>
                                    <th class="text-center danger">{{ $ujian2->where('cabang','Jakarta')->where('program',1)->count() }}</th>
                                    <th class="text-center danger">{{ $ujian2->where('cabang','Jakarta')->where('program',19)->count() }}</th>
                                    <th class="text-center danger">{{ $ujian2->where('cabang','Jakarta')->where('program',18)->count() }}</th>
                                    <th class="text-center danger">{{ $ujian2->where('cabang','Jakarta')->where('program',2)->count() }}</th>
                                    <th class="text-center danger">{{ $ujian2->where('cabang','Jakarta')->where('program',22)->count() }}</th>
                                    <th class="text-center warning">{{ $ujian2->count() }}</th>
                                </tr>
                                </tfoot>
                            </table>
                            <div class="row">
                                <h5 style="text-align:center">ROAD TO GOCENG!!!</h5>
                                <div class="col-md-6 col-md-offset-3">
                                    <table class="table table-hover table-bordered table-condensed" style="font-size: 12px;">
                                        <thead>
                                        <tr>
                                            <td>Program</td>
                                            <td width="30%">Total Kelulusan</td>
                                            <td width="30%">Lulus Event GOCENG</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>WPPE</td>
                                            <td>{{ $wppe + 6 }}</td>
                                            <td>{{ $wppe_goceng }}</td>
                                        </tr>
                                        <tr>
                                            <td>WPPE P</td>
                                            <td>{{ $wppe_p }}</td>
                                            <td>{{ $wppe_p_goceng }}</td>
                                        </tr>
                                        <tr>
                                            <td>WPPE PT</td>
                                            <td>{{ $wppe_pt }}</td>
                                            <td>{{ $wppe_pt_goceng }}</td>
                                        </tr>
                                        <tr>
                                            <td>WMI</td>
                                            <td>{{ $wmi + 15 }}</td>
                                            <td>{{ $wmi_goceng }}</td>
                                        </tr>
                                        <tr>
                                            <td>ASPM</td>
                                            <td>{{ $aspm }}</td>
                                            <td>{{ $aspm_goceng }}</td>
                                        </tr>
                                        </tbody>
                                        @php
                                            $total_lulusan = $wppe + $wppe_p + $wppe_pt + $wmi + $aspm + 21;
                                            $total_lulusan_goceng = $wppe_goceng + $wppe_p_goceng + $wppe_pt_goceng + $wmi_goceng + $aspm_goceng;
                                            $kurang = 5000 - $total_lulusan;
                                        @endphp
                                        <tfoot>
                                        <tr class="danger">
                                            <td><strong>TOTAL</strong></td>
                                            <td><strong>{{ $total_lulusan }}</strong></td>
                                            <td><strong>{{ $total_lulusan_goceng }}</strong></td>
                                        </tr>
                                        {{--<tr class="warning">--}}
                                            {{--<td colspan="3" class="text-center">--}}
                                                {{--<h5>Untuk Mencapai 5000 butuh</h5>--}}
                                                {{--<h3>{{ $kurang }} Lulusan lagi...</h3>--}}
                                                {{--<h4>Cemungudh Kaka!!!</h4>--}}
                                            {{--</td>--}}
                                        {{--</tr>--}}
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Page Default -->
@endsection