@extends('layout.home')
@section('content')
    <section data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="/assets/images/ticmi/ujian.jpg" class="img-responsive" alt="Capital Market Professional Development Program">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            <h4 style="text-align:center">UJIAN ULANG GRATIS</h4>

                            <p style="text-align:justify;line-height: 25px;">
                                @include('flash::message')
                                Untuk menyambut <strong>HUT Kemerdekaan Republik Indonesia</strong> dan <strong>HUT Pasar Modal Indonesia</strong>, TICMI mengadakan program Bimbingan dan Ujian ulang Gratis untuk peserta pelatihan sertifikasi pasar modal yang dilaksanakan TICMI.
                                Program tersebut akan diselenggarakan pada :
                            </p>

                            <div class="table-responsive">
                                <h5>I. UJIAN ULANG GRATIS TICMI</h5>
                                <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th width="10%">Lokasi</th>
                                        <th width="20%">Program</th>
                                        <th width="35%">Tanggal/Jam</th>
                                        <th>Lokasi</th>
                                    </tr>
                                </thead>
                                <tbody style="font-size: 12px;">
                                    <tr>
                                        <td>Jakarta</td>
                                        <td>WPPE, WPPE Pemasaran, WPPE Pemasaran Terbatas, WMI, ASPM</td>
                                        <td>
                                            01 Agustus 2017 s.d 18 Agustus 2017 <br>
                                            Sesi 1 : 09:00 - 11:00 WIB | Sesi 2 : 13:00 - 15:00 <br>
                                            Sesi 3 : 14:00 - 18:00 WIB
                                        </td>
                                        <td>Library TICMI, Gedung Bursa Efek Indonesia Tower 2 Lantai 1</td>
                                    </tr>
                                    <tr>
                                        <td>Surabaya<span class="text-red">*</span></td>
                                        <td>WPPE Pemasaran </td>
                                        <td>
                                            Selasa, 1 Agustus 2017 <br> Sesi 1 : 09:00 s.d 11:00 & Sesi 2 : 13:00 s.d 15:00
                                            <br>
                                            Rabu, 2 Agustus 2017 <br> Sesi 1 : 09:00 s.d 11:00
                                        </td>
                                        <td>Kampus 2 STIE Perbanas Surabaya Jl. Wonorejo Utara 16, Rungkut, Surabaya</td>
                                    </tr>
                                    <tr>
                                        <td>Surabaya<span class="text-red">*</span></td>
                                        <td>WPPE </td>
                                        <td>
                                            Rabu, 2 Agustus 2017 <br> Sesi 2 : 13:00 s.d 15:00
                                        </td>
                                        <td>Kampus 2 STIE Perbanas Surabaya Jl. Wonorejo Utara 16, Rungkut, Surabaya</td>
                                    </tr>
                                    <tr>
                                        <td>Surabaya<span class="text-red">*</span></td>
                                        <td>WPPE Pemasaran Terbatas </td>
                                        <td>
                                            Rabu, 2 Agustus 2017 <br> Sesi 2 : 13:00 s.d 15:00
                                        </td>
                                        <td>Kampus 2 STIE Perbanas Surabaya Jl. Wonorejo Utara 16, Rungkut, Surabaya</td>
                                    </tr>
                                    <tr>
                                        <td>Semarang<span class="text-red">*</span></td>
                                        <td>WPPE, WPPE Pemasaran</td>
                                        <td>
                                            Rabu, 26 Juli 2017 dan Kamis, 27 Juli 2017 <br>
                                            Sesi 1 : 08:30 s.d 10:30 & Sesi 2 : 11:00 s.d 13:00 <br>
                                            Sesi 3 : 13:30 s.d 15:30 & Sesi 4 : 16:00 s.d 18:00 <br>
                                            Jum'at, 28 Juli 2017 <br>
                                            Sesi 1 : 08:30 s.d 10:30 & Sesi 2 : 13:30 s.d 15:30 <br>
                                            Sesi 3 : 16:00 s.d 18:00
                                        </td>
                                        <td>
                                            LP3M INVESTA, Gd. BNI SEKURITAS lt.III Thamrin Square B.5 jl. Thamrin no.5 Semarang.
                                            Tlp.024.3566414 sd 3566418
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Pontianak<span class="text-red">*</span></td>
                                        <td>WPPE, WPPE Pemasaran</td>
                                        <td>Kamis, 27 Juli 2017 / 08:00 - 15:00</td>
                                        <td>Kantor Perwakilan BEI Pontianak, Komplek Perkantoran Central Perdana Blok A2-A3, Jl. Perdana – Kota Pontianak, 78124</td>
                                    </tr>
                                    <tr>
                                        <td>Yogyakarta<span class="text-red">*</span></td>
                                        <td>WPPE, WPPE Pemasaran, WMI</td>
                                        <td>Kamis-Jum'at, 10 Agustus s.d 11 Agustus 2017 / 08:00 - 10:00 WIB</td>
                                        <td>Laboratorium Komputer C, Gedung sayap selatan lt.3, Fakultas Ekonomika dan Bisnis, Universitas Gadjah Mada.
                                            Jl. Sosiohumaniora No.1 Bulaksumur, Yogyakarta</td>
                                    </tr>
                                    <tr>
                                        <td>Bandung<span class="text-red">*</span></td>
                                        <td>WPPE Pemasaran</td>
                                        <td>
                                            Sabtu, 29 Juli dan 5 Agustus 2017 <br>
                                            Sesi 1 : 08:00 - 10:00 & Sesi 2 : 13:00 - 15:00
                                        </td>
                                        <td>Pusat Informasi GO Public BEI, Jl. P.H.H. Mustofa No. 33, Kota Bandung</td>
                                    </tr>
                                    <tr>
                                        <td>Jayapura<span class="text-red">*</span></td>
                                        <td>WPPE, WPPE Pemasaran, WPPE Pemasaran Terbatas dan WMI</td>
                                        <td>
                                            Jum'at, 28 Juli 2017 / 09:00 WIT s.d Selesai
                                        </td>
                                        <td>Kantor Perwakilan BEI Jayapura, Komplek Jayapura Pacifik Permai Blok H No.19, Jayapura</td>
                                    </tr>
                                    <tr>
                                        <td>Palembang<span class="text-red">*</span></td>
                                        <td>WPPE, WPPE Pemasaran</td>
                                        <td>
                                            Senin, 7 Agustus 2017 / 14:00 WIB s.d 16:00 WIB
                                        </td>
                                        <td>Kantor Perwakilan BEI Palembang, Jl. Angkatan 45, No. 13-14, RT 0014/RW 004, Kel. Demang Lebar Daun, Kec. Ilir Barat I, Kota Palembang</td>
                                    </tr>
                                    <tr>
                                        <td>Kendari<span class="text-red">*</span></td>
                                        <td>WPPE, WPPE Pemasaran</td>
                                        <td>
                                            Selasa, 8 Agustus 2017 / 10:00 WIB s.d 12:00 WITA
                                        </td>
                                        <td>Kantor Perwakilan BEI Kendari, Jl. Syekh Yusuf No.20, Kota Kendari</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2">Padang<span class="text-red">*</span></td>
                                        <td>WPPE Pemasaran</td>
                                        <td>
                                            Minggu, 6 Agustus 2017 / 10:00 WIB s.d 12:00 WIB
                                        </td>
                                        <td>UPI YPTK Padang, Jl. Raya Lubuk Begalung Nan XX, Lubuk Begalung, Kota Padang</td>
                                    </tr>
                                    <tr>
                                        <td>WPPE, WPPE Pemasaran</td>
                                        <td>
                                            3 Agustus 2017 s.d 9 Agustus 2017 <br>
                                            Sesi 1 : 10:00-12:00 WIB | Sesi 2 : 13:00-15:00 WIB
                                        </td>
                                        <td>Kantor Perwakilan BEI Padang, JL. Pondok No. 90 A, Padang – Indonesia</td>
                                    </tr>
                                    <tr>
                                        <td>Bengkulu<span class="text-red">*</span></td>
                                        <td>WPPE Pemasaran</td>
                                        <td>
                                            27 Juli 2017 s.d 09 Agustus 2017 / 10:00 WIB s.d 16:00 WIB
                                        </td>
                                        <td>Galeri Investasi IAIN Bengkulu, Jalan Raden Patah Kel. Pagar Dewa, Selebar, Kota Bengkulu</td>
                                    </tr>
                                    <tr>
                                        <td>Aceh<span class="text-red">*</span></td>
                                        <td>WPPE dan WPPE Pemasaran</td>
                                        <td>
                                            Senin, 07 Agustus 2017 <br>
                                            Sesi 1 : 09:00-11:00 WIB | Sesi 2 : 11:00-13:00 WIB <br>
                                            Sesi 3 : 14:00-16:00 WIB
                                        </td>
                                        <td>Kantor Perwakilan BEI Aceh, Jl. Tengku Imeum Leung Bata No. 84, Banda Aceh</td>
                                    </tr>
                                    <tr>
                                        <td>Medan<span class="text-red">*</span></td>
                                        <td>WPPE dan WPPE Pemasaran</td>
                                        <td>
                                            23 Juli 2017 s.d 09 Agustus 2017 / 09:00 - 16:00 WIB
                                        </td>
                                        <td>Kantor Perwakilan BEI Medan, Jl. Asia No. 182, Medan</td>
                                    </tr>
                                    <tr>
                                        <td>Pangkalpinang<span class="text-red">*</span></td>
                                        <td>WPPE dan WPPE Pemasaran</td>
                                        <td>
                                            31 Juli 2017 s.d 02 Agustus 2017 / 09:00 - 16:00 WIB
                                        </td>
                                        <td>Kantor Perwakilan BEI Pangkalpinang, Jl. Jend. Sudirman, Kelurahan Pasar Padi, Kecamatan Girimaya, Kota Pangkalpinang</td>
                                    </tr>
                                    <tr>
                                        <td>Lampung<span class="text-red">*</span></td>
                                        <td>WPPE Pemasaran</td>
                                        <td>
                                            07 Agustus 2017 <br>
                                            Sesi 1 : 10:00-12:00 WIB | Sesi 2 : 13:00-15:00 WIB
                                        </td>
                                        <td>Labkom Lt.3 Fakultas Ekonomi Universitas Muhammadiyah Metro, Jl. Ki Hajar Dewantara No. 116 Kota Metro, Lampung</td>
                                    </tr>
                                    <tr>
                                        <td>Ambon<span class="text-red">*</span></td>
                                        <td>WPPE, WPPE Pemasaran</td>
                                        <td>
                                            Kamis, 03 Agustus 2017 / 10:00-12:00 WIT
                                        </td>
                                        <td>Kantor Perwakilan BEI Ambon, Jalan Philip Latumahina No. 16, Kel. Honipopu, Kec. Sirimau RT 001/RW 003, Kota Ambon</td>
                                    </tr>
                                    <tr>
                                        <td>Manokwari<span class="text-red">*</span></td>
                                        <td>WPPE, WPPE Pemasaran</td>
                                        <td>
                                            Selasa, 08 Agustus 2017 / 10:00-12:00 WIT
                                        </td>
                                        <td>Kantor Perwakilan BEI Manokwari, Jl. Trikora Wosi Manokwari</td>
                                    </tr>
                                    <tr>
                                        <td>Jambi<span class="text-red">*</span></td>
                                        <td>WPPE, WPPE Pemasaran</td>
                                        <td>
                                            Sabtu, 05 Agustus 2017 <br>
                                            Sesi 1 : 10:00 - 12:00 WIB | Sesi 2 : 13:00 - 15:00 WIB
                                        </td>
                                        <td>Kantor Perwakilan BEI Jambi, Jl. Kolonel Abun Jani No.11A dan 11B, Kel. Selamat Kec. Telanaipura, Kota Jambi</td>
                                    </tr>
                                    <tr>
                                        <td>Medan<span class="text-red">*</span></td>
                                        <td>WPPE, WPPE Pemasaran</td>
                                        <td>
                                            02 Agustus 2017 s.d 09 Agustus 2017<br>
                                            Sesi 1 : 10:00 - 12:00 WIB | Sesi 2 : 13:00 - 15:00 WIB
                                        </td>
                                        <td>Pusat Informasi Go Public BEI Medan, Jl. Asia No. 182, Medan</td>
                                    </tr>
                                    <tr>
                                        <td>Pekanbaru<span class="text-red">*</span></td>
                                        <td>WPPE, WPPE Pemasaran</td>
                                        <td>
                                            02 Agustus 2017 s.d 09 Agustus 2017<br>
                                            Sesi 1 : 10:00 - 12:00 WIB | Sesi 2 : 13:00 - 15:00 WIB
                                        </td>
                                        <td>Kantor Perwakilan BEI Pekanbaru, Jl. Jenderal Sudirman No. 73 (Sudirman Bawah) Pekanbaru - Riau</td>
                                    </tr>
                                    <tr>
                                        <td>Batam<span class="text-red">*</span></td>
                                        <td>WPPE, WPPE Pemasaran</td>
                                        <td>
                                            07 Agustus 2017 s.d 09 Agustus 2017<br>
                                            Sesi 1 : 10:00 - 12:00 WIB | Sesi 2 : 13:00 - 15:00 WIB
                                        </td>
                                        <td>Kantor Perwakilan BEI Batam, Komplek Mahkota Raya Blok A No. 11 Batam Center, Kota Batam, Kepri - Indonesia</td>
                                    </tr>
                                    <tr>
                                        <td>Banjarmasin<span class="text-red">*</span></td>
                                        <td>WPPE, WPPE Pemasaran</td>
                                        <td>
                                            07 Agustus 2017 s.d 09 Agustus 2017<br>
                                            Sesi 1 : 10:00 - 12:00 WITA | Sesi 2 : 13:00 - 15:00 WITA
                                        </td>
                                        <td>Kantor Perwakilan BEI Banjarmasin, Jl. Ahmad Yani, Kilometer (PAL) 1,5, No. 103, Banjarmasin - Kalimantan Selatan</td>
                                    </tr>
                                    <tr>
                                        <td>Denpasar<span class="text-red">*</span></td>
                                        <td>WPPE, WPPE Pemasaran</td>
                                        <td>
                                            07 Agustus 2017 s.d 09 Agustus 2017<br>
                                            Sesi 1 : 10:00 - 12:00 WITA | Sesi 2 : 13:00 - 15:00 WITA
                                        </td>
                                        <td>Kantor Perwakilan BEI Denpasar, Jl. P.B. Sudirman 10 X Kav. 2, Denpasar, Bali - Indonesia</td>
                                    </tr>
                                    <tr>
                                        <td>Palangkaraya<span class="text-red">*</span></td>
                                        <td>WPPE, WPPE Pemasaran</td>
                                        <td>
                                            Rabu, 09 Agustus 2017<br>
                                            Sesi 1 : 10:00 - 12:00 WIB | Sesi 2 : 13:00 - 15:00 WIB
                                        </td>
                                        <td>Kantor Perwakilan BEI Palangkaraya, Jl. RTA Milono KM 1,5 Ruko No. 1 Kota Palangka Raya</td>
                                    </tr>
                                    <tr>
                                        {{--<td>Banda Aceh, Bandung, Bengkulu, Jayapura, Lampung, Manado, Padang, Pontianak, Semarang dan Surabaya</td>--}}
                                        {{--<td>WPPE Pemasaran</td>--}}
                                        {{--<td>Sabtu, 29 Juli 2017 / 08:00-18:00</td>--}}
                                    {{--</tr>--}}
                                </tbody>
                            </table>
                                <span class="text-danger">*Untuk daerah luar Jakarta, bimbingan akan dapat diakses melalui rekaman video.</span>
                                <br>
                                <br>
                            </div>


                            <div class="row">
                                <div class="col-sm-12">
                                    <h5>II. UJIAN GRATIS KHUSUS JAKARTA</h5>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">UJIAN ULANG GRATIS JAKARTA</h3>
                                        </div>
                                        <div class="panel-body">
                                            <ol>
                                                <li>Ujian Gratis ini berlaku untuk semua program sertifikasi TICMI.</li>
                                                <li>
                                                    Ujian Gratis akan di selenggarakan mulai dari tanggal <strong>25 Juli 2017</strong> sampai dengan <strong>9 Agustus 2017</strong>, dimana perharinya di bagi menjadi 3 Sesi dengan kuota
                                                    persesi nya yaitu 36 Peserta. Berikut jadwal sesi perharinya :
                                                    <ol style="list-style: lower-roman;">
                                                        <li>Sesi 1 : 09:00 s.d 11:00</li>
                                                        <li>Sesi 2 : 13:00 s.d 15:00</li>
                                                        <li>Sesi 2 : 16:00 s.d 18:00</li>
                                                    </ol>
                                                </li>
                                                <li>Peserta yang dapat mengikuti ujian gratis ini adalah peserta yang sudah melaksanakan Ujian Perdana.</li>
                                                <li>Untuk mengikuti Ujian gratis ini silakan mendaftar melalui pendaftaran dibawah.</li>
                                                <li>Untuk dapat mencetak Sertifikat pada hari yang sama silahkan membawa kartu identitas seperti : Foto Copy KTP/SIM/Surat Keterangan dan Foto Copy Ijazah Terakhir</li>
                                                <li>Ujian dilaksanakan di <strong>Library TICMI</strong> Gedung Bursa Efek Indonesia Tower 2 Lantai 1 Jl. Jend Sudirman Kav 52-53</li>
                                            </ol>
                                        </div>
                                    </div>
                                    <div class="alert alert-success" role="alert"><p class="text-center">Dapatkan <strong>Sertifikat di hari yang sama</strong> jika Anda lulus ujian ulang (khusus Jakarta).</p></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">SYARAT PENDAFTARAN</h3>
                                        </div>
                                        <div class="panel-body">
                                            <ol>
                                                <li>Peserta sudah pernah mengikuti program pelatihan sertifikasi yang diselenggarakan oleh TICMI dan sudah mengikuti Ujian Perdana</li>
                                                <li>Peserta melakukan pendaftaran online pada website TICMI.</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">SYARAT MENDAPATKAN SERTIFIKAT DI HARI YANG SAMA</h3>
                                        </div>
                                        <div class="panel-body">
                                            <ol>
                                                <li>Peserta membawa fotocopy KTP/SIM/Surat Keterangan dan Ijazah Terakhir yang akan diserahkan pada saat mengambil Sertifikat.</li>
                                                <li>Hanya berlaku untuk yang ujian di Jakarta.</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">SYARAT MENGIKUTI UJIAN</h3>
                                        </div>
                                        <div class="panel-body">
                                            <ol>
                                                <li>Peserta mengikuti bimbingan ujian terlebih dahulu pada jadwal yang sudah ditentukan.</li>
                                                <li>Peserta yang dapat mengikuti ujian adalah yang ujian perdananya dilaksanakan setelah 31 Januari 2016.</li>
                                                <li>
                                                    Peserta diwajibkan membawa dan menggunakan Laptop sendiri untuk melakukan ujian, dengan syarat spesifikasi laptop sebagai berikut :
                                                    <ul style="list-style: disc;margin-left: 20px;">
                                                        <li>Procesor laptop disarankan setara Dual Core atau diatasnya</li>
                                                        <li>RAM minimal 2Gb</li>
                                                        <li>Sudah ter-install Browser Mozila Firefox versi 50 atau diatasnya</li>
                                                        <li>Dapat terhubung dengan internet dengan menggunakan WIFI.</li>
                                                        <li>Daya tahan baterai laptop minimal 2 jam untuk dapat menyala terus menerus selama berlangsungnya ujian</li>
                                                    </ul>
                                                </li>
                                                <li>Peserta menunjukkan email konfirmasi pendaftaran pada saat melakukan registrasi pada hari H</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">PENDAFTARAN</h3>
                                        </div>
                                        <div class="panel-body">
                                            @include('errors.list')
                                            @include('flash::message')
                                            {{ Form::open(['url'=>route('bimbel-ujian.store'), 'id'=>'form-bimbel', 'class'=>'form-horizontal', 'name'=>'formbimbel']) }}
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    {{ Form::email('email',null,['class'=>'form-control sel-email','placeholder'=>'Masukkan Email Pendaftaran Anda']) }}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    {{ Form::select('cabang',$allCabang,null,['class'=>'form-control sel-cabang','placeholder'=>'Pilih Cabang']) }}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    {{ Form::select('kd_program',[1=>'WPPE',19=>'WPPE Pemasaran',18=>'WPPE Pemasaran Terbatas',2=>'WMI',22=>'ASPM'],null,['class'=>'form-control sel-program','placeholder'=>'Pilih Program']) }}
                                                </div>
                                            </div>
                                            <div class="form-group tgl_wrapper" style="">
                                                <div class="col-sm-12 text-center">
                                                    <img src="{{ asset('assets/images/loading3.gif') }}" class="img-loader" alt="Loading" align="center" style="display: none;">
                                                    {{ Form::select('tanggal_jam',[],null,['class'=>'form-control sel-tanggal','placeholder'=>'Pilih Tanggal dan Jam']) }}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-7 col-sm-offset-2">                            <br>
                                                    <button class="btn btn-lg btn-block btn-loading btn-bimbel" disabled data-toggle="loading" data-loading-text="<i class='fa fa-spinner fa-pulse fa-fw' style='font-size: 14px;color: #fff;'></i> Loading" type="button">Daftar Ujian Ulang <i class="fa fa-paper-plane" style="font-size: 14px;color: #fff;"></i></button>
                                                </div>
                                            </div>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                </div>
                            </div>
                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                    <!-- Blog Share Post -->
                    <div class="share">
                        <h5>Share Post : </h5>
                        <ul class="social-icons">
                            <li class="fb-share-button"  data-href="{{ url()->current() }}" data-layout="button" data-size="small" data-mobile-iframe="true">
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}&src=sdkpreparse" class="fb-xfbml-parse-ignore" target="_blank" title="Facebook">Facebook</a>
                                {{--                                <div class="fb-share-button" data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"></a></div>--}}
                                {{--<div class="fb-share-button" data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>--}}
                            </li>
                        </ul><!-- Blog Social Share -->
                    </div><!-- Blog Share Post -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Page Default -->
@endsection