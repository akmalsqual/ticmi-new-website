@extends('layout.home')
@section('content')
    <section data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="/assets/images/ticmi/ujian.jpg" class="img-responsive" alt="Capital Market Professional Development Program">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            <h4 style="text-align:center">BIMBINGAN & UJIAN ULANG GRATIS</h4>

                            <div class="alert alert-success" role="alert"><p class="text-center">Dapatkan <strong>Sertifikat di hari yang sama</strong> jika Anda lulus ujian ulang.</p></div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">TERIMA KASIH</h3>
                                        </div>
                                        <div class="panel-body">
                                            <div class="alert alert-info" role="alert">
                                                <p class="text-center"><strong>Perdaftaran Berhasil </strong> kami mengirim email notifikasi ke email Anda, silahkan cek email Anda. Pastikan Anda sudah membaca syarat-syarat pendaftaran dan ujian dibawah.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">SYARAT PENDAFTARAN</h3>
                                        </div>
                                        <div class="panel-body">
                                            <ol>
                                                <li>Peserta sudah pernah mengikuti program pelatihan sertifikasi yang diselenggarakan oleh TICMI dan sudah mengikuti Ujian Perdana</li>
                                                <li>Peserta melakukan pendaftaran online pada website TICMI.</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">SYARAT MENDAPATKAN SERTIFIKAT DI HARI YANG SAMA</h3>
                                        </div>
                                        <div class="panel-body">
                                            <ol>
                                                <li>Peserta membawa fotocopy KTP/SIM/Surat Keterangan dan Ijazah Terakhir yang akan diserahkan pada saat mengambil Sertifikat.</li>
                                                <li>Hanya berlaku untuk yang ujian di Jakarta.</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">SYARAT MENGIKUTI UJIAN</h3>
                                        </div>
                                        <div class="panel-body">
                                            <ol>
                                                <li>Peserta mengikuti bimbingan ujian terlebih dahulu pada jadwal yang sudah ditentukan.</li>
                                                <li>Peserta yang bisa mengikuti ujian adalah peserta pelatihan sertifikasi TICMI yang masa berlaku ujian 18 bulannya belum berakhir.</li>
                                                <li>
                                                    Peserta diwajibkan membawa dan menggunakan Laptop sendiri untuk melakukan ujian, dengan syarat spesifikasi laptop sebagai berikut :
                                                    <ul style="list-style: disc;margin-left: 20px;">
                                                        <li>Procesor laptop disarankan setara Dual Core atau diatasnya</li>
                                                        <li>RAM minimal 2Gb</li>
                                                        <li>Sudah ter-install Browser Mozila Firefox versi 50 atau diatasnya</li>
                                                        <li>Dapat terhubung dengan internet dengan menggunakan WIFI.</li>
                                                        <li>Daya tahan baterai laptop minimal 2 jam untuk dapat menyala terus menerus selama berlangsungnya ujian</li>
                                                    </ul>
                                                </li>
                                                <li>Peserta menunjukkan email konfirmasi pendaftaran pada saat melakukan registrasi</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                </div>
                            </div>
                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                    <!-- Blog Share Post -->
                    <div class="share">
                        <h5>Share Post : </h5>
                        <ul class="social-icons">
                            <li class="fb-share-button"  data-href="{{ url()->current() }}" data-layout="button" data-size="small" data-mobile-iframe="true">
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}&src=sdkpreparse" class="fb-xfbml-parse-ignore" target="_blank" title="Facebook">Facebook</a>
                                {{--                                <div class="fb-share-button" data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"></a></div>--}}
                                {{--<div class="fb-share-button" data-href="{{ route('cmpdp') }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>--}}
                            </li>
                        </ul><!-- Blog Social Share -->
                    </div><!-- Blog Share Post -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Page Default -->
@endsection