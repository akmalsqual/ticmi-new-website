@extends("layout.home")
@section('content')

    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('./assets/images/banner/profile.jpg') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">{{ $pengajar->nama }}</h3>
                        <h6 class="sub-title">{{ $pengajar->jabatan }} - {{ $pengajar->institusi }}</h6>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->


    <!-- Page Main -->
    <div role="main" class="main">
        <div class="page-default bg-grey team-single">
            <!-- Container -->
            <div class="container">
                <div class="row">
                    <!-- Sidebar -->
                    <div class="col-md-3">
                        <!-- aside -->
                        <aside class="sidebar">

                            <!-- Widget -->
                            <div class="widget">
                                <h5 class="widget-title">Pengajar Kami<span></span></h5>
                                <ul class="thumbnail-widget thumb-circle">
                                    @foreach($otherPengajar as $item)
                                        <li>
                                            <div class="thumb-wrap" style="width: 55px;">
                                                <img alt="{{ $item->nama }}" class="img-responsive" src="http://203.148.84.28/images/{{ explode(':',$item->foto)[0] }}.jpg" style="width: 66px;height: 66px;">
                                            </div>
                                            <div class="thumb-content"><h5><a class="dark" href="{{ route('pengajar.view',['id'=>$item->id,'nama'=>str_replace(' ','-',strtolower($item->nama))]) }}">{{ $item->nama }}</a></h5><span class="dark">{{ $item->institusi }}</span></div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div><!-- Widget -->

                            <!-- Widget -->
                            <div class="widget">
                                <h5 class="widget-title">Program Kami<span></span></h5>
                                <ul class="thumbnail-widget">
                                    @if($batch && $batch->count() > 0)
                                        @foreach($batch as $value)
                                            <li>
                                                <div class="thumb-wrap" style="width: 50px;">
                                                    @if($value->program->nm_program == "WPPE")
                                                        @if($value->kelas->type->nama == "Regular")
                                                            <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wppe-reguler-3.jpg') }}" style="width: 100px;" width="100" height="60">
                                                        @elseif($value->kelas->type->nama == "Waiver")
                                                            <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wppe-waiver-3.jpg') }}" style="width: 100px;" width="100" height="60">
                                                        @else
                                                        @endif
                                                    @elseif($value->program->nm_program == "WMI")
                                                        @if($value->kelas->type->nama == "Regular")
                                                            <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wmi-reguler-3.jpg') }}" style="width: 100px;" width="100" height="60">
                                                        @elseif($value->kelas->type->nama == "Waiver")
                                                            <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wmi-waiver-3.jpg') }}" style="width: 100px;" width="100" height="60">
                                                        @else
                                                        @endif
                                                    @elseif($value->program->nm_program == "ASPM")
                                                        @if($value->kelas->type->nama == "Regular")
                                                            <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/aspm-reguler-3.jpg') }}" style="width: 100px;" width="100" height="60">
                                                        @elseif($value->kelas->type->nama == "Waiver")
                                                            <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/aspm-akselerasi-3.jpg') }}" style="width: 100px;" width="100" height="60">
                                                        @else
                                                        @endif
                                                    @elseif($value->program->nm_program == "PT-ASPM")
                                                        <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/aspm-akselerasi-3.jpg') }}" style="width: 100px;" width="100" height="60">
                                                    @else
                                                        <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wppe-waiver-3.jpg') }}" style="width: 100px;" width="100" height="60">
                                                    @endif
                                                    {{--<img width="60" height="60" alt="Thumb" class="img-responsive" src="images/default/course-thumb-01.jpg">--}}
                                                </div>
                                                <div class="thumb-content"><a href="{{ url('daftar/'.$value->id.'/'.$value->nama) }}" class="dark">{{ $value->nama }}</a><span>Rp{{ number_format($value->harga) }}</span></div>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul><!-- Thumbnail Widget -->
                            </div><!-- Widget -->

                        </aside><!-- aside -->
                    </div><!-- Column -->

                    <!-- Page Content -->
                    <div class="col-md-9">
                        <div class="row team-list">
                            <!-- Member Image Column -->
                            <div class="col-md-4">
                                <div class="owl-carousel dots-inline"
                                     data-animatein=""
                                     data-animateout=""
                                     data-items="1" data-margin=""
                                     data-loop="true"
                                     data-merge="true"
                                     data-nav="false"
                                     data-dots="true"
                                     data-stagepadding=""
                                     data-mobile="1"
                                     data-tablet="1"
                                     data-desktopsmall="1"
                                     data-desktop="1"
                                     data-autoplay="false"
                                     data-delay="3000"
                                     data-navigation="true">

                                    <div class="item"><img class="img-responsive" src="http://203.148.84.28/images/{{ explode(':',$pengajar->foto)[0] }}.jpg" alt="{{ $pengajar->nama }}" width="400" height="500"></div>
                                    <div class="item"><img class="img-responsive" src="http://203.148.84.28/images/{{ explode(':',$pengajar->foto)[0] }}.jpg" alt="{{ $pengajar->nama }}" width="400" height="500"></div>
                                    <div class="item"><img class="img-responsive" src="http://203.148.84.28/images/{{ explode(':',$pengajar->foto)[0] }}.jpg" alt="{{ $pengajar->nama }}" width="400" height="500"></div>
                                </div><!-- carousel -->
                            </div><!-- Coloumn -->
                            <!-- Coloumn -->
                            <div class="col-md-8">
                                <div class="member-detail-wrap">
                                    <h4 class="member-name">{{ $pengajar->nama }}</h4>
                                    <span class="position">{{ $pengajar->jabatan }} {{ $pengajar->institusi }}</span>
                                    <p>Wanita penyuka <em>Karedok</em> ini gemar sekali dengan <em>Travelling</em>. Bercita-cita untuk dapat keliling dunia dengan keluarga. Sudah menggeluti dibidang keuangan sejak 2009 dan kini bekerja di OJK. </p>
                                    <div class="share">
                                        @if($pengajar->share_fb || $pengajar->share_twitter || $pengajar->share_linkedin || $pengajar->share_email)
                                            <h5>Connect : </h5>
                                        @endif
                                        <ul class="social-icons color round">
                                            @if($pengajar->share_fb)
                                                <li class="facebook"><a href="{{ $pengajar->share_fb }}" target="_blank" title="Facebook">Facebook</a></li>
                                            @endif
                                            @if($pengajar->share_twitter)
                                                <li class="twitter"><a href="{{ $pengajar->share_twitter }}" target="_blank" title="Twitter">Twitter</a></li>
                                            @endif
                                            @if($pengajar->share_linkedin)
                                                <li class="linkedin"><a href="{{ $pengajar->share_linkedin }}" target="_blank" title="Linkedin">Linkedin</a></li>
                                            @endif
                                            @if($pengajar->share_email)
                                                <li class="mail"><a href="mailto:{{ $pengajar->share_email }}" target="_blank" title="{{ $pengajar->share_email }}">{{ $pengajar->share_email }}</a></li>
                                            @endif
                                        </ul><!-- Blog Social Share -->
                                    </div>
                                    <blockquote>
                                        <p>The different between the novice and the master is that the master has failed <strong>more</strong> times than the novice has tried.</p>
                                    </blockquote>
                                </div><!-- Member Detail Wrapper -->
                                <iframe width="100%" height="315" src="https://www.youtube.com/embed/kQIRM-5dsSo?autoplay=0&showinfo=0&controls=0" frameborder="0" allowfullscreen></iframe>
                            </div><!-- Member Detail Column -->

                            <div class="col-md-12">

                            </div>

                            <!-- Column -->
                            <div class="col-md-12  margin-top-50">
                                <h4 class="title-simple">Keahlian</h4>
                                <table class="table default bordered">
                                    <tbody>
                                    @if($keahlian)
                                        @foreach($keahlian as $item)
                                            <tr class="info">
                                                <td> {{ $item->nama }} </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div><!-- Column -->

                        </div><!-- Row -->
                    </div><!-- Column -->
                </div><!-- Row -->
            </div><!-- Container -->
        </div><!-- Page Default -->
    </div><!-- Page Main -->

@endsection