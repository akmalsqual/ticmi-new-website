@extends('layout.home')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('{{ url('assets/images/banner/profile.jpg') }}') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Login</h3>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <!-- Page Main -->
    <section class="relative bg-light typo-dark parallax-bg bg-cover overlay white md"  data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.5">
        <!-- Container -->
        <div class="container  parent-has-overlay">

            <div class="row shop-forms">
                <div class="col-sm-12">
                    <!-- Row -->
                    <div class="row">
                        <div class="col-md-offset-4 col-md-4 content-box shadow bg-white">
                            <ul class="text-center">
                                <!-- Page Template Logo -->
                                <li class="logo-wrap">
                                    <a href="" class="logo">
                                        <img width="211" height="40" alt="The Indonesia Capital Market Institute" class="img-responsive" src="{{ url('/assets/images/default/logo-ticmi.jpg') }}">
                                    </a>
                                </li><!-- Page Template Logo -->
                                <!-- Page Template Content -->
                                <li class="template-content">
                                    <div class="contact-form">
                                        @include('errors.list')
                                        @include('flash::message')
                                        <!-- Form Begins -->
                                            {{ Form::open(['url'=>route('login')]) }}
                                            <!-- Field 1 -->
                                            <div class="input-text form-group text-left">
                                                <label>Email</label>
                                                {!! Form::text('email',null,['class'=>'input-name form-control','placeholder'=>'Email']) !!}
                                            </div>
                                            <!-- Field 2 -->
                                            <div class="input-email form-group text-left">
                                                <a class="pull-right" href="{{ route('forgotpassword') }}">(Lupa Password?)</a>
                                                <label>Password</label>
                                                {!! Form::password('password',['class'=>'form-control','placeholder'=>'Password']) !!}
                                            </div>
                                            <!-- Button -->
                                            {!! Form::submit('Login',['class'=>'btn','data-loading-text'=>'Loading ...','data-toggle'=>'loading','data-confirm'=>'false']) !!}
                                            {{ Form::close() }}
                                            <p>
                                                <br>
                                                Belum mempunyai akun? Silahkan daftar <a href="{{ url('register') }}">disini</a>
                                            </p>
                                    </div>
                                </li><!-- Page Template Content -->
                            </ul>
                        </div><!-- Column -->
                    </div><!-- Row -->
                </div>
            </div><!-- Row -->

        </div><!-- Container -->
    </section><!-- Page Default -->
    <!-- Page Main -->


@endsection