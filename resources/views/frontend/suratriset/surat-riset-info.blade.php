@extends('layout.member.base')
@section('member-content')
    <div class="content-box shadow bg-white  shop-forms">
    @if($roles->name == 'register')
        <table class="table table-bordered bg-google-red typo-light" style="">
            <tr>
                <td width="25%"><span>No Pengajuan <br> <strong>#{{ $suratRiset->invoiceNumber }}</strong></span></td>
                <td width="25%"><span>Biaya <br> <strong>Rp{{ number_format($suratRiset->price,0,',','.') }}</strong></span></td>
                <td width="25%"><span>Status Pengajuan <br> <strong>Diajukan</strong></span></td>
                <td width="25%"><span>Metode Pembayaran <br> <strong>Transfer</strong></span></td>
            </tr>
        </table>

    @else

    @endif
    </div>
@endsection