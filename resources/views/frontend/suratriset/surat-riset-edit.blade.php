@extends('layout.member.base')
@section('member-content')
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h4 class="panel-title">Pengajuan Surat Riset</h4>
        </div>
        <div class="panel-body">
            {!! Form::model($suratRiset,['url'=>route('member.suratriset.update',['suratriset'=>$suratRiset]),'files'=>true,'method'=>'PUT','class'=>'form-horizontal']) !!}
            @include('frontend.suratriset.surat-riset-form')
            {!! Form::close() !!}
        </div>
    </div>
@endsection