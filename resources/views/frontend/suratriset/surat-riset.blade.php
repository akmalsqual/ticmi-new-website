@extends('layout.member.base')
@section('member-content')
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h4 class="panel-title">Pengajuan Surat Riset</h4>
        </div>
        <div class="panel-body">
            @include('errors.list')
            @include('flash::message')

            @if(auth()->user()->is_valid_sid)
                <div class="alert alert-info">
                    <p>
                        Anda sudah bisa mengajukan <strong>Permohonan Surat Riset</strong>. Anda bisa mengajukan Permohonan Surat Riset sebanyak 1 (satu) kali. Pastikan isian Permohonan Surat Riset Anda terisi
                        dengan benar dan sesuai.
                    </p>
                </div>
                @if($suratRiset->count() == 0)
                    <a href="{{ route('member.suratriset.create') }}" class="btn btn-info pull-right">Buat Permohonan Surat Riset</a>
                    <div class="clearfix"></div>
                    <br>
                @endif
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>No Surat Pengantar</th>
                        <th>Tanggal</th>
                        <th>Judul T.A/Skripsi/Thesis</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($suratRiset && $suratRiset->count() > 0)
                        @foreach($suratRiset as $item)
                            <tr>
                                <td>{{ $item->noSuratPengantar }}</td>
                                <td>{{ $item->createdDate }}</td>
                                <td>{{ $item->judulTugasAkhir }}</td>
                                <td>
                                    <div class="btn-group">
                                        @if(empty($item->approveSuperAdmin))
                                            <a href="{{ route('member.suratriset.edit',['suratriset'=>$item->idResearchApplication]) }}" class="btn btn-info btn-xs" title="Ubah Pengajuan Surat Riset"><i class="fa fa-pencil"></i></a>
                                            <a href="{{ route('member.suratriset.destroy',['suratriset'=>$item->idResearchApplication]) }}" class="btn btn-danger btn-xs delconfirm" data-del-message="Apakah Anda yakin untuk hapus pengajuan ini ? Data yang sudah dihapus tidak bisa di kembalikan." title="Hapus Pengajuan Surat Riset"><i class="fa fa-times"></i></a>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4">Belum ada pengajuan surat riset</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            @else
                @if($sumAmountPembelian >= 100000)
                    <div class="alert alert-info">
                        <p>
                            Anda sudah bisa mengajukan <strong>Permohonan Surat Riset</strong>. Anda bisa mengajukan Permohonan Surat Riset sebanyak 1 (satu) kali. Pastikan isian Permohonan Surat Riset Anda terisi
                            dengan benar dan sesuai.
                        </p>
                    </div>
                    @if($suratRiset->count() == 0)
                        <a href="{{ route('member.suratriset.create') }}" class="btn btn-info pull-right">Buat Permohonan Surat Riset</a>
                        <div class="clearfix"></div>
                        <br>
                    @endif
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>No Surat Pengantar</th>
                            <th>Tanggal</th>
                            <th>Judul T.A/Skripsi/Thesis</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($suratRiset && $suratRiset->count() > 0)
                            @foreach($suratRiset as $item)
                                <tr>
                                    <td>{{ $item->noSuratPengantar }}</td>
                                    <td>{{ $item->createdDate }}</td>
                                    <td>{{ $item->judulTugasAkhir }}</td>
                                    <td>
                                        <div class="btn-group">
                                            @if(empty($item->approveSuperAdmin))
                                                <a href="{{ route('member.suratriset.edit',['suratriset'=>$item->idResearchApplication]) }}" class="btn btn-info btn-xs" title="Ubah Pengajuan Surat Riset"><i class="fa fa-pencil"></i></a>
                                                <a href="{{ route('member.suratriset.destroy',['suratriset'=>$item->idResearchApplication]) }}" class="btn btn-danger btn-xs delconfirm" data-del-message="Apakah Anda yakin untuk hapus pengajuan ini ? Data yang sudah dihapus tidak bisa di kembalikan." title="Hapus Pengajuan Surat Riset"><i class="fa fa-times"></i></a>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4">Belum ada pengajuan surat riset</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                @else
                    <div class="alert alert-info">
                        <p>
                            Untuk dapat mengajukan surat riset, silahkan melakukan Pembelian Data minimal senilai <strong>Rp100.000 (Seratus Ribu Rupiah)</strong>.
                            Jika sudah melakukan Pembelian Data dan Pembayaran sudah di verifikasi oleh TICMI maka menu untuk pengajuan Surat Riset akan tampil di halaman ini.
                        </p>
                    </div>
                @endif
            @endif
        </div>
    </div>
@endsection