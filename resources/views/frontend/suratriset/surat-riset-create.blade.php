@extends('layout.member.base')
@section('member-content')
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h4 class="panel-title">Pengajuan Surat Riset</h4>
        </div>
        <div class="panel-body">
            {!! Form::open(['url'=>route('member.suratriset.store'),'files'=>true,'class'=>'form-horizontal']) !!}
            @include('errors.list')
            @include('flash::message')
            @include('frontend.suratriset.surat-riset-form')
            {!! Form::close() !!}
        </div>
    </div>
@endsection