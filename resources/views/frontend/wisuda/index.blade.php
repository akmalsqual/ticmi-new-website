@extends('layout.home')
@section('content')

    <section data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">
            <div class="row">
                <!-- Blog Column -->
                <div class="col-xs-12 blog-single">
                    <div class="blog-single-wrap">
                        <div class="blog-img-wrap">
                            <img width="1920" height="700" src="/assets/images/ticmi/wisuda/banner.jpg" class="img-responsive" alt="Capital Market Professional Development Program">
                        </div>
                        <!-- Blog Detail Wrapper -->
                        <div class=" content-box bg-white shadow">
                            <h5 class="text-center"><a href="#">Wisuda Akbar TICMI Periode II 2017</a></h5>
                            <p class="text-justify">
                                <strong>The Indonesia Capital Market Intitute (TICMI)</strong> mengundang semua lulusan Program Sertifikasi dengan periode kelulusan
                                <strong>April 2017</strong> s/d <strong>Oktober 2017</strong> untuk mengikuti acara Wisuda Akbar TICMI Periode II yang akan di selenggarakan pada :
                            </p>
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <table class="table table-bordered table-hover table-striped">
                                        <tbody>
                                        <tr>
                                            <td align="right" width="30%">Hari / Tanggal</td>
                                            <td>Rabu, 15 November 2017</td>
                                        </tr>
                                        <tr>
                                            <td align="right">Waktu</td>
                                            <td>Pukul 14:30 s/d 17:00 WIB</td>
                                        </tr>
                                        <tr>
                                            <td align="right">Tempat</td>
                                            <td>Main Hall BEI, Gedung Bursa Efek Indonesia, Jl. Jend. Sudirman Kav. 52-53, Jakarta-12190 </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <strong>Acara ini tidak dikenakan biaya</strong> <br>
                                    Kami mohon kesediaan Ibu/Bapak/Saudara/i untuk melakukan konfirmasi kehadiran dengan mengisi form daftar kehadiran.
                                    <br>
                                    <br>
                                </div>
                                <div class="col-sm-8 col-sm-offset-2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">Form Konfirmasi Kehadiran</div>
                                        </div>
                                        <div class="panel-body">
                                            @include('errors.list')
                                            @include('flash::message')
                                            {!! Form::open(['url'=>route('wisuda.store'),'class'=>'form-horizontal']) !!}
                                            <div class="form-group">
                                                <label for="nama" class="hidden-xs col-sm-3 control-label">Nama Lengkap</label>
                                                <div class="col-sm-9">
                                                    {{ Form::text('nama',null,['class'=>'form-control','placeholder'=>'Nama Lengkap (tanpa gelar)']) }}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="nama" class="hidden-xs col-sm-3 control-label">Email</label>
                                                <div class="col-sm-9">
                                                    {{ Form::text('email',null,['class'=>'form-control','placeholder'=>'Email yang Anda gunakan untuk mendaftar di TICMI']) }}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="nama" class="hidden-xs col-sm-3 control-label">Program yang Anda ikuti</label>
                                                <div class="col-sm-9">
                                                    {{ Form::select('program',$program,null,['class'=>'form-control','placeholder'=>'Pilih Program yang Anda ikuti']) }}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="nama" class="hidden-xs col-sm-3 control-label">No Telepon Genggam</label>
                                                <div class="col-sm-9">
                                                    {{ Form::text('no_hp',null,['class'=>'form-control','placeholder'=>'Nomor Telepon Genggam']) }}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="nama" class="hidden-xs col-sm-3 control-label">Konfirmasi Kehadiran</label>
                                                <div class="col-sm-9">
                                                    {{ Form::select('is_confirm',['Saya tidak dapat hadir','Saya Akan hadir'],null,['class'=>'form-control','placeholder'=>'Konfirmasi Kehadiran']) }}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    {!! Form::submit('Submit',['class'=>'btn btn-lg btn-block','data-loading-text'=>'Loading ...','data-toggle'=>'loading']) !!}
                                                </div>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    {{--<strong>Konfirmasi kehadiran sudah di tutup.</strong> <br>--}}
                                    Jika Anda mempunyai pertanyaan dan membutuhkan informasi lebih lanjut silahkan email ke : info@ticmi.co.id atau hubungi callcenter di : 0800 100 9000
                                    <br>
                                </div>
                            </div>

                        </div><!-- Blog Detail Wrapper -->
                    </div><!-- Blog Wrapper -->

                    <!-- Blog Share Post -->
                    <div class="share">
                        <h5>Share Post : </h5>
                        <ul class="social-icons">
                            <li class="fb-share-button"  data-href="{{ route('wisuda') }}" data-layout="button" data-size="small" data-mobile-iframe="true">
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('wisuda') }}&src=sdkpreparse" class="fb-xfbml-parse-ignore" target="_blank" title="Facebook">Facebook</a>
                            </li>
                        </ul><!-- Blog Social Share -->
                    </div><!-- Blog Share Post -->

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Page Default -->
@endsection