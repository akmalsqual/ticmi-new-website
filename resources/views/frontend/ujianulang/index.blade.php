@extends('layout.home')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('./assets/images/banner/profile.jpg') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Ujian Ulang</h3>
                        <h5 class="sub-title">Wakil Perantara Pedagang Efek Pemasaran</h5>
                        <br/>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <!-- Course Wrapper -->
            <div class="row course-single">
                <!-- Course Banner Image -->
                <div class="col-sm-12">
                    <div class="owl-crousel">
                        <!--                        <img alt="Course" class="img-responsive" src="./assets/images/course/wppe-product.jpg" width="1920" height="966">-->
                    </div>
                </div><!-- Column -->

            </div><!-- Course Wrapper -->
            <br/>
            <div class="row course-single content-box bg-white shadow">
                <!-- Course Detail -->
                <div class="col-sm-6">
                    <div class="owl-crousel">
                        <img alt="Course" class="img-responsive" src="{{ url('assets/images/ticmi/course-ticmi.jpg') }}" width="1920" height="966">
                    </div>
                </div><!-- Column -->

                <!-- Course Detail -->
                <div class="col-sm-6">
                    <div class="course-detail">
                        <!-- Course Content -->
                        <div class="course-meta">
                            <h4>WPPE PEMASARAN</h4>
                            <span class="cat bg-yellow">WPPE-P</span><span class="cat bg-green">Ujian Ulang</span>
                            <!--<h4>Wakil Perantara Pedagang Efek</h4>-->
                            <ul class="course-meta-icons">
                                <li><i class="fa fa-dollar"></i><span>Biaya Ujian</span><h5>Gratis 1(satu) kali ujian ulang </h5></li>
                                <li><i class="fa fa-users"></i><span>Minimum Peserta</span><h5>30 Orang/Sesi</h5></li>
                                <li><i class="fa fa-map-marker"></i><span>Tempat</span><h5>TICMI, Gd Bursa Efek Indonesia Jl. Jenderal Sudirman kav 52-53 Jakarta 12190</h5></li>
                                <li><i class="fa fa-clock-o"></i><span>Periode Ujian</span><h5>Januari 2017</h5></li>
                                <li><i class="fa fa-clock-o"></i><span>Durasi Ujian</span><h5>Ujian Ulang 2 Jam <br/><small>(disesuaikan dengan modul yang diulang)</small> </h5></li>
                            </ul>
                        </div>

                    </div><!-- Course Detail -->
                </div><!-- Column -->

            </div>

            <div class="row course-full-detail content-box bg-white shadow">
                <div class="col-sm-12">
                    <div>
                        <h4>Syarat dan Ketentuan</h4>
                        <ol>
                            <li>Ujian ulang ini hanya berlaku untuk program WPPE Pemasaran</li>
                            <li>Periode ujian ulang gratis ini berlaku dari 1 Januari 2017 sampai dengan 31 Januari 2017</li>
                            <li>Ujian ulang gratis hanya berlaku untuk 1 kali ujian ulang</li>
                            <li>Peserta yang bisa mengikuti kegiatan ini adalah peserta yang sudah mengikuti pelatihan dan sudah ujian perdana pada Program WPPE Pemasaran TICMI</li>
                            <li>Peserta melakukan pendaftaran ujian ulang di website www.ticmi.co.id</li>
                        </ol>
                        <h4>Jadwal Ujian</h4>
                        <div class="table-responsive">
                            <table class="table table-responsive">
                                <thead>
                                <tr class="warning">
                                    <td width="25%">Cabang</td>
                                    <td width="45%">Lokasi</td>
                                    <td>Jadwal</td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="info">
                                    <td>Jakarta</td>
                                    <td>TICMI, Gd Bursa Efek Indonesia Jl. Jenderal Sudirman kav 52-53 Jakarta 12190</td>
                                    <td>
                                        <strong>Tanggal :</strong> 9 - 12 Januari 2017 <br/>
                                        <p>
                                            <div class="pull-left"><strong>Jam Ujian : </strong></div>
                                            <div class="pull-left" style="padding-left: 5px;">
                                                Sesi I : 10:00 - 12:00 <br/>
                                                Sesi II : 14:00 - 16:00
                                            </div>
                                        </p>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <p class="text-center">
                        <a href="{{ route('daftarujianulang.form') }}" class="btn btn-lg"><span class="fa fa-angle-double-right"></span> Saya ingin daftar</a>
                    </p>
                </div><!-- Column -->
            </div><!-- row -->
        </div><!-- Container -->
    </div><!-- Page Default -->
@endsection