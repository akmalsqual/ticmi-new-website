@extends('layout.home')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('./assets/images/banner/profile.jpg') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Pendaftaran Ujian Ulang WPPE</h3>
                        <h6 class="sub-title">Wakil Perantara Pedagang Efek</h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-lgrey">
        <!-- Container -->
        <div class="container">

            <!-- Form Begins -->
            {{ Form::open(['url'=>route('daftarujianulang.store'),'id'=>'form-ujianulang']) }}
            {{ Form::hidden('cabang',$cabang) }}
            {{ Form::hidden('kd_program',$kd_program,['class'=>'kd_program']) }}
            <div class="row course-single content-box bg-google-red shadow ">

                <div class="col-md-10 col-md-offset-1">
                @include('errors.list')
                @include('flash::message')
                <!-- Field 1 -->
                    <!-- Field 2 -->
                    <div class="form-group">
                        <div class="col-sm-8">
                            {{ Form::email('email',null,['class'=>'form-control sel-email','placeholder'=>'Masukkan Email Pendaftaran Anda']) }}
                        </div>
                        <div class="col-sm-4">
                            {{ Form::select('cabang',$allCabang,null,['class'=>'form-control sel-cabang']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-7 col-sm-offset-5">
                            <br>
                            <button class="btn btn-loading btn-ujianulang" data-toggle="loading" data-loading-text="<i class='fa fa-spinner fa-pulse fa-fw' style='font-size: 14px;color: #fff;'></i> Loading" type="button">Daftar <i class="fa fa-paper-plane" style="font-size: 14px;color: #fff;"></i></button>
                        </div>
                    </div>
                    <!-- Button -->
                    {{--<button class="btn" type="submit">Send Now <i class="fa fa-paper-plane" style="font-size: 14px;color: #fff;"></i></button>--}}
                </div><!-- Column -->

            </div>
            <br>
            <div class="row course-single content-box bg-white shadow form-ujianulang" style="display: none;">
                <div class="col-sm-12">
                    <div class="title-container text-left sm">
                        <div class="title-wrap">
                            <h4 class="title typo-light">Pemilihan Jadwal</h4>
                            <span class="separator line-separator"></span>
                        </div>

                        <div role="alert" class="alert alert-info typo-dark">
                            <strong>Informasi</strong> Anda hanya bisa memilih 1 (satu) Sesi
                        </div>

                    </div><!-- Name -->

                    <!-- Jakarta -->

                    <div class="Jakarta" style="display: none;">
                        <div class="row">
                            <div class="col-sm-4">
                                <h5>Rabu, 1 Maret 2017</h5>
                                <div class="radio">
                                    <label>
                                        @if($jkt11 && $jkt11->count() >= 15)
                                            {{ Form::radio('sesi','March 01 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                            Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 01 2017|10:00-12:00',null,[]) }}
                                            Sesi I (10:00 - 12:00) ({{ 15 - $jkt11->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        @if($jkt12 && $jkt12->count() >= 15)
                                            {{ Form::radio('sesi','March 01 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                            Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 01 2017|14:00-16:00',null,[]) }}
                                            Sesi II (14:00 - 16:00) ({{ 15 - $jkt12->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>

                                <h5>Kamis, 2 Maret 2017</h5>
                                <div class="radio">
                                    <label>
                                        @if($jkt21 && $jkt21->count() >= 15)
                                            {{ Form::radio('sesi','March 02 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                            Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 02 2017|10:00-12:00',null,[]) }}
                                            Sesi I (10:00 - 12:00) ({{ 15 - $jkt21->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        @if($jkt22 && $jkt22->count() >= 15)
                                            {{ Form::radio('sesi','March 02 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                            Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 02 2017|14:00-16:00',null,[]) }}
                                            Sesi II (14:00 - 16:00) ({{ 15 - $jkt22->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>

                                <h5>Senin, 6 Maret 2017</h5>
                                <div class="radio">
                                    <label>
                                        @if($jkt61 && $jkt61->count() >= 15)
                                            {{ Form::radio('sesi','March 06 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                            Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 06 2017|10:00-12:00',null,[]) }}
                                            Sesi I (10:00 - 12:00) ({{ 15 - $jkt61->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        @if($jkt62 && $jkt62->count() >= 15)
                                            {{ Form::radio('sesi','March 06 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                            Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 06 2017|14:00-16:00',null,[]) }}
                                            Sesi II (14:00 - 16:00) ({{ 15 - $jkt62->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>

                                <h5>Selasa, 7 Maret 2017</h5>
                                <div class="radio">
                                    <label>
                                        @if($jkt71 && $jkt71->count() >= 15)
                                            {{ Form::radio('sesi','March 07 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                            Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 07 2017|10:00-12:00',null,[]) }}
                                            Sesi I (10:00 - 12:00) ({{ 15 - $jkt71->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        @if($jkt72 && $jkt72->count() >= 15)
                                            {{ Form::radio('sesi','March 07 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                            Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 07 2017|14:00-16:00',null,[]) }}
                                            Sesi II (14:00 - 16:00) ({{ 15 - $jkt72->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>

                                <h5>Rabu, 8 Maret 2017</h5>
                                <div class="radio">
                                    <label>
                                        @if($jkt81 && $jkt81->count() >= 15)
                                            {{ Form::radio('sesi','March 08 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                            Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 08 2017|10:00-12:00',null,[]) }}
                                            Sesi I (10:00 - 12:00) ({{ 15 - $jkt81->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        @if($jkt82 && $jkt82->count() >= 15)
                                            {{ Form::radio('sesi','March 08 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                            Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 08 2017|14:00-16:00',null,[]) }}
                                            Sesi II (14:00 - 16:00) ({{ 15 - $jkt82->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>

                                <h5>Kamis, 9 Maret 2017</h5>
                                <div class="radio">
                                    <label>
                                        @if($jkt91 && $jkt91->count() >= 15)
                                            {{ Form::radio('sesi','March 09 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                            Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 09 2017|10:00-12:00',null,[]) }}
                                            Sesi I (10:00 - 12:00) ({{ 15 - $jkt91->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        @if($jkt92 && $jkt92->count() >= 15)
                                            {{ Form::radio('sesi','March 09 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                            Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 09 2017|14:00-16:00',null,[]) }}
                                            Sesi II (14:00 - 16:00) ({{ 15 - $jkt92->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <h5>Senin, 13 Maret 2017</h5>
                                <div class="radio">
                                    <label>
                                        @if($jkt131 && $jkt131->count() >= 15)
                                            {{ Form::radio('sesi','March 13 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                            Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 13 2017|10:00-12:00',null,[]) }}
                                            Sesi I (10:00 - 12:00) ({{ 15 - $jkt131->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        @if($jkt132 && $jkt132->count() >= 15)
                                            {{ Form::radio('sesi','March 13 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                            Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 13 2017|14:00-16:00',null,[]) }}
                                            Sesi II (14:00 - 16:00) ({{ 15 - $jkt132->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <h5>Selasa, 14 Maret 2017</h5>
                                <div class="radio">
                                    <label>
                                        @if($jkt141 && $jkt141->count() >= 15)
                                            {{ Form::radio('sesi','March 14 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                            Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 14 2017|10:00-12:00',null,[]) }}
                                            Sesi I (10:00 - 12:00) ({{ 15 - $jkt141->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        @if($jkt142 && $jkt142->count() >= 15)
                                            {{ Form::radio('sesi','March 14 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                            Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 14 2017|14:00-16:00',null,[]) }}
                                            Sesi II (14:00 - 16:00) ({{ 15 - $jkt142->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <h5>Rabu, 15 Maret 2017</h5>
                                <div class="radio">
                                    <label>
                                        @if($jkt151 && $jkt151->count() >= 15)
                                            {{ Form::radio('sesi','March 15 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                            Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 15 2017|10:00-12:00',null,[]) }}
                                            Sesi I (10:00 - 12:00) ({{ 15 - $jkt151->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        @if($jkt152 && $jkt152->count() >= 15)
                                            {{ Form::radio('sesi','March 15 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                            Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 15 2017|14:00-16:00',null,[]) }}
                                            Sesi II (14:00 - 16:00) ({{ 15 - $jkt152->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <h5>Kamis, 16 Maret 2017</h5>
                                <div class="radio">
                                    <label>
                                        @if($jkt161 && $jkt161->count() >= 15)
                                            {{ Form::radio('sesi','March 16 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                            Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 16 2017|10:00-12:00',null,[]) }}
                                            Sesi I (10:00 - 12:00) ({{ 15 - $jkt161->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        @if($jkt162 && $jkt162->count() >= 15)
                                            {{ Form::radio('sesi','March 16 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                            Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 16 2017|14:00-16:00',null,[]) }}
                                            Sesi II (14:00 - 16:00) ({{ 15 - $jkt162->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <h5>Senin, 20 Maret 2017</h5>
                                <div class="radio">
                                    <label>
                                        @if($jkt201 && $jkt201->count() >= 15)
                                            {{ Form::radio('sesi','March 20 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                            Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 20 2017|10:00-12:00',null,[]) }}
                                            Sesi I (10:00 - 12:00) ({{ 15 - $jkt201->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        @if($jkt202 && $jkt202->count() >= 15)
                                            {{ Form::radio('sesi','March 20 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                            Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 20 2017|14:00-16:00',null,[]) }}
                                            Sesi II (14:00 - 16:00) ({{ 15 - $jkt202->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <h5>Selasa, 21 Maret 2017</h5>
                                <div class="radio">
                                    <label>
                                        @if($jkt211 && $jkt211->count() >= 15)
                                            {{ Form::radio('sesi','March 21 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                            Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 21 2017|10:00-12:00',null,[]) }}
                                            Sesi I (10:00 - 12:00) ({{ 15 - $jkt211->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        @if($jkt212 && $jkt212->count() >= 15)
                                            {{ Form::radio('sesi','March 21 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                            Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 21 2017|14:00-16:00',null,[]) }}
                                            Sesi II (14:00 - 16:00) ({{ 15 - $jkt212->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <h5>Rabu, 22 Maret 2017</h5>
                                <div class="radio">
                                    <label>
                                        @if($jkt221 && $jkt221->count() >= 15)
                                            {{ Form::radio('sesi','March 22 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                            Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 22 2017|10:00-12:00',null,[]) }}
                                            Sesi I (10:00 - 12:00) ({{ 15 - $jkt221->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        @if($jkt222 && $jkt222->count() >= 15)
                                            {{ Form::radio('sesi','March 22 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                            Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 22 2017|14:00-16:00',null,[]) }}
                                            Sesi II (14:00 - 16:00) ({{ 15 - $jkt222->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <h5>Kamis, 23 Maret 2017</h5>
                                <div class="radio">
                                    <label>
                                        @if($jkt231 && $jkt231->count() >= 15)
                                            {{ Form::radio('sesi','March 23 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                            Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 23 2017|10:00-12:00',null,[]) }}
                                            Sesi I (10:00 - 12:00) ({{ 15 - $jkt231->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        @if($jkt232 && $jkt232->count() >= 15)
                                            {{ Form::radio('sesi','March 23 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                            Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 23 2017|14:00-16:00',null,[]) }}
                                            Sesi II (14:00 - 16:00) ({{ 15 - $jkt232->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <h5>Senin, 27 Maret 2017</h5>
                                <div class="radio">
                                    <label>
                                        @if($jkt271 && $jkt271->count() >= 15)
                                            {{ Form::radio('sesi','March 27 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                            Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 27 2017|10:00-12:00',null,[]) }}
                                            Sesi I (10:00 - 12:00) ({{ 15 - $jkt271->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        @if($jkt272 && $jkt272->count() >= 15)
                                            {{ Form::radio('sesi','March 27 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                            Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 27 2017|14:00-16:00',null,[]) }}
                                            Sesi II (14:00 - 16:00) ({{ 15 - $jkt272->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <h5>Selasa, 28 Maret 2017</h5>
                                <div class="radio">
                                    <label>
                                        @if($jkt281 && $jkt281->count() >= 15)
                                            {{ Form::radio('sesi','March 28 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                            Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 28 2017|10:00-12:00',null,[]) }}
                                            Sesi I (10:00 - 12:00) ({{ 15 - $jkt281->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        @if($jkt282 && $jkt282->count() >= 15)
                                            {{ Form::radio('sesi','March 28 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                            Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 28 2017|14:00-16:00',null,[]) }}
                                            Sesi II (14:00 - 16:00) ({{ 15 - $jkt282->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <h5>Rabu, 29 Maret 2017</h5>
                                <div class="radio">
                                    <label>
                                        @if($jkt291 && $jkt291->count() >= 15)
                                            {{ Form::radio('sesi','March 29 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                            Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 29 2017|10:00-12:00',null,[]) }}
                                            Sesi I (10:00 - 12:00) ({{ 15 - $jkt291->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        @if($jkt292 && $jkt292->count() >= 15)
                                            {{ Form::radio('sesi','March 29 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                            Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 29 2017|14:00-16:00',null,[]) }}
                                            Sesi II (14:00 - 16:00) ({{ 15 - $jkt292->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <h5>Kamis, 30 Maret 2017</h5>
                                <div class="radio">
                                    <label>
                                        @if($jkt301 && $jkt301->count() >= 15)
                                            {{ Form::radio('sesi','March 30 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                            Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 30 2017|10:00-12:00',null,[]) }}
                                            Sesi I (10:00 - 12:00) ({{ 15 - $jkt301->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        @if($jkt302 && $jkt302->count() >= 15)
                                            {{ Form::radio('sesi','March 30 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                            Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                        @else
                                            {{ Form::radio('sesi','March 30 2017|14:00-16:00',null,[]) }}
                                            Sesi II (14:00 - 16:00) ({{ 15 - $jkt302->count() }} Kursi Tersisa)
                                        @endif
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Surabaya -->

                    <div class="Surabaya" style="display: none;">
                        <h5>Senin, 20 Maret 2017</h5>
                        <div class="radio">
                            <label>
                                @if($sby1Sesi1 && $sby1Sesi1->count() >= 50)
                                    {{ Form::radio('sesi','March 20 2017|09:00-11:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (09:00 - 11:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','March 20 2017|09:00-11:00',null,[]) }}
                                    Sesi I (09:00 - 11:00) ({{ 50 - $sby1Sesi1->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                    </div>

                    <!-- Aceh -->

                    <div class="Banda Aceh" style="display: none">
                        <h5>Selasa, 17 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($aceh1 && $aceh1->count() >= 20)
                                    {{ Form::radio('sesi','January 17 2017|09:00-11:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (09:00 - 11:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 17 2017|09:00-11:00',null,[]) }}
                                    Sesi I (09:00 - 11:00) ({{ 20 - $aceh1->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <h5>Rabu, 18 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($aceh2 && $aceh2->count() >= 20)
                                    {{ Form::radio('sesi','January 18 2017|15:00-17:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (15:00 - 17:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 18 2017|15:00-17:00',null,[]) }}
                                    Sesi I (15:00 - 17:00) ({{ 20 - $aceh2->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <h5>Rabu, 25 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($aceh3 && $aceh3->count() >= 20)
                                    {{ Form::radio('sesi','January 25 2017|09:00-11:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (09:00 - 11:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 25 2017|09:00-11:00',null,[]) }}
                                    Sesi I (09:00 - 11:00) ({{ 20 - $aceh3->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                    </div>

                    <!-- Balikpapan -->
                    <div class="Balikpapan" style="display: none">
                        <h5>Rabu, 25 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($balikpapan1 && $balikpapan1->count() >= 25)
                                    {{ Form::radio('sesi','January 25 2017|15:00-17:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (15:00 - 17:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 25 2017|15:00-17:00',null,[]) }}
                                    Sesi I (15:00 - 17:00) ({{ 25 - $balikpapan1->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <h5>Kamis, 26 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($balikpapan2 && $balikpapan2->count() >= 25)
                                    {{ Form::radio('sesi','January 26 2017|15:00-17:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (15:00 - 17:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 26 2017|15:00-17:00',null,[]) }}
                                    Sesi I (15:00 - 17:00) ({{ 25 - $balikpapan2->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                    </div>

                    <!-- Pontianak -->
                    <div class="Pontianak" style="display: none">
                        <h5>Rabu, 17 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($pontianak1 && $pontianak1->count() >= 10)
                                    {{ Form::radio('sesi','January 17 2017|13:00-15:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (13:00 - 15:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 17 2017|13:00-15:00',null,[]) }}
                                    Sesi I (13:00 - 15:00) ({{ 10 - $pontianak1->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <h5>Kamis, 18 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($pontianak2 && $pontianak2->count() >= 10)
                                    {{ Form::radio('sesi','January 18 2017|13:00-15:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (13:00 - 15:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 18 2017|13:00-15:00',null,[]) }}
                                    Sesi I (13:00 - 15:00) ({{ 10 - $pontianak2->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                    </div>

                    <!-- Bandung -->
                    <div class="Bandung" style="display: none">
                        <h5>Kamis, 12 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($bandung1 && $bandung1->count() >= 30)
                                    {{ Form::radio('sesi','January 17 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 17 2017|14:00-16:00',null,[]) }}
                                    Sesi I (14:00 - 16:00) ({{ 30 - $bandung1->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <h5>Jum'at, 13 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($bandung2 && $bandung2->count() >= 30)
                                    {{ Form::radio('sesi','January 18 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 18 2017|14:00-16:00',null,[]) }}
                                    Sesi I (14:00 - 16:00) ({{ 30 - $bandung2->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <h5>Selasa, 17 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($bandung3 && $bandung3->count() >= 30)
                                    {{ Form::radio('sesi','January 17 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 17 2017|10:00-12:00',null,[]) }}
                                    Sesi I (10:00 - 12:00) ({{ 30 - $bandung3->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <h5>Rabu, 18 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($bandung4 && $bandung4->count() >= 30)
                                    {{ Form::radio('sesi','January 18 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 18 2017|10:00-12:00',null,[]) }}
                                    Sesi I (10:00 - 12:00) ({{ 30 - $bandung4->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <h5>Kamis, 19 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($bandung5 && $bandung5->count() >= 30)
                                    {{ Form::radio('sesi','January 19 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 19 2017|14:00-16:00',null,[]) }}
                                    Sesi I (14:00 - 16:00) ({{ 30 - $bandung5->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                    </div>

                    <!-- Pekanbaru -->
                    <div class="Pekanbaru" style="display: none">
                        <h5>Rabu, 25 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($riau11 && $riau11->count() >= 10)
                                    {{ Form::radio('sesi','January 25 2017|09:00-11:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (09:00 - 11:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 25 2017|09:00-11:00',null,[]) }}
                                    Sesi I (09:00 - 11:00) ({{ 10 - $riau11->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                @if($riau12 && $riau12->count() >= 10)
                                    {{ Form::radio('sesi','January 25 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                    Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 25 2017|14:00-16:00',null,[]) }}
                                    Sesi II (14:00 - 16:00) ({{ 10 - $riau12->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <h5>Kamis, 26 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($riau21 && $riau21->count() >= 10)
                                    {{ Form::radio('sesi','January 26 2017|09:00-11:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (09:00 - 11:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 26 2017|09:00-11:00',null,[]) }}
                                    Sesi I (09:00 - 11:00) ({{ 10 - $riau21->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                @if($riau22 && $riau22->count() >= 10)
                                    {{ Form::radio('sesi','January 26 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                    Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 26 2017|14:00-16:00',null,[]) }}
                                    Sesi II (14:00 - 16:00) ({{ 10 - $riau22->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <h5>Jum'at, 27 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($riau31 && $riau31->count() >= 10)
                                    {{ Form::radio('sesi','January 27 2017|09:00-11:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (09:00 - 11:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 27 2017|09:00-11:00',null,[]) }}
                                    Sesi I (09:00 - 11:00) ({{ 10 - $riau31->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                @if($riau32 && $riau32->count() >= 10)
                                    {{ Form::radio('sesi','January 27 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                    Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 27 2017|14:00-16:00',null,[]) }}
                                    Sesi II (14:00 - 16:00) ({{ 10 - $riau32->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                    </div>


                    <!-- Jambi -->
                    <div class="Jambi" style="display: none">
                        <h5>Jum'at, 13 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($jambi1 && $jambi1->count() >= 10)
                                    {{ Form::radio('sesi','January 13 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 13 2017|14:00-16:00',null,[]) }}
                                    Sesi I (14:00 - 16:00) ({{ 10 - $jambi1->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <h5>Sabtu, 14 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($jambi2 && $jambi2->count() >= 10)
                                    {{ Form::radio('sesi','January 14 2017|09:00-11:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (09:00 - 11:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 14 2017|09:00-11:00',null,[]) }}
                                    Sesi I (09:00 - 11:00) ({{ 10 - $jambi2->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                    </div>

                    <!-- Makassar -->
                    <div class="Makassar" style="display: none">
                        <h5>Rabu, 18 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($makassar11 && $makassar11->count() >= 9)
                                    {{ Form::radio('sesi','January 18 2017|13:00-15:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (13:00 - 15:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 18 2017|13:00-15:00',null,[]) }}
                                    Sesi I (13:00 - 15:00) ({{ 9 - $makassar11->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                @if($makassar12 && $makassar12->count() >= 9)
                                    {{ Form::radio('sesi','January 18 2017|15:00-17:00',null,['disabled'=>'disabled']) }}
                                    Sesi II (15:00 - 17:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 18 2017|15:00-17:00',null,[]) }}
                                    Sesi II (15:00 - 17:00) ({{ 9 - $makassar12->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <h5>Kamis, 19 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($makassar21 && $makassar21->count() >= 9)
                                    {{ Form::radio('sesi','January 18 2017|13:00-15:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (13:00 - 15:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 18 2017|13:00-15:00',null,[]) }}
                                    Sesi I (13:00 - 15:00) ({{ 9 - $makassar21->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                @if($makassar22 && $makassar22->count() >= 9)
                                    {{ Form::radio('sesi','January 18 2017|15:00-17:00',null,['disabled'=>'disabled']) }}
                                    Sesi II (15:00 - 17:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 18 2017|15:00-17:00',null,[]) }}
                                    Sesi II (15:00 - 17:00) ({{ 9 - $makassar22->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <h5>Jum'at, 20 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($makassar31 && $makassar31->count() >= 9)
                                    {{ Form::radio('sesi','January 20 2017|13:00-15:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (13:00 - 15:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 20 2017|13:00-15:00',null,[]) }}
                                    Sesi I (13:00 - 15:00) ({{ 9 - $makassar31->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                @if($makassar32 && $makassar32->count() >= 9)
                                    {{ Form::radio('sesi','January 20 2017|15:00-17:00',null,['disabled'=>'disabled']) }}
                                    Sesi II (15:00 - 17:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 20 2017|15:00-17:00',null,[]) }}
                                    Sesi II (15:00 - 17:00) ({{ 9 - $makassar32->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                    </div>

                    <!-- Semarang -->
                    <div class="Semarang" style="display: none">
                        <h5>Kamis, 19 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($semarang11 && $semarang11->count() >= 30)
                                    {{ Form::radio('sesi','January 19 2017|09:00-11:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (09:00 - 11:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 19 2017|09:00-11:00',null,[]) }}
                                    Sesi I (09:00 - 11:00) ({{ 30 - $semarang11->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                @if($semarang12 && $semarang12->count() >= 30)
                                    {{ Form::radio('sesi','January 19 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                    Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 19 2017|14:00-16:00',null,[]) }}
                                    Sesi II (14:00 - 16:00) ({{ 30 - $semarang12->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <h5>Jum'at, 20 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($semarang21 && $semarang21->count() >= 30)
                                    {{ Form::radio('sesi','January 20 2017|09:00-11:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (09:00 - 11:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 20 2017|09:00-11:00',null,[]) }}
                                    Sesi I (09:00 - 11:00) ({{ 30 - $semarang21->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                @if($semarang22 && $semarang22->count() >= 30)
                                    {{ Form::radio('sesi','January 20 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                    Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 20 2017|14:00-16:00',null,[]) }}
                                    Sesi II (14:00 - 16:00) ({{ 30 - $semarang22->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <h5>Kamis, 26 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($semarang31 && $semarang31->count() >= 30)
                                    {{ Form::radio('sesi','January 26 2017|09:00-11:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (09:00 - 11:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 26 2017|09:00-11:00',null,[]) }}
                                    Sesi I (09:00 - 11:00) ({{ 30 - $semarang31->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                @if($semarang32 && $semarang32->count() >= 30)
                                    {{ Form::radio('sesi','January 26 2017|14:00-16:00',null,['disabled'=>'disabled']) }}
                                    Sesi II (14:00 - 16:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 26 2017|14:00-16:00',null,[]) }}
                                    Sesi II (14:00 - 16:00) ({{ 30 - $semarang32->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                    </div>

                    <!-- Padang -->
                    <div class="Padang" style="display: none">
                        <h5>Kamis, 12 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($padang1 && $padang1->count() >= 20)
                                    {{ Form::radio('sesi','January 12 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 12 2017|14:00-16:00',null,[]) }}
                                    Sesi I (10:00 - 12:00) ({{ 20 - $padang1->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <h5>Kamis, 19 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($padang2 && $padang2->count() >= 20)
                                    {{ Form::radio('sesi','January 19 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 19 2017|10:00-12:00',null,[]) }}
                                    Sesi I (10:00 - 12:00) ({{ 20 - $padang2->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                        <h5>Kamis, 26 Januari 2017</h5>
                        <div class="radio">
                            <label>
                                @if($padang3 && $padang3->count() >= 20)
                                    {{ Form::radio('sesi','January 26 2017|10:00-12:00',null,['disabled'=>'disabled']) }}
                                    Sesi I (10:00 - 12:00) <strong class="text-danger">(Kelas Sudah Penuh)</strong>
                                @else
                                    {{ Form::radio('sesi','January 26 2017|10:00-12:00',null,[]) }}
                                    Sesi I (10:00 - 12:00) ({{ 20 - $padang3->count() }} Kursi Tersisa)
                                @endif
                            </label>
                        </div>
                    </div>

                </div><!-- Column -->

                <div class="form-group">
                    <div class="col-sm-6 col-sm-offset-5">
                        <button class="btn btn-loading" data-toggle="loading" data-loading-text="<i class='fa fa-spinner fa-pulse fa-fw' style='font-size: 14px;color: #fff;'></i> Loading" type="submit">Daftar <i class="fa fa-paper-plane" style="font-size: 14px;color: #fff;"></i></button>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection