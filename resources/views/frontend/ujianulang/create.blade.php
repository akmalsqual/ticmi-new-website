@extends('layout.home')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('./assets/images/banner/profile.jpg') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Pendaftaran Ujian Ulang</h3>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <section data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md" style="padding-top: 25px;">
        <!-- Container -->
        <div class="container parent-has-overlay">

            <!-- Form Begins -->
            {{ Form::open(['url'=>route('daftarujianulang.store'),'id'=>'form-ujianulang', 'class'=>'form-horizontal']) }}
            <div class="row course-single content-box bg-white shadow ">

                <div class="col-md-6 col-md-offset-3">
                    <div class="title-container text-left sm">
                        <div class="title-wrap">
                            <h4 class="title typo-light">Pemilihan Jadwal</h4>
                            <span class="separator line-separator"></span>
                        </div>

                        <div role="alert" class="alert alert-info typo-dark">
                            <strong>Informasi</strong> : Anda hanya bisa memilih 1 (satu) Sesi
                        </div>

                    </div><!-- Name -->
                    @include('errors.list')
                    @include('flash::message')
                    <!-- Field 1 -->
                    <!-- Field 2 -->
                    <div class="form-group">
                        <div class="col-sm-12">
                            {{ Form::email('email',null,['class'=>'form-control sel-email','placeholder'=>'Masukkan Email Pendaftaran Anda']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            {{ Form::select('cabang',$allCabang,null,['class'=>'form-control sel-cabang','placeholder'=>'Pilih Cabang']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            {{ Form::select('kd_program',$allProgram,null,['class'=>'form-control sel-cabang','placeholder'=>'Pilih Program']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div class="input-group">
                                {{ Form::text('tanggal',null,['class'=>'form-control datepicker-ujian','readonly'=>'readonly','id'=>'datepicker-ujian','placeholder'=>'Pilih tanggal ujian']) }}
                                <span class="input-group-btn">
                                <a class="btn btn-warning" type="button" id="datepicker-button"><span class="fa fa-calendar"></span></a>
                            </span>
                            </div><!-- /input-group -->
{{--                            {{ Form::text('tanggal',null,['class'=>'form-control datepicker-ujian','readonly'=>'readonly','id'=>'datepicker-ujian','placeholder'=>'Pilih tanggal ujian']) }}--}}
                        </div>
                    </div>
                    <div class="form-group pilih-sesi" style="display: none">
                        <div class="col-sm-6">
                            <div class="radio">
                                <label>
                                    {{ Form::radio('sesi','10:00-12:00',null,['class'=>'sesi_1']) }}
                                    Sesi I (10:00 - 12:00) <span class="kuota-sesi1"></span> <strong class="text-danger full-sesi1" style="display: none;">(Kelas Sudah Penuh)</strong>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="radio">
                                <label>
                                    {{ Form::radio('sesi','14:00-16:00',null,['class'=>'sesi_2']) }}
                                    Sesi II (14:00 - 16:00) <span class="kuota-sesi2"></span> <strong class="text-danger full-sesi2" style="display: none;">(Kelas Sudah Penuh)</strong>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-7 col-sm-offset-2">                            <br>
                            <button class="btn btn-lg btn-block btn-loading btn-ujianulang" disabled data-toggle="loading" data-loading-text="<i class='fa fa-spinner fa-pulse fa-fw' style='font-size: 14px;color: #fff;'></i> Loading" type="submit">Daftar Ujian Ulang <i class="fa fa-paper-plane" style="font-size: 14px;color: #fff;"></i></button>
                        </div>
                    </div>
                    <!-- Button -->
                    {{--<button class="btn" type="submit">Send Now <i class="fa fa-paper-plane" style="font-size: 14px;color: #fff;"></i></button>--}}
                </div><!-- Column -->

            </div>
            {{ Form::close() }}
        </div>
    </section>
@endsection