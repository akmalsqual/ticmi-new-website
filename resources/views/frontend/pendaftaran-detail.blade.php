@extends("layout.home")
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('./assets/images/banner/profile.jpg') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">{{ $batch->nama }}</h3>
                        <h6 class="sub-title">{{ $batch->keterangan }}</h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <script type="application/ld+json">
    {
      "@@context": "http://schema.org",
      "@@type": "Course",
      "name": "{{ $batch->nama }}",
      "description": "{{ $batch->keterangan }} {{ $batch->nama }}",
      "provider": {
        "@@type": "EducationalOrganization",
        "name": "The Indonesia Capital Market Institute",
        "sameAs": "http://ticmi.co.id"
      }
    }
    </script>


    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <!-- Course Wrapper -->
            <div class="row course-single">
                <!-- Course Banner Image -->
                <div class="col-sm-7">
                    <div class="owl-crousel">
                        <!--                        <img alt="Course" class="img-responsive" src="./assets/images/course/aspm-product.jpg" width="1920" height="966">-->
                    </div>
                </div><!-- Column -->
            </div><!-- Course Wrapper -->

            <div class="row course-single content-box  bg-white shadow">
                {{--<p class="text-center">--}}
                    {{--@if($batch->kd_type != 1)--}}
                        {{--<a href="http://akademik.ticmi.co.id/index/daftar{{ $batch->kd_type }}/{{ $batch->id }}/Pendaftaran {{ $batch->nama }}.html" class="btn btn-block"><i class="fa fa-angle-double-right"></i> Saya ingin daftar</a>--}}
                    {{--@else--}}
                        {{--<a href="http://akademik.ticmi.co.id/index/daftar/{{ $batch->cabang }}/{{ $batch->id }}/Pendaftaran {{ $batch->nama }}.html" class="btn btn-block"><i class="fa fa-angle-double-right"></i> Saya ingin daftar</a>--}}
                    {{--@endif--}}
                {{--</p>--}}
                <!-- Course Banner Image -->
                <div class="col-sm-6">
                    <div class="owl-crousel">
                        <img alt="Course" class="img-responsive" src="{{ url('assets/images/ticmi/course-ticmi.jpg') }}" width="1920" height="966">
                    </div>
                </div><!-- Column -->

                <!-- Course Detail -->
                <div class="col-sm-6 ">
                    <div class="course-detail">
                        <!-- Course Content -->
                        <div class="course-meta">
                            <h4>{{ $batch->program->nm_program." ".$batch->kelas->type->nama }}</h4>
                            <span class="cat bg-yellow">{{ $batch->program->nm_program }}</span><span class="cat bg-green">Sertifikasi</span>
                            <!--<h4>Wakil Perantara Pedagang Efek</h4>-->
                            <ul class="course-meta-icons">
                                <li><i class="fa fa-dollar"></i><span>Biaya Investasi</span><h5>IDR{{ number_format($batch->harga,0,".",",") }}</h5></li>
                                <li><i class="fa fa-clock-o"></i><span>Tanggal Mulai Pelatihan</span><h5>{{ $batch->tanggal->formatLocalized('%A, %#d %B %Y') }}</h5></li>
                                <li><i class="fa fa-map-marker"></i><span>Tempat</span><h5>{{ $batch->lokasi }}</h5></li>
                                @if($batch->program->nm_program == "WPPE" || $batch->program->nm_program == "WMI" )
                                    @if($batch->kelas->type->nama == "Waiver")
                                        <li><i class="fa fa-users"></i><span>Minimum Peserta</span><h5>{{ $batch->minimal }} Orang</h5></li>
                                        <li><i class="fa fa-clock-o"></i><span>Durasi Pelatihan</span><h5>8 Jam</h5></li>
                                        <li><i class="fa fa-clock-o"></i><span>Durasi Ujian</span><h5>Ujian Perdana 2 Jam </h5></li>
                                    @else
                                        <li><i class="fa fa-users"></i><span>Minimum Peserta</span><h5>{{ $batch->minimal }} Orang</h5></li>
                                        <li><i class="fa fa-clock-o"></i><span>Durasi Pelatihan</span><h5>75 Jam</h5></li>
                                        <li><i class="fa fa-clock-o"></i><span>Durasi Ujian</span><h5>Ujian Perdana 2 Jam </h5></li>
                                    @endif
                                @elseif($batch->program->nm_program == "WPPE Pemasaran")
                                    @if($batch->kelas->type->nama == "Waiver")
                                        <li><i class="fa fa-users"></i><span>Minimum Peserta</span><h5>{{ $batch->minimal }} Orang</h5></li>
                                        <li><i class="fa fa-clock-o"></i><span>Durasi Pelatihan</span><h5>8 Jam</h5></li>
                                        <li><i class="fa fa-clock-o"></i><span>Durasi Ujian</span><h5>Ujian Perdana 2 Jam </h5></li>
                                    @else
                                        <li><i class="fa fa-users"></i><span>Minimum Peserta</span><h5>{{ $batch->minimal }} Orang</h5></li>
                                        <li><i class="fa fa-clock-o"></i><span>Durasi Pelatihan</span><h5>16 Jam (2 Hari)</h5></li>
                                        <li><i class="fa fa-clock-o"></i><span>Durasi Ujian</span><h5>Ujian Perdana 2 Jam </h5></li>
                                    @endif
                                @elseif($batch->program->nm_program == "WPPE Pemasaran Terbatas")
                                        <li><i class="fa fa-users"></i><span>Minimum Peserta</span><h5>{{ $batch->minimal }} Orang</h5></li>
                                        <li><i class="fa fa-clock-o"></i><span>Durasi Pelatihan</span><h5>8 Jam</h5></li>
                                        <li><i class="fa fa-clock-o"></i><span>Durasi Ujian</span><h5>Ujian Perdana 1.5 Jam </h5></li>
                                @elseif($batch->program->nm_program == "ASPM")
                                    @if($batch->kelas->type->nama == "Waiver")
                                        <li><i class="fa fa-users"></i><span>Minimum Peserta</span><h5>{{ $batch->minimal }} Orang</h5></li>
                                        <li><i class="fa fa-clock-o"></i><span>Durasi Pelatihan</span><h5>56 Jam</h5></li>
                                        <li><i class="fa fa-clock-o"></i><span>Durasi Ujian</span><h5>Ujian Perdana 2 Jam </h5></li>
                                    @else
                                        <li><i class="fa fa-users"></i><span>Minimum Peserta</span><h5>{{ $batch->minimal }} Orang</h5></li>
                                        <li><i class="fa fa-clock-o"></i><span>Durasi Pelatihan</span><h5>80 Jam</h5></li>
                                        <li><i class="fa fa-clock-o"></i><span>Durasi Ujian</span><h5>Ujian Perdana 2 Jam </h5></li>
                                    @endif
                                @elseif($batch->program->nm_program == "PT-ASPM")
                                    <li><i class="fa fa-users"></i><span>Minimum Peserta</span><h5>{{ $batch->minimal }} Orang</h5></li>
                                    <li><i class="fa fa-clock-o"></i><span>Durasi Pelatihan</span><h5>Tidak ada</h5></li>
                                    <li><i class="fa fa-clock-o"></i><span>Durasi Ujian</span><h5>Ujian 2 Jam </h5></li>
                                @endif
                            </ul>
                        </div>
                    </div><!-- Course Detail -->
                </div><!-- Column -->
            </div>

            <div class="row course-full-detail content-box  bg-white shadow">
                <div class="col-sm-12">
                    <div class="">
                        {!! $batch->program->keterangan !!}

                        <p class="text-center">
                        @if($batch->kelas->tipe != 1)
                            <a href="https://akademik.ticmi.co.id/index/daftar{{ $batch->kelas->tipe }}/{{ $batch->id }}/Pendaftaran {{ $batch->nama }}.html" class="btn btn-danger btn-lg"><span class="fa fa-angle-double-right"></span> Saya ingin daftar</a>
                        @else
                            <a href="https://akademik.ticmi.co.id/index/daftar/{{ $batch->cabang }}/{{ $batch->id }}/Pendaftaran {{ $batch->nama }}.html" class="btn btn-danger btn-lg"><span class="fa fa-angle-double-right"></span> Saya ingin daftar</a>
                        @endif
                        </p>
                    </div>
                </div><!-- Column -->
            </div><!-- row -->

            <div class="row course-full-detail content-box  bg-white shadow">
                <h5>Program Kami lainnya : </h5>
                <div class="owl-carousel show-nav-hover dots-dark nav-square dots-square navigation-color" data-animatein="zoomIn" data-animateout="slideOutDown" data-items="1" data-margin="30" data-loop="true" data-merge="true" data-nav="true" data-dots="false" data-stagepadding="" data-mobile="1" data-tablet="3" data-desktopsmall="3"  data-desktop="4" data-autoplay="true" data-delay="3000" data-navigation="true">

                    @if($otherbatch && $otherbatch->count() > 0)
                        @foreach($otherbatch as $value)
                            <div class="item">
                                <!-- Related Wrapper -->
                                <div class="related-wrap">
                                    <!-- Related Image Wrapper -->
                                    <div class="img-wrap">
                                        @if($value->program->nm_program == "WPPE")
                                            @if($value->kelas->type->nama == "Regular")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wppe-reguler-3.jpg') }}" width="600" height="220">
                                            @elseif($value->kelas->type->nama == "Waiver")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wppe-waiver-3.jpg') }}" width="600" height="220">
                                            @else
                                            @endif
                                        @elseif($value->program->nm_program == "WPPE Pemasaran")
                                            @if($value->kelas->type->nama == "Regular")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wppe-pemasaran-reguler.jpg') }}" width="600" height="220">
                                            @elseif($value->kelas->type->nama == "Waiver")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wppe-pemasaran-waiver.jpg') }}" width="600" height="220">
                                            @else
                                            @endif
                                        @elseif($value->program->nm_program == "WPPE Pemasaran Terbatas")
                                            <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wppe-pemasaran-terbatas.jpg') }}" width="600" height="220">
                                        @elseif($value->program->nm_program == "WMI")
                                            @if($value->kelas->type->nama == "Regular")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wmi-reguler-3.jpg') }}" width="600" height="220">
                                            @elseif($value->kelas->type->nama == "Waiver")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wmi-waiver-3.jpg') }}" width="600" height="220">
                                            @else
                                            @endif
                                        @elseif($value->program->nm_program == "WPEE")
                                            @if($value->kelas->type->nama == "Regular")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wpee-reguler.jpg') }}" width="600" height="220">
                                            @elseif($value->kelas->type->nama == "Waiver")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wpee-waiver.jpg') }}" width="600" height="220">
                                            @else
                                            @endif
                                        @elseif($value->program->nm_program == "ASPM")
                                            @if($value->kelas->type->nama == "Regular")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/aspm-reguler-3.jpg') }}" width="600" height="220">
                                            @elseif($value->kelas->type->nama == "Waiver")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/aspm-akselerasi-3.jpg') }}" width="600" height="220">
                                            @else
                                            @endif
                                        @elseif($value->program->nm_program == "Placement Test ASPM")
                                            <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/aspm-akselerasi-3.jpg') }}" width="600" height="220">
                                        @else
                                            <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wppe-waiver-3.jpg') }}" width="600" height="220">
                                        @endif
                                    </div>
                                    <!-- Related Content Wrapper -->
                                    <div class="related-content">
                                        <a href="{{ url('daftar/'.$value->id.'/'.$value->nama) }}" title="{{ $value->nama }}">+</a><span>{{ $value->nama }}</span>
                                        <h5 class="title">{{ $value->nama }}</h5>
                                    </div><!-- Related Content Wrapper -->
                                </div><!-- Related Wrapper -->
                            </div><!-- Item -->
                        @endforeach
                    @else
                    @endif

                </div><!-- Related Post -->

            </div>
        </div><!-- Container -->
    </div><!-- Page Default -->
@endsection