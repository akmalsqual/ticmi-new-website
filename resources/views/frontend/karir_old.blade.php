@extends('layout.home')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Karir</h3>
                        <h4 class="sub-title">TICMI Career Development Center</h4>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <div class="row course-single content-box bg-white shadow">
                <div class="col-sm-12">
                    <h5 class="text-center text-info">Selamat bagi Anda yang telah lulus ujian sertifikasi TICMI.</h5>
                    <p class="text-center">
                        Untuk meningkatkan pelayanan TICMI kepada industri Pasar Modal, kami telah meluncurkan pelayanan baru yaitu <strong>Career Development Center (CDC)</strong>.
                        CDC membantu alumni TICMI untuk mendapatkan lisensi OJK dan bekerja di Perusahaan Efek atau Manajemen Investasi.
                    </p>
                </div>
            </div><!-- Row -->
            <br>
            <div class="row course-single pad-tb-40 content-box bg-white shadow">
                <div class="col-sm-12">
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <h4 class="text-center"><a href="http://www.henanputihrai.com" target="_blank">PT. Henan Putihrai</a> </h4>
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#henan1" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Marketing / Sales
                                    </a>
                                </h4>
                            </div>
                            <div id="henan1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <strong>Persyaratan :</strong>
                                    <ol>
                                        <li>Usia maksimal 30 tahun</li>
                                        <li>Berpenampilan menarik, pekerja keras dan disiplin</li>
                                        <li>Pendidikan minimal S1 dari segala jurusan diutamakan jurusan <em>Bisnis dan Ekonomi</em></li>
                                        <li>Dapat bekerja dengan target</li>
                                        <li>Mampu bekerja dalam tim</li>
                                        <li>Memiliki kemampuan komunikasi dan interpersonal yang baik</li>
                                        <li>Diutamakan memiliki izin WPPE (Wakil Perantara Pedagang Efek)</li>
                                    </ol>
                                    <strong>Tugas dan Tanggung Jawab :</strong>
                                    <ol>
                                        <li>Mencari nasabah baru</li>
                                        <li>Menjalin hubungan baik dengan nasabah</li>
                                        <li>Melakukan edukasi dan membantu mengenali kebutuhan investasi nasabah</li>
                                        <li>Memberikan solusi investasi kepada nasabah</li>
                                        <li>Membuat laporan hasil penjualan secara berkala</li>
                                        <li>Mencapai target yang telah ditetapkan</li>
                                    </ol>

                                    <p class="text-center">Silahkan kirim lamaran ke : <br> <a href="mailto:recruitment@henanputihrai.com">recruitment@henanputihrai.com</a> </p>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->

                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#henan2" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Research Analyst
                                    </a>
                                </h4>
                            </div>
                            <div id="henan2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <strong>Persyaratan :</strong>
                                    <ol>
                                        <li>Usia maksimal 30 tahun</li>
                                        <li>Pendidikan minimal S1 dari segala jurusan diutamakan jurusan <em>Bisnis dan Ekonomi</em></li>
                                        <li>Diutamakan berpengalaman minimal 1 tahun sebagai <em>Research Analyst</em> di bidang sekuritas atau asset management </li>
                                        <li>Memiliki pengetahuan dan keterampilan analitis ekuitas baik analisa fundamental maupun analisa teknikal</li>
                                        <li>Mampu bekerja dalam tim</li>
                                        <li>Memiliki kemampuan komunikasi dan interpersonal yang baik</li>
                                    </ol>
                                    <strong>Tugas dan Tanggung Jawab :</strong>
                                    <ol>
                                        <li>Melakukan dan memberikan analisa baik secara harian, mingguan, bulanan atau disesuiakan dengan kebutuhan</li>
                                        <li>Memberikan rekomendasi dan opini mengenai peluang untuk transaksi jual beli saham setiap hari (<em>morning briefing</em>)</li>
                                        <li>Melakukan company visit, menghadiri public expose</li>
                                    </ol>
                                    <p class="text-center">Silahkan kirim lamaran ke : <br> <a href="mailto:recruitment@henanputihrai.com">recruitment@henanputihrai.com</a> </p>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                    </div>
                    <br>
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <h4 class="text-center"><a href="https://www.indopremier.com" target="_blank">PT. Indo Premier Securities</a> </h4>
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#ipot1" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Wealth Management Account Executive
                                    </a>
                                </h4>
                            </div>
                            <div id="ipot1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <strong>Kualifikasi :</strong>
                                    <ol>
                                        <li>S1 jurusan Marketing atau Keuangan</li>
                                        <li>Lebih diutamakan yang sudah lulus WPPE</li>
                                        <li>Wanita usia max 28 tahun</li>
                                        <li>Bermotivasi kerja <em>client oriented</em></li>
                                        <li>Memiliki <em>communication and presentation skills</em> yang baik</li>
                                        <li>Bisa berbahasa Inggris lisan maupun tulisan dengan baik</li>
                                        <li>Bisa bekerja sama dalam team work</li>
                                        <li>Bisa menggunakan computer (Microsoft Word, Microsoft Excel dan Power Point)</li>
                                    </ol>
                                    Silahkan surat lamaran dan resume kandidat dapat dikirimkan via email ke <a href="mailto:shirly@ipc.co.id">shirly@ipc.co.id</a> dan <a href="mailto:michelle.angela@ipc.co.id">michelle.angela@ipc.co.id</a>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#ipot2" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Marketing officer
                                    </a>
                                </h4>
                            </div>
                            <div id="ipot2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p>
                                        Posisi untuk penempatan di : kantor galeri PLUIT, PURI dan BATAM
                                    </p>
                                    <strong>Kualifikasi :</strong>
                                    <ol>
                                        <li>Semua lulusan S1/D3 semua jurusan</li>
                                        <li>Wanita dengan max umur 25 tahun per 2017</li>
                                        <li>Bermotivasi kerja <em>client oriented</em></li>
                                        <li>Memiliki <em>communication, negotiation and presentation skills</em> yang baik</li>
                                        <li>Bisa berbahasa Inggris lisan maupun tulisan dengan baik</li>
                                        <li>Bisa bekerja sama dalam team work</li>
                                        <li>Bisa menggunakan computer (Microsoft Word, Microsoft Excel dan Power Point)</li>
                                    </ol>
                                    Silahkan surat lamaran dan resume kandidat dapat dikirimkan via email ke <a href="mailto:agam.rauzul@ipc.co.id">agam.rauzul@ipc.co.id</a> dan <a href="mailto:michelle.angela@ipc.co.id">michelle.angela@ipc.co.id</a>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#ipot3" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Marketing and Sales
                                    </a>
                                </h4>
                            </div>
                            <div id="ipot3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="text-justify">
                                        The role requires the ability to maintain relationships with previous customers, new costumer and acquire new business leads using your networking,
                                        corporation, innovation and interpersonal skills. You’ll need to advice customers on our retail product placement according to their needs and
                                        combine with current market trend. You have to build networks and relationships to the company, to offer them the business opportunities in the
                                        capital markets and its products for the welfare of its employees. You’ll work independently and as part of a team to hit and exceed sales
                                        targets in a competitive, positive working environment.
                                    </p>
                                    <strong>Requirements :</strong>
                                    <ol>
                                        <li>Minimum Diploma or Bachelor Degree Preferable Majoring in economy or management</li>
                                        <li>Prioritized maximum 27-year-old woman</li>
                                        <li>Good looking, attractive and can work in a team or individually</li>
                                        <li>Minimum experience as a marketing or sales of at least 2 years of work, fresh graduates are welcome to apply.</li>
                                        <li>Have expertise in the field of communication, negotiation and presentation</li>
                                        <li>Have Strong Business Sense and Target Oriented</li>
                                        <li>Has extensive social network and is able to transform into business opportunities</li>
                                        <li>Fluent in both Bahasa Indonesia and English</li>
                                    </ol>
                                    for those interested please submitted his curriculum vitae to:  <a href="mailto:agam.rauzul@ipc.co.id">agam.rauzul@ipc.co.id</a>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->

                    </div>
                    <br>
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <h4 class="text-center"><a href="https://www.indopremier.com" target="_blank">PT. Danareksa</a> </h4>
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#danareksa2" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Sales
                                    </a>
                                </h4>
                            </div>
                            <div id="danareksa2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <br/>
                                    PT. Danareksa cabang Bandung membuka kesempatan berkarir bagi lulusan WPPE Pemasaran TICMI dengan kualifikasi sebagai berikut :
                                    <ol>
                                        <li>Minimal DIII</li>
                                        <li>Lulus sertifikasi WPPE Pemasaran</li>
                                        <li>Lulus seleksi</li>
                                    </ol>
                                    Silahkan surat lamaran dan resume kandidat dapat dikirimkan via email ke: <a href="mailto:maryadi@danareksa.com">maryadi@danareksa.com</a>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->

                    </div>
                    <br>
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <h4 class="text-center"><a href="https://www.indopremier.com" target="_blank">PT. Panin Sekuritas Tbk.</a> </h4>
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#panin1" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Equity Sales
                                    </a>
                                </h4>
                            </div>
                            <div id="panin1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    We are a reputable Securities Company which have licenses as brokerage and underwriter. We have AA-(idn) Rating by Fitch Ratings. Currently we are looking for talented people to fill in below position:
                                    <h5 class="text-center">Equity Sales</h5>
                                    <strong>Job Responsibility :</strong>
                                    <ol>
                                        <li>Maintaining recent customers on securities transaction and able to establish a new customer</li>
                                        <li>Receiving order from customers for securities transaction</li>
                                        <li>Updating information to customers for investment decision</li>
                                    </ol>
                                    <strong>Requirement :</strong>
                                    <ol>
                                        <li>Minimum Bachelor Degree from any studies, preferring in Accounting or Finance</li>
                                        <li>Passed an exam on Capital Market Standard Profession or hold a license of WPPE from Bapepam LK / OJK is an advantage</li>
                                        <li>Having a good analytical, communication skill, self-motivated and interest in Capital Market Industry</li>
                                        <li>Proficient in English both oral and written</li>
                                        <li>Proficient in Mic. Office application</li>
                                        <li>Achievement and target oriented</li>
                                    </ol>
                                    <p>Hired Candidates will be given a competitive compensation and other benefits (THR, Health Insurance, etc) and permanent employee status.</p>

                                    <p><strong>For qualified candidate, please apply with details of your CV, recent photograph (coloured) and other supporting documents to :</strong></p>

                                    <h5 class="text-center">HR/ Gen.Affairs Division - PT. Panin Sekuritas Tbk.</h5>
                                    <h5 class="text-center">Email – <a href="mailto:hrd@pans.co.id">hrd@pans.co.id</a> </h5>

                                    <p class="text-center"><small>Only short-listed candidate will be notified for a test.</small></p>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->

                    </div>
                    <br>
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <h4 class="text-center"><a href="https://www.indopremier.com" target="_blank">CIMB Securities.</a> </h4>
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#cimb1" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Head Client Services
                                    </a>
                                </h4>
                            </div>
                            <div id="cimb1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <strong>Responsibilities :</strong>
                                    <ol>
                                        <li>Direct, manage and be accountable for the customer service function for CIMB Singapore’s clients</li>
                                        <li>Review and where necessary, make recommendations regarding all customer interfaces and delivery channels for retail sales force</li>
                                        <li>Manage and control the account opening process in compliance with regulatory requirements for all account types (retail and institutions)</li>
                                        <li>Responsible for ongoing revisions of relevant terms and conditions governing clients’ account relationship with CIMB</li>
                                        <li>Spearhead re-engineering of workflow processes with an aim to improve service delivery</li>
                                        <li>Oversee the retail support team to ensure relevant support for TRs and FAs</li>
                                        <li>Handle and manage all complaints and disputes pertaining to retail clients</li>
                                        <li>Responsible for the efficient delivery of Call Centre services</li>
                                        <li>Co-ordinate and support product managers on product launches</li>
                                    </ol>
                                    <strong>Requirements :</strong>
                                    <ol>
                                        <li>Holding WPPE License</li>
                                        <li>Min 5 years experiences in the same field, advantage in the banking, finance or investment, sector, preferably stock broking</li>
                                        <li>Graduated from S1 in economics, banking, finance, marketing</li>
                                        <li>Fluency in English both spoken and written</li>
                                        <li>Good presentation, communication and influencing skills for effective stakeholder/staff engagement</li>
                                        <li>Self initiative & thinking of a need perspective on Customer Services.</li>
                                    </ol>
                                    <p>If you are confident to take up this challenging opportunity, please send your latest comprehensive CV to :</p>

                                    <h5 class="text-center">HR Department</h5>
                                    <h5 class="text-center"><a href="mailto:evy.hasanah@cimb.com">evy.hasanah@cimb.com</a> </h5>

                                    <p class="text-center"><small>Only short-listed candidates will be notified.</small></p>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->

                    </div>
                    <br>
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <h4 class="text-center"><a href="https://www.indopremier.com" target="_blank">Mirae Asset Sekuritas</a> </h4>
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#mirae1" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Risk Management
                                    </a>
                                </h4>
                            </div>
                            <div id="mirae1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="text-center">
                                        Job Opening at PT Mirae Asset Sekuritas Indonesia <br>
                                        January 2017
                                    </p>
                                    <strong>Pre-requisites :</strong>
                                    <ol>
                                        <li>Male max 30 years old</li>
                                        <li>Having WPPE license from OJK is a must</li>
                                        <li>Knowledge of companies listed on the Indonesia stock exchanges and familiarity with development in the stock market will be an added advantage</li>
                                        <li>Good degree in finance, accounting , economics or related subjects from a reputable university</li>
                                        <li>Good communication skills, have high motivation, dedication and integrity</li>
                                        <li>Fluent in spoken and written English</li>
                                    </ol>
                                    <p class="text-center">Please send CV and Resume to :</p>
                                    <h5 class="text-center"><a href="mailto:richa@miraeasset.co.id">richa@miraeasset.co.id</a> </h5>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#mirae2" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Accounting & Finance Staff
                                    </a>
                                </h4>
                            </div>
                            <div id="mirae2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="text-center">
                                        Job Opening at PT Mirae Asset Sekuritas Indonesia <br>
                                        January 2017
                                    </p>
                                    <strong>Qualification/ Requirement :</strong>
                                    <ol>
                                        <li>Minimum Bachelor degree (S1) in economic/accounting from reputable university</li>
                                        <li>Good command in English, oral and written</li>
                                        <li>Understanding Accounting & Finance</li>
                                        <li>Familiar with Microsoft Excel and Word</li>
                                        <li>Detail oriented,discipline,responsible, able to work individually or as part of team</li>
                                        <li>Having 1-2 years experiences (fresh-graduated are welcome)</li>
                                    </ol>
                                    <p class="text-center">Please send CV and Resume to :</p>
                                    <h5 class="text-center"><a href="mailto:richa@miraeasset.co.id">richa@miraeasset.co.id</a> </h5>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#mirae3" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Mutual Fund Operation Staff
                                    </a>
                                </h4>
                            </div>
                            <div id="mirae3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="text-center">
                                        Job Opening at PT Mirae Asset Sekuritas Indonesia <br>
                                        January 2017
                                    </p>
                                    <strong>Requirement :</strong>
                                    <ol>
                                        <li>Bachelor / Master Degree in Finance (Economics)</li>
                                        <li>Minimum 3 years working experience in Mutual Fund related fields</li>
                                        <li>Possess active WAPERD and WMI license for at least 3 years</li>
                                        <li>Good knowledge on Fund Settlement process</li>
                                        <li>Experianced using the S-INVEST system</li>
                                        <li>Able to work individually and in teams</li>
                                    </ol>
                                    <strong>Job Responsibility  :</strong>
                                    <ol>
                                        <li>Create mutual fund transaction flow</li>
                                        <li>Create mutual fund transaction SOP</li>
                                        <li>Create transaction reports</li>
                                        <li>Monitor & analyze the transactions</li>
                                        <li>Analyze and suggest system improvements</li>
                                        <li>Make requirements for system improvements & communicate with IT</li>
                                    </ol>
                                    <p class="text-center">Please send CV and Resume to :</p>
                                    <h5 class="text-center"><a href="mailto:richa@miraeasset.co.id">richa@miraeasset.co.id</a> </h5>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#mirae4" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Online Marketing Staff
                                    </a>
                                </h4>
                            </div>
                            <div id="mirae4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="text-center">
                                        Job Opening at PT Mirae Asset Sekuritas Indonesia <br>
                                        January 2017
                                    </p>
                                    <strong>Requirement :</strong>
                                    <ol>
                                        <li>Bachelor / Master Degree in Finance Economics</li>
                                        <li>Overseas graduate preferred</li>
                                        <li>Good knowledge on Online Marketing channels</li>
                                        <li>Familiar with basic IT knowledge</li>
                                        <li>Able to work individually and in teams</li>
                                        <li>Good English proficiency</li>
                                        <li>Maximum age 35 years old</li>
                                        <li>Minimum of 1 year working experience as Marketing/IT</li>
                                    </ol>
                                    <strong>Job Responsibility  :</strong>
                                    <ol>
                                        <li>Prepare monthly and annual online marketing plan</li>
                                        <li>Monitoring and analyzing sales performance</li>
                                        <li>Communicate the marketing plans to the sales team</li>
                                        <li>Analyze and suggest system improvements</li>
                                        <li>Make requirements for system improvements and communicate with IT</li>
                                    </ol>
                                    <p class="text-center">Please send CV and Resume to :</p>
                                    <h5 class="text-center"><a href="mailto:richa@miraeasset.co.id">richa@miraeasset.co.id</a> </h5>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                    </div>
                    <br>
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <h4 class="text-center"><a href="http://www.victoriasecurities.co.id" target="_blank">PT Victoria Securities Indonesia</a> </h4>
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#victoria1" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Compliance
                                    </a>
                                </h4>
                            </div>
                            <div id="victoria1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="text-center">
                                        Sebagai salah satu perusahaan sekuritas yang berkembang pesat, PT Victoria Securities
                                        Indonesia ingin mengundang alumni TICMI yang memenuhi kualifikasi untuk posisi berikut:
                                        <h5 class="text-center">Compliance</h5>
                                    </p>
                                    <strong>Deskripsi Kerja Detail :</strong>
                                    <ol>
                                        <li>Melakukan analisa kegiatan Operasional perusahaan, untuk dibandingkan kesesuaiaannya dengan SOP, sekaligus Audit intemal</li>
                                        <li>Melakukan perbaikan SOP agar kegiatan opersional lebih efektifdan efisien</li>
                                        <li>Membantu direksi maupun divisi Iainnya untuk menyusun jawaban atas pertanyaan otoritas</li>
                                        <li>Mampu membaca dan memahami peraturan perusahaan efek maupun peraturan lainnya yang terkait, untuk kemudian disosialisasikan ke divisi lainnya</li>
                                        <li>Membuat dan mengirimkan laporan-laporan kepatuhan yang diminta otoritas maupun dir-kom</li>
                                        <li>Sebagai back-up risk-management untuk membuat laporan harian dan pemberian approval trading</li>
                                    </ol>
                                    <strong>Kualifikasi Pekerja</strong>
                                    <ol>
                                        <li>Memiliki tata Bahasa yang baik</li>
                                        <li>Rapi dalam menyimpan file</li>
                                        <li>Memiliki analyical thinking dan cepat belajar</li>
                                        <li>Minimal moderate dalam menggunakan excel, word dan power point</li>
                                        <li>Mau berkembang dan terus belajar</li>
                                        <li>Dapat bekerja dalam tekanan (semua pekerjaan ada deadline)</li>
                                        <li>Team worker</li>
                                        <li>Pribadi yang menyenangkan</li>
                                        <li>Minimal lulusan S1 jurusan yang berhubungan dengan dunia Accounting</li>
                                        <li>Berpengalaman minimal 1 tahun di Perusahaan Efek ataupun perbankan</li>
                                        <li>Kisaran usia 2l - 25 tahun</li>
                                    </ol>
                                    <p class="text-left">Dapat mengirimkan Surat Lamaran Pekerjaan dan CV ke :</p>
                                    <h6 class="text-left">
                                        Divisi HR-D <br>
                                        PT Victoria Securities Indonesia <br>
                                        Victoria Suites, Senayan City Panin Tower Lt. 8 <br>
                                        Jl. Asia Afrika Lot 19, Jakarta 10270 <br>
                                        alau Email ke: <a href="mailto:hrd@victoriasecurities.co.id">hrd@victoriasecurities.co.id</a> dan <a href="mailto:secretary@victoriasecurities.co.id">secretary@victoriasecurities.co.id</a>  <br>

                                    </h6>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#victoria2" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Customer Service
                                    </a>
                                </h4>
                            </div>
                            <div id="victoria2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="text-center">
                                        Sebagai salah satu perusahaan sekuritas yang berkembang pesat, PT Victoria Securities
                                        Indonesia ingin mengundang alumni TICMI yang memenuhi kualifikasi untuk posisi berikut:
                                        <h5 class="text-center">Customer Service</h5>
                                    </p>
                                    <strong>Deskripsi Kerja Detail :</strong>
                                    <ol>
                                        <li>Melayani calon nasabah untuk melakukan pembukaan rekening efek, RDN dan lainnya</li>
                                        <li>Melayani request nasabah terkait perubahan ataupun penambahan data personal maupun portofolio</li>
                                        <li>Membuat laporan customer service</li>
                                        <li>Merapikan dan menata dokumen pembukaan rekening efek nasabah dan dokumen lainnya yang berkaitan dengan transaksi nasabah</li>
                                        <li>Melakukan verifikasi face to face dengan nasabah (bila diperlukan, termasuk berkunjung ke lokasi nasabah)</li>
                                        <li>Melakukan pengumuman kepada nasabah, mengirimkan laporan portofolio nasabah dan laporan-laporan yang berkaitan dengan customer service</li>
                                    </ol>
                                    <strong>Kualifikasi Pekerja</strong>
                                    <ol>
                                        <li>Berpenampilan menarik, serta mampu berbicara dihadapan umum</li>
                                        <li>Memiliki tata Bahasa yang baik</li>
                                        <li>Rapi dalam menyimpan file</li>
                                        <li>Minimal moderate dalam menggunakan excel, word dan power point</li>
                                        <li>Marketing minded (mampu menjual), cepat belajar</li>
                                        <li>Mau berkembang dan terus belajar</li>
                                        <li>Dapat bekerja dalam tekanan (semua pekerjaan ada deadline)</li>
                                        <li>Team worker</li>
                                        <li>Pribadi yang menyenangkan</li>
                                        <li>Minimal lulusan S1 </li>
                                        <li>Kisaran usia 2l - 25 tahun</li>
                                    </ol>
                                    <p class="text-left">Dapat mengirimkan Surat Lamaran Pekerjaan dan CV ke :</p>
                                    <h6 class="text-left">
                                        Divisi HR-D <br>
                                        PT Victoria Securities Indonesia <br>
                                        Victoria Suites, Senayan City Panin Tower Lt. 8 <br>
                                        Jl. Asia Afrika Lot 19, Jakarta 10270 <br>
                                        alau Email ke: <a href="mailto:hrd@victoriasecurities.co.id">hrd@victoriasecurities.co.id</a> dan <a href="mailto:secretary@victoriasecurities.co.id">secretary@victoriasecurities.co.id</a> <br>

                                    </h6>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#victoria3" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Finance
                                    </a>
                                </h4>
                            </div>
                            <div id="victoria3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="text-center">
                                        Sebagai salah satu perusahaan sekuritas yang berkembang pesat, PT Victoria Securities
                                        Indonesia ingin mengundang alumni TICMI yang memenuhi kualifikasi untuk posisi berikut:
                                        <h5 class="text-center">Finance</h5>
                                    </p>
                                    <strong>Deskripsi Kerja Detail :</strong>
                                    <ol>
                                        <li>Menyiapkan laporan arus kas setiap hari</li>
                                        <li>Memeriksa posisi saldo bank setiap hari</li>
                                        <li>Menyimpan, menyiapkan cek atau giro untuk pembayaran kepada nasabah dan pihak lain</li>
                                        <li>Memasukkan data pengeluaran (payment voucher) ke system setelah menerima trade confirmation</li>
                                        <li>Melakukan penagihan ke nasabah</li>
                                        <li>Melakukan korespondensi dengan bank untuk konfirmasi saldo bank, permintaan overdraft maupun pinjaman bank</li>
                                        <li>
                                            Menyimpan catatan tambahan dan dokumen pendukung lainnya, antara lain :
                                            <ol style="list-style-type: lower-alpha;">
                                                <li>Bukti pengeluaran cek</li>
                                                <li>Rekening bank</li>
                                                <li>Pembatalan cek ( jika ada)</li>
                                                <li>Rekonsiliasi rekening bank</li>
                                                <li>Pemberitahuan debet dan kredit rekening efek</li>
                                                <li>Saldo semua akun dalam buku besar (general ledger) dalam bentuk neraca saldo, sekurang-kurangnya setiap bulan</li>
                                                <li>Catatan harian yang merupakan bukti dari semua pendebetan dan pengkeditan kas untuk hari tersebut</li>
                                                <li>Rekonsiliasi harian antara buku besar (general ledger) dan buku pembantu efek (securities ledger)</li>
                                            </ol>
                                        </li>
                                    </ol>
                                    <strong>Kualifikasi Pekerja</strong>
                                    <ol>
                                        <li>Pendidikan minimal D3 dan pengalaman kerja 1 tahun dibidang keuangan.</li>
                                        <li>Memiliki pengetahuan tentang komputer (database, spread sheet dan work processor).</li>
                                        <li>Mampu bekerja sama sebagai team dan dibawah tekanan</li>
                                    </ol>
                                    <p class="text-left">Dapat mengirimkan Surat Lamaran Pekerjaan dan CV ke :</p>
                                    <h6 class="text-left">
                                        Divisi HR-D <br>
                                        PT Victoria Securities Indonesia <br>
                                        Victoria Suites, Senayan City Panin Tower Lt. 8 <br>
                                        Jl. Asia Afrika Lot 19, Jakarta 10270 <br>
                                        alau Email ke: <a href="mailto:hrd@victoriasecurities.co.id">hrd@victoriasecurities.co.id</a> dan <a href="mailto:secretary@victoriasecurities.co.id">secretary@victoriasecurities.co.id</a> <br>

                                    </h6>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#victoria4" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Marketing & Business Development Staff
                                    </a>
                                </h4>
                            </div>
                            <div id="victoria4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="text-center">
                                        Sebagai salah satu perusahaan sekuritas yang berkembang pesat, PT Victoria Securities
                                        Indonesia ingin mengundang alumni TICMI yang memenuhi kualifikasi untuk posisi berikut:
                                        <h5 class="text-center">Marketing & Business Development Staff</h5>
                                    </p>
                                    <strong>General Requirement :</strong>
                                    <ol>
                                        <li>Min Bachelor Degree in Economic fields from a reputable university with min GPA of 2.70 out of4.00 scale</li>
                                        <li>Age Max 35 years old</li>
                                        <li>Experience at least one year in the field of marketing on the capital market or two years at capital market</li>
                                        <li>Have outstanding communication & presentation skills and ability to work as a team and out going person</li>
                                    </ol>
                                    <strong>Special Requirement :</strong>
                                    <ol>
                                        <li>Having a broker-dealer license (WPPE) or marketing licence is an advantage</li>
                                    </ol>
                                    <p class="text-left">Competitive compensation will be offered to the successful candidates.
                                        Please submit your Comprehensive Resume with Recent Photograph and related
                                        Certification, to the following address :</p>
                                    <h6 class="text-left">
                                        Divisi HR-D <br>
                                        PT Victoria Securities Indonesia <br>
                                        Victoria Suites, Senayan City Panin Tower Lt. 8 <br>
                                        Jl. Asia Afrika Lot 19, Jakarta 10270 <br>
                                        alau Email ke: <a href="mailto:hrd@victoriasecurities.co.id">hrd@victoriasecurities.co.id</a> dan <a href="mailto:secretary@victoriasecurities.co.id">secretary@victoriasecurities.co.id</a> <br>

                                    </h6>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#victoria5" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Research Analyst
                                    </a>
                                </h4>
                            </div>
                            <div id="victoria5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="text-center">
                                        Sebagai salah satu perusahaan sekuritas yang berkembang pesat, PT Victoria Securities
                                        Indonesia ingin mengundang alumni TICMI yang memenuhi kualifikasi untuk posisi berikut:
                                        <h5 class="text-center">Research Analyst</h5>
                                    </p>
                                    <strong>Deskripsi Kerja Detail :</strong>
                                    <ol>
                                        <li>Menjawab pertanyaan seputar dunia pasar modal dan keuangan dengan melakukan metode penelitian</li>
                                        <li>Mencari data melalui internet maupun bloomberg, melakukan analisa data menggunakan excel</li>
                                        <li>Membuat kesimpulan / hasil analisa data yang diperoleh</li>
                                        <li>Melakukan menghadiri public expose, ataupun company visit (bila diperlukan), serta membuat laporan hasil kunjungan</li>
                                        <li>Melakukan analisa teknikal terkait grafik harga saham</li>
                                        <li>Membuat company report, industry report, dan Market outlook</li>
                                        <li>Membuat rangkuman perusahaan</li>
                                        <li>Menjelaskan hasil analisa kepada client maupun sales/dealer</li>
                                    </ol>
                                    <strong>Kualifikasi Pekerja :</strong>
                                    <ol>
                                        <li>Minimal moderate dalam menggunakan excel, word dan power point</li>
                                        <li>Fasih menggunakan computer beserta tools terkini</li>
                                        <li>Memiliki analyical thinking</li>
                                        <li>Mau berkembang dan terus belajar</li>
                                        <li>Dapat bekerja dalam tekanan (semua pekerjaan ada deadline)</li>
                                        <li>Creative thinker</li>
                                        <li>Team worker</li>
                                        <li>Pribadi yang menyenangkan</li>
                                        <li>Minimal lulusan Sl jurusan yang berhubungan dengan dunia keuangan dari universitas ternama</li>
                                        <li>Memiliki penampilan menarik dan mampu berbicara dihadapan umum</li>
                                        <li>Kisaran usia 2l - 25 tahun</li>
                                    </ol>
                                    <p class="text-left">Dapat mengirimkan Surat Lamaran Pekerjaan dan CV ke :</p>
                                    <h6 class="text-left">
                                        Divisi HR-D <br>
                                        PT Victoria Securities Indonesia <br>
                                        Victoria Suites, Senayan City Panin Tower Lt. 8 <br>
                                        Jl. Asia Afrika Lot 19, Jakarta 10270 <br>
                                        alau Email ke: <a href="mailto:hrd@victoriasecurities.co.id">hrd@victoriasecurities.co.id</a> dan <a href="mailto:secretary@victoriasecurities.co.id">secretary@victoriasecurities.co.id</a> <br>

                                    </h6>
                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                    </div>
                    <br>
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <h4 class="text-center"><a href="https://www.esi-on.com" target="_blank">PT. Equity Securities Indonesia</a> </h4>
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#equity1" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Accounting
                                    </a>
                                </h4>
                            </div>
                            <div id="equity1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="text-center">
                                        Membutuhkan segera
                                    </p>
                                    <strong>Kualifikasi :</strong>
                                    <ol>
                                        <li>Wanita/Pria Usia max 35 tahun</li>
                                        <li>Fresh graduate, diutamakan berpengalaman min 1 tahun</li>
                                        <li>Pendidikan minimal S1 Akuntansi</li>
                                        <li>Bekerja Full time (Senin - Jum'at)</li>
                                        <li>Inisiatif, cekatan dan mau bekerja keras</li>
                                    </ol>
                                    <p class="text-center">
                                        Kirimkan surat lamaran, CV berikut foto uk. 4 x 6 ke : <br>
                                        PT. Equity Securities Indonesia <br>
                                        Wisma Sudirman Lt.14 Jl. Jend Sudirman Kav.34 Jakarta 10220 <br>
                                        Atau melalui email ke <h5 class="text-center"><a href="mailto:hrd@esi-on.com">hrd@esi-on.com</a> </h5>
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#equity2" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Marketing
                                    </a>
                                </h4>
                            </div>
                            <div id="equity2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="text-center">
                                        Membutuhkan segera
                                    </p>
                                    <strong>Kualifikasi :</strong>
                                    <ol>
                                        <li>Wanita/Pria Usia max 35 tahun</li>
                                        <li>Pendidikan minimal D3 Management, Akuntansi, Ekonomi (diutamakan mempunyai izin Wakil Perantara Pedagang Efek)</li>
                                        <li>Berpenampilan menarik serta memiliki kemampuan komunikasi yang baik</li>
                                        <li>Bisa menggunakan komputer</li>
                                        <li>Mampu bekerja dibawah tekanan dan mau bekerja keras</li>
                                        <li>Fasilitas: Gaji dan Komisi</li>
                                    </ol>
                                    <p class="text-center">
                                        Kirimkan surat lamaran, CV berikut foto uk. 4 x 6 ke : <br>
                                        PT. Equity Securities Indonesia <br>
                                        Wisma Sudirman Lt.14 Jl. Jend Sudirman Kav.34 Jakarta 10220 <br>
                                        Atau melalui email ke <h5 class="text-center"><a href="mailto:hrd@esi-on.com">hrd@esi-on.com</a> </h5>
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                    </div>
                    <br>
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <h4 class="text-center"><a href="https://www.in-sekuritas.com" target="_blank">PT. Investindo Nusantara Sekuritas</a> </h4>
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#invest1" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Risk Management (RM)
                                    </a>
                                </h4>
                            </div>
                            <div id="invest1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="text-center">
                                        Job Opening at PT.Investindo Nusantara Sekuritas
                                    </p>
                                    <strong>Pre - Requisites  :</strong>
                                    <ol>
                                        <li>Pria, Maksimal 35 tahun</li>
                                        <li>Memiliki ijin WPPE dari OJK</li>
                                        <li>Pendidikan S1/S2 semua jurusan namun lebih disukai dari jurusan Accounting, Finance, Ekonomi dari Universitas yang terakreditasi baik/sangat baik</li>
                                        <li>Memiliki pengetahuan tentang Perusahaan Sekuritas dan memiliki pengetahuan tentang industri Pasar Modal</li>
                                        <li>Memiliki pengalaman dibidangnya minimal 2 tahun, lebih disukai dari industry Pasar Modal atau Jasa Keuangan</li>
                                        <li>Dapat melakukan identifikasi resiko investasi secara berkala dan merekomendasikan tindakan pencegahannya</li>
                                        <li>Dapat mengawasi pengukuran resikoinvestasi dan memfasilitasi pengukuran resiko di masing – masing unit kerja</li>
                                        <li>Dapat mengevaluasi batasan resiko dan batasan transaksi secara berkala serta melakukan evaluasi penyusunan profil resiko dan inventaris resiko</li>
                                        <li>Mengusai Ms Office dengan baik</li>
                                        <li>Memiliki rasa inisiatif yang tinggi, mampu mengutarakan pendapatnya dengan baik,kreatif,teliti,serta mampu bekerja secara individual maupun tim</li>
                                    </ol>
                                    <p class="text-center">
                                        Apabila berminat  dapat mengirimkan CV / Resume, transcript Nilai, Pas Photo berwarna dan dokumen pendukung lainnya ke : <br>
                                        evie@in-sekuritas.com <br>
                                        <h5 class="text-center">position code <strong>MUST BE</strong> stated on the e-mail subject.</h5>
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                    </div>
                    <br>
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <h4 class="text-center"><a href="https://ticmi.co.id" target="_blank">The Indonesia Capital Market Institute</a> </h4>
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#ticmi1" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        IT OFFICER
                                    </a>
                                </h4>
                            </div>
                            <div id="ticmi1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="text-center">
                                        Adalah anak perusahaan Bursa Efek Indonesia yang bergerak di bidang Pusat Referensi, Edukasi dan Sertifikasi Pasar Modal Indonesia, membuka kesempatan posisi sebagai :
                                        <br>
                                        <strong>IT Officer</strong>
                                    </p>
                                    <strong>SPESIFIKASI MINIMUM  :</strong>
                                    <ol>
                                        <li>Pria</li>
                                        <li>Memiliki minimal 1 tahun pengalaman sebagai programmer web (PHP, HTML, CSS atau JavaScript dan Java) adalah wajib</li>
                                        <li>Memiliki kemampuan yang baik tentang cara menginstal, memelihara, dan pemecahan masalah MySQL Server / MS.SQL akan menjadi keuntungan</li>
                                        <li>Memiliki kemampuan yang baik dalam Ms. Window Server atau Linux Server</li>
                                        <li>Memiliki pengetahuan tentang OOP</li>
                                        <li>Memiliki pengetahuan tentang kerangka kerja (<em>framework</em>) PHP (LARAVEL, Codeigniter)</li>
                                        <li>Memiliki analisa dan keterampilan pemecahan masalah dengan baik </li>
                                        <li>Mampu bekerja secara individu atau sebagai sebuah tim dan di bawah tekanan</li>
                                        <li>Memiliki kemampuan yang baik tentang cara menginstal, memelihara, dan jaringan pemecahan masalah (TCP / IP, LAN, WAN, VPN)</li>
                                        <li>Memiliki kemampuan pemecahan pada hardware (Printer, Scanner, Router) masalah yang baik dan Software (Office, Virus)</li>
                                        <li>Jujur, bertanggung jawab dan mau belajar</li>
                                        <li>D3 / S1 di bidang Ilmu Komputer / Teknologi Informasi</li>
                                    </ol>
                                    <strong>SPESIFIKASI PEKERJAAN</strong>
                                    <ol>
                                        <li>Mengembangkan solusi perangkat lunak sesuai dengan kebutuhan dan desain</li>
                                        <li>Memberikan dukungan sistem tingkat pertama (pemecahan masalah dan penyelesaian masalah)</li>
                                        <li>Melakukan pemeliharaan perangkat lunak aplikasi, pengembangan, pengujian & debugging aplikasi</li>
                                        <li>Instalasi , migrasi, tips dan semua kegiatan yang berhubungan dengan sistem commissioning</li>
                                        <li>Mengembangkan standar untuk pengguna hardware dan software</li>
                                    </ol>
                                    <p class="text-center">
                                        Jika anda tertarik dan memenuhi persyaratan pekerjaan diatas, silahkan mengirimkan CV melalui email ke : <br>
                                        <strong>hr@ticmi.co.id</strong> <br>
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                    </div>
                    <br>
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <h4 class="text-center"><a href="https://danakita.co.id" target="_blank">Danakita</a> </h4>
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#danakita1" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        FUND MANAGER
                                    </a>
                                </h4>
                            </div>
                            <div id="danakita1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <strong>Pesyaratan :</strong>
                                    <ol>
                                        <li>Pendidikan minimal lulusan S1</li>
                                        <li>Pria atau wanita, usia maksimal 30 tahun</li>
                                        <li>Telah memiliki lisensi Wakil Manajer Investasi dari OJK</li>
                                        <li>Mampu bekerja secara individu maupun dalam tim</li>
                                        <li>Penempatan di Jakarta</li>
                                    </ol>
                                    <p class="text-center">
                                        CV dapat dikirimkan melalui email ke : <br>
                                        <strong>info@danakita.co.id</strong> <br>
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                    </div>
                    <br>
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <h4 class="text-center"><a href="https://phillip.co.id" target="_blank">Phillip Sekuritas Indonesia</a> </h4>
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#phillip1" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Risk Management
                                    </a>
                                </h4>
                            </div>
                            <div id="phillip1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="">We are part of Phillip Capital Group Singapore & a member of Indonesia Stock Exchange, seeking young and highly motivated candidates for the following position :
                                        <span class="text-center">Risk Management</span>
                                    </p>
                                    <strong>Your roles :</strong>
                                    <ol>
                                        <li>Establish and monitor key risk indicators, as well as implement corrective action plans to mitigate risks</li>
                                        <li>Analyze transactions, internal reports and financial information for potensial fraud risks</li>
                                        <li>Responsible for manage risk management system</li>
                                        <li>Maintain reports of significan risk and recommendations</li>
                                        <li>Arrange and make sure the implementation on parameter trading limit</li>
                                        <li>Create policies, procedures and control assessments in response to identified risks</li>
                                    </ol>
                                    <strong>The requirements :</strong>
                                    <ol>
                                        <li>Male</li>
                                        <li>Age max. 35 years old</li>
                                        <li>Bachelor degree preferably Accounting/ Finance / Economy major  from reputable University, min GPA 3,00</li>
                                        <li>Having experience either in the capital market / financial service / banking min. 2 years</li>
                                        <li>Having WPPE license is a plus</li>
                                        <li>Computer literate</li>
                                        <li>Proactive and able to work as a team</li>
                                        <li>Proficiency in English is essential</li>
                                    </ol>
                                    <p class="text-center">
                                        If you meet the requirements above, please send your cover letter in English, along with your updated CV by email: <br>
                                        <strong>to : seisca@phillip.co.id</strong> <br>
                                        with the subject line: .<em>“Risk Management”</em>
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#phillip2" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Customer Service
                                    </a>
                                </h4>
                            </div>
                            <div id="phillip2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="">We are part of Phillip Capital Group Singapore & a member of Indonesia Stock Exchange, seeking young and highly motivated candidates for the following position :
                                        <span class="text-center">Customer Service</span>
                                    </p>
                                    <strong>Job Description :</strong>
                                    <ol>
                                        <li>Verification of identity of New Customer</li>
                                        <li>Replying all questions from Customers either from email/fax/phone call.</li>
                                        <li>Receiving all complaints from Customers and try to solve the problem.</li>
                                        <li>Keeping with confidentially the Data of Customers</li>
                                        <li>Ensure the customer that already understand about Online Trading System</li>
                                        <li>Input Data from Opening Account to Back Office System</li>
                                        <li>Running instructions either to transfer of shares or to close the account, as well as verification to </li>
                                        <li>Ensure those instructions forwarded to the concerned division.</li>
                                    </ol>
                                    <strong>Requirement :</strong>
                                    <ol>
                                        <li>Male / Female, age max 28 years old</li>
                                        <li>Bachelor degree in any disciplines</li>
                                        <li>Having experience either in the capital market / financial service / banking min. 1 years</li>
                                        <li>Fresh graduates or graduates with less than 1 years of working experience are welcomed</li>
                                        <li>Honest, responsible, discipline, hard worker and high dedicated</li>
                                        <li>Able to have good public conversation</li>
                                        <li>Computer literate </li>
                                        <li>Marketing minded and can persuade people</li>
                                        <li>Able to work in team work and under pressure</li>
                                    </ol>
                                    <p class="text-center">
                                        If you meet the requirements above, please send your cover letter in English, along with your updated CV by email: <br>
                                        <strong>to : seisca@phillip.co.id</strong> <br>
                                        with the subject line: .<em>“Customer Service”</em>
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#phillip3" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Sales for Reksadana
                                    </a>
                                </h4>
                            </div>
                            <div id="phillip3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="">We are part of Phillip Capital Group Singapore & a member of Indonesia Stock Exchange, seeking young and highly motivated candidates for the following position :
                                        <span class="text-center">Sales for Reksadana</span>
                                    </p>
                                    <strong>Job Description :</strong>
                                    <ol>
                                        <li>Looking for new customers</li>
                                        <li>Maintain good relationship with customers</li>
                                        <li>Give an education and helping what customers needed</li>
                                        <li>Give investment solution for customers</li>
                                        <li>Make sales report</li>
                                        <li>Must achieve the targets</li>
                                    </ol>
                                    <strong>Requirement :</strong>
                                    <ol>
                                        <li>Male / Female</li>
                                        <li>Diploma/ Bachelor degree any studies prefer Accounting / Finance / Business</li>
                                        <li>Must have min. WAPERD  and max WMI</li>
                                        <li>Having experience either in the capital market min. 2 years (prefer for reksadana)</li>
                                        <li>Honest, responsible, discipline, hard worker and high dedicated</li>
                                        <li>Able to have good communication skill and high self motivated</li>
                                        <li>Computer literate </li>
                                        <li>Marketing minded and can persuade people</li>
                                        <li>Able to work in team work and under pressure</li>
                                    </ol>
                                    <p class="text-center">
                                        If you meet the requirements above, please send your cover letter in English, along with your updated CV by email: <br>
                                        <strong>to : seisca@phillip.co.id</strong> <br>
                                        with the subject line: .<em>“Sales for Reksandana”</em>
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#phillip4" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Equity Research Analyst
                                    </a>
                                </h4>
                            </div>
                            <div id="phillip4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="">We are part of Phillip Capital Group Singapore & a member of Indonesia Stock Exchange, seeking young and highly motivated candidates for the following position :
                                        <span class="text-center">Equity Research Analyst</span>
                                    </p>
                                    <strong>Job Description :</strong>
                                    <ol>
                                        <li>Carry out fundamental analysis and equity research on stock sector / industry you are assigned to.</li>
                                        <li>Develop financial models for companies under coverage.</li>
                                        <li>Write indepth research reports for companies and sector under coverage, including Initiating Coverage Reports and timely Update Reports.</li>
                                        <li>Visit listed companies, attend analyst briefings and public exposes, and meet with </li>
                                        <li>Investor Relations and/or management of companies under coverage.</li>
                                    </ol>
                                    <strong>Requirement :</strong>
                                    <ol>
                                        <li>Female, Graduate from reputable university, preferably accounting / finance / economy major.</li>
                                        <li>Having overseas graduate is a plus.</li>
                                        <li>Having experience either in the capital market as an analyst or in auditing firms is preferable.</li>
                                        <li>Excellent analytical skill with strong knowledge on finance.</li>
                                        <li>Strong understanding of accounting, finance, and equity valuation is essential.</li>
                                        <li>Proactive and able to work as a team.</li>
                                        <li>Having good communication skill, presentable personality and ability to speak in public is essential.</li>
                                        <li>Proficiency in English, especially in written form is a must.</li>
                                    </ol>
                                    <p class="text-center">
                                        If you meet the requirements above, please send your cover letter in English, along with your updated CV by email: <br>
                                        <strong>to : seisca@phillip.co.id</strong> <br>
                                        with the subject line: .<em>“Equity Research Analyst”</em> <br>
                                        <strong>If you have written research reports in English before, please also send us a softcopy of your report as an additional consideration for your application. Only shortlisted and qualified candidates will be notified for an interview.</strong>
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#phillip5" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Database Administrator (DBA)
                                    </a>
                                </h4>
                            </div>
                            <div id="phillip5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="">We are part of Phillip Capital Group Singapore & a member of Indonesia Stock Exchange, seeking young and highly motivated candidates for the following position :
                                        <span class="text-center">Database Administrator (DBA)</span>
                                    </p>
                                    <strong>Job Description :</strong>
                                    <ol>
                                        <li>Expert in managing database servers, database design, store procedures, trigger and jobs in MS.SQL server</li>
                                        <li>Responsible for the performance, integrity and security of a database</li>
                                        <li>Work closely with system analyst, application & web developers</li>
                                    </ol>
                                    <strong>Requirement :</strong>
                                    <ol>
                                        <li>Male, Degree in Computer Science/Information Technology or equivalent.</li>
                                        <li>Experience Administator server database min 2-3 years</li>
                                        <li>Good database knowledge especially SQL Server, XML and language programmer VB</li>
                                        <li>Good written and verbal communication skills.</li>
                                        <li>Bilingual : Bahasa Indonesia  & English</li>
                                        <li>Proactive and able to work as a team.</li>
                                        <li>Able to work in team work and under pressure</li>
                                    </ol>
                                    <p class="text-center">
                                        If you meet the requirements above, please send your cover letter in English, along with your updated CV by email: <br>
                                        <strong>to : seisca@phillip.co.id</strong> <br>
                                        with the subject line: .<em>“DBA”</em>
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                    </div>
                    <br>
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <h4 class="text-center"><a href="https://phillip.co.id" target="_blank">Phillip Assets Management</a> </h4>
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#phillipasset1" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Risk Management
                                    </a>
                                </h4>
                            </div>
                            <div id="phillipasset1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <strong>Persyaratan :</strong>
                                    <ol>
                                        <li>Pendidikan minimal S1 jurusan Ekonomi dan memiliki pengalaman kerja di bidang investasi, minimal 2 (dua) tahun.</li>
                                        <li>Memiliki pengetahuan tentang produk investasi Pasar Modal dan Peraturan Pasar Modal.</li>
                                        <li>Memiliki izin orang perseorangan sebagai Wakil Manajer Investasi dari Bapepam dan LK, dan mempunyai pengalaman kerja di bidang Pasar Modal dan / atau Keuangan, minimal 3 (tiga) tahun.</li>
                                    </ol>
                                    <p class="text-center">
                                        CV dapat dikirimkan melalui email ke : <br>
                                        <strong>to : hrd-mi@phillip.co.id</strong> <br>
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#phillipasset2" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Fund Manager
                                    </a>
                                </h4>
                            </div>
                            <div id="phillipasset2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <strong>Requirement :</strong>
                                    <ol>
                                        <li>Minimum 3 year experience as a Fund Manager.</li>
                                        <li>Having a Bachelor Degree in related fields.</li>
                                        <li>Having a WMI license from Bapepam & LK.</li>
                                        <li>Good Personality</li>
                                    </ol>
                                    <p class="text-center">
                                        CV dapat dikirimkan melalui email ke : <br>
                                        <strong>to : hrd-mi@phillip.co.id</strong> <br>
                                        <strong><em>General Requirement : </em> Must be dicipline, honets, self - motivated, creative, willing to work, independently or as a team, player and computer literate.</strong>
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#phillipasset3" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Dealer
                                    </a>
                                </h4>
                            </div>
                            <div id="phillipasset3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <strong>Requirement :</strong>
                                    <ol>
                                        <li>Minimum 2 year experience in related fields.</li>
                                        <li>Having a Diploma III or Bachelor degree majoring in Economy.</li>
                                        <li>Having a WPPE license from Bapepam & LK.</li>
                                        <li>Good Personality</li>
                                    </ol>
                                    <p class="text-center">
                                        CV dapat dikirimkan melalui email ke : <br>
                                        <strong>to : hrd-mi@phillip.co.id</strong> <br>
                                        <strong><em>General Requirement : </em> Must be dicipline, honets, self - motivated, creative, willing to work, independently or as a team, player and computer literate.</strong>
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#phillipasset4" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Finance/Accounting Staff
                                    </a>
                                </h4>
                            </div>
                            <div id="phillipasset4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <strong>Requirement :</strong>
                                    <ol>
                                        <li>Having a Diploma III or Bachelor degree majoring in Finance or Accounting.</li>
                                        <li>Minimum 2 year experience in related fields.</li>
                                        <li>Having good understanding in banking issues and capital market regulation.</li>
                                    </ol>
                                    <p class="text-center">
                                        CV dapat dikirimkan melalui email ke : <br>
                                        <strong>to : hrd-mi@phillip.co.id</strong> <br>
                                        <strong><em>General Requirement : </em> Must be dicipline, honets, self - motivated, creative, willing to work, independently or as a team, player and computer literate.</strong>
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#phillipasset5" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Marketing Communication
                                    </a>
                                </h4>
                            </div>
                            <div id="phillipasset5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <strong>Requirement :</strong>
                                    <ol>
                                        <li>Minimum 2 year experience in marketing mutual fung products.</li>
                                        <li>Manage social media channels for mutual fund products.</li>
                                        <li>Having good networking with financial institution.</li>
                                        <li>Good Communication skill</li>
                                        <li>Good Personality</li>
                                    </ol>
                                    <p class="text-center">
                                        CV dapat dikirimkan melalui email ke : <br>
                                        <strong>to : hrd-mi@phillip.co.id</strong> <br>
                                        <strong><em>General Requirement : </em> Must be dicipline, honets, self - motivated, creative, willing to work, independently or as a team, player and computer literate.</strong>
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                    </div>
                    <br>
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <h4 class="text-center"><a href="https://www.rhb.com.my" target="_blank">PT. RHB Securities Indonesia</a> </h4>
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#rhb1" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Corporate Finance & Debt Capital Market
                                    </a>
                                </h4>
                            </div>
                            <div id="rhb1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <strong>Responsibilities :</strong>
                                    <ol>
                                        <li>Analyze and develop a deep understanding of the client’s business and understand clients objectives for the transaction</li>
                                        <li>Analyze and develop complex financial models. </li>
                                        <li>Analyze markets, industries and corporates and generate outputs.</li>
                                        <li>Prepare financing strategy reports based on quantitative and qualitative factors and market realities. </li>
                                        <li>Prepare information memoranda, marketing materials/presentations and term sheets for transactions</li>
                                        <li>Review and recheck to ensure zero error in transaction documentation</li>
                                        <li>Interact with regulators such as the Capital Market Authority and the Central Bank and other advisors on transactions</li>
                                        <li>Contribute to the team through business development and generation of ideas.</li>
                                        <li>Execute the transaction as planned</li>
                                    </ol>
                                    <strong>Requirements :</strong>
                                    <ol>
                                        <li>Degree in Finance, Banking, Accounting or equivalent professional qualifications</li>
                                        <li>Minimum 1-3 years of experience ideally either in the area of financial modelling, financial analysis and due diligence or placement, products and execution.</li>
                                        <li>Excellent report writing skills. </li>
                                        <li>Excellent oral and written communication skills and presentation skills</li>
                                        <li>Ability to work under tight deadlines in a competitive environment and produce high-quality output</li>
                                        <li>Initiative and ability to generate ideas and carry the same forward.</li>
                                        <li>Ability to work both independently and as a team</li>
                                        <li>Only candidates with the above requirements will be processed </li>
                                    </ol>
                                    <p>
                                        <strong>About the RHB Banking Group</strong> <br>
                                        The RHB Banking Group is the fourth largest fully integrated financial services group in Malaysia. The Group’s core businesses are streamlined into seven Strategic Business Groups (“SBGs”): Retail Banking, Business Banking, Group Transaction Banking, Corporate & Investment Banking, Islamic Banking, Global Financial Banking and Group Treasury. These businesses are offered through its main subsidiaries - RHB Bank Berhad, RHB Investment Bank Berhad, OSK Investment Bank Berhad, RHB Insurance Berhad and RHB Islamic Bank Berhad, while its asset management and unit trust businesses are undertaken by RHB Investment Management Berhad and OSK-UOB Investment Management Berhad. RHB’s Global Financial Banking Division includes commercial banking operations in Singapore, Thailand and Brunei. The Group also has a non-ringgit based offshore funding operation in Labuan as well as a representative office in Vietnam. It is RHB Banking Group’s aspiration to deliver superior customer experience and shareholder value, and to be recognised as a leading multinational financial services group.
                                    </p>
                                    <p class="text-center">
                                        If you think you have what we are looking for, please apply online or submit your application with a comprehensive resume to: <br>
                                        PT RHB Securities Indonesia <br>
                                        Human Resources Department <br>
                                        <strong>rhbosk.id.hr.dept@rhbgroup.com</strong> <br>
                                        www.rhb.com.my
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#rhb2" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Research Analyst (Fixed Income)
                                    </a>
                                </h4>
                            </div>
                            <div id="rhb2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <strong>Responsibilities :</strong>
                                    <ol>
                                        <li>To create financial modeling and to obtain understanding of Fixed Income industry that is assigned.</li>
                                        <li>To market his/her research report to Fixed Income clients. </li>
                                        <li>To assist client request that is related to Fixed Income market data research.</li>
                                        <li>Develop research infrastructure in a team which is expanding its regional and global coverage </li>
                                        <li>Monitor, evaluate and explain the fixed income markets in the assigned country and/or region, focusing on revenue-relevant, liquid, and thematically important markets</li>
                                        <li>Regularly write and project manage both strategic and trading-oriented investment ideas</li>
                                        <li>Coordinate research with in-house company analysts and external research companies</li>
                                        <li>Liaise and strengthen relationships with internal and external clients</li>
                                    </ol>
                                    <strong>Requirements :</strong>
                                    <ol>
                                        <li>Min. 3 years experiences in capital market / financial industry especially in Research Fixed Income</li>
                                        <li>Bachelor or Master Degree or Professional Qualification in the relevant discipline</li>
                                        <li>Strong leadership skills</li>
                                        <li>Ability to lead and motivate team and increase productivity</li>
                                        <li>Ability to manage & deliver the expectations of the internal client</li>
                                        <li>Good analytical and communication skills with pleasant personality.</li>
                                        <li>Ability to work both independently and as a team</li>
                                        <li>Only candidates with the above requirements will be processed</li>
                                    </ol>
                                    <p>
                                        <strong>About the RHB Banking Group</strong> <br>
                                        The RHB Banking Group is the fourth largest fully integrated financial services group in Malaysia. The Group’s core businesses are streamlined into seven Strategic Business Groups (“SBGs”): Retail Banking, Business Banking, Group Transaction Banking, Corporate & Investment Banking, Islamic Banking, Global Financial Banking and Group Treasury. These businesses are offered through its main subsidiaries - RHB Bank Berhad, RHB Investment Bank Berhad, OSK Investment Bank Berhad, RHB Insurance Berhad and RHB Islamic Bank Berhad, while its asset management and unit trust businesses are undertaken by RHB Investment Management Berhad and OSK-UOB Investment Management Berhad. RHB’s Global Financial Banking Division includes commercial banking operations in Singapore, Thailand and Brunei. The Group also has a non-ringgit based offshore funding operation in Labuan as well as a representative office in Vietnam. It is RHB Banking Group’s aspiration to deliver superior customer experience and shareholder value, and to be recognised as a leading multinational financial services group.
                                    </p>
                                    <p class="text-center">
                                        If you think you have what we are looking for, please apply online or submit your application with a comprehensive resume to: <br>
                                        PT RHB Securities Indonesia <br>
                                        Human Resources Department <br>
                                        <strong>rhbosk.id.hr.dept@rhbgroup.com</strong> <br>
                                        www.rhb.com.my
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#rhb3" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Sales Trainee
                                    </a>
                                </h4>
                            </div>
                            <div id="rhb3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <strong>Responsibilities :</strong>
                                    <ol>
                                        <li>Services marketing</li>
                                        <li>Provide market outlook and portfolio updates to clients</li>
                                        <li>Achieve number  of accounts</li>
                                    </ol>
                                    <strong>Requirements :</strong>
                                    <ol>
                                        <li>Degree in Finance, Economics, Accountancy, Business or related disciplines</li>
                                        <li>Fresh graduates or graduates with less than 1 years of working experience</li>
                                        <li>Candidate with high drive for sales to achieve target in a high competitive environment</li>
                                        <li>Candidates with other professional qualifications will have an added advantage</li>
                                        <li>Energetic and full of enthusiasim</li>
                                        <li>Good interpersonal and communication skills</li>
                                        <li>Self confident, sales driven and mature</li>
                                        <li>Candidates with WPPE license will have an added advantage</li>
                                    </ol>
                                    <p>
                                        <strong>About the RHB Banking Group</strong> <br>
                                        The RHB Banking Group is the fourth largest fully integrated financial services group in Malaysia. The Group’s core businesses are streamlined into seven Strategic Business Groups (“SBGs”): Retail Banking, Business Banking, Group Transaction Banking, Corporate & Investment Banking, Islamic Banking, Global Financial Banking and Group Treasury. These businesses are offered through its main subsidiaries - RHB Bank Berhad, RHB Investment Bank Berhad, OSK Investment Bank Berhad, RHB Insurance Berhad and RHB Islamic Bank Berhad, while its asset management and unit trust businesses are undertaken by RHB Investment Management Berhad and OSK-UOB Investment Management Berhad. RHB’s Global Financial Banking Division includes commercial banking operations in Singapore, Thailand and Brunei. The Group also has a non-ringgit based offshore funding operation in Labuan as well as a representative office in Vietnam. It is RHB Banking Group’s aspiration to deliver superior customer experience and shareholder value, and to be recognised as a leading multinational financial services group.
                                    </p>
                                    <p class="text-center">
                                        If you think you have what we are looking for, please apply online or submit your application with a comprehensive resume to: <br>
                                        PT RHB Securities Indonesia <br>
                                        Human Resources Department <br>
                                        <strong>rhbosk.id.hr.dept@rhbgroup.com</strong> <br>
                                        www.rhb.com.my
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#rhb4" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Equity Sales Representative
                                    </a>
                                </h4>
                            </div>
                            <div id="rhb4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <strong>Responsibilities :</strong>
                                    <ol>
                                        <li>Menjalin hubungan dengan nasabah</li>
                                        <li>Mencari solusi bagi masalah yang dialami nasabah</li>
                                        <li>Mengadakan event yang berkaitan kegiatan marketing</li>
                                        <li>Membuat laporan kinerja bulanan</li>
                                        <li>Menjelaskan tentang pasar modal bagi nasabah baru</li>
                                        <li>Memperluas jaringan marketing perusahaan</li>
                                        <li>Meningkatkan Awareness perusahaan</li>
                                    </ol>
                                    <strong>Requirements :</strong>
                                    <ol>
                                        <li>Sarjana Keuangan, Ekonomi, Akuntansi, Bisnis atau disiplin terkait</li>
                                        <li>Fresh graduate atau berpengalaman dengan minimum 1 tahun pengalaman kerja</li>
                                        <li>Mempunyai semangat yang tinggi untuk penjualan dan mencapai target dalam lingkungan yang kompetitif</li>
                                        <li>Kualifikasi professional lainnya akan menjadi nilai tambah</li>
                                        <li>Energik dan penuh antusiasme</li>
                                        <li>Memiliki keterampilan interpersonal dan komunikasi </li>
                                        <li>Percaya diri dan berpenampilan menarik</li>
                                        <li>Memiliki izin WPPE</li>
                                        <li>Bersedia ditempatlkan di <em>Padang, Solo, Bengkulu, Jambi, Ambon, Manokwari, Palangka Raya, Balikpapan, Manado, Denpasar, Semarang, Pangkal Pinang, Lampung dan Banda Aceh</em></li>
                                    </ol>
                                    <p>
                                        <strong>About the RHB Banking Group</strong> <br>
                                        The RHB Banking Group is the fourth largest fully integrated financial services group in Malaysia. The Group’s core businesses are streamlined into seven Strategic Business Groups (“SBGs”): Retail Banking, Business Banking, Group Transaction Banking, Corporate & Investment Banking, Islamic Banking, Global Financial Banking and Group Treasury. These businesses are offered through its main subsidiaries - RHB Bank Berhad, RHB Investment Bank Berhad, OSK Investment Bank Berhad, RHB Insurance Berhad and RHB Islamic Bank Berhad, while its asset management and unit trust businesses are undertaken by RHB Investment Management Berhad and OSK-UOB Investment Management Berhad. RHB’s Global Financial Banking Division includes commercial banking operations in Singapore, Thailand and Brunei. The Group also has a non-ringgit based offshore funding operation in Labuan as well as a representative office in Vietnam. It is RHB Banking Group’s aspiration to deliver superior customer experience and shareholder value, and to be recognised as a leading multinational financial services group.
                                    </p>
                                    <p class="text-center">
                                        If you think you have what we are looking for, please apply online or submit your application with a comprehensive resume to: <br>
                                        PT RHB Securities Indonesia <br>
                                        Human Resources Department <br>
                                        <strong>rhbosk.id.hr.dept@rhbgroup.com</strong> <br>
                                        www.rhb.com.my
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                    </div>
                    <br>
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <h4 class="text-center"><a href="http://www.valburysecurities.co.id" target="_blank">PT. Valbury Asia Securities</a> </h4>
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#valburi1" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Marketing/Sales
                                    </a>
                                </h4>
                            </div>
                            <div id="valburi1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <strong>Kualifikasi :</strong>
                                    <ol>
                                        <li>Dapat berkomunikasi dengan baik dan menyukai tantangan</li>
                                        <li>Usia maksimum 30 tahun </li>
                                        <li>Lulusan min D3 ( diutamakan yang sudah paham tentang Pasar Modal/memiliki lisensi WPPE) </li>
                                        <li>Fresh Graduate are welcome</li>
                                        <li>Memiliki kemampuan komunikasi dan interpersonal yang baik</li>
                                        <li>Energik, berinisiatif & berintegritas </li>
                                    </ol>
                                    <strong>Fasilitas :</strong>
                                    <ol>
                                        <li>Gaji Pokok + komisi</li>
                                        <li>Tunjangan Kesehatan</li>
                                    </ol>
                                    <p> Waktu Bekerja
                                        Waktu regular, Senin - Jumat
                                    </p>
                                    <p class="text-center">
                                        Email ke : <br>
                                        valburymakassar@yahoo.com atau <br>
                                        vas.makassar@id.valbury.com <br>
                                        FB Page : https://www.facebook.com/ValburyAsiaSekuritasMakassar/

                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                    </div>
                    <br>
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <h4 class="text-center"><a href="http://www.sucorinvest.com" target="_blank">PT Sucorinvest Central Gani</a> </h4>
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#sucorinvest1" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Equity Retail Marketing
                                    </a>
                                </h4>
                            </div>
                            <div id="sucorinvest1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <strong>Requirement :</strong>
                                    <ol>
                                        <li>Candidate must possess at leas Bachelor’s Degree in Marketing or equivalent</li>
                                        <li>Required language(s) : Bahasa Indonesia & English</li>
                                        <li>At least 2 Year(s) of working experience in the related field is required for this position </li>
                                        <li>Required Skill(s) : Wajib Memiliki License WPPE (SK dari OJK)</li>
                                        <li>Preferably Staff (non-management & non-supervisor) specialized in Sales – Retali/General or Equivalent</li>
                                    </ol>
                                    <strong>Job Description :</strong>
                                    <ol>
                                        <li>Mencapai target sales yang sebelumnya telah ditentukan oleh Head of Branch Network</li>
                                        <li>Meningkatkan pendapatan perusahaan dengan menambah jumlah nasabah / agen / transaksi</li>
                                        <li>Memberikan edukasi (pengetahuan/pemahaman ) kepada nasabah / calon nasabah mengenai pasar modal dan product Sucorinvest Central Gani</li>
                                        <li>Secara rutin bertemu dengan nasabah  / calon nasabah yang potensial, untuk melakukan pendekatan personal</li>
                                        <li>Memasarkan produk dan service Sucorinvest</li>
                                    </ol>
                                    <p class="text-center">
                                        CV dapat dikirimkan melalui email ke : ahadien.triawan@sucorinvest.com
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                    </div>
                    <br>
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <h4 class="text-center"><a href="http://www.kiwoom.co.id" target="_blank">PT. Kiwoom Sekuritas Indonesia</a> </h4>
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#kiwoom1" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Risk Management
                                    </a>
                                </h4>
                            </div>
                            <div id="kiwoom1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="text-center">Do you want to be number 1 in capital market? Let’s join us. Kiwoom Securities is No. 1 Securities Company in Korea. Together we can be No. 1. </p>
                                    <strong>General Qualification  :</strong>
                                    <ol>
                                        <li>Male/Female, Minimum Bachelor Degree (S1)  </li>
                                        <li>Having minimum 1 year of working experience in Capital Market Industry</li>
                                        <li>Familiar with OJK and IDX regulation</li>
                                        <li>Good command  in English both oral and written</li>
                                        <li>Having strong analytical thinking</li>
                                        <li>Have a good  communication and interpersonal skills</li>
                                        <li>Able to work as a team and individually</li>
                                        <li>Creative, Proactive and high initiative</li>
                                        <li>Able to work under pressure and tight deadline</li>
                                        <li>Adaptive and fast learner</li>
                                        <li>Computer literate (especially Microsoft office)</li>
                                    </ol>
                                    <p class="text-center">
                                        Only shortlisted candidates will be notified. If you meet the above requirements you can send your resume to hrd@kiwoom.co.id
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#kiwoom2" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Fund Manager
                                    </a>
                                </h4>
                            </div>
                            <div id="kiwoom2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <strong>General Qualification  :</strong>
                                    <ol>
                                        <li>Male/Female, maximum age 35 years old  </li>
                                        <li>Having WMI (Investment Management) license from OJK is a must</li>
                                        <li>Having 3 years experience in fund management</li>
                                        <li>Able to work in a team or independently</li>
                                        <li>Able to communicate in English both written and oral</li>
                                    </ol>
                                    <p class="text-center">
                                        Only shortlisted candidates will be notified. If you meet the above requirements you can send your resume to hrd@kiwoom.co.id
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                    </div>
                    <br>
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <h4 class="text-center"><a href="http://www.oso-securities.com" target="_blank">PT. OSO Securities </a> </h4>
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#oso1" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Marketing
                                    </a>
                                </h4>
                            </div>
                            <div id="oso1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="text-center">As member of the Indonesia Stock Exchange
                                        PT. OSO Securities Branch Aceh and Makasar Urgently needed
                                    </p>
                                    <strong>Qualification  :</strong>
                                    <ol>
                                        <li>Male / Female, Max 30 years old</li>
                                        <li>Min D3 ( Fresh Graduate are welcome )</li>
                                        <li>Good communicating skill</li>
                                        <li>Self motivated and energetic person</li>
                                        <li>WPPE license holders are preferred</li>
                                    </ol>
                                    <p class="text-center">
                                        Please send your cv to : hrd@oso-securities.com
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#oso2" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Legal Administration
                                    </a>
                                </h4>
                            </div>
                            <div id="oso2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="text-center">As member of the Indonesia Stock Exchange
                                        PT. OSO Securities Urgently needed Legal Administration
                                    </p>
                                    <strong>Qualification  :</strong>
                                    <ol>
                                        <li>Male , Max 30 years old</li>
                                        <li>Bachelor degree preferably Accounting/Law ( Fresh Graduate are welcome )</li>
                                        <li>Good communicating skill</li>
                                        <li>Self motivated and energetic person</li>
                                        <li>WPPE license holders are preferred</li>
                                    </ol>
                                    <p class="text-center">
                                        Please send your cv to : hrd@oso-securities.com
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                    </div>
                    <br>
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <h4 class="text-center"><a href="http://www.paramitra.com" target="_blank">PT. Paramitra Alfa Sekuritas </a> </h4>
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#paramitra1" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Head Equity
                                    </a>
                                </h4>
                            </div>
                            <div id="paramitra1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="text-center">
                                        Kesempatan Berkarir <br>
                                        Perusahaan Efek, Anggota Bursa Efek Indonesia yang sedang tumbuh pesat
                                        Membutuhkan professional berintegritas untuk posisi : <br>
                                        <strong>Head Equity</strong>
                                    </p>
                                    <strong>Persyaratan  :</strong>
                                    <ol>
                                        <li>Pria / Wanita</li>
                                        <li>Minimal S1 dari segala jurusan, diutamakan Bisnis dan ekonomi</li>
                                        <li>Memiliki Izin WPPE dari OJK </li>
                                        <li>Pengalaman min. 5 tahun sebagai sales equity.</li>
                                        <li>Memiliki kemampuan yang baik</li>
                                        <li>Mampu bekerja dalam tim</li>
                                        <li>Mampu bekerja dengan target</li>
                                    </ol>
                                    <strong>Job Description   :</strong>
                                    <ol>
                                        <li>Membangun relasi dengan nasabah dan memberikan nasihat atas kondisi pasar.</li>
                                        <li>Bertanggung jawab untuk menghasilkan pendapatan dan berkoordinasi dengan sales.</li>
                                        <li>Memberikan nasihat proses, prosedur dan strategi investasi efek.</li>
                                        <li>Mempertahankan dan meningkatkan produktivitas secara keseluruhan divisi equity secara tim maupun individual.</li>
                                    </ol>
                                    <p class="text-center">
                                        Kirim lamaran, CV, dan Photo terbaru ke : <br/>
                                        PT Paramitra Alfa Sekuritas <br>
                                        Cyber 2 Tower 20th Floor, Suite 2001 <br>
                                        Jl. HR Rasuna Said Blok X-5 No. 13 Jakarta 12950 <br>

                                        Atau email ke : <br>
                                        hrd@paramitra.com

                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                    </div>
                    <br>
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <h4 class="text-center"><a href="http://www.uobkayhian.com" target="_blank">PT. UOB Kay Hian Securities</a> </h4>
                        <!-- Panel -->
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#uob1" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Remisier & Equity Sales for Solo Cyber Branch
                                    </a>
                                </h4>
                            </div>
                            <div id="uob1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p class="text-center">
                                        We are multinational stock broker company seeking for Senior stock broker with criteria as follows :
                                    </p>
                                    <ol>
                                        <li>Male or Female</li>
                                        <li>WPPE or WPPE Pemasaran licensed</li>
                                        <li>Home based Solo and Yogyakarta (Central Java)</li>
                                        <li>Good interpersonal skill and aggressive</li>
                                        <li>Prior experience in Stock Broker would be an advantage</li>
                                    </ol>
                                    <strong>Benefit   :</strong>
                                    <ol>
                                        <li>Best commission sharing scheme and Salary</li>
                                        <li>Expand Bussiness Opportunity in Capital Market</li>
                                    </ol>
                                    <p class="text-center">
                                        Please send in your CV with details, within 30 Days of this advertisement to :
                                        <br> fentyyudyastuti@uobkayhian.com
                                    </p>

                                </div>
                            </div>
                        </div> <!-- Panel Default -->
                    </div>
                </div>
            </div>
        </div><!-- Container -->
    </div> <!-- Page Default -->
@endsection