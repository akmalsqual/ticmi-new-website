@extends('layout.home')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">Karir</h3>
                        <h4 class="sub-title">TICMI Career Development Center</h4>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <div class="row course-single content-box bg-white shadow">
                <div class="col-sm-12">
                    <h5 class="text-center text-info">Selamat bagi Anda yang telah lulus ujian sertifikasi TICMI.</h5>
                    <p class="text-center">
                        Untuk meningkatkan pelayanan TICMI kepada industri Pasar Modal, kami telah meluncurkan pelayanan baru yaitu <strong>Career Development Center (CDC)</strong>.
                        CDC membantu alumni TICMI untuk mendapatkan lisensi OJK dan bekerja di Perusahaan Efek atau Manajemen Investasi.
                    </p>
                </div>
            </div><!-- Row -->
            <br>
            <div class="row course-single pad-tb-40 content-box bg-white shadow">
                <div class="col-sm-12">
                    @if($karir && $karir->count() > 0)
                        @foreach($karir as $item)
                            <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                <h4 class="text-center"><a href="#" target="_blank">{{ $item->name }}</a> </h4>
                                @php($no = 1)
                                @foreach($item->karir as $lowongan)
                                    @if($lowongan->valid_until >= date("Y-m-d"))
                                    <!-- Panel -->
                                    <div class="panel panel-default active">
                                        <div class="panel-heading" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{{ strtolower(str_replace([' ','.',','],'',$item->name)).$no }}" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                                    {{ $lowongan->posisi }}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="{{ strtolower(str_replace([' ','.',','],'',$item->name)).$no }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                            <div class="panel-body">
                                                {!! $lowongan->keterangan !!}
                                            </div>
                                        </div>
                                    </div> <!-- Panel Default -->
                                    @endif
                                    @php($no++)
                                @endforeach
                            </div>
                            <br/>
                        @endforeach
                    @endif
                </div>
            </div>
        </div><!-- Container -->
    </div> <!-- Page Default -->
@endsection