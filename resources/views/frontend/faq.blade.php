@extends("layout.home")
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('./assets/images/banner/profile.jpg') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">FAQ</h3>
                        <h6 class="sub-title">Frequently Asked Question</h6>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default  typo-dark">
        <!-- Container -->
        <div class="container">
            <div class="row">

                <!-- Page Content -->
                <div class="col-md-12">
                    <!-- Course Wrapper -->
                    <div class="row course-single">
                        <!-- Course Banner Image -->
                        <div class="col-sm-12">
                            <div class="owl-crousel">
                                <!--                                <img alt="Course" class="img-responsive" src="images/course/course-single-01.jpg" width="1920" height="966">-->
                            </div>
                        </div><!-- Column -->

                    </div><!-- Course Wrapper -->

                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <div  id="cara-daftar-program-sertifikasi" class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#pendaftaranSertifikasi" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Pendaftaran Program Sertifikasi
                                    </a>
                                </h4>
                            </div>
                            <div id="pendaftaranSertifikasi" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    {{--<ol>--}}
                                        {{--<li>Calon peserta melakukan pendaftaran peserta program sertifikasi secara <em>online</em> di <a href="www.ticmi.co.id">www.ticmi.co.id</a> </li>--}}
                                        {{--<li>Setelah melakukan pendaftaran Calon peserta akan menerima Automatic Email Confirmation dari TICMI (periksa email Anda dan juga periksa folder Spam pada email Anda).</li>--}}
                                        {{--<li>Calon peserta melakukan pembayaran melalui <strong>Bank BCA Cab. Bursa Efek Indonesia No Rekening: 4581.34.9992</strong>  atas nama <strong>PT Indonesian Capital Market Electronic Library</strong> sesuai jadwal yang telah ditentukan</li>--}}
                                        {{--<li>Calon peserta melakukan konfirmasi pembayaran secara online di <a href="www.ticmi.co.id">www.ticmi.co.id</a> (pada menu konfirmasi pembayaran)</li>--}}
                                        {{--<li>Setelah melakukan konfirmasi Calon peserta akan menerima email konfirmasi dari TICMI (periksa inbox email Anda dan juga folder spam email Anda)</li>--}}
                                        {{--<li>--}}
                                            {{--Calon peserta melakukan <em>Registrasi Administrasi</em> di Kantor TICMI dengan membawa persyaratan, untuk <em>Registrasi Administrasi</em> detailnya seperti berikut :--}}
                                            {{--<ul type="disc" style="list-style: disc;margin-left: 30px;">--}}
                                                {{--<li>Peserta menyerahkan bukti transfer/setor asli, fotokopi KTP, fotokopi ijazah pendidikan terakhir dan pas foto (ukuran 3x4) sebanyak 2 lembar kepada bagian Administrasi Akademik TICMI</li>--}}
                                                {{--<li>Peserta akan menerima kuitansi pembayaran 2 (dua) rangkap dari TICMI untuk kemudian ditukarkan dengan bahan dan modul pengajaran</li>--}}
                                                {{--<li>Peserta hadir untuk mengikuti program pendidikan dan pelatihan sesuai jadwal pelatihan yang telah ditentukan</li>--}}
                                            {{--</ul>--}}
                                        {{--</li>--}}

                                    {{--</ol>--}}
                                    <p class="text-center">

                                        <strong class="text-center">Gambar Alur Proses Pendaftaran Sertifikasi TICMI</strong>
                                        <br>

                                        <img src="{{ asset('assets/images/ticmi/pendaftaran-sertifikasi-ticmi.png') }}" alt="Proses Pendaftaran Sertifikasi TICMI">
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default" id="penundaan-keikutsertaan">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#penundaanIkutserta" aria-expanded="false" aria-controls="collapseTwo">
                                        Penundaan Keikutsertaan Program Sertifikasi
                                    </a>
                                </h4>
                            </div>
                            <div id="penundaanIkutserta" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <ol>
                                        <li>Penundaan dapat dilakukan apabila peserta menderita sakit dan diharuskan untuk  menjalani perawatan atau mendapat tugas kantor dan/atau mutasi ke luar kota/negeri dalam jangka waktu yang cukup lama.</li>
                                        <li>Penundaan berlaku maksimal selama <strong>1 (satu) tahun</strong>, lebih dari itu dianggap hangus</li>
                                        <li>Peserta akan dikenakan biaya keikut-sertaan baru yang berlaku saat kembali mendaftarkan diri</li>
                                    </ol>
                                    <p>Peserta wajib membuat surat permohonan yang melampirkan surat penugasan/ surat keterangan sakit dari institusi terkait yang berwenang dan mengisi formulir permohonan penundaan keikut-sertaan yang sudah disediakan</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default" id="pengunduran-diri">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#pengunduranDiri" aria-expanded="false" aria-controls="collapseThree">
                                        Pengunduran Diri dan Pengembalian Biaya Keikut-sertaan
                                    </a>
                                </h4>
                            </div>
                            <div id="pengunduranDiri" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                                <div class="panel-body">
                                    <p>Peserta yang mengundurkan diri setelah melakukan pembayaran biaya keikut-sertaan dapat menerima pengembalian dengan ketentuan sebagai berikut :</p>
                                    <ol>
                                        <li>Jika pengunduran diri disampaikan sampai dengan 30 (tiga puluh) hari kalender sebelum pelatihan dimulai, akan dikenakan biaya administrasi sebesar 10% dari biaya pelatihan</li>
                                        <li>Jika pengunduran diri disampaikan antara 30 (tiga puluh) hari sampai 7 (tujuh) hari kalender sebelum pelatihan dimulai, akan dikenakan biaya administrasi sebesar 25% dari biaya pelatihan</li>
                                        <li>Jika pengunduran diri disampaikan kurang 7 (tujuh) hari kalender sebelum pelatihan dimulai, akan dikenakan biaya administrasi sebesar 50% dari biaya pelatihan</li>
                                        <li>Jika pengunduran diri disampaikan setelah program dimulai, tidak ada pengembalian biaya pelatihan, dan tidak bisa digantikan dengan orang lain</li>
                                        <li>Jika pengunduran diri disampaikan karena peserta menderita sakit dan diharuskan untuk menjalani perawatan atau mendapat tugas kantor dan/atau mutasi ke luar kota/ LN dalam jangka waktu yang cukup lama, maka biaya akan dikembalikan secara proporsional dengan terlebih dahulu membuat surat permohonan yang melampirkan surat penugasan/ surat keterangan sakit dari institusi terkait yang berwenang.</li>
                                        <li>Jika pengunduran diri dikarenakan pembatalan kelas yang dilakukan oleh TICMI maka biaya pendaftaran akan dikembalikan 100% (belum termasuk dikenakan biaya administrasi bank sebesar Rp10.000,-)</li>
                                        <li>Peserta wajib mengisi formulir permohonan pengembalian biaya training yang sudah disediakan dan pengembalian hanya dapat dilakukan melalui transfer antar bank dengan biaya pengiriman ditanggung pemohon, dengan lama waktu pengembalian selambat-lambatnya 14 hari kerja.</li>
                                        <li>Peserta yang tidak ingin mengundurkan diri dapat mengajukan permohonan penundaan keikut-sertaan</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default" id="penundaan-training">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#penundaanTraining" aria-expanded="false" aria-controls="collapseThree">
                                        Penundaan Periode Training Sertifikasi
                                    </a>
                                </h4>
                            </div>
                            <div id="penundaanTraining" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                                <div class="panel-body">
                                    <p>Peserta yang sudah melakukan pembayar biaya pelatihan hanya dapat menunda keikut-sertaannya dengan ketentuan sebagai berikut:</p>
                                    <ol>
                                        <li>Peserta diperbolehkan menunda training hanya jika periode yang dipilih belum dimulai.</li>
                                        <li>Penundaan training hanya dapat dilaksanakan untuk <strong>1 (satu)</strong> periode berikutnya.</li>
                                        <li>Peserta yang ingin melakukan penundaan waktu training wajib mengisi formulir yang sudah disediakan di bagian akademik.</li>
                                        <li>Penundaan training yang disampaikan kepada bagian akademik TICMI karena alasan khusus*  atau selambat-lambatnya <strong>7 (tujuh) hari</strong> kalender sebelum training dimulai, tidak dikenakan biaya tambahan.</li>
                                    </ol>
                                    <p>*/ Alasan khusus seperti: menderita sakit dan diharuskan untuk menjalani      perawatan atau  mendapat tugas kantor dan/atau mutasi ke luar kota/ LN      dalam jangka  waktu yang cukup lama</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default" id="syarat-keikutsertaan">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#syaratKeikutsertaan" aria-expanded="false" aria-controls="collapseThree">
                                        Syarat Keikut-sertaan Program Sertifikasi
                                    </a>
                                </h4>
                            </div>
                            <div id="syaratKeikutsertaan" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                                <div class="panel-body">
                                    <p>Syarat Keikutsertaan untuk WPPE, WMI dan ASPM:</p>
                                    <ol>
                                        <li>Berusia minimal 18 tahun</li>
                                        <li>Minimum telah lulus SMTA dan /atau yang sederajat Untuk WPPE</li>
                                        <li>Minimum Lulusan Diploma III dan /atau yang sederajat untuk WMI</li>
                                        <li>Minimum Lulusan S1 untuk ASPM</li>
                                        <li>Melakukan pendaftaran, pembayaran dan menyerahkan fotokopi KTP dan fotokopi ijazah terakhir, ke kantor admisi</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default" id="pengajuan-licensi-ojk">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#pengajuanLicensiOjk" aria-expanded="false" aria-controls="collapseThree">
                                        Cara Mengajukan Lisensi ke OJK
                                    </a>
                                </h4>
                            </div>
                            <div id="pengajuanLicensiOjk" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                                <div class="panel-body">
                                    <img src="{{ url('assets/images/ticmi/pengajuan-lisensi-ojk2.jpg') }}" alt="Cara pengajuan lisensi OJK">
                                    <img src="{{ url('assets/images/ticmi/Langkah-langkah pengajuan lisensi WPPE-1.jpg') }}" class="img-responsive" alt="Proses pengajuan Lisensi OJK">
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default" id="pengajuan-sertifikat-ticmi">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#pengajuanSertifikatTicmi" aria-expanded="false" aria-controls="collapseThree">
                                        Proses Pengajuan Sertifikat TICMI
                                    </a>
                                </h4>
                            </div>
                            <div id="pengajuanSertifikatTicmi" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                                <div class="panel-body">
                                    <img src="{{ url('assets/images/ticmi/pengajuan-sertifikasi-ticmi.JPG') }}" alt="Proses pengajuan sertifikat TICMI">
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default" id="syarat-keikutsertaan">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#bataswaktuujian" aria-expanded="false" aria-controls="collapseThree">
                                        Batas Waktu Ujian
                                    </a>
                                </h4>
                            </div>
                            <div id="bataswaktuujian" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                                <div class="panel-body">
                                    Batas waktu peserta melakukan ujian, baik ujian perdana maupun ujian ulang adalah 6 (enam) bulan setelah kelas berakhir. Apabila sudah melewati batas waktu 6 (enam) bulan dan peserta belum lulus maka peserta diharuskan untuk mengikuti pelatihan kembali.
                                </div>
                            </div>
                        </div>
                    </div>

                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Default -->
@endsection