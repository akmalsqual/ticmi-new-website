<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
    <title>E-mail blast TICMI</title>
</head>
<body style="height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ececec;border-top: 5px solid #e13138;padding:10px 0;">
<table class="m_-7119382030963698508gmail-container m_-7119382030963698508gmail-float-center" style="margin: 0pt auto; padding: 0pt; color: rgb(10, 10, 10); font-family: helvetica,arial,sans-serif; font-size: 16px; font-style: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-image: none; background-position: 0% 50%; background-repeat: repeat; background-color: rgb(254, 254, 254); border-collapse: collapse; float: none; vertical-align: top; width: 580px;" align="center">
    <tbody>
    <tr style="padding: 0pt; vertical-align: top;">
        <td style="margin: 0pt; padding: 0pt; font-family:  &quot;Trebuchet MS&quot;, &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Sans&quot;, Tahoma, sans-serif; line-height: 1.3; vertical-align: top; border-collapse: collapse;">
            <table class="m_-7119382030963698508gmail-spacer" style="padding: 0pt; border-collapse: collapse; vertical-align: top; width: 580px;">
                <tbody>
                <tr style="padding: 0pt; vertical-align: top;">
                    <td style="margin: 0pt; padding: 0pt; font-family: helvetica,arial,sans-serif; line-height: 16px; vertical-align: top; border-collapse: collapse;" height="16"><br></td>
                </tr>
                </tbody>
            </table>
            <table class="m_-7119382030963698508gmail-row" style="padding: 0pt; border-collapse: collapse; vertical-align: top; width: 580px;">
                <tbody>
                <tr style="padding: 0pt; vertical-align: top;">
                    <th class="m_-7119382030963698508gmail-small-12 m_-7119382030963698508gmail-large-12 m_-7119382030963698508gmail-columns m_-7119382030963698508gmail-first m_-7119382030963698508gmail-last" style="margin: 0pt auto; padding: 0pt 16px 16px; font-weight: 400; line-height: 1.3; text-align: left; width: 564px;">
                        <table style="padding: 0pt; border-collapse: collapse; vertical-align: top; width: 549.091px;">
                            <tbody>
                            <tr style="padding: 0pt; vertical-align: top;">
                                <th style="margin: 0pt; padding: 0pt; font-weight: 400; line-height: 1.3; text-align: left;">
                                    <center style="min-width: 532px; width: 549.091px;"><img alt="The Corporate Finance Qualification" src="https://www.dropbox.com/s/nm3lnp2g8uskzed/Corpfin%20Header.png?raw=1" class="m_-7119382030963698508gmail-float-center CToWUd a6T" tabindex="0" style="margin: 0pt auto; cursor: pointer; outline-color: invert; outline-style: none; outline-width: 0px; clear: both; display: block; float: none; height: 200px; max-width: 100%; width: 600px;" align="middle"></center>
                                </th>
                                <th class="m_-7119382030963698508expander" style="margin: 0pt; padding: 0pt; font-weight: 400; line-height: 1.3; text-align: left; width: 0pt;"><br></th>
                            </tr>
                            </tbody>
                        </table>
                    </th>
                </tr>
                </tbody>
            </table>
            <table class="m_-7119382030963698508gmail-row" style="padding: 0pt; border-collapse: collapse; vertical-align: top; width: 580px;">
                <tbody>
                <tr style="padding: 0pt; vertical-align: top;">
                    <th class="m_-7119382030963698508gmail-small-12 m_-7119382030963698508gmail-large-12 m_-7119382030963698508gmail-columns m_-7119382030963698508gmail-first" style="margin: 0pt auto; padding: 0pt 8px 16px 16px; font-weight: 400; line-height: 1.3; text-align: left; width: 564px;">
                        <table style="padding: 0pt; border-collapse: collapse; vertical-align: top; width: 557.273px;">
                            <tbody>
                            <tr style="padding: 0pt; vertical-align: top;">
                                <th style="margin: 0pt; padding: 0pt; font-weight: 400; line-height: 1.3; text-align: left;">
                                    <div style="text-align: justify;">
                                        Dear Distinguished Colleagues,<br><br>Fidelitas
                                        Consult (Fidelitas), along with The Indonesia Capital Market Institute
                                        (TICMI) cordially invites you to participate in our Corporate Finance
                                        qualification preparatory class. By joining our classes, you will have
                                        the chance to obtain insights from real corporate finance and capital
                                        market practitioners in the industry and become a member of a chartered
                                        global community of investment and securities professionals.<br><br>Along
                                        with this email we've attached a short description of our program to
                                        introduce to you the benefits and unique features of our preparatory
                                        class which includes :<br>
                                        <ul>
                                            <li style="margin-left: 15px;">Prominent guest speakers</li>
                                            <li style="margin-left: 15px;">Case studies</li>
                                            <li style="margin-left: 15px;">Business strategy sessions</li>
                                            <li style="margin-left: 15px;">Mockup exam and review sessions.</li>
                                        </ul>
                                        <br>
                                    </div>
                                    <table class="m_-7119382030963698508gmail-button" style="margin: 0pt 0pt 16px; padding: 0pt; border-collapse: collapse; vertical-align: top; width: 100%;">
                                        <tbody>
                                        <tr style="padding: 0pt; vertical-align: top;">
                                            <td style="margin: 0pt; padding: 0pt; font-family: helvetica,arial,sans-serif; line-height: 1.3; vertical-align: top; border-collapse: collapse;">
                                                <table style="padding: 0pt; border-collapse: collapse; vertical-align: top; width: 100%;">
                                                    <tbody>
                                                    <tr style="padding: 0pt; vertical-align: top;">
                                                        <td style="border: 2px solid #e13138; margin: 0pt; padding: 10pt; text-align: center; font-family: helvetica,arial,sans-serif; background-image: none; background-position: 0% 50%; background-repeat: repeat; background-color: #e13138; color: rgb(254, 254, 254); line-height: 1.3; vertical-align: top; border-collapse: collapse;"><a href="http://www.fidelitas-cfqual.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://www.fidelitas-cfqual.com/&amp;source=gmail&amp;ust=1478585649030000&amp;usg=AFQjCNGvPvCTAnL8O3hIsxGqTJO7714BHQ" style="border: 0pt solid rgb(33, 153, 232); margin: 0pt; padding: 8px 16px; color: rgb(254, 254, 254); font-weight: 700; line-height: 1.3; text-decoration: none;">Click here to learn more!</a></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </th>
                            </tr>
                            </tbody>
                        </table>
                    </th>
                </tr>
                </tbody>
            </table>
            <table class="m_-7119382030963698508gmail-row" style="padding: 0pt; border-collapse: collapse; vertical-align: top; width: 580px;">
                <tbody>
                <tr style="padding: 0pt; vertical-align: top;">
                    <th style="margin: 0pt; padding: 0pt; font-weight: 400; line-height: 1.3; text-align: left;">
                        <center style="min-width: 580px; width: 580px;"><img alt="Corporate Finance Savvy" src="https://www.dropbox.com/s/h646dfl2tdomsi8/slide2-1080.png?raw=1" class="m_-7119382030963698508gmail-float-center CToWUd a6T" tabindex="0" style="margin: 0pt auto; cursor: pointer; outline-color: invert; outline-style: none; outline-width: 0px; clear: both; display: block; float: none; height: 500px; max-width: 100%; width: 500px;" align="middle"></center>
                        <table class="m_-7119382030963698508gmail-spacer" style="padding: 0pt; border-collapse: collapse; vertical-align: top; width: 580px;">
                            <tbody>
                            <tr style="padding: 0pt; vertical-align: top;">
                                <td style="margin: 0pt; padding: 0pt; font-family: helvetica,arial,sans-serif; line-height: 16px; vertical-align: top; border-collapse: collapse;" height="16"><br></td>
                            </tr>
                            </tbody>
                        </table>
                    </th>
                </tr>
                </tbody>
            </table>
            <table class="m_-7119382030963698508gmail-row" style="padding: 0pt; border-collapse: collapse; vertical-align: top; width: 580px;">
                <tbody>
                <tr style="padding: 0pt; vertical-align: top;">
                    <th class="m_-7119382030963698508gmail-small-12 m_-7119382030963698508gmail-large-12 m_-7119382030963698508gmail-columns m_-7119382030963698508gmail-first m_-7119382030963698508gmail-last" style="margin: 0pt auto; padding: 0pt 16px 16px; font-weight: 400; line-height: 1.3; text-align: left; width: 564px;">
                        <h6 style="margin: 0pt 0pt 10px; padding: 0pt; color: inherit; font-size: 18px; line-height: 1.3;">"Corporate Finance skills matter as you ascend in your career or business."</h6>
                        <div style="text-align: justify;">The
                            Corporate Finance qualification is a globally recognized three-level
                            qualification that was created with a joint partnership between The
                            Chartered Institute for Securities and Investments, United Kingdom
                            (CISI UK) and the Institute of Chartered Accountants in England and
                            Wales (ICAEW). CF qualification focuses on developing practical and
                            transactional skills required by companies whilst planning and
                            executing corporate actions.<br><br>Our Corporate Finance preparatory
                            class will not only prepare our course takers for the CISI exam, but
                            more than that, we offer industry experience and insights through our
                            distinguished lecturers and guest speakers briefings, case studies
                            discussions and business strategy sessions that the student takes away
                            for the rest of their professional life &#8211; regardless of whether they
                            proceed with the exam or not.<br><br>
                        </div>
                        <table style="padding: 0pt; border-collapse: collapse; vertical-align: top; width: 549.091px;">
                            <tbody></tbody>
                        </table>
                        <table class="m_-7119382030963698508gmail-row" style="padding: 0pt; border-collapse: collapse; vertical-align: top; width: 549.091px;">
                            <tbody>
                            <tr style="padding: 0pt; vertical-align: top;">
                                <th class="m_-7119382030963698508gmail-small-12 m_-7119382030963698508gmail-large-6 m_-7119382030963698508gmail-columns m_-7119382030963698508gmail-first" style="margin: 0pt auto; padding: 0pt 0pt 16px; font-weight: 400; line-height: 1.3; text-align: left; width: 288.182px;">
                                    <table style="padding: 0pt; border-collapse: collapse; vertical-align: top; width: 289px; height: 294px;">
                                        <tbody>
                                        <tr style="padding: 0pt; vertical-align: top;">
                                            <th style="margin: 0pt; padding: 0pt; font-weight: 400; line-height: 1.3; text-align: left;">Class Schedule<br><span style="font-weight: 700;">Date :</span><br>December 3<sup>rd</sup><span class="Apple-converted-space">&nbsp;</span>2016 &#8211; March 11<sup>th</sup><span class="Apple-converted-space">&nbsp;</span>2017 (every Saturday)<br><br><span style="font-weight: 700;">Time :</span><br>08.45 &#8211; 16.45<br><br><span style="font-weight: 700;">Place :<span class="Apple-converted-space">&nbsp;</span></span><br>Seminar Room Indonesian<span class="Apple-converted-space">&nbsp;</span><br>Stock Exchange, Tower 2, 1<small><sup>st</sup></small><span class="Apple-converted-space">&nbsp;</span>Floor<br>Jl. Jend. Sudirman Kav. 52-53<br>Jakarta 12190, Indonesia</th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </th>
                                <th class="m_-7119382030963698508gmail-small-12 m_-7119382030963698508gmail-large-6 m_-7119382030963698508gmail-columns m_-7119382030963698508gmail-last" style="margin: 0pt auto; padding: 0pt 0pt 16px; font-weight: 400; line-height: 1.3; text-align: left; width: 260.909px;">
                                    <table style="padding: 0pt; border-collapse: collapse; vertical-align: top; width: 260.909px;">
                                        <tbody>
                                        <tr style="padding: 0pt; vertical-align: top;">
                                            <th style="margin: 0pt; padding: 0pt; font-weight: 400; line-height: 1.3; text-align: left;">
                                                <table style="padding: 0pt; border-collapse: collapse; vertical-align: top; width: 260.909px;">
                                                    <tbody>
                                                    <tr style="padding: 0pt; vertical-align: top;">
                                                        <th style="margin: 0pt; padding: 0pt; font-weight: 400; line-height: 1.3; text-align: center;"><img class="m_-7119382030963698508gmail-small-float-center CToWUd" src="https://www.dropbox.com/s/34oqelb50hj8lp3/logo_w_ticmi.png?raw=1" alt="Fidelitas Consult - Accredited Training Partner CISI UK" style="clear: both; display: block; height: 240px; max-width: 100%; width: 229px;"></th>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </th>
                            </tr>
                            </tbody>
                        </table>
                        <table class="m_-7119382030963698508gmail-row" style="padding: 0pt; border-collapse: collapse; vertical-align: top; width: 549.091px;">
                            <tbody>
                            <tr style="padding: 0pt; vertical-align: top;">
                                <th class="m_-7119382030963698508gmail-small-12 m_-7119382030963698508gmail-large-12 m_-7119382030963698508gmail-columns m_-7119382030963698508gmail-first m_-7119382030963698508gmail-last" style="margin: 0pt auto; padding: 0pt 0pt 16px; font-weight: 400; line-height: 1.3; text-align: left; width: 549.091px;">
                                    <div style="text-align: justify;">
                                        <table style="padding: 0pt; border-collapse: collapse; text-align: left; vertical-align: top; width: 549.091px;">
                                            <tbody>
                                            <tr style="padding: 0pt; vertical-align: top;">
                                                <th style="margin: 0pt; padding: 0pt; font-weight: 400; line-height: 1.3; text-align: left;">
                                                    <div style="text-align: justify;">
                                                        <p class="m_-7119382030963698508gmail-lead" style="margin: 0pt 0pt 10px; padding: 0pt; font-size: 20px; line-height: 1.6; text-align: center;">Get 50% off CISI UK exams and 20% discount on preparatory class fees.</p>
                                                        <table class="m_-7119382030963698508gmail-button m_-7119382030963698508gmail-large m_-7119382030963698508expand" style="margin: 0pt 0pt 16px; padding: 0pt; border-collapse: collapse; text-align: left; vertical-align: top; width: 549.091px;">
                                                            <tbody>
                                                            <tr style="padding: 0pt; vertical-align: top;">
                                                                <td style="margin: 0pt; padding: 0pt; font-family: helvetica,arial,sans-serif; line-height: 1.3; vertical-align: top; border-collapse: collapse;">
                                                                    <table style="padding: 0pt; border-collapse: collapse; vertical-align: top; width: 547.273px;">
                                                                        <tbody>
                                                                        <tr style="padding: 0pt; vertical-align: top;">
                                                                            <td style="border: 2px solid #e13138; margin: 0pt; padding: 0pt; font-family: helvetica,arial,sans-serif; background-image: none; background-position: 0% 50%; background-repeat: repeat; background-color: #e13138; color: rgb(254, 254, 254); line-height: 1.3; vertical-align: top; border-collapse: collapse;">
                                                                                <center style="width: 545.455px;"><a href="http://www.fidelitas-cfqual.com/" align="center" class="m_-7119382030963698508gmail-float-center" target="_blank" data-saferedirecturl="http://www.fidelitas-cfqual.com/&amp;source=gmail&amp;ust=1478585649030000&amp;usg=AFQjCNGvPvCTAnL8O3hIsxGqTJO7714BHQ" style="border: 0pt solid #e13138; margin: 0pt; padding: 10px 10pt; color: rgb(254, 254, 254); font-size: 20px; font-weight: 700; line-height: 2.3; text-decoration: none; width: auto;">Visit our website to learn more.</a></center>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="m_-7119382030963698508expander" style="margin: 0pt; padding: 0pt; font-family: helvetica,arial,sans-serif; line-height: 1.3; vertical-align: top; width: 0pt; border-collapse: collapse;"><br></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </th>
                                                <th class="m_-7119382030963698508expander" style="margin: 0pt; padding: 0pt; font-weight: 400; line-height: 1.3; text-align: left; width: 0pt;"><br></th>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </th>
                            </tr>
                            </tbody>
                        </table>
                        <table class="m_-7119382030963698508gmail-row" style="padding: 0pt; border-collapse: collapse; vertical-align: top; width: 549.091px;" bgcolor="#d6e5e3">
                            <tbody>
                            <tr style="padding: 0pt; vertical-align: top;">
                                <th class="m_-7119382030963698508gmail-small-12 m_-7119382030963698508gmail-large-6 m_-7119382030963698508gmail-columns m_-7119382030963698508gmail-first" style="margin: 0pt auto; padding: 0pt 0pt 16px; font-weight: 400; line-height: 1.3; text-align: left; width: 274.545px;">
                                    <table style="padding: 0pt; border-collapse: collapse; vertical-align: top; width: 100%;">
                                        <tbody>
                                        <tr style="padding: 0pt; vertical-align: top;">
                                            <th style="margin: 0pt; padding: 5pt;font-size: 13px; font-weight: 400; line-height: 1.3; text-align: left;">
                                                <h5 style="margin: 0pt 0pt 10px; padding: 0pt; color: inherit; font-size: 16px; font-weight: 400; line-height: 1.3;">Contact Info :</h5>
                                                <strong>The Indonesia Capital Market Institute</strong> <br>
                                                Indonesian Stock Exchange Building, Tower II, 1st Floor Jl. Jend. Sudirman Kav. 52-53, Jakarta-12190
                                                <br>
                                                Call Center : 0800 100 9000 (Toll Free) <br>
                                                P : (021) 515 23 18 <br>
                                                E : info@ticmi.co.id
                                            </th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </th>
                                <th class="m_-7119382030963698508gmail-small-12 m_-7119382030963698508gmail-large-6 m_-7119382030963698508gmail-columns m_-7119382030963698508gmail-last" style="margin: 0pt auto; padding: 0pt 0pt 16px; font-weight: 400; line-height: 1.3; text-align: left; width: 274.545px;">
                                    <table style="padding: 0pt; border-collapse: collapse; vertical-align: top; width: 100%;">
                                        <tbody>
                                        <tr style="padding: 0pt; vertical-align: top;">
                                            <th style="margin: 0pt; padding: 0pt;font-size: 13px; font-weight: 400; line-height: 1.3; text-align: left;">
                                                <h5 style="margin: 0pt 0pt 10px; padding: 0pt; color: inherit; font-size: 20px; font-weight: 400; line-height: 1.3;">&nbsp;</h5>
                                                <strong>PT. Fidelitas Konsultama Internasional</strong>
                                                Office 8 Building, 19th Floor, Unit K,
                                                Sudirman CBD Lot 28,
                                                Jl. Jend. Sudirman Kav. 52-53,
                                                Jakarta 12190 <br>
                                                Phone&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 021 29333747 <br>
                                                Whatsapp : 08118245559 <br>
                                                Email :<span class="Apple-converted-space">&nbsp;</span><a href="mailto:contact@fidelitas-cfqual.com" target="_blank" style="margin: 0pt; padding: 0pt; color: rgb(33, 153, 232); line-height: 1.3; text-decoration: none;">contact@fidelitas-cfqual.com</a>
                                                <br>
                                                Website :<span class="Apple-converted-space">&nbsp;</span><a href="http://www.fidelitas-cfqual.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://www.fidelitas-cfqual.com/&amp;source=gmail&amp;ust=1478585649030000&amp;usg=AFQjCNGvPvCTAnL8O3hIsxGqTJO7714BHQ" style="margin: 0pt; padding: 0pt; color: rgb(33, 153, 232); line-height: 1.3; text-decoration: none;">www.fidelitas-cfqual.com</a>
                                                <p style="margin: 0pt 0pt 10px; padding: 0pt; line-height: 1.3;"></p>
                                            </th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </th>
                            </tr>
                            </tbody>
                        </table>

                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContent">
                            <tbody><tr>
                                <td align="center" valign="top" style="padding-top: 9px;padding-right: 9px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                        <tbody><tr>
                                            <td align="center" valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                <!--[if mso]>
                                                <table align="center" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                <![endif]-->

                                                <!--[if mso]>
                                                <td align="center" valign="top">
                                                <![endif]-->


                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                    <tbody><tr>
                                                        <td valign="top" style="padding-right: 10px;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                <tbody><tr>
                                                                    <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                            <tbody><tr>

                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                    <a href="https://twitter.com/TICMI_ID" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24" class=""></a>
                                                                                </td>


                                                                            </tr>
                                                                            </tbody></table>
                                                                    </td>
                                                                </tr>
                                                                </tbody></table>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>

                                                <!--[if mso]>
                                                </td>
                                                <![endif]-->

                                                <!--[if mso]>
                                                <td align="center" valign="top">
                                                <![endif]-->


                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                    <tbody><tr>
                                                        <td valign="top" style="padding-right: 10px;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                <tbody><tr>
                                                                    <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                            <tbody><tr>

                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                    <a href="https://www.facebook.com/TICMI.ID" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24" class=""></a>
                                                                                </td>


                                                                            </tr>
                                                                            </tbody></table>
                                                                    </td>
                                                                </tr>
                                                                </tbody></table>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>

                                                <!--[if mso]>
                                                </td>
                                                <![endif]-->

                                                <!--[if mso]>
                                                <td align="center" valign="top">
                                                <![endif]-->


                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                    <tbody><tr>
                                                        <td valign="top" style="padding-right: 10px;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                <tbody><tr>
                                                                    <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                            <tbody><tr>

                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                    <a href="https://www.instagram.com/ticmi_id/" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24" class=""></a>
                                                                                </td>


                                                                            </tr>
                                                                            </tbody></table>
                                                                    </td>
                                                                </tr>
                                                                </tbody></table>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>

                                                <!--[if mso]>
                                                </td>
                                                <![endif]-->

                                                <!--[if mso]>
                                                <td align="center" valign="top">
                                                <![endif]-->


                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                    <tbody><tr>
                                                        <td valign="top" style="padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                <tbody><tr>
                                                                    <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                            <tbody><tr>

                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                    <a href="http://ticmi.co.id" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24" class=""></a>
                                                                                </td>


                                                                            </tr>
                                                                            </tbody></table>
                                                                    </td>
                                                                </tr>
                                                                </tbody></table>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>

                                                <!--[if mso]>
                                                </td>
                                                <![endif]-->

                                                <!--[if mso]>
                                                </tr>
                                                </table>
                                                <![endif]-->
                                            </td>
                                        </tr>
                                        </tbody></table>
                                </td>
                            </tr>
                            </tbody></table>


                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <tbody class="mcnTextBlockOuter">
                            <tr>
                                <td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <!--[if mso]>
                                    <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                        <tr>
                                    <![endif]-->

                                    <!--[if mso]>
                                    <td valign="top" width="800" style="width:800px;">
                                    <![endif]-->
                                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                                        <tbody><tr>

                                            <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #656565;font-family: Helvetica;font-size: 12px;line-height: 150%;text-align: center;">

                                                <em>Copyright © ticmi.co.id, All rights reserved.</em><br>
                                                <br>
                                                <strong>Our mailing address is:</strong><br>
                                                info@ticmi.co.id<br>
                                                <br>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        </tbody></table>
                                    <!--[if mso]>
                                    </td>
                                    <![endif]-->

                                    <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            </tbody>
                        </table></td>
                    </th>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>