@extends('layout.home')
@section('content')
    <!-- Page Header -->
    <div class="typo-dark " style="background: url('./assets/images/banner/profile.jpg') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-center" style="text-align: center;">
                        <img src="{{ url('assets/images/default/logo-ticmi.jpg') }}" align="center" width="200" alt="The Indonesia Capital Market Institute">
                        <img src="{{ url('assets/images/risk/logo/Logo-BSNI.png') }}" align="center" alt="Badan Standarisasi Nasional Indonesia">
                        <img src="{{ url('assets/images/risk/logo/komtek-03-10.jpg') }}" align="center" width="170" alt="Komtek 03 10">
                        <img src="{{ url('assets/images/risk/logo/veda-praxis.png') }}" align="center" width="200" alt="Veda Praxis">
                    </div>
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper text-center">
                        <!-- Title & Sub Title -->
                        <h3 class="title" style="font-size: 25px; margin-bottom: 10px;">{{ $pelatihan->workshop_name }}</h3>
                        <h6 class="sub-title" style="font-size: 18px;">{{ $pelatihan->sub_title }}</h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <!-- Course Wrapper -->
            <div class="row course-single">
                <!-- Course Banner Image -->
                <div class="col-sm-7">
                    <div class="owl-crousel">
                        <!--                        <img alt="Course" class="img-responsive" src="./assets/images/course/aspm-product.jpg" width="1920" height="966">-->
                    </div>
                </div><!-- Column -->
            </div><!-- Course Wrapper -->

            <div class="row course-single content-box  bg-white shadow">
                <div class="col-sm-6">
                    <div class="owl-crousel">
                        <img alt="Course" class="img-responsive" src="{{ url('assets/images/ticmi/course-ticmi.jpg') }}" width="1920" height="966">
                    </div>
                </div><!-- Column -->

                <!-- Course Detail -->
                <div class="col-sm-6 ">
                    <div class="course-detail">
                        <!-- Course Content -->
                        <div class="course-meta">
                            {{--<h4>{{ $pelatihan->workshop_name }}</h4>--}}
                            <span class="cat bg-yellow">SEMINAR</span>
                            <!--<h4>Wakil Perantara Pedagang Efek</h4>-->
                            <ul class="course-meta-icons">
                                <li><i class="fa fa-dollar"></i><span>Biaya Investasi</span><h5>Rp{{ number_format($pelatihan->biaya) }}</h5></li>
                                <li><i class="fa fa-clock-o"></i><span>Waktu Pelaksanaan</span><h5>{{ GeneralHelper::waktuPelaksanaan($pelatihan->tgl_mulai, $pelatihan->tgl_selesai) }}</h5></li>
                                <li><i class="fa fa-map-marker"></i><span>Tempat</span><h5>{{ $pelatihan->lokasi }}</h5></li>
                                @if($pelatihan->minimal_peserta)
                                    <li><i class="fa fa-users"></i><span>Maksimal Peserta</span><h5>{{ $pelatihan->minimal_peserta }} Peserta</h5></li>
                                @endif
                                <li><i class="fa fa-clock-o"></i><span>Waktu</span><h5>{{ $pelatihan->waktu }}</h5></li>
                            </ul>
                        </div>
                    </div><!-- Course Detail -->
                </div><!-- Column -->
                <div class="col-sm-12 text-center">
                    <br>
                    <a href="{{ route('pelatihan.daftar',['workshopslug'=>$pelatihan->slugs]) }}" class="btn btn-lg"><span class="fa fa-angle-double-right"></span> Saya ingin daftar</a>
                </div>
            </div>

            <div class="row course-full-detail content-box  bg-white shadow">
                <div class="col-sm-12">
                    <div class="">
                        <p class="text-justify">
                            Pengelolaan ketidakpastian dalam bentuk risiko yang dihadapi oleh suatu organisasi secara tepat, telah menjadi kebutuhan mendasar dan mendesak, dan oleh karenanya penerapan Manajemen Risiko yang sesuai dengan karakteristik suatu organisasi telah menjadi salah satu pilar penopang dalam upaya mencapai Good Corporate Governance. Potensi risiko yang tidak dikelola dan ditangani dengan baik dapat berubah menjadi masalah. Kemudian bila masalah itu tidak dikelola dengan baik dan segera diatasi oleh organisasi, akan dapat menjadi krisis. Krisis yang dibiarkan berlarut-larut pada akhirnya akan berubah menjadi bencana, dan bila itu terjadi maka eksistensi suatu organisasi tinggal menunggu waktunya untuk hilang.
                        </p>
                        <p class="text-justify">
                            Pengelolaan risiko pada perusahaan umumnya ditujukan untuk mengelola ketidakpastian dan ancaman yang dapat mengganggu operasional perusahaan atau menghambat tercapainya tujuan perusahaan. Oleh karena itu, penerapan Manajemen Risiko membutuhkan pendekatan dan metodologi terstruktur yang mengacu pada standar yang berlaku umum.
                        </p>
                        <p class="text-justify">
                            Salah satu standar Manajemen Risiko yang umum digunakan adalah ISO 31000 Risk management, yang telah diadopsi Indonesia menjadi SNI ISO 31000, dan dapat menjadi satu rujukan dasar nasional yang kuat yang diakui dan tertelusur dengan sistem internasional dalam proses adaptasi mereka terhadap berbagai tuntutan dan dinamika pelaksanaan manajemen risiko di tingkat regional dan global. Standar ini menyediakan prinsip dan panduan generik untuk penerapan Manajemen Risiko, termasuk panduan bagaimana mengelola risiko dalam konteks organisasi untuk mencapai sasaran yang telah ditetapkannya. Bagi perusahaan dengan kompleksitas bisinis yang besar, manfaat penerapan Manajemen Risiko akan terasa semakin besar bila terintegrasi dengan penerapan standar sistem manajemen lainnya seperti Standar Sistem Manajemen Keamanan Informasi ISO 27001 dan Standar Sistem Manjemen Mutu ISO 9001. Semua standar sistem manajemen lainnya tersebut selalu memasukkan salah satu klausul didalamnya dengan satu klausul khusus terkait pengelolaan risiko sesuai sektornya, yang secara khusus pasti akan mengacu pada ketentuan yang ada pada ISO 31000.
                        </p>
                        <p class="text-justify">
                            Seminar Manajemen Risiko ini akan memaparkan SNI ISO 31000 serta mendiskusikan integrasi dengan standar sistem manajemen lainnya dan agar peserta dapat lebih memahami apa itu SNI ISO 31000 dan manfaat penerapannya bagi organisasi yang bersangkutan.
                        </p>

                        <h5>Tujuan : </h5>
                        <ol>
                            <li>Peserta mendapatkan informasi mengenai penerapan Manajemen Risiko berbasis ISO 31000 melalui <em>point of view</em> Badan Standarisasi Nasional (BSN).</li>
                            <li>Peserta memahami integrasi ISO 31000 dengan standar ISO 27001 dan ISO 9001, sehingga akan meningkatkan efektivitas pengelolaan Manajemen Risiko di perusahaannya.</li>
                            <li>Peserta mamahami <em>cost of compliance</em> dari penerapan Manajemen Risiko berbasis seri standar SNI ISO 31000</li>
                        </ol>
                        <h5>Target Peserta</h5>
                        <ol>
                            <li>Supervisor, Manager, ataupun level di atasnya pada Divisi Manajemen Risiko, <em>Business Continuity Management</em>, <em>Information Technology</em>, ataupun divisi lainnya yang membutuhkan pengetahuan mengenai Manajemen Risiko</li>
                            <li>Praktisi atau Profesional pada industri rentan risiko yang ingin menambah pengetahuan mengenai integrasi ISO 31000 dengan standar lainnya.</li>
                        </ol>
                        <h5>Pelaksanaan Acara Seminar</h5>
                        <table class="table table-bordered table-condensed table-striped" style="width: 80%;" align="center">
                            <tr>
                                <td>Hari / Tanggal</td>
                                <td>Kamis, 16 Maret 2017.</td>
                            </tr>
                            <tr>
                                <td>Tempat</td>
                                <td>Ruang Seminar Lantai 1 Gedung Bursa Efek Indonesia, Jakarta</td>
                            </tr>
                            <tr>
                                <td>Waktu</td>
                                <td>08.00 – 12.00 WIB</td>
                            </tr>
                            <tr>
                                <td>Investasi</td>
                                <td>Rp500.000,-</td>
                            </tr>
                        </table>

                        <h5>Pembukaan</h5>
                        <ol>
                            <li>Pembuka Acara <strong>Ibu Erningsih</strong> - Deputi Bidang Informasi dan Pemasyarakatan Badan Standardisasi Nasional</li>
                        </ol>

                        <h5>Pembicara dan Panelis</h5>
                        <ol>
                            <li><strong>Charles R. Forst</strong> - Anggota Komite Teknis Perumusan SNI 03-10 Manajemen Risiko - Badan Standardisasi Nasional</li>
                            <li><strong>M. Mukhlis</strong> - Anggota Komite Teknis Perumusan SNI 03-10 Manajemen Risiko - Badan Standardisasi Nasional</li>
                            <li><strong>Satya Rinaldi, CISA</strong> - <em>Chief Strategist</em> dan <em>Partner Veda Praxis</em> yang berpengalaman dalam implementasi dan <em>assessment</em> Manajemen Risiko pada berbagai industri, antara lain Industri Perbankan dan Keuangan, Teknologi Informasi, Manufaktur, dan BUMN.
                            </li>
                        </ol>
                        <table class="table table-bordered table-condensed table-striped">
                            <tr>
                                <td colspan="2" style="height: 40px; background: #e13138;color: #fff; border-color: #e13138;" class="text-center"><strong>Jadwal Seminar</strong></td>
                            </tr>
                            <tr>
                                <td class="text-center">08.00 – 09.00 WIB</td>
                                <td>Registrasi</td>
                            </tr>
                            <tr>
                                <td class="text-center">09.00 – 09.15 WIB</td>
                                <td>Pembukaan oleh Ibu Erningsih - Deputi Bidang Informasi dan Pemasyarakatan Badan Standardisasi Nasional</td>
                            </tr>
                            <tr>
                                <td class="text-center"><strong>Sesi 1</strong> <br> 09.15-10.00 WIB</td>
                                <td>
                                    Sosialisasi tentang ISO31000 oleh: Charles R. Forst - Anggota Komite Teknik Badan Standardisasi Nasional ISO 31000
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center"><strong>Sesi 2</strong> <br> 10.00-11.00 WIB</td>
                                <td>
                                    Integrasi ISO 31000 (Risk Management) dengan ISO 27001 dan ISO 9001, oleh:
                                    <ol style="list-style-type: lower-alpha">
                                        <li>M. Mukhlis - Anggota Komite Teknik Badan Standardisasi Nasional</li>
                                        <li>Satya Rinaldi, CISA - Veda Praxis</li>
                                    </ol>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center"><strong>Sesi 3</strong> <br> 11.00-12.00 WIB</td>
                                <td>
                                    Panel diskusi practical penerapan Risk Management dan Cost of MR Compliance, oleh:
                                    <ol style="list-style-type: lower-alpha">
                                        <li>M. Mukhlis - Anggota Komite Teknik Badan Standardisasi Nasional</li>
                                        <li>Satya Rinaldi, CISA - Veda Praxis</li>
                                    </ol>
                                    Moderator, oleh:Syahraki Syahrir, CISA, CISM, CA.
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">12.00 – 13.00 WIB</td>
                                <td>Penutupan dan Makan Siang </td>
                            </tr>
                        </table>
                    </div>
                    <p class="text-center">
                        <br>
                        <a href="{{ route('pelatihan.daftar',['workshopslug'=>$pelatihan->slugs]) }}" class="btn btn-lg"><span class="fa fa-angle-double-right"></span> Saya ingin daftar</a>
                    </p>

                </div><!-- Column -->
            </div><!-- row -->

            <div class="row course-full-detail content-box  bg-white shadow">
                <h4>Program Kami lainnya : </h4>
                <div class="owl-carousel show-nav-hover dots-dark nav-square dots-square navigation-color" data-animatein="zoomIn" data-animateout="slideOutDown" data-items="1" data-margin="30" data-loop="true" data-merge="true" data-nav="true" data-dots="false" data-stagepadding="" data-mobile="1" data-tablet="2" data-desktopsmall="2"  data-desktop="3" data-autoplay="true" data-delay="3000" data-navigation="true">

                    @if($otherbatch && $otherbatch->count() > 0)
                        @foreach($otherbatch as $value)
                            <div class="item">
                                <!-- Related Wrapper -->
                                <div class="related-wrap">
                                    <!-- Related Image Wrapper -->
                                    <div class="img-wrap">
                                        @if($value->program->nm_program == "WPPE")
                                            @if($value->kelas->type->nama == "Regular")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wppe-reguler-3.jpg') }}" width="600" height="220">
                                            @elseif($value->kelas->type->nama == "Waiver")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wppe-waiver-3.jpg') }}" width="600" height="220">
                                            @else
                                            @endif
                                        @elseif($value->program->nm_program == "WMI")
                                            @if($value->kelas->type->nama == "Regular")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wmi-reguler-3.jpg') }}" width="600" height="220">
                                            @elseif($value->kelas->type->nama == "Waiver")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wmi-waiver-3.jpg') }}" width="600" height="220">
                                            @else
                                            @endif
                                        @elseif($value->program->nm_program == "ASPM")
                                            @if($value->kelas->type->nama == "Regular")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/aspm-reguler-3.jpg') }}" width="600" height="220">
                                            @elseif($value->kelas->type->nama == "Waiver")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/aspm-akselerasi-3.jpg') }}" width="600" height="220">
                                            @else
                                            @endif
                                        @elseif($value->program->nm_program == "PT-ASPM")
                                            <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/aspm-akselerasi-3.jpg') }}" width="600" height="220">
                                        @else
                                            <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wppe-waiver-3.jpg') }}" width="600" height="220">
                                        @endif
                                    </div>
                                    <!-- Related Content Wrapper -->
                                    <div class="related-content">
                                        <a href="{{ url('daftar/'.$value->id.'/'.$value->nama) }}" title="{{ $value->nama }}">+</a><span>{{ $value->nama }}</span>
                                        <h5 class="title">{{ $value->nama }}</h5>
                                    </div><!-- Related Content Wrapper -->
                                </div><!-- Related Wrapper -->
                            </div><!-- Item -->
                        @endforeach
                    @else
                    @endif

                </div><!-- Related Post -->

            </div>
        </div><!-- Container -->
    </div><!-- Page Default -->
@endsection