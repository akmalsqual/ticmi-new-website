@extends('layout.home')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('{{ url('assets/images/banner/cart.jpg') }}') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h4 class="title">Data Peserta {{ $workshop->workshop_name }}</h4>
                        <h6 class="sub-title">&nbsp;</h6>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->


    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <div class="row course-single content-box bg-white shadow">
                <div class="col-sm-12">

                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <td width="50%">Total pendaftar</td>
                            <td>Sudah melakukan pembayaran</td>
                        </tr>
                        </thead>
                        <tbody>
                        @if(is_object($workshop->peserta) && $workshop->peserta()->count() > 0)
                            <tr>
                                <td class="text-center">{{ $workshop->peserta()->count() }} Pendaftar</td>
                                <td class="text-center">{{ $workshop->peserta->where('is_payment_approve',1)->count() }} Peserta</td>
                            </tr>
                        @else
                            <tr>
                                <td class="text-center">0 Pendaftar</td>
                                <td class="text-center">0 Peserta</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>

                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th width="30">No</th>
                            <th>Nama</th>
                            <th>Company</th>
                            <th>Jabatan</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(is_object($workshop->peserta) && $workshop->peserta()->count() > 0)
                            @php($no=1)
                            @foreach($workshop->peserta as $item)
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>
                                        {{ $item->nama }}
                                        @if($item->referral)
                                            <span class="badge badge-success pull-right"><span class="fa fa-mail-reply showtooltip" title="Referal dari {{ $item->referral->nama }}"></span></span>
                                        @endif
                                    </td>
                                    <td>{{ $item->company }}</td>
                                    <td>{{ $item->jabatan }}</td>
                                    <td>
                                        @if($item->is_payment_approve == 1)
                                            <span class="label label-success">Payment Approved</span>
                                        @else
                                            @if($item->is_confirm == 1)
                                                <span class="label label-primary">On Verification</span>
                                            @else
                                                <span class="label label-warning">Waiting for Confirmation</span>
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                                @php($no++)
                            @endforeach
                        @else
                            <tr>
                                <td align="center">Belum ada data</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div><!-- Row -->
        </div> <!-- End Container -->
    </div> <!-- End Page Default -->
@endsection