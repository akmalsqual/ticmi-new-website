@extends('layout.home')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark " style="background: url('/assets/images/banner/profile.jpg') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper ">
                        <!-- Title & Sub Title -->
                        <h3 class="title" style="font-size: 25px; margin-bottom: 10px;">{{ $pelatihan->workshop_name }}</h3>
                        <h6 class="sub-title" style="font-size: 18px;">{{ $pelatihan->sub_title }}</h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <!-- Course Wrapper -->
            <div class="row course-single">
                <!-- Course Banner Image -->
                <div class="col-sm-7">
                    <div class="owl-crousel">
                        <!--                        <img alt="Course" class="img-responsive" src="./assets/images/course/aspm-product.jpg" width="1920" height="966">-->
                    </div>
                </div><!-- Column -->
            </div><!-- Course Wrapper -->

            <div class="row course-single content-box  bg-white shadow">
                <div class="col-sm-6">
                    <div class="owl-crousel">
                        <img alt="Course" class="img-responsive" src="{{ url('assets/images/ticmi/course-ticmi.jpg') }}" width="1920" height="966">
                    </div>
                </div><!-- Column -->

                <!-- Course Detail -->
                <div class="col-sm-6 ">
                    <div class="course-detail">
                        <!-- Course Content -->
                        <div class="course-meta">
                            {{--<h4>{{ $pelatihan->workshop_name }}</h4>--}}
                            <span class="cat bg-yellow">{{ strtoupper($pelatihan->program->nm_program) }}</span>
                            <!--<h4>Wakil Perantara Pedagang Efek</h4>-->
                            <ul class="course-meta-icons">
                                <li><i class="fa fa-dollar"></i><span>Biaya Investasi</span><h5>Rp{{ number_format($pelatihan->biaya) }}</h5></li>
                                <li><i class="fa fa-clock-o"></i><span>Waktu Pelaksanaan</span><h5>{{ GeneralHelper::waktuPelaksanaan($pelatihan->tgl_mulai, $pelatihan->tgl_selesai) }}</h5></li>
                                <li><i class="fa fa-map-marker"></i><span>Tempat</span><h5>{{ $pelatihan->lokasi }}</h5></li>
                                <li><i class="fa fa-map-marker"></i><span>Ruangan</span><h5>{{ $pelatihan->ruangan }}</h5></li>
                                @if($pelatihan->minimal_peserta)
                                    <li><i class="fa fa-users"></i><span>Minimal Peserta</span><h5>{{ $pelatihan->minimal_peserta }} Peserta</h5></li>
                                @endif
                                <li><i class="fa fa-clock-o"></i><span>Waktu</span><h5>{{ $pelatihan->waktu }}</h5></li>
                            </ul>
                        </div>
                    </div><!-- Course Detail -->
                </div><!-- Column -->
                <div class="col-sm-12 text-center">
                    <br>
                    <a href="{{ route('pelatihan.checkout',['workshopslug'=>$pelatihan->slugs]) }}" class="btn btn-lg" onclick="ga('send', 'event', 'Pendaftaran Workshop (checkout)', '{{ $pelatihan->workshop_name }}', '{{ url()->current() }}' )"><span class="fa fa-angle-double-right"></span> Saya ingin daftar</a>
                </div>
            </div>

            <div class="row course-full-detail content-box  bg-white shadow">
                <div class="col-sm-12">
                    <div class="">
                        {!! $pelatihan->deskripsi !!}
                    </div>
                    <p class="text-center">
                        <br>
                        <a href="{{ route('pelatihan.checkout',['workshopslug'=>$pelatihan->slugs]) }}" onclick="ga('send', 'event', 'Pendaftaran Workshop (checkout)', '{{ $pelatihan->workshop_name }}', '{{ url()->current() }}' )" class="btn btn-lg"><span class="fa fa-angle-double-right"></span> Saya ingin daftar</a>
                    </p>

                </div><!-- Column -->
            </div><!-- row -->

            <div class="row course-full-detail content-box  bg-white shadow">
                <h4>Program Kami lainnya : </h4>
                <div class="owl-carousel show-nav-hover dots-dark nav-square dots-square navigation-color" data-animatein="zoomIn" data-animateout="slideOutDown" data-items="1" data-margin="30" data-loop="true" data-merge="true" data-nav="true" data-dots="false" data-stagepadding="" data-mobile="1" data-tablet="2" data-desktopsmall="2"  data-desktop="3" data-autoplay="true" data-delay="3000" data-navigation="true">

                    @if($otherbatch && $otherbatch->count() > 0)
                        @foreach($otherbatch as $value)
                            <div class="item">
                                <!-- Related Wrapper -->
                                <div class="related-wrap">
                                    <!-- Related Image Wrapper -->
                                    <div class="img-wrap">
                                        @if($value->program->nm_program == "WPPE")
                                            @if($value->kelas->type->nama == "Regular")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wppe-reguler-3.jpg') }}" width="600" height="220">
                                            @elseif($value->kelas->type->nama == "Waiver")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wppe-waiver-3.jpg') }}" width="600" height="220">
                                            @else
                                            @endif
                                        @elseif($value->program->nm_program == "WPPE Pemasaran")
                                            @if($value->kelas->type->nama == "Regular")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wppe-pemasaran-reguler.jpg') }}" width="600" height="220">
                                            @elseif($value->kelas->type->nama == "Waiver")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wppe-pemasaran-waiver.jpg') }}" width="600" height="220">
                                            @else
                                            @endif
                                        @elseif($value->program->nm_program == "WPPE Pemasaran Terbatas")
                                            <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wppe-pemasaran-terbatas.jpg') }}" width="600" height="220">
                                        @elseif($value->program->nm_program == "WMI")
                                            @if($value->kelas->type->nama == "Regular")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wmi-reguler-3.jpg') }}" width="600" height="220">
                                            @elseif($value->kelas->type->nama == "Waiver")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wmi-waiver-3.jpg') }}" width="600" height="220">
                                            @else
                                            @endif
                                        @elseif($value->program->nm_program == "WPEE")
                                            @if($value->kelas->type->nama == "Regular")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wpee-reguler.jpg') }}" width="600" height="220">
                                            @elseif($value->kelas->type->nama == "Waiver")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wpee-waiver.jpg') }}" width="600" height="220">
                                            @else
                                            @endif
                                        @elseif($value->program->nm_program == "ASPM")
                                            @if($value->kelas->type->nama == "Regular")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/aspm-reguler-3.jpg') }}" width="600" height="220">
                                            @elseif($value->kelas->type->nama == "Waiver")
                                                <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/aspm-akselerasi-3.jpg') }}" width="600" height="220">
                                            @else
                                            @endif
                                        @elseif($value->program->nm_program == "Placement Test ASPM")
                                            <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/aspm-akselerasi-3.jpg') }}" width="600" height="220">
                                        @elseif($value->program->nm_program == "Forum")
                                            <img alt="Event" class="img-responsive" src="./assets/images/course/forum-course-ticmi.jpg" width="600" height="220">
                                        @else
                                            <img alt="Event" class="img-responsive" src="{{ url('assets/images/course/wppe-waiver-3.jpg') }}" width="600" height="220">
                                        @endif
                                    </div>
                                    <!-- Related Content Wrapper -->
                                    <div class="related-content">
                                        <a href="{{ url('daftar/'.$value->id.'/'.$value->nama) }}" title="{{ $value->nama }}">+</a><span>{{ $value->nama }}</span>
                                        <h5 class="title">{{ $value->nama }}</h5>
                                    </div><!-- Related Content Wrapper -->
                                </div><!-- Related Wrapper -->
                            </div><!-- Item -->
                        @endforeach
                    @else
                    @endif

                </div><!-- Related Post -->

            </div>
        </div><!-- Container -->
    </div><!-- Page Default -->
@endsection