@extends('layout.home')
@section('content')
    <!-- Page Header -->
    <div class="page-header typo-dark  hidden-xs" style="background: url({{ url('/assets/images/banner/profile.jpg') }}) top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title" style="font-size: 35px;">{{ $pelatihan->workshop_name }}</h3>
                        <h6 class="sub-title">&nbsp;</h6>
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <!-- Section -->
    <section data-background="{{ url('/assets/images/ticmi/library-ticmi-small.jpg') }}"  data-stellar-background-ratio="0.8" class="relative bg-light typo-dark parallax-bg bg-cover overlay white md">
        <div class="container parent-has-overlay">
            {{ Form::open(['url'=>route('pelatihan.store',['workshopslug'=>$pelatihan->slugs])]) }}
            <div class="row">
                <div class="row course-single content-box">

                    <div class="col-md-6 col-md-offset-3 content-box bg-white" style="padding: 20px;">
                        <div class="title-container text-left sm typo-dark">
                            <div class="title-wrap">
                                <h4 class="title typo-dark">Pendaftaran</h4>
                                <span class="separator line-separator"></span>
                            </div>
                        </div><!-- Name -->
                        @include('errors.list')
                        @include('flash::message')
                        <div role="alert" class="alert alert-info typo-dark">
                            <strong>Informasi</strong> Harap isi data pendaftaran berikut dengan benar
                        </div>

                        <!-- Field 1 -->
                        <div class="input-text form-group">
                            {{--<input type="text" name="contact_name" class="input-name form-control" placeholder="Nama Lengkap" />--}}
                            {{ Form::text('nama',null,['class'=>'form-control','placeholder'=>'Nama Lengkap']) }}
                        </div>
                        <!-- Field 2 -->
                        <div class="input-email form-group">
                            {{--<input type="email" name="contact_email" class="input-email form-control" placeholder="Email"/>--}}
                            {{ Form::email('email',null,['class'=>'form-control','placeholder'=>'Email']) }}
                        </div>
                        <!-- Field 3 -->
                        <div class="input-email form-group">
                            {{--<input type="text" name="contact_phone" class="input-phone form-control" placeholder="Nomor HP"/>--}}
                            {{ Form::text('no_hp',null,['class'=>'form-control','placeholder'=>'No HP']) }}
                        </div>
                        <!-- Field 3 -->
                        <div class="input-email form-group">
                            {{--<input type="text" name="contact_phone" class="input-phone form-control" placeholder="Institusi (Perusahaan / Universitas / Instansi)"/>--}}
                            {{ Form::text('company',null,['class'=>'form-control','placeholder'=>'Institusi (Perusahaan / Universitas / Instansi)']) }}
                        </div>
                        <!-- Field 3 -->
                        <div class="input-email form-group">
                            {{--<input type="text" name="contact_phone" class="input-phone form-control" placeholder="Posisi / Jabatan"/>--}}
                            {{ Form::text('jabatan',null,['class'=>'form-control','placeholder'=>'Jabatan / Posisi']) }}
                        </div>
                        <!-- Button -->
                        <div class="form-group">
                            <div class="">
                                <button class="btn btn-block btn-lg btn-loading" data-toggle="loading" data-loading-text="Loading" type="submit">Daftar <i class="fa fa-paper-plane" style="font-size: 14px;color: #fff;"></i></button>
                            </div>
                        </div>
                    </div><!-- Column -->

                </div>
            </div><!-- Row -->
            {{ Form::close() }}
        </div><!-- Container -->
    </section><!-- Section -->

@endsection