@extends('layout.home')
@section('content')

    <!-- Page Header -->
    <div class="page-header typo-dark" style="background: url('{{ url('assets/images/banner/cart.jpg') }}') top right no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Page Header Wrapper -->
                    <div class="page-header-wrapper">
                        <!-- Title & Sub Title -->
                        <h3 class="title">{{ $workshop->workshop_name }}</h3>
                        <h6 class="sub-title">Konfirmasi Pendaftaran Seminar / Workshop</h6>
                        <!--<ol class="breadcrumb">-->
                        <!--<li><a href="index.html">Home</a></li>-->
                        <!--<li class="active">Blog</li>-->
                        <!--</ol>-->
                        <!-- Breadcrumb -->
                    </div><!-- Page Header Wrapper -->
                </div><!-- Coloumn -->
            </div><!-- Row -->
        </div><!-- Container -->
    </div><!-- Page Header -->

    <div class="page-default bg-grey typo-dark">
        <!-- Container -->
        <div class="container">
            <div class="row shop-forms">
                {!! Form::open(['url'=>route('pelatihan.store',['workshopslug'=>$workshop->slugs]),'name'=>'formCheckout','class'=>'form-horizontal','id'=>'formCheckout']) !!}
                <div class="col-md-8">
                    <div class="panel-group accordion" id="accordion">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Konfirmasi Pendaftaran {{ $workshop->workshop_name }}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="accordion-body collapse in" role="tabpanel">
                                <div class="panel-body">
                                    @include('errors.list')
                                    @include('flash::message')
                                    <table cellspacing="0" class="shop_table cart">
                                        <tbody>
                                        <tr>
                                            <td width="30%" align="right"><strong>Nama Peserta</strong></td>
                                            <td>{{ auth()->user()->name }}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%" align="right"><strong>Nama Seminar / Workshop</strong></td>
                                            <td>{{ $workshop->workshop_name }}</td>
                                        </tr>
                                        <tr>
                                            <td align="right"><strong>Tanggal</strong></td>
                                            <td>{{ GeneralHelper::waktuPelaksanaan($workshop->tgl_mulai,$workshop->tgl_selesai) }}</td>
                                        </tr>
                                        <tr>
                                            <td align="right"><strong>Waktu</strong></td>
                                            <td>{{ $workshop->waktu }}</td>
                                        </tr>
                                        <tr>
                                            <td align="right"><strong>Lokasi - Ruangan</strong></td>
                                            <td>{{ $workshop->lokasi }} - {{ $workshop->ruangan }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        @if(!empty($workshop->jml_peserta_diskon) && empty($workshop->jml_group_referral))
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                            Informasi Promo / Diskon
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="accordion-body collapse in" role="tabpanel">
                                    <div class="panel-body">
                                        {!! $workshop->pesan_email_umum !!}
                                    </div>
                                </div>
                            </div>
                        @elseif(empty($workshop->jml_peserta_diskon) && !empty($workshop->jml_group_referral))
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                            Promo Pendaftaran Group
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="accordion-body collapse in" role="tabpanel">
                                    <div class="panel-body">
                                        <p>{!! $workshop->pesan_email_umum !!}</p>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-3 control-label">Nama</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="add_referral_name" class="form-control add_referral_name" id="" placeholder="Masukkan Nama Peserta Tambahan">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="add_referral_email" class="form-control add_referral_email" id="" placeholder="Masukkan Email Peserta Tambahan">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-3 control-label">No Handphone</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="add_referral_no_hp" class="form-control add_referral_no_hp" id="" placeholder="Masukkan No Handphone Peserta Tambahan">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-2 col-sm-offset-2">
                                                        <button type="button" class="btn btn-warning btnAddReferral" data-url="{{ route('pelatihan.checkreferral',['workshopslug'=>$workshop->slugs]) }}">Tambah</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                @if(session()->has('referral_promo'))
                                                    <table class="table table-bordered table-condensed">
                                                        <thead>
                                                        <tr>
                                                            <th colspan="5" class="text-center" align="center">Daftar Peserta Tambahan</th>
                                                        </tr>
                                                        <tr>
                                                            <th width="30">No</th>
                                                            <th width="25%">Nama</th>
                                                            <th width="35%">Email</th>
                                                            <th width="25%">Ho HP</th>
                                                            <th class="text-center">Aksi</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @php($no = 1)
                                                        @foreach(session()->get('referral_promo') as $key => $value)
                                                            <tr>
                                                                <td>{{ $no }}</td>
                                                                <td>{{ $value['nama'] }}</td>
                                                                <td>{{ $value['email'] }}</td>
                                                                <td>{{ $value['no_hp'] }}</td>
                                                                <td class="text-center"><a href="{{ route('pelatihan.removereferral',['workshopslug'=>$workshop->slugs,'idreferral'=>$key]) }}" class="btn btn-xs btn-danger delconfirm" title="Hapus data peserta ini" data-del-message="Apakah Anda yakin untuk menghapus data peserta <strong>{{ $value['nama'] }}</strong>"><i class="fa fa-trash-o"></i> </a> </td>
                                                            </tr>
                                                            @php($no++)
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                    </div> <!-- Panel Group -->
                </div>
                <div class="col-md-4">

                    <div class="content-box shadow bg-white">
                        <h4>Data Tambahan</h4>
                        {{ Form::text('company',null,['class'=>'form-control','placeholder'=>'Institusi (Perusahaan / Universitas / Instansi)']) }}
                        {{ Form::text('jabatan',null,['class'=>'form-control','placeholder'=>'Jabatan / Posisi']) }}
                        @if($workshop->program->id == 34)
                            <div class="alert alert-info">
                                Jika Anda mempunyai Surat Izin WPEE, harap lampirkan <strong>No Surat</strong> dan <strong>Tanggal Surat</strong> pada kolom dibawah
                            </div>
                            {{ Form::text('no_sk_ojk',null,['class'=>'form-control','placeholder'=>'No Surat Keputusan Izin WPEE dari OJK']) }}
                            {{ Form::text('tgl_sk_ojk',null,['class'=>'form-control datepicker','readonly'=>'readonly','placeholder'=>'Tanggal Surat Keputusan Izin WPEE dari OJK']) }}
                        @endif
                        <h4>Total Pembayaran</h4>
                        <table cellspacing="0" class="cart-totals table">
                            <tbody>
                                    @if(empty($workshop->jml_peserta_diskon) && !empty($workshop->jml_group_referral))
                                        @php
                                            $jmlpeserta         = count(session()->get('referral_promo')) + 1;
                                            if ($jmlpeserta > 1) {
                                                $harga              = $workshop->biaya - $workshop->nominal_diskon;
                                            } else {
                                                $harga              = $workshop->biaya;
                                            }
                                            $total_pembayaran   = $harga * $jmlpeserta;
                                        @endphp
                                        <tr class="total">
                                            <td><strong>Jml Peserta</strong></td>
                                            <td class="text-right">{{ $jmlpeserta }} x {{ '@'.number_format($harga) }}</td>
                                        </tr>
                                        <tr class="total">
                                            <th>
                                                <strong>Total Biaya</strong>
                                            </th>
                                            <td class="text-right">
                                                <strong><span class="amount">Rp{{ number_format($total_pembayaran,0,',','.') }}</span></strong>
                                            </td>
                                        </tr>
                                    @else
                                        <tr class="total">
                                            <th>
                                                <strong>Biaya</strong>
                                            </th>
                                            <td class="text-right">
                                                <strong><span class="amount">Rp{{ number_format($workshop->biaya,0,',','.') }}</span></strong>
                                            </td>
                                        </tr>
                                    @endif
                            <tr>
                                <td colspan="2">
                                    {!! Form::hidden('nama',trim(auth()->user()->name)) !!}
                                    {!! Form::hidden('email',trim(auth()->user()->email)) !!}
                                    {!! Form::hidden('no_hp',trim(auth()->user()->no_hp)) !!}
                                    <button type="button" class="btn btn-block buttonCheckout" data-message="Lanjutkan Pendaftaran {{ $workshop->workshop_name }}">Konfirmasi Pendaftaran</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
                {!! Form::close() !!}
            </div><!-- Row -->
        </div> <!-- End Container -->
    </div> <!-- End Page Default -->
@endsection