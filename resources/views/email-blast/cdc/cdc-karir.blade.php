<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <!-- NAME: NOTE SHEET - VERTICAL -->
    <!--[if gte mso 15]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TICMI Career Development Center</title>

    <style type="text/css">
        p{
            margin:10px 0;
            padding:0;
        }
        table{
            border-collapse:collapse;
        }
        h1,h2,h3,h4,h5,h6{
            display:block;
            margin:0;
            padding:0;
        }
        img,a img{
            border:0;
            height:auto;
            outline:none;
            text-decoration:none;
        }
        body,#bodyTable,#bodyCell{
            height:100%;
            margin:0;
            padding:0;
            width:100%;
        }
        .mcnPreviewText{
            display:none !important;
        }
        #outlook a{
            padding:0;
        }
        img{
            -ms-interpolation-mode:bicubic;
        }
        table{
            mso-table-lspace:0pt;
            mso-table-rspace:0pt;
        }
        .ReadMsgBody{
            width:100%;
        }
        .ExternalClass{
            width:100%;
        }
        p,a,li,td,blockquote{
            mso-line-height-rule:exactly;
        }
        a[href^=tel],a[href^=sms]{
            color:inherit;
            cursor:default;
            text-decoration:none;
        }
        p,a,li,td,body,table,blockquote{
            -ms-text-size-adjust:100%;
            -webkit-text-size-adjust:100%;
        }
        .ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
            line-height:100%;
        }
        a[x-apple-data-detectors]{
            color:inherit !important;
            text-decoration:none !important;
            font-size:inherit !important;
            font-family:inherit !important;
            font-weight:inherit !important;
            line-height:inherit !important;
        }
        a.mcnButton{
            display:block;
        }
        .mcnImage{
            vertical-align:bottom;
        }
        .mcnTextContent{
            word-break:break-word;
        }
        .mcnTextContent img{
            height:auto !important;
        }
        .mcnDividerBlock{
            table-layout:fixed !important;
        }
        /*
        @tab Page
@@section background style
	@tip Set the background color and top border for your email. You may want to choose colors that match your company's branding.
	*/
        body,#bodyTable,#templateFooter{
            /*@editable*/background-color:#EBEBEB;
        }
        /*
        @tab Page
@@section background style
	@tip Set the background color and top border for your email. You may want to choose colors that match your company's branding.
	*/
        #bodyCell{
            /*@editable*/border-top:0;
        }
        /*
        @tab Page
@@section heading 1
	@tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
	@style heading 1
	*/
        h1{
            /*@editable*/color:#D83826 !important;
            /*@editable*/font-family:Helvetica;
            /*@editable*/font-size:30px;
            /*@editable*/font-style:normal;
            /*@editable*/font-weight:bold;
            /*@editable*/line-height:125%;
            /*@editable*/letter-spacing:-1px;
            /*@editable*/text-align:center;
        }
        /*
        @tab Page
@@section heading 2
	@tip Set the styling for all second-level headings in your emails.
	@style heading 2
	*/
        h2{
            /*@editable*/color:#404040 !important;
            /*@editable*/font-family:Helvetica;
            /*@editable*/font-size:26px;
            /*@editable*/font-style:normal;
            /*@editable*/font-weight:bold;
            /*@editable*/line-height:150%;
            /*@editable*/letter-spacing:normal;
            /*@editable*/text-align:center;
        }
        /*
        @tab Page
@@section heading 3
	@tip Set the styling for all third-level headings in your emails.
	@style heading 3
	*/
        h3{
            /*@editable*/color:#808080 !important;
            /*@editable*/font-family:Helvetica;
            /*@editable*/font-size:18px;
            /*@editable*/font-style:normal;
            /*@editable*/font-weight:bold;
            /*@editable*/line-height:125%;
            /*@editable*/letter-spacing:normal;
            /*@editable*/text-align:center;
        }
        /*
        @tab Page
@@section heading 4
	@tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
	@style heading 4
	*/
        h4{
            /*@editable*/color:#CCCCCC !important;
            /*@editable*/font-family:Helvetica;
            /*@editable*/font-size:10px;
            /*@editable*/font-style:normal;
            /*@editable*/font-weight:normal;
            /*@editable*/line-height:125%;
            /*@editable*/letter-spacing:normal;
            /*@editable*/text-align:center;
        }
        /*
        @tab Preheader
@@section preheader style
	@tip Set the background color and borders for your email's preheader area.
	*/
        #templatePreheader{
            /*@editable*/background-color:#da4343;
            /*@editable*/border-top:0;
            /*@editable*/border-bottom:0;
        }
        /*
        @tab Preheader
@@section preheader text
	@tip Set the styling for your email's preheader text. Choose a size and color that is easy to read.
	*/
        .preheaderContainer .mcnTextContent,.preheaderContainer .mcnTextContent p{
            /*@editable*/color:#ffffff;
            /*@editable*/font-family:Helvetica;
            /*@editable*/font-size:10px;
            /*@editable*/line-height:125%;
            /*@editable*/text-align:left;
        }
        /*
        @tab Preheader
@@section preheader link
	@tip Set the styling for your email's header links. Choose a color that helps them stand out from your text.
	*/
        .preheaderContainer .mcnTextContent a{
            /*@editable*/color:#606060;
            /*@editable*/font-weight:normal;
            /*@editable*/text-decoration:underline;
        }
        /*
        @tab Header
@@section header style
	@tip Set the background color and borders for your email's header area.
	*/
        #templateHeader{
            /*@editable*/background-color:#;
            /*@editable*/border-top:0;
            /*@editable*/border-bottom:0;
        }
        /*
        @tab Header
@@section header text
	@tip Set the styling for your email's header text. Choose a size and color that is easy to read.
	*/
        .headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
            /*@editable*/color:#606060;
            /*@editable*/font-family:Helvetica;
            /*@editable*/font-size:16px;
            /*@editable*/line-height:150%;
            /*@editable*/text-align:left;
        }
        /*
        @tab Header
@@section header link
	@tip Set the styling for your email's header links. Choose a color that helps them stand out from your text.
	*/
        .headerContainer .mcnTextContent a{
            /*@editable*/color:#D83826;
            /*@editable*/font-weight:bold;
            /*@editable*/text-decoration:none;
        }
        /*
        @tab Body
@@section body style
	@tip Set the background color and borders for your email's body area.
	*/
        #templateBody,.leafContainer{
            /*@editable*/background-color:#EBEBEB;
        }
        /*
        @tab Body
@@section body style
	@tip Set the background color and borders for your email's body area.
	*/
        #templateBody{
            /*@editable*/border-top:0;
            /*@editable*/border-bottom:0;
        }
        /*
        @tab Body
@@section body text
	@tip Set the border for your email's body text container.
	*/
        .bodyBackground{
            /*@tab Body
@@section body text
@tip Set the border for your email's body text container.*/background-color:#FFFFFF;
            /*@editable*/border:1px solid #E5E5E5;
        }
        /*
        @tab Body
@@section body text
	@tip Set the styling for your email's body text. Choose a size and color that is easy to read.
	*/
        .bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
            /*@editable*/color:#606060;
            /*@editable*/font-family:Helvetica;
            /*@editable*/font-size:16px;
            /*@editable*/line-height:150%;
            /*@editable*/text-align:left;
        }
        /*
        @tab Body
@@section body text
	@tip Set the styling for your email's body text. Choose a size and color that is easy to read.
	*/
        .bodyContainer{
            /*@editable*/border-top:1px solid #FBD5D5;
        }
        /*
        @tab Body
@@section body link
	@tip Set the styling for your email's body links. Choose a color that helps them stand out from your text.
	*/
        .bodyContainer .mcnTextContent a{
            /*@editable*/color:#543680;
            /*@editable*/font-weight:bold;
            /*@editable*/text-decoration:none;
        }
        /*
        @tab Footer
@@section footer style
	@tip Set the background color and borders for your email's footer area.
	*/
        #templateFooter{
            /*@editable*/border-top:0;
            /*@editable*/border-bottom:0;
        }
        /*
        @tab Footer
@@section footer text
	@tip Set the styling for your email's footer text. Choose a size and color that is easy to read.
	*/
        .footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
            /*@editable*/color:#808080;
            /*@editable*/font-family:Helvetica;
            /*@editable*/font-size:10px;
            /*@editable*/line-height:125%;
            /*@editable*/text-align:center;
        }
        /*
        @tab Footer
@@section footer link
	@tip Set the styling for your email's footer links. Choose a color that helps them stand out from your text.
	*/
        .footerContainer .mcnTextContent a{
            /*@editable*/color:#606060;
            /*@editable*/font-weight:normal;
            /*@editable*/text-decoration:underline;
        }
        @media only screen and (max-width: 480px){
            body,table,td,p,a,li,blockquote{
                -webkit-text-size-adjust:none !important;
            }

        }	@media only screen and (max-width: 480px){
            body{
                width:100% !important;
                min-width:100% !important;
            }

        }	@media only screen and (max-width: 480px){
            #bodyCell{
                padding-top:10px !important;
            }

        }	@media only screen and (max-width: 480px){
            .templateContainer{
                max-width:600px !important;
                width:100% !important;
            }

        }	@media only screen and (max-width: 480px){
            .clearPaddingL{
                padding-left:0 !important;
            }

        }	@media only screen and (max-width: 480px){
            .leafContainer{
                width:10px !important;
            }

        }	@media only screen and (max-width: 480px){
            .mobileHide{
                display:none;
            }

        }	@media only screen and (max-width: 480px){
            .mcnImage{
                height:auto !important;
                width:100% !important;
            }

        }	@media only screen and (max-width: 480px){
            .mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
                max-width:100% !important;
                width:100% !important;
            }

        }	@media only screen and (max-width: 480px){
            .mcnBoxedTextContentContainer{
                min-width:100% !important;
            }

        }	@media only screen and (max-width: 480px){
            .mcnImageGroupContent{
                padding:9px !important;
            }

        }	@media only screen and (max-width: 480px){
            .mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
                padding-top:9px !important;
            }

        }	@media only screen and (max-width: 480px){
            .mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
                padding-top:18px !important;
            }

        }	@media only screen and (max-width: 480px){
            .mcnImageCardBottomImageContent{
                padding-bottom:9px !important;
            }

        }	@media only screen and (max-width: 480px){
            .mcnImageGroupBlockInner{
                padding-top:0 !important;
                padding-bottom:0 !important;
            }

        }	@media only screen and (max-width: 480px){
            .mcnImageGroupBlockOuter{
                padding-top:9px !important;
                padding-bottom:9px !important;
            }

        }	@media only screen and (max-width: 480px){
            .mcnTextContent,.mcnBoxedTextContentColumn{
                padding-right:18px !important;
                padding-left:18px !important;
            }

        }	@media only screen and (max-width: 480px){
            .mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
                padding-right:18px !important;
                padding-bottom:0 !important;
                padding-left:18px !important;
            }

        }	@media only screen and (max-width: 480px){
            .mcpreview-image-uploader{
                display:none !important;
                width:100% !important;
            }

        }	@media only screen and (max-width: 480px){
            /*
            @tab Mobile Styles
@@section heading 1
	@tip Make the first-level headings larger in size for better readability on small screens.
	*/
            h1{
                /*@editable*/font-size:24px !important;
                /*@editable*/line-height:125% !important;
            }

        }	@media only screen and (max-width: 480px){
            /*
            @tab Mobile Styles
@@section heading 2
	@tip Make the second-level headings larger in size for better readability on small screens.
	*/
            h2{
                /*@editable*/font-size:20px !important;
                /*@editable*/line-height:125% !important;
            }

        }	@media only screen and (max-width: 480px){
            /*
            @tab Mobile Styles
@@section heading 3
	@tip Make the third-level headings larger in size for better readability on small screens.
	*/
            h3{
                /*@editable*/font-size:18px !important;
                /*@editable*/line-height:125% !important;
            }

        }	@media only screen and (max-width: 480px){
            /*
            @tab Mobile Styles
@@section heading 4
	@tip Make the fourth-level headings larger in size for better readability on small screens.
	*/
            h4{
                /*@editable*/font-size:16px !important;
                /*@editable*/line-height:125% !important;
            }

        }	@media only screen and (max-width: 480px){
            /*
            @tab Mobile Styles
@@section Boxed Text
	@tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
            .mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
                /*@editable*/font-size:18px !important;
                /*@editable*/line-height:125% !important;
            }

        }	@media only screen and (max-width: 480px){
            /*
            @tab Mobile Styles
@@section Preheader Visibility
	@tip Set the visibility of the email's preheader on small screens. You can hide it to save space.
	*/
            #templatePreheader{
                /*@editable*/display:block !important;
            }

        }	@media only screen and (max-width: 480px){
            /*
            @tab Mobile Styles
@@section Preheader Text
	@tip Make the preheader text larger in size for better readability on small screens.
	*/
            .preheaderContainer .mcnTextContent,.preheaderContainer .mcnTextContent p{
                /*@editable*/font-size:14px !important;
                /*@editable*/line-height:115% !important;
            }

        }	@media only screen and (max-width: 480px){
            /*
            @tab Mobile Styles
@@section Header Text
	@tip Make the header text larger in size for better readability on small screens.
	*/
            .headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
                /*@editable*/font-size:18px !important;
                /*@editable*/line-height:125% !important;
            }

        }	@media only screen and (max-width: 480px){
            /*
            @tab Mobile Styles
@@section Body Text
	@tip Make the body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
            .bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
                /*@editable*/font-size:18px !important;
                /*@editable*/line-height:125% !important;
            }

        }	@media only screen and (max-width: 480px){
            /*
            @tab Mobile Styles
@@section footer text
	@tip Make the body content text larger in size for better readability on small screens.
	*/
            .footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
                /*@editable*/font-size:14px !important;
                /*@editable*/line-height:115% !important;
            }

        }</style></head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="background-color:#EBEBEB;">
<!--*|IF:MC_PREVIEW_TEXT|*-->
<!--[if !gte mso 9]><!----><span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">*|MC_PREVIEW_TEXT|*</span><!--<![endif]-->
<!--*|END:IF|*-->
<center>
    <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        <tr>
            <td align="center" valign="top" id="bodyCell">
                <!-- BEGIN TEMPLATE // -->
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center" valign="top">
                            <!-- BEGIN PREHEADER // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templatePreheader">
                                <tr>
                                    <td align="center" valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" class="templateContainer">
                                            <tr>
                                                <td align="right" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="600" class="templateContainer">
                                                        <tr>
                                                            <td valign="top" class="preheaderContainer" style="padding-top:10px; padding-bottom:10px;"><table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                    <tbody class="mcnTextBlockOuter">
                                                                    <tr>
                                                                        <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
                                                                            <!--[if mso]>
                                                                            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                                                                <tr>
                                                                            <![endif]-->

                                                                            <!--[if mso]>
                                                                            <td valign="top" width="600" style="width:600px;">
                                                                            <![endif]-->
                                                                            <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                <tbody><tr>

                                                                                    <td class="mcnTextContent" style="padding: 0px 18px 9px; text-align: center;" valign="top">

                                                                                        <a href="{{ url('karir') }}" target="_blank">View this email in your browser</a>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody></table>
                                                                            <!--[if mso]>
                                                                            </td>
                                                                            <![endif]-->

                                                                            <!--[if mso]>
                                                                            </tr>
                                                                            </table>
                                                                            <![endif]-->
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!-- // END PREHEADER -->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <!-- BEGIN HEADER // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                <tr>
                                    <td align="center" valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" class="templateContainer">
                                            <tr>
                                                <td align="right" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="600" class="templateContainer">
                                                        <tr>
                                                            <td valign="top" class="headerContainer" style="padding-top:15px; padding-bottom:10px;"><table class="mcnImageBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                    <tbody class="mcnImageBlockOuter">
                                                                    <tr>
                                                                        <td style="padding:0px" class="mcnImageBlockInner" valign="top">
                                                                            <table class="mcnImageContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                <tbody><tr>
                                                                                    <td class="mcnImageContent" style="padding-right: 0px; padding-left: 0px; padding-top: 0; padding-bottom: 0; text-align:center;" valign="top">


                                                                                        <img alt="" src="https://gallery.mailchimp.com/c8a2464db224b385ebcb962f2/images/a84f6e62-a24d-4198-b7b4-facf815055ee.png" style="max-width:600px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage" width="600" align="middle">


                                                                                    </td>
                                                                                </tr>
                                                                                </tbody></table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!-- // END HEADER -->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <!-- BEGIN BODY // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                <tr>
                                    <td align="center" valign="top" class="clearPaddingL" style="padding-top:15px; padding-right:15px; padding-bottom:0; padding-left:5px;">
                                        <table border="0" cellpadding="0" cellspacing="0" width="615" class="templateContainer">
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="right" colspan="2" valign="top" class="leafContainer" style="border-bottom:0 !important; padding-left:15px;">
                                                                <img src="https://gallery.mailchimp.com/27aac8a65e64c994c4416d6b8/images/notepad_top_horz_600x40.png" height="40" width="600" class="mcnImage" style="display:block; max-width:600px;">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" valign="top" width="15" class="leafContainer">
                                                                <img src="https://gallery.mailchimp.com/27aac8a65e64c994c4416d6b8/images/leafshadowvert.png" height="540" width="15">
                                                            </td>
                                                            <td align="left" valign="top" width="100%" style="background-color:#FFFFFF;">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td valign="top" width="100%" class="bodyContainer" style="padding-top:10px; padding-bottom:10px;"><table class="mcnDividerBlock" style="min-width: 100%;background-color: #8D080D;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody class="mcnDividerBlockOuter">
                                                                                <tr>
                                                                                    <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                                                                                        <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #EAEAEA;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                            <tbody><tr>
                                                                                                <td>
                                                                                                    <span></span>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody></table>
                                                                                        <!--
                                                                                                        <td class="mcnDividerBlockInner" style="padding: 18px;">
                                                                                                        <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
                                                                                        -->
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table><table class="mcnCaptionBlock" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody class="mcnCaptionBlockOuter">
                                                                                <tr>
                                                                                    <td class="mcnCaptionBlockInner" style="padding:9px;" valign="top">


                                                                                        <table class="mcnCaptionBottomContent" width="false" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                <td class="mcnCaptionBottomImageContent" style="padding:0 9px 9px 9px;" valign="top" align="center">



                                                                                                    <img alt="" src="https://gallery.mailchimp.com/c8a2464db224b385ebcb962f2/images/c33c6525-e404-4c87-b939-7178dbc11cda.jpg" style="max-width:197px;" class="mcnImage" width="197">


                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="mcnTextContent" style="padding: 0px 9px;color: #706E6E;" width="564" valign="top">
                                                                                                    <h4 class="text-center"><strong><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:25px"><a href="http://ticmi.co.id/karir#" target="_blank"><span style="color:#ff9900"><span style="background-color: #FFFFFF;">Binaartha Sekuritas, PT</span></span></a></span></span></strong></h4>

                                                                                                    <div class="panel panel-default active">
                                                                                                        <div class="panel-heading" id="headingOne" role="tab">
                                                                                                            <h4 class="panel-title" style="text-align: left;"><br>
                                                                                                                <br>
                                                                                                                <br>
                                                                                                                <span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:19px"><a aria-controls="collapseOne" aria-expanded="true" data-parent="#accordion" data-toggle="collapse" href="http://ticmi.co.id/karir#binaarthasekuritaspt1" role="button"><span style="color: #008080;">Internal Audit </span></a></span></span></h4>
                                                                                                        </div>

                                                                                                        <div aria-expanded="true" aria-labelledby="headingOne" class="panel-collapse collapse in" id="binaarthasekuritaspt1" role="tabpanel">
                                                                                                            <div class="panel-body">
                                                                                                                <p style="color: #706E6E;"><span style="font-size:14px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><strong>Requirements :</strong></span></span></p>

                                                                                                                <ol>
                                                                                                                    <li><span style="font-size:13px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif">Female</span></span></li>
                                                                                                                    <li><span style="font-size:13px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif">Bachelor's Degree majoring in Accounting/Management/Audit/Tax from reputable university with min GPA 3.0.</span></span></li>
                                                                                                                    <li><span style="font-size:13px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif">Proven internal audit skills</span></span></li>
                                                                                                                </ol>

                                                                                                                <div style="text-align: left;">
                                                                                                                    <hr></div>

                                                                                                                <div class="panel-heading" id="headingOne" role="tab">
                                                                                                                    <h4 class="panel-title" style="text-align: left;"><br>
                                                                                                                        <br>
                                                                                                                        <strong><span style="font-size:19px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><a aria-controls="collapseOne" aria-expanded="true" data-parent="#accordion" data-toggle="collapse" href="http://ticmi.co.id/karir#binaarthasekuritaspt2" role="button"><span style="color: #008080;">IT Staff </span></a></span></span></strong></h4>
                                                                                                                </div>

                                                                                                                <div aria-expanded="true" aria-labelledby="headingOne" class="panel-collapse collapse in" id="binaarthasekuritaspt2" role="tabpanel">
                                                                                                                    <div class="panel-body">
                                                                                                                        <p style="color: #706E6E;"><span style="font-size:14px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><strong>Requirements :</strong></span></span></p>

                                                                                                                        <ol>
                                                                                                                            <li><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px">Male or female</span></span></li>
                                                                                                                            <li><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px">Aged 20 - 30tahun</span></span></li>
                                                                                                                            <li><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px">Educated Min. Bachelor's Degree</span></span></li>
                                                                                                                        </ol>

                                                                                                                        <hr>
                                                                                                                        <div class="panel-heading" id="headingOne" role="tab">
                                                                                                                            <h4 class="panel-title" style="text-align: left;"><br>
                                                                                                                                <span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:19px"><a aria-controls="collapseOne" aria-expanded="true" data-parent="#accordion" data-toggle="collapse" href="http://ticmi.co.id/karir#binaarthasekuritaspt3" role="button"><span style="color: #008080;">Sales Equity or Sales Fixed Income </span></a></span></span></h4>
                                                                                                                        </div>

                                                                                                                        <div aria-expanded="true" aria-labelledby="headingOne" class="panel-collapse collapse in" id="binaarthasekuritaspt3" role="tabpanel">
                                                                                                                            <div class="panel-body">
                                                                                                                                <p style="color: #706E6E;"><span style="font-size:14px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><strong>Requirements :</strong></span></span></p>

                                                                                                                                <ol>
                                                                                                                                    <li><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px">Male or Famale</span></span></li>
                                                                                                                                    <li><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px">Max. 28 years</span></span></li>
                                                                                                                                    <li><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px">Attractive, high morale, and confidence</span></span></li>
                                                                                                                                </ol>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody></table>





                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table><table class="mcnButtonBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody class="mcnButtonBlockOuter">
                                                                                <tr>
                                                                                    <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" class="mcnButtonBlockInner" valign="top" align="center">
                                                                                        <table class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #2BAADF;" cellspacing="0" cellpadding="0" border="0">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td class="mcnButtonContent" style="font-family: Arial; font-size: 16px; padding: 15px;" valign="middle" align="center">
                                                                                                    <a class="mcnButton " title="read more" href="http://ticmi.co.id/karir" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">read more</a>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table><table class="mcnDividerBlock" style="min-width: 100%;background-color: #8D080D;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody class="mcnDividerBlockOuter">
                                                                                <tr>
                                                                                    <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                                                                                        <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #EAEAEA;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                            <tbody><tr>
                                                                                                <td>
                                                                                                    <span></span>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody></table>
                                                                                        <!--
                                                                                                        <td class="mcnDividerBlockInner" style="padding: 18px;">
                                                                                                        <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
                                                                                        -->
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table><table class="mcnCaptionBlock" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody class="mcnCaptionBlockOuter">
                                                                                <tr>
                                                                                    <td class="mcnCaptionBlockInner" style="padding:9px;" valign="top">


                                                                                        <table class="mcnCaptionBottomContent" width="false" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                <td class="mcnCaptionBottomImageContent" style="padding:0 9px 9px 9px;" valign="top" align="center">



                                                                                                    <img alt="" src="https://gallery.mailchimp.com/c8a2464db224b385ebcb962f2/images/2c9381ce-2634-4e95-8ad8-e7fbef021c33.jpg" style="max-width:198px;" class="mcnImage" width="198">


                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="mcnTextContent" style="padding:0 9px 0 9px;" width="564" valign="top">
                                                                                                    <h4 style="text-align: center;"><span style="font-size:24px"><a href="http://ticmi.co.id/karir#" target="_blank"><span style="color:#cc9900">Indo Premier Securities, PT</span></a></span></h4>

                                                                                                    <h4 style="text-align: left;"><br>
                                                                                                        <br>
                                                                                                        <br>
                                                                                                        <strong><span style="color:#008080"><span style="font-size:18px">Junior Account Executive Wealth Management</span></span></strong></h4>

                                                                                                    <div aria-expanded="true" aria-labelledby="headingOne" class="panel-collapse collapse in" id="indopremiersecuritiespt4" role="tabpanel">
                                                                                                        <div class="panel-body">
                                                                                                            <p><strong><span style="font-size:14px">Kualifikasi:</span></strong></p>

                                                                                                            <ol>
                                                                                                                <li><span style="font-size:14px">S1 jurusan Marketing dan bersertifikasi WPPE</span></li>
                                                                                                                <li><span style="font-size:14px">Wanita usia max 27 tahun </span></li>
                                                                                                                <li><span style="font-size:14px">Marketing oriented</span></li>
                                                                                                            </ol>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody></table>





                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table><table class="mcnButtonBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody class="mcnButtonBlockOuter">
                                                                                <tr>
                                                                                    <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" class="mcnButtonBlockInner" valign="top" align="center">
                                                                                        <table class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #2BAADF;" cellspacing="0" cellpadding="0" border="0">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td class="mcnButtonContent" style="font-family: Arial; font-size: 16px; padding: 15px;" valign="middle" align="center">
                                                                                                    <a class="mcnButton " title="read more" href="http://ticmi.co.id/karir" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">read more</a>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table><table class="mcnDividerBlock" style="min-width: 100%;background-color: #8D080D;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody class="mcnDividerBlockOuter">
                                                                                <tr>
                                                                                    <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                                                                                        <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #EAEAEA;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                            <tbody><tr>
                                                                                                <td>
                                                                                                    <span></span>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody></table>
                                                                                        <!--
                                                                                                        <td class="mcnDividerBlockInner" style="padding: 18px;">
                                                                                                        <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
                                                                                        -->
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table><table class="mcnCaptionBlock" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody class="mcnCaptionBlockOuter">
                                                                                <tr>
                                                                                    <td class="mcnCaptionBlockInner" style="padding:9px;" valign="top">


                                                                                        <table class="mcnCaptionBottomContent" width="false" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                <td class="mcnCaptionBottomImageContent" style="padding:0 9px 9px 9px;" valign="top" align="center">



                                                                                                    <img alt="" src="https://gallery.mailchimp.com/c8a2464db224b385ebcb962f2/images/bb988f7a-a5e9-41b2-9453-5b12105b4948.jpg" style="max-width:232px;" class="mcnImage" width="232">


                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="mcnTextContent" style="padding:0 9px 0 9px;" width="564" valign="top">
                                                                                                    <div class="row course-single content-box bg-white shadow">
                                                                                                        <div class="col-sm-12">
                                                                                                            <h4 class="text-center" style="text-align: center;"><br>
                                                                                                                <br>
                                                                                                                <span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:25px"><a href="http://ticmi.co.id/karir#" target="_blank"><span style="color:#ff9900">Investindo Nusantara Sekuritas, PT</span></a></span></span></h4>

                                                                                                            <h4 class="panel-title" style="text-align: left;"><br>
                                                                                                                &nbsp;</h4>
                                                                                                            &nbsp;

                                                                                                            <div class="panel panel-default active">
                                                                                                                <div aria-expanded="true" aria-labelledby="headingOne" class="panel-collapse collapse in" id="binaarthasekuritaspt1" role="tabpanel">
                                                                                                                    <div class="panel-body">
                                                                                                                        <div aria-expanded="true" aria-labelledby="headingOne" class="panel-collapse collapse in" id="binaarthasekuritaspt2" role="tabpanel">
                                                                                                                            <div class="panel-body">
                                                                                                                                <div aria-expanded="true" aria-labelledby="headingOne" class="panel-collapse collapse in" id="binaarthasekuritaspt3" role="tabpanel">
                                                                                                                                    <div class="panel-body"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:19px"><span style="color: #008080;"><strong><span arial="">COMPLIANCE (CP)</span></strong></span></span><br>
<span style="font-size:14px"><span arial=""><span style="color: #414042;">Jakarta Raya</span></span></span></span>

                                                                                                                                        <p><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:14px"><strong><span arial=""><span style="color: #414042;">Kualifikasi :</span></span></strong></span></span></p>

                                                                                                                                        <ol>
                                                                                                                                            <li style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px"><span arial=""><span style="color: #414042;">Pria / Wanita&nbsp; max. 35 tahun</span></span></span></span></li>
                                                                                                                                            <li style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px"><span arial=""><span style="color: #414042;">Pendidikan Minimun Sarjana dengan GPA min. 2.75 </span></span></span></span></li>
                                                                                                                                            <li style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px"><span arial=""><span style="color: #414042;">Memiliki pengalaman sebagai Compliance pada Perusahaan Sekuritas atau Management Investasi Minimal 2 tahun (Wajib)</span></span></span></span></li>
                                                                                                                                        </ol>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody></table>





                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table><table class="mcnButtonBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody class="mcnButtonBlockOuter">
                                                                                <tr>
                                                                                    <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" class="mcnButtonBlockInner" valign="top" align="center">
                                                                                        <table class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #2BAADF;" cellspacing="0" cellpadding="0" border="0">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td class="mcnButtonContent" style="font-family: Arial; font-size: 16px; padding: 15px;" valign="middle" align="center">
                                                                                                    <a class="mcnButton " title="read more" href="http://ticmi.co.id/karir" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">read more</a>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table><table class="mcnDividerBlock" style="min-width: 100%;background-color: #8D080D;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody class="mcnDividerBlockOuter">
                                                                                <tr>
                                                                                    <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                                                                                        <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #EAEAEA;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                            <tbody><tr>
                                                                                                <td>
                                                                                                    <span></span>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody></table>
                                                                                        <!--
                                                                                                        <td class="mcnDividerBlockInner" style="padding: 18px;">
                                                                                                        <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
                                                                                        -->
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table><table class="mcnCaptionBlock" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody class="mcnCaptionBlockOuter">
                                                                                <tr>
                                                                                    <td class="mcnCaptionBlockInner" style="padding:9px;" valign="top">


                                                                                        <table class="mcnCaptionBottomContent" width="false" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                <td class="mcnCaptionBottomImageContent" style="padding:0 9px 9px 9px;" valign="top" align="center">



                                                                                                    <img alt="" src="https://gallery.mailchimp.com/c8a2464db224b385ebcb962f2/images/e74d38ec-c7dd-490f-a289-0bdd018e428d.jpg" style="max-width:173px;" class="mcnImage" width="173">


                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="mcnTextContent" style="padding:0 9px 0 9px;" width="564" valign="top">
                                                                                                    <div class="row course-single content-box bg-white shadow">
                                                                                                        <div class="col-sm-12">
                                                                                                            <h4 class="text-center" style="text-align: center;"><br>
                                                                                                                <br>
                                                                                                                <span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:25px"><a href="http://ticmi.co.id/karir#" target="_blank"><span style="color:#ff9900">Kiwoom Sekuritas Indonesia, PT</span></a></span></span></h4>

                                                                                                            <h4 class="text-center">&nbsp;</h4>

                                                                                                            <h4 class="panel-title" style="text-align: left;"><br>
                                                                                                                <br>
                                                                                                                <br>
                                                                                                                <span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:19px"><a aria-controls="collapseOne" aria-expanded="true" data-parent="#accordion" data-toggle="collapse" href="http://ticmi.co.id/karir#binaarthasekuritaspt2" role="button"><span style="color: #008080;"><strong>IT Staff</strong> </span></a></span></span></h4>

                                                                                                            <div class="panel panel-default active">
                                                                                                                <div aria-expanded="true" aria-labelledby="headingOne" class="panel-collapse collapse in" id="binaarthasekuritaspt1" role="tabpanel">
                                                                                                                    <div class="panel-body">
                                                                                                                        <div aria-expanded="true" aria-labelledby="headingOne" class="panel-collapse collapse in" id="binaarthasekuritaspt2" role="tabpanel">
                                                                                                                            <div class="panel-body">
                                                                                                                                <p><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:14px"><strong>Requirements :</strong></span></span></p>

                                                                                                                                <ol>
                                                                                                                                    <li><span style="font-size:13px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif">Male or female</span></span></li>
                                                                                                                                    <li><span style="font-size:13px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif">Aged 20 - 30tahun</span></span></li>
                                                                                                                                    <li><span style="font-size:13px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif">Educated Min. Bachelor's Degree</span></span></li>
                                                                                                                                </ol>

                                                                                                                                <div class="panel-heading" id="headingOne" role="tab">
                                                                                                                                    <hr>&nbsp;
                                                                                                                                    <div class="panel-heading" id="headingOne" role="tab">
                                                                                                                                        <h4 class="panel-title" style="text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:19px"><strong><a aria-controls="collapseOne" aria-expanded="true" data-parent="#accordion" data-toggle="collapse" href="http://ticmi.co.id/karir#kiwoomsekuritasindonesiapt4" role="button"><span style="color: #008080;">CUSTOMER SERVICE (CS) </span></a></strong></span></span></h4>
                                                                                                                                    </div>

                                                                                                                                    <div aria-expanded="true" aria-labelledby="headingOne" class="panel-collapse collapse in" id="kiwoomsekuritasindonesiapt4" role="tabpanel">
                                                                                                                                        <div class="panel-body">
                                                                                                                                            <p style="margin:0in; margin-bottom:.0001pt"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:14px"><strong>GENERAL QUALIFICATIONS :</strong></span></span></p>

                                                                                                                                            <ul>
                                                                                                                                                <li style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px">Male/Female, maximum age 26 years old</span></span></li>
                                                                                                                                                <li style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px">Minimum Diploma (D3)</span></span></li>
                                                                                                                                                <li style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px">Having good communication skill and problem solving</span></span></li>
                                                                                                                                            </ul>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody></table>





                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table><table class="mcnButtonBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody class="mcnButtonBlockOuter">
                                                                                <tr>
                                                                                    <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" class="mcnButtonBlockInner" valign="top" align="center">
                                                                                        <table class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #2BAADF;" cellspacing="0" cellpadding="0" border="0">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td class="mcnButtonContent" style="font-family: Arial; font-size: 16px; padding: 15px;" valign="middle" align="center">
                                                                                                    <a class="mcnButton " title="read more" href="http://ticmi.co.id/karir" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">read more</a>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table><table class="mcnDividerBlock" style="min-width: 100%;background-color: #8D080D;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody class="mcnDividerBlockOuter">
                                                                                <tr>
                                                                                    <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                                                                                        <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #EAEAEA;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                            <tbody><tr>
                                                                                                <td>
                                                                                                    <span></span>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody></table>
                                                                                        <!--
                                                                                                        <td class="mcnDividerBlockInner" style="padding: 18px;">
                                                                                                        <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
                                                                                        -->
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table><table class="mcnCaptionBlock" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody class="mcnCaptionBlockOuter">
                                                                                <tr>
                                                                                    <td class="mcnCaptionBlockInner" style="padding:9px;" valign="top">


                                                                                        <table class="mcnCaptionBottomContent" width="false" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                <td class="mcnCaptionBottomImageContent" style="padding:0 9px 9px 9px;" valign="top" align="center">



                                                                                                    <img alt="" src="https://gallery.mailchimp.com/c8a2464db224b385ebcb962f2/images/cfe87d51-8420-4712-badc-59ae7ace02f2.jpg" style="max-width:341px;" class="mcnImage" width="341">


                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="mcnTextContent" style="padding:0 9px 0 9px;" width="564" valign="top">
                                                                                                    <div class="row course-single content-box bg-white shadow">
                                                                                                        <div class="col-sm-12">
                                                                                                            <h4 class="text-center" style="text-align: center;"><br>
                                                                                                                <br>
                                                                                                                <span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:25px"><a href="http://ticmi.co.id/karir#" target="_blank"><span style="color:#ff9900"><span style="background-color: #FFFFFF;">Phillip Sekuritas Indonesia, PT</span></span></a></span></span></h4>

                                                                                                            <h4 class="text-center" style="text-align: center;"><br>
                                                                                                                <span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="color: #006400;"><span style="font-size:12px"><span style="line-height:normal"><span style="background-color:#FFFFFF">We are part of Phillip Capital Group Singapore &amp; a member of Indonesia Stock Exchange, seeking young and highly motivated candidates for the following position : </span></span></span></span></span></h4>

                                                                                                            <p style="margin-bottom: 0.0001pt; text-align: left;"><br>
                                                                                                                <span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:19px"><strong><span style="color: #008080;"><span style="line-height:normal"><span style="background-color:#FFFFFF">Compliance Officer</span></span></span></strong></span></span></p>
                                                                                                            <span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:14px"><span style="line-height:normal"><strong><span style="background-color:#FFFFFF">Job Descriptions:</span></strong></span></span></span>

                                                                                                            <ul>
                                                                                                                <li style="text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px"><span style="line-height:normal"><span style="background-color:#FFFFFF">Following the development of capital markets, in particular the regulations in force in the capital market</span></span></span></span></li>
                                                                                                                <li style="text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px"><span style="line-height:normal"><span style="background-color:#FFFFFF">Oversee execution of securities transactions relating to investment products, to conform to the laws of the Republic of Indonesia as OJK Regulation, Regulation of the Company the Investment Policy Committee and Investment Contract concerned</span></span></span></span></li>
                                                                                                            </ul>

                                                                                                            <hr>
                                                                                                            <div style="text-align: left;">&nbsp;</div>

                                                                                                            <p style="text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:19px"><span style="color: #008080;"><strong><span arial=""><span style="background-color:#FFFFFF">Copywriter &amp; Content Marketing (Marketing Communication)</span></span></strong></span></span></span></p>

                                                                                                            <p style="text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:14px"><strong><span arial=""><span style="background-color:#FFFFFF">Requirements:</span></span></strong></span></span></p>

                                                                                                            <ul>
                                                                                                                <li style="text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px"><span arial=""><span style="background-color:#FFFFFF">Min D3 or Bachelor degree, graduated from reputable University, preferably in Communication or Marketing major.</span></span></span></span></li>
                                                                                                                <li style="text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px"><span arial=""><span style="background-color:#FFFFFF">Preferably candidates with 1 year experience in the same field, fresh graduates are welcome to apply</span></span></span></span></li>
                                                                                                            </ul>

                                                                                                            <hr>
                                                                                                            <p style="margin-bottom: 7.5pt; text-align: left;"><br>
                                                                                                                <span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="color: #008080;"><span style="font-size:18px"><span style="line-height:normal"><strong><span style="background-color:#FFFFFF">Sales / Marketing (Reksadana)</span></strong></span></span></span></span></p>

                                                                                                            <p style="margin-bottom: 0.0001pt; text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:14px"><span style="line-height:normal"><strong><span style="background-color:#FFFFFF">Requirement :</span></strong></span></span></span></p>

                                                                                                            <ol>
                                                                                                                <li style="margin-bottom: 0.0001pt; text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px"><span style="line-height:normal"><span style="background-color:#FFFFFF">Male / Female</span></span></span></span></li>
                                                                                                                <li style="margin-bottom: 0.0001pt; text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px"><span style="line-height:normal"><span style="background-color:#FFFFFF">Diploma/ Bachelor degree any studies prefer Accounting / Finance / Business </span></span></span></span></li>
                                                                                                                <li style="margin-bottom: 0.0001pt; text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px"><span style="line-height:normal"><span style="background-color:#FFFFFF">Having experience in </span><strong><span style="background-color:#FFFFFF">Mutual funds</span></strong><span style="background-color:#FFFFFF"> or </span><strong><span style="background-color:#FFFFFF">Wealth management </span></strong><span style="background-color:#FFFFFF">or </span><strong><span style="background-color:#FFFFFF">Banking</span></strong></span></span></span></li>
                                                                                                                <li style="margin-bottom: 0.0001pt; text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px"><span style="background:white none repeat scroll 0% 0%"><span style="line-height:normal"><span style="background-color:#FFFFFF">MUST HAVE min. WAPERD&nbsp; and max WMI and/or AAJI license </span></span></span></span></span></li>
                                                                                                            </ol>

                                                                                                            <hr>
                                                                                                            <div style="text-align: left;"><br>
                                                                                                                <span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:19px"><span style="color: #008080;"><strong><span style="line-height:115%"><span style="background-color:#FFFFFF">Equity Research Analyst</span></span></strong></span></span></span></div>

                                                                                                            <p style="margin-bottom: 0.0001pt; text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:14px"><span style="line-height:normal"><strong><span style="background-color:#FFFFFF">Requirements:</span></strong></span></span></span></p>

                                                                                                            <ul>
                                                                                                                <li style="margin-top: 0in; margin-right: 0in; margin-bottom: 0.0001pt; text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px"><span style="line-height:normal"><span style="background-color:#FFFFFF">Graduate from reputable university, preferably accounting / finance / economy major.</span></span></span></span></li>
                                                                                                                <li style="margin-top: 0in; margin-right: 0in; margin-bottom: 0.0001pt; text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px"><span style="line-height:normal"><span style="background-color:#FFFFFF">Having overseas graduate is a Plus.</span></span></span></span></li>
                                                                                                                <li style="margin-top: 0in; margin-right: 0in; margin-bottom: 0.0001pt; text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px"><span style="line-height:normal"><span style="background-color:#FFFFFF">Having experience either in the capital market as an analyst or in auditing firms is preferable (min. 1 year).</span></span></span></span></li>
                                                                                                            </ul>

                                                                                                            <hr>
                                                                                                            <p style="text-align: left;"><br>
                                                                                                                <span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:19px"><strong><span arial=""><span style="color: #008080;"><span style="background-color:#FFFFFF">DEALER</span></span></span></strong></span></span></p>

                                                                                                            <p style="margin-bottom: 0.0001pt; text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:14px"><span style="line-height:normal"><strong><span arial=""><span style="background-color:#FFFFFF">R</span></span></strong><strong><span arial=""><span style="background-color:#FFFFFF">equirements:</span></span></strong></span></span></span></p>

                                                                                                            <ol>
                                                                                                                <li style="margin-bottom: 0.0001pt; text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px"><span style="line-height:normal"><span arial=""><span style="background-color:#FFFFFF">Fresh graduate or minimum 2 years experience in financial company</span></span></span></span></span></li>
                                                                                                                <li style="margin-bottom: 0.0001pt; text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px"><span style="line-height:normal"><span arial=""><span style="background-color:#FFFFFF">Having a Bachelor Degree in related fields</span></span></span></span></span></li>
                                                                                                                <li style="margin-bottom: 0.0001pt; text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px"><span style="line-height:normal"><span arial=""><span style="background-color:#FFFFFF">Communicative and Good personality</span></span></span></span></span></li>
                                                                                                                <li style="margin-bottom: 0.0001pt; text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px"><span style="line-height:normal"><span arial=""><span style="background-color:#FFFFFF">Having License of Broker Dealer Representative (WPPE) </span></span></span></span></span></li>
                                                                                                            </ol>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody></table>





                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table><table class="mcnButtonBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody class="mcnButtonBlockOuter">
                                                                                <tr>
                                                                                    <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" class="mcnButtonBlockInner" valign="top" align="center">
                                                                                        <table class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #2BAADF;" cellspacing="0" cellpadding="0" border="0">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td class="mcnButtonContent" style="font-family: Arial; font-size: 16px; padding: 15px;" valign="middle" align="center">
                                                                                                    <a class="mcnButton " title="read more" href="http://ticmi.co.id/karir" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">read more</a>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table><table class="mcnDividerBlock" style="min-width: 100%;background-color: #8D080D;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody class="mcnDividerBlockOuter">
                                                                                <tr>
                                                                                    <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                                                                                        <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #EAEAEA;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                            <tbody><tr>
                                                                                                <td>
                                                                                                    <span></span>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody></table>
                                                                                        <!--
                                                                                                        <td class="mcnDividerBlockInner" style="padding: 18px;">
                                                                                                        <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
                                                                                        -->
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table><table class="mcnCaptionBlock" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody class="mcnCaptionBlockOuter">
                                                                                <tr>
                                                                                    <td class="mcnCaptionBlockInner" style="padding:9px;" valign="top">


                                                                                        <table class="mcnCaptionBottomContent" width="false" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                <td class="mcnCaptionBottomImageContent" style="padding:0 9px 9px 9px;" valign="top" align="center">



                                                                                                    <img alt="" src="https://gallery.mailchimp.com/c8a2464db224b385ebcb962f2/images/a74a106e-866e-476a-bc85-45f4e1b053e6.jpg" style="max-width:306px;" class="mcnImage" width="306">


                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="mcnTextContent" style="padding:0 9px 0 9px;" width="564" valign="top">
                                                                                                    <h4 class="text-center" style="text-align: center;"><br>
                                                                                                        <br>
                                                                                                        <span style="font-family:arial,helvetica neue,helvetica,sans-serif"><strong><span style="font-size:25px"><a href="http://ticmi.co.id/karir#" target="_blank"><span style="color:#ff9900">Sinarmas Sekuritas, PT</span></a></span></strong></span></h4>

                                                                                                    <div style="text-align: left;"><br>
                                                                                                        &nbsp;</div>

                                                                                                    <div id="headingOne" role="tab">
                                                                                                        <h4 style="text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:19px"><strong><a aria-controls="collapseOne" aria-expanded="true" data-parent="#accordion" data-toggle="collapse" href="http://ticmi.co.id/karir#sinarmassekuritaspt1" role="button"><span style="color: #008080;">Dealer </span></a></strong></span></span></h4>
                                                                                                    </div>

                                                                                                    <div aria-expanded="true" aria-labelledby="headingOne" id="sinarmassekuritaspt1" role="tabpanel">
                                                                                                        <div>
                                                                                                            <div style="text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:14px"><strong>Requirements:</strong></span></span></div>

                                                                                                            <ol>
                                                                                                                <li style="text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px">Candidate must possess at least a Diploma or Bachelor's Degree in any filed</span></span></li>
                                                                                                                <li style="text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px">Goog Looking and Communicative</span></span></li>
                                                                                                                <li style="text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px">Have Certificate or License of Broker Dealer Representative (WPPE/WPPE Pemasaran)</span></span></li>
                                                                                                            </ol>

                                                                                                            <hr>
                                                                                                            <h4 class="panel-title" style="text-align: left;"><br>
                                                                                                                <span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:19px"><strong><a aria-controls="collapseOne" aria-expanded="true" data-parent="#accordion" data-toggle="collapse" href="http://ticmi.co.id/karir#sinarmassekuritaspt2" role="button"><span style="color: #008080;">Sales Equity </span></a></strong></span></span></h4>

                                                                                                            <div aria-expanded="true" aria-labelledby="headingOne" class="panel-collapse collapse in" id="sinarmassekuritaspt2" role="tabpanel">
                                                                                                                <div style="text-align: left;">
                                                                                                                    <p style="text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:14px"><strong>Requirements:</strong></span></span></p>

                                                                                                                    <ol>
                                                                                                                        <li style="text-align: left;"><span style="font-size:13px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif">Candidate must possess at least a Diploma or Bachelor's Degree in any filed</span></span></li>
                                                                                                                        <li style="text-align: left;"><span style="font-size:13px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif">Goog Looking and Communicative</span></span></li>
                                                                                                                        <li style="text-align: left;"><span style="font-size:13px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif">Have Certificate or License of Broker Dealer Representative (WPPE/WPPE Pemasaran)</span></span></li>
                                                                                                                    </ol>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody></table>





                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table><table class="mcnButtonBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody class="mcnButtonBlockOuter">
                                                                                <tr>
                                                                                    <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" class="mcnButtonBlockInner" valign="top" align="center">
                                                                                        <table class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #2BAADF;" cellspacing="0" cellpadding="0" border="0">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td class="mcnButtonContent" style="font-family: Arial; font-size: 16px; padding: 15px;" valign="middle" align="center">
                                                                                                    <a class="mcnButton " title="read more" href="http://ticmi.co.id/karir" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">read more</a>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table><table class="mcnDividerBlock" style="min-width: 100%;background-color: #8D080D;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody class="mcnDividerBlockOuter">
                                                                                <tr>
                                                                                    <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                                                                                        <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #EAEAEA;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                            <tbody><tr>
                                                                                                <td>
                                                                                                    <span></span>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody></table>
                                                                                        <!--
                                                                                                        <td class="mcnDividerBlockInner" style="padding: 18px;">
                                                                                                        <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
                                                                                        -->
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table><table class="mcnCaptionBlock" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody class="mcnCaptionBlockOuter">
                                                                                <tr>
                                                                                    <td class="mcnCaptionBlockInner" style="padding:9px;" valign="top">


                                                                                        <table class="mcnCaptionBottomContent" width="false" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                <td class="mcnCaptionBottomImageContent" style="padding:0 9px 9px 9px;" valign="top" align="center">



                                                                                                    <img alt="" src="https://gallery.mailchimp.com/c8a2464db224b385ebcb962f2/images/ea9c6766-052b-457d-aa61-aa4cc59ff382.jpg" style="max-width:220px;" class="mcnImage" width="220">


                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="mcnTextContent" style="padding:0 9px 0 9px;" width="564" valign="top">
                                                                                                    <h4 class="text-center" style="text-align: center;"><br>
                                                                                                        <br>
                                                                                                        <span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:25px"><strong><a href="http://ticmi.co.id/karir#" target="_blank"><span style="color:#ff9900">RHB Sekuritas Indonesia, PT</span></a></strong></span></span></h4>
                                                                                                    &nbsp;

                                                                                                    <p style="margin-bottom: 0.0001pt; text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="color: #008080;"><span style="font-size:19px"><span style="line-height:150%"><strong><span style="line-height:150%"><span arial="">Equity Sales</span></span></strong></span></span></span></span></p>

                                                                                                    <p style="margin-bottom:.0001pt; text-align:justify"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="line-height:150%"><span style="font-size:14px"><strong><span style="line-height:150%"><span arial="">Requirements</span></span></strong><strong><span style="line-height:150%"><span arial="">:</span></span></strong></span> </span></span></p>

                                                                                                    <ol>
                                                                                                        <li style="text-align:justify"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px"><span style="line-height:115%"><span lang="EN-GB"><span style="line-height:115%"><span arial="">Candidate with extensive and established</span></span></span> <span style="line-height:115%"><span arial="">individual or high net worth client base</span></span><span lang="IN"><span style="line-height:115%"><span arial="">.</span></span></span>&nbsp; </span></span></span></li>
                                                                                                        <li style="text-align:justify"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px"><span style="line-height:115%"><span lang="EN-GB"><span style="line-height:115%"><span arial="">Preferably with experience in capital markets&nbsp; or within a financial institution</span></span></span><span lang="IN"><span style="line-height:115%"><span arial="">.</span></span></span></span></span></span></li>
                                                                                                        <li style="text-align:justify"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px"><span style="line-height:115%"><span lang="EN-GB"><span style="line-height:115%"><span arial="">WPPE license (Broker Dealer Representative License )</span></span></span><span lang="IN"><span style="line-height:115%"><span arial=""> is a must.</span></span></span></span></span></span></li>
                                                                                                        <li style="text-align:justify"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px"><span style="line-height:115%"><span lang="EN-GB"><span style="line-height:115%"><span arial="">Basic knowledge of equity trading</span></span></span><span lang="IN"><span style="line-height:115%"><span arial="">.</span></span></span></span></span></span></li>
                                                                                                    </ol>

                                                                                                    <hr>&nbsp;
                                                                                                    <p style="margin: 0in 0in 7.5pt; text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:19px"><span style="color: #008080;"><strong><span style="background:white none repeat scroll 0% 0%">Equity Sales | Direct Sales Team</span></strong></span></span><br>
<strong><span style="font-size:10.5pt"><span style="background:white none repeat scroll 0% 0%"><span style="font-size:14px">University Internship</span></span></span></strong></span></p>

                                                                                                    <p style="margin: 0in 0in 7.5pt; text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:14px"><strong>Penempatan:</strong></span></span></p>

                                                                                                    <p style="margin: 0in 0in 7.5pt; text-align: left;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:13px">Diseluruh Area PT. RHB Sekuritas Indonesia</span></span></p>

                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody></table>





                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table><table class="mcnButtonBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody class="mcnButtonBlockOuter">
                                                                                <tr>
                                                                                    <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" class="mcnButtonBlockInner" valign="top" align="center">
                                                                                        <table class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #2BAADF;" cellspacing="0" cellpadding="0" border="0">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td class="mcnButtonContent" style="font-family: Arial; font-size: 16px; padding: 15px;" valign="middle" align="center">
                                                                                                    <a class="mcnButton " title="read more" href="http://ticmi.co.id/karir" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">read more</a>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" align="right" valign="top">
                                                                <img src="https://gallery.mailchimp.com/27aac8a65e64c994c4416d6b8/images/leafshadowhorz_rev.png" height="15" width="540" class="mcnImage" style="display:block; max-width:540px;">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!-- // END BODY -->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="padding-bottom:40px;">
                            <!-- BEGIN FOOTER // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter">
                                <tr>
                                    <td align="center" valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" class="templateContainer">
                                            <tr>
                                                <td align="right" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="600" class="templateContainer">
                                                        <tr>
                                                            <td valign="top" class="footerContainer" style="padding-top:10px; padding-bottom:10px;"><table class="mcnButtonBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                    <tbody class="mcnButtonBlockOuter">
                                                                    <tr>
                                                                        <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" class="mcnButtonBlockInner" valign="top" align="center">
                                                                            <table class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 2px;background-color: #2BAADF;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td class="mcnButtonContent" style="font-family: Arial; font-size: 18px; padding: 20px;" valign="middle" align="center">
                                                                                        <a class="mcnButton " title="Visit us" href="http://ticmi.co.id/karir" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Visit us</a>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table><table class="mcnFollowBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                    <tbody class="mcnFollowBlockOuter">
                                                                    <tr>
                                                                        <td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">
                                                                            <table class="mcnFollowContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tbody><tr>
                                                                                    <td style="padding-left:9px;padding-right:9px;" align="center">
                                                                                        <table style="min-width:100%;" class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                            <tbody><tr>
                                                                                                <td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">
                                                                                                    <table cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                                        <tbody><tr>
                                                                                                            <td valign="top" align="center">
                                                                                                                <!--[if mso]>
                                                                                                                <table align="center" border="0" cellspacing="0" cellpadding="0">
                                                                                                                    <tr>
                                                                                                                <![endif]-->

                                                                                                                <!--[if mso]>
                                                                                                                <td align="center" valign="top">
                                                                                                                <![endif]-->


                                                                                                                <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                    <tbody><tr>
                                                                                                                        <td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                                                                                            <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                                                                <tbody><tr>
                                                                                                                                    <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                                                                                        <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                                            <tbody><tr>

                                                                                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                                                                                    <a href="https://www.facebook.com/TICMI.ID/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                                                                                </td>


                                                                                                                                            </tr>
                                                                                                                                            </tbody></table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                </tbody></table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    </tbody></table>

                                                                                                                <!--[if mso]>
                                                                                                                </td>
                                                                                                                <![endif]-->

                                                                                                                <!--[if mso]>
                                                                                                                <td align="center" valign="top">
                                                                                                                <![endif]-->


                                                                                                                <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                    <tbody><tr>
                                                                                                                        <td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                                                                                            <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                                                                <tbody><tr>
                                                                                                                                    <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                                                                                        <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                                            <tbody><tr>

                                                                                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                                                                                    <a href="http://www.twitter.com/ticmi_id" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                                                                                </td>


                                                                                                                                            </tr>
                                                                                                                                            </tbody></table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                </tbody></table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    </tbody></table>

                                                                                                                <!--[if mso]>
                                                                                                                </td>
                                                                                                                <![endif]-->

                                                                                                                <!--[if mso]>
                                                                                                                <td align="center" valign="top">
                                                                                                                <![endif]-->


                                                                                                                <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                    <tbody><tr>
                                                                                                                        <td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                                                                                            <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                                                                <tbody><tr>
                                                                                                                                    <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                                                                                        <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                                            <tbody><tr>

                                                                                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                                                                                    <a href="https://www.instagram.com/ticmi_id" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                                                                                </td>


                                                                                                                                            </tr>
                                                                                                                                            </tbody></table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                </tbody></table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    </tbody></table>

                                                                                                                <!--[if mso]>
                                                                                                                </td>
                                                                                                                <![endif]-->

                                                                                                                <!--[if mso]>
                                                                                                                <td align="center" valign="top">
                                                                                                                <![endif]-->


                                                                                                                <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                    <tbody><tr>
                                                                                                                        <td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                                                                                            <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                                                                <tbody><tr>
                                                                                                                                    <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                                                                                        <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                                            <tbody><tr>

                                                                                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                                                                                    <a href="https://www.youtube.com/channel/UCtJ15ddZV_JhHtPXa-Jj9Lw" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-youtube-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                                                                                </td>


                                                                                                                                            </tr>
                                                                                                                                            </tbody></table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                </tbody></table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    </tbody></table>

                                                                                                                <!--[if mso]>
                                                                                                                </td>
                                                                                                                <![endif]-->

                                                                                                                <!--[if mso]>
                                                                                                                <td align="center" valign="top">
                                                                                                                <![endif]-->


                                                                                                                <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                    <tbody><tr>
                                                                                                                        <td style="padding-right:0; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                                                                                            <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                                                                <tbody><tr>
                                                                                                                                    <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                                                                                        <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                                            <tbody><tr>

                                                                                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                                                                                    <a href="http://www.ticmi.co.id" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                                                                                </td>


                                                                                                                                            </tr>
                                                                                                                                            </tbody></table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                </tbody></table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    </tbody></table>

                                                                                                                <!--[if mso]>
                                                                                                                </td>
                                                                                                                <![endif]-->

                                                                                                                <!--[if mso]>
                                                                                                                </tr>
                                                                                                                </table>
                                                                                                                <![endif]-->
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        </tbody></table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody></table>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody></table>

                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table><table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                    <tbody class="mcnTextBlockOuter">
                                                                    <tr>
                                                                        <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
                                                                            <!--[if mso]>
                                                                            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                                                                <tr>
                                                                            <![endif]-->

                                                                            <!--[if mso]>
                                                                            <td valign="top" width="600" style="width:600px;">
                                                                            <![endif]-->
                                                                            <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                <tbody><tr>

                                                                                    <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">

                                                                                        <em>Copyright © | 2017 | The Indonesia Capital Market Institute</em><br>
                                                                                        <br>
                                                                                        <strong>Our mailing address is:</strong>

                                                                                        <h2 class="null">cdc@ticmi.co.id</h2>
                                                                                        <br>
                                                                                        Want to change how you receive these emails?<br>
                                                                                        You can <a href="*|UPDATE_PROFILE|*">update your preferences</a> or <a href="*|UNSUB|*">unsubscribe from this list</a>.
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody></table>
                                                                            <!--[if mso]>
                                                                            </td>
                                                                            <![endif]-->

                                                                            <!--[if mso]>
                                                                            </tr>
                                                                            </table>
                                                                            <![endif]-->
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!-- // END FOOTER -->
                        </td>
                    </tr>
                </table>
                <!-- // END TEMPLATE -->
            </td>
        </tr>
    </table>
</center>
</body>
</html>